<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login','Auth\LoginController@index')->name('login');
Route::post('login','Auth\LoginController@login')->name('login');
Route::get('logout','Auth\LoginController@logout')->name('logout');

Route::middleware(['web','auth'])->group(function () {
    Route::get('/','DashboardController@index')->name('dashboard');

    Route::middleware(['check.permission'])->group(function () {
        /**
        * Setting
        * */ 
        Route::name('setting.')->prefix('setting')->group(function () {
            Route::name('role.')->prefix('role')->group(function(){
                Route::resource('', 'Setting\RoleController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
    
                Route::post('access','Setting\RoleController@access')->name('access');
            });
    
            Route::resource('permission','Setting\PermissionController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('permission');
    
            Route::name('user.')->prefix('user')->group(function(){
                Route::resource('', 'Setting\UserController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
    
                Route::get('{id}/change-status/{status}','Setting\UserController@change_status')->name('change.status');
            });
    
            // Route::resource('user-api','Setting\UserApiController')->only([
            //     'index', 'edit', 'store', 'destroy'
            // ])->names('user_api');
    
            // Route::resource('domain','Setting\DomainController')->only([
            //     'index', 'edit', 'store', 'destroy'
            // ])->names('domain');
    
            Route::resource('menu','Setting\MenuController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('menu');
    
            Route::resource('setting','Setting\SettingController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('setting');
        });
    
        /**
        * Master
        * */ 
        Route::name('master.')->prefix('master')->group(function () {
            Route::resource('brand','Master\BrandController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('brand');
    
            Route::resource('category','Master\CategoryController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('category');
    
            Route::resource('bank','Master\BankController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('bank');
    
            Route::resource('employee','Master\EmployeeController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('employee');
    
            Route::resource('branch','Master\BranchController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('branch');
    
            Route::resource('member','Master\MemberController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('member');
    
            Route::resource('supplier','Master\SupplierController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('supplier');
    
            Route::name('product.')->prefix('product')->group(function(){
                Route::post('upload','Master\ProductController@upload')->name('upload');
                Route::delete('upload','Master\ProductController@upload')->name('upload');
                
                Route::resource('', 'Master\ProductController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
                ]);
            });
    
            Route::name('product.identity.')->prefix('product-identity')->group(function(){
                Route::resource('', 'Master\ProductIdentityController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index'
                ]);
            });
    
            Route::name('promo.')->prefix('promo')->group(function(){
                Route::post('upload','Master\PromoController@upload')->name('upload');
                Route::delete('upload','Master\PromoController@upload')->name('upload');
                
                Route::resource('', 'Master\PromoController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
                ]);
            });
        });
    
        /**
        * Finance
        * */ 
        Route::name('finance.')->prefix('finance')->group(function () {
            Route::resource('category-expend','Finance\CategoryExpendController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('category.expend');
    
            Route::resource('incentive','Finance\IncentiveController')->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ])->names('incentive');
    
            Route::name('expend.')->prefix('expend')->group(function(){
                Route::post('upload','Finance\ExpendController@upload')->name('upload');
                Route::delete('upload','Finance\ExpendController@upload')->name('upload');
                
                Route::resource('', 'Finance\ExpendController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
    
            Route::name('debt.payment.po.')->prefix('debt-payment-po')->group(function(){
                Route::post('upload','Finance\DebtPaymentPOController@upload')->name('upload');
                Route::delete('upload','Finance\DebtPaymentPOController@upload')->name('upload');
                
                Route::resource('', 'Finance\DebtPaymentPOController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
                ]);
            });
    
            Route::name('debt.payment.so.')->prefix('debt-payment-so')->group(function(){
                Route::post('upload','Finance\DebtPaymentSOController@upload')->name('upload');
                Route::delete('upload','Finance\DebtPaymentSOController@upload')->name('upload');
                
                Route::resource('', 'Finance\DebtPaymentSOController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
                ]);
            });
        });
    
        /**
        * Transaction
        * */ 
        Route::name('transaction.')->prefix('transaction')->group(function () {
            Route::name('purchase.order.')->prefix('purchase-order')->group(function(){
                Route::post('upload','Transaction\PurchaseOrderController@upload')->name('upload');
                Route::delete('upload','Transaction\PurchaseOrderController@upload')->name('upload');
                
                Route::resource('', 'Transaction\PurchaseOrderController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'show', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
    
            Route::name('retur.purchase.order.')->prefix('retur-purchase-order')
            ->group(function(){
                Route::post('upload','Transaction\ReturPurchaseOrderController@upload')
                    ->name('upload');
                Route::delete('upload','Transaction\ReturPurchaseOrderController@upload')
                    ->name('upload');
                
                Route::resource('', 'Transaction\ReturPurchaseOrderController',[
                    'parameters' => [
                        '' => 'id',
                    ]
                ])->only([
                    'index', 'show', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
    
            Route::name('sales.order.')->prefix('sales-order')->group(function(){
                Route::post('upload','Transaction\SalesOrderController@upload')->name('upload');
                Route::delete('upload','Transaction\SalesOrderController@upload')->name('upload');
                Route::any('{id}/print-small','Transaction\SalesOrderController@printSmall')->name('note.small');
                
                Route::resource('', 'Transaction\SalesOrderController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'show', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
    
            Route::name('retur.sales.order.')->prefix('retur-sales-order')
            ->group(function(){
                Route::post('upload','Transaction\ReturSalesOrderController@upload')
                    ->name('upload');
                Route::delete('upload','Transaction\ReturSalesOrderController@upload')
                    ->name('upload');
                
                Route::resource('', 'Transaction\ReturSalesOrderController',[
                    'parameters' => [
                        '' => 'id',
                    ]
                ])->only([
                    'index', 'show', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
    
            Route::name('mutation.product.')->prefix('mutation-product')->group(function(){
                Route::post('upload','Transaction\MutationProductController@upload')->name('upload');
                Route::delete('upload','Transaction\MutationProductController@upload')->name('upload');
                
                Route::resource('', 'Transaction\MutationProductController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'show', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
    
            Route::name('stock.opname.')->prefix('stock-opname')->group(function(){
                Route::post('upload','Transaction\StockOpnameController@upload')->name('upload');
                Route::delete('upload','Transaction\StockOpnameController@upload')->name('upload');
                Route::get('processed/{id}','Transaction\StockOpnameController@change_waiting')->name('change.waiting');
                Route::get('verified/{id}','Transaction\StockOpnameController@change_process')->name('change.process');
                Route::get('revert-verified/{id}','Transaction\StockOpnameController@revert_verified')->name('revert.verified');
                Route::any('print/{id}','Transaction\StockOpnameController@print')->name('print');
                Route::get('form-verfied/{id}','Transaction\StockOpnameController@form_verified')->name('form.verified');
                
                Route::resource('', 'Transaction\StockOpnameController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'show', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });

            Route::name('pulse.')->prefix('pulse')->group(function(){
                Route::resource('', 'Transaction\PulseController',['parameters' => [
                    '' => 'id',
                ]])->only([
                    'index', 'edit', 'create', 'store', 'update', 'destroy'
                ]);
            });
        });
    
        /**
        * Log
        * */ 
        Route::name('log.')->prefix('log')->group(function () {
            Route::resource('log-history','Log\LogHistoryController')->only([
                'index'
            ])->names('log.history');
    
            Route::resource('log-bank-transaction','Log\LogBankTransactionController')->only([
                'index'
            ])->names('log.bank.transaction');
    
            Route::resource('log-stock-transaction','Log\LogStockTransactionController')->only([
                'index'
            ])->names('log.stock.transaction');
        });
    
        /**
        * Report
        * */ 
        Route::name('report.')->prefix('report')->group(function () {
            Route::any('stock','Report\StockController@index')->name('stock.index');
            Route::any('product-identity','Report\ProductIdentityController@index')->name('product.identity.index');
            Route::any('sales-order','Report\SalesOrderController@index')->name('sales.order.index');
            Route::any('purchase-order','Report\PurchaseOrderController@index')->name('purchase.order.index');
            Route::any('expend-all','Report\ExpendAllController@index')->name('expend.all.index');
            Route::any('income-all','Report\IncomeAllController@index')->name('income.all.index');
            Route::any('financial-statement','Report\FinanceStatementController@index')->name('financial.statement.index');
            Route::any('loss-profit','Report\LossProfitController@index')->name('loss.profit.index');
            Route::any('supplier-tax','Report\SupplierTaxController@index')->name('supplier.tax.index');
            Route::any('income-expend','Report\IncomeExpendController@index')->name('income.expend.index');
        });
    });

    Route::name('setting.user.')->prefix('setting/user')->group(function () {
        Route::any('profile','Setting\UserController@profile')->name('profile');
        Route::post('upload','Setting\UserController@upload')->name('upload');
        Route::delete('upload','Setting\UserController@upload')->name('upload');
    });
    
    /**
    * User Permission
    * */
    Route::get('/js/permission.js', function () {
        $has_permissions = \Session::get('permission');
        $has_permissions = $has_permissions->pluck('id', 'slug')->toArray();
        header('Content-Type: text/javascript');
        echo('window.posp = ' . json_encode($has_permissions) . ';');
        exit();
    })->name('assets.permission');

    /**
    * Localization
    * */
    Route::get('/js/lang.js', function () {
        if (config('app.env') == 'local' || config('app.env') == 'development') {
            $lang = \Session::get('language');
    
            $files   = glob(resource_path('lang/' . $lang . '/*.php'));
            $strings = [];

            foreach ($files as $file) {
                $name           = basename($file, '.php');
                $strings[$name] = require $file;
            }
        } else {
            $strings = Cache::rememberForever('lang.js', function () {
                $lang = \Session::get('language');
    
                $files   = glob(resource_path('lang/' . $lang . '/*.php'));
                $strings = [];
    
                foreach ($files as $file) {
                    $name           = basename($file, '.php');
                    $strings[$name] = require $file;
                }
    
                return $strings;
            });
        }

        header('Content-Type: text/javascript');
        echo('window.i18n = ' . json_encode($strings) . ';');
        exit();
    })->name('assets.lang');

    Route::name('ajax.')->prefix('ajax')->group(function(){
        Route::get('list-product','Master\ProductController@ajaxListProduct')->name('list.product');
        Route::get('list-product-identity/{id?}','Master\ProductController@product_identity')->name('list.product.identity');
        Route::get('list-product-po/{id?}','Transaction\PurchaseOrderController@ajaxDetailPO')->name('list.product.po');
        Route::get('list-product-so/{id?}','Transaction\SalesOrderController@ajaxDetailSO')->name('list.product.so');
        Route::get('list-product-sop/{id?}','Transaction\StockOpnameController@ajaxDetailSOP')->name('list.product.so');
        Route::get('category','Master\CategoryController@ajaxCategory')->name('category');
        Route::get('brand','Master\BrandController@ajaxBrand')->name('brand');
        Route::get('product','Master\ProductController@ajaxProduct')->name('product');
        Route::get('sales-order/{id?}','Transaction\SalesOrderController@ajaxSalesOrder')->name('sales.order');
    });
});