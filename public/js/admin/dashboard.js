// Get context with jQuery - using jQuery's .get() method.
var profitLossCanvas = $('#profitLoss').get(0).getContext('2d')
var profitLossData = {
    labels: [],
    datasets: [{
        label: trans('dashboard.label.sales_order'),
        backgroundColor: 'rgba(60,141,188,0.9)',
        pointColor: '#000',
        data: []
    }]
}

for (let index = start_date.getDate(); index <= end_date.getDate(); index++) {
    var exists = index in chart_so;
    profitLossData.labels.push(index);
    profitLossData.datasets[0].data.push((exists ? chart_so[index] : 0));
}

var profitLossOptions = {
    maintainAspectRatio: false,
    responsive: true,
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
            gridLines: {
                display: false,
            },
            scaleLabel: {
                display: true,
                labelString: trans('dashboard.label.date')
            }
        }],
        yAxes: [{
            gridLines: {
                display: false,
            },
            ticks: {
                max: 100,
                min: 0,
                stepSize: 10
            }
        }]
    }
}

// This will get the first returned node in the jQuery collection.
var profitLoss = new Chart(profitLossCanvas, {
    type: 'line',
    data: profitLossData,
    options: profitLossOptions
})