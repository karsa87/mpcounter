$('.btn-edit-incentive').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="no_transaction"]').val(result.data.no_transaction);
                $('input[name="date_transaction"]').val(result.data.date_transaction);
                $('input[name="amount"]').val(result.data.amount.format());
                $('textarea[name="information"]').val(result.data.information);
                $('input[name="investor"]').val(result.data.investor);

                $("#form-bank_id").val(result.data.bank_id).trigger('change');
                $("#form-branch_id").val(result.data.branch_id).trigger('change');
                $("#form-type").val(result.data.type).trigger('change');
                $("#form-type").prop('disabled', true);
                
                $('#form-incentive').attr('action', result.url_edit);
                $('#form-incentive').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});