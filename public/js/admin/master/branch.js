$('.btn-edit-branch').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);
                $('textarea[name="address"]').val(result.data.address);
                $('input[name="phone"]').val(result.data.phone);
                $('input[name="branch_manager"]').val(result.data.branch_manager);
                $('input[name="branch_manager_phone"]').val(result.data.branch_manager_phone);
                $('#branch-status').prop('checked', result.data.status ? true : false);
                
                $('#form-branch').attr('action', result.url_edit);
                $('#form-branch').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});