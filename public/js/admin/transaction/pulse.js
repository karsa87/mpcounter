$('.btn-edit-pulse').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="no_transaction"]').val(result.data.no_transaction);
                $('input[name="date"]').val(result.data.date);
                $('input[name="phone"]').val(result.data.phone);
                $('input[name="total_pulse"]').val(result.data.total_pulse.format());
                $('input[name="sell_price"]').val(result.data.sell_price.format());
                $('input[name="capital_price"]').val(result.data.capital_price.format());
                $("#form-bank_id").val(result.data.bank_id).trigger('change');
                $("#form-branch_id").val(result.data.branch_id).trigger('change');

                $('#form-pulse').attr('action', result.url_edit);
                $('#form-pulse').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});