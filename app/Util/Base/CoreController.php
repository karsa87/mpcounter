<?php
namespace App\Util\Base;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Validator;
use Request;

class CoreController extends Controller
{
    public $log;
    public function __construct()
    {
        if( request('debugbar') )
        {
            \Debugbar::enable();
        }
        else
        {
            \Debugbar::disable();
        }
    }
    
    /*
	|--------------------------------------------------------------------------
	| BUILD LIST QUERY
	|--------------------------------------------------------------------------
	|@params $object : object table
	*/
    protected function rowLists($object)
    {
        return $object;
    }
    
    /*
	|--------------------------------------------------------------------------
	| DELETE
	|--------------------------------------------------------------------------
	|@params $object
	*/
    protected function delete($object, $method='hard', $flaging = [])
    {
        $data_before = $object->toJson();
        $data_after = [];
        $status = false;
        
        if( $method == 'hard' )
        {
            if($object->delete())
            {
                $status = true;
            }
        }
        elseif( $method == 'soft' )
        {
            $object->deleted_at = date('Y-m-d H:i:s');
            
            if( $object->save() )
            {
                $data_after = $object->toJson();
                $status = true;
            }
        }
        
        return [
            'status' => $status,
            'data' => [
                'data_before' => $data_before,
                'data_after' => $data_after,
            ]
        ];
    }
    
    /*
	|--------------------------------------------------------------------------
	| SAVE
	|--------------------------------------------------------------------------
	|@params $object : object table
	*/
    protected function save($object, $input, $param_validator, $primary = "id", $old_value_primary = "")
    {
        $validator = Validator::make($input, 
        (isset($param_validator['rules']) ? $param_validator['rules'] : $param_validator),
        (isset($param_validator['messages']) ? $param_validator['messages'] : []));

        if( isset($param_validator['remove']) )
        {
            foreach ($param_validator['remove'] as $c) 
            {
                if( array_key_exists($c, $input) ) 
                {
                    unset($input[$c]);
                }
            }
        }
        
        $id = null;
        $status = false;
        $data_before = [];
        $data_after = [];
        if (!$validator->fails()) 
        {
            if( isset($input[$primary]) && $input[$primary])
            {
                $id = !empty($old_value_primary) ? $old_value_primary : $input[$primary];
                $object->where($primary, $id);
            }

            $tmp_object = clone $object;
            $model = clone $object;
            $model = $model->getModel();
            $row = $tmp_object->first();

            //UPDATE
            if( $row && isset($input[$primary]) && $input[$primary] )
            {
                $data_before = $row->toArray();
                $data_after = $input;
                $row->fill($input);
                if( $model->timestamps )
                {
                    $row->updated_at = date('Y-m-d H:i:s');
                }

                if ($row->log_pic) {
                    $row->updated_by = auth()->user()->id;
                }

                if( $row->save() )
                {
                    $object = $row;
                    $id = $object->id;
                    $status = true;
                }
            }
            else //INSERT
            {
                $data_after = $input;
                if( array_key_exists('id', $input) )
                {
                    unset($input['id']);
                }
                
                if( $model->timestamps )
                {
                    $input['updated_at'] = date('Y-m-d H:i:s');
                    $input['created_at'] = date('Y-m-d H:i:s');
                }

                if ($model->log_pic) {
                    $input['created_by'] = auth()->user()->id;
                }

                $object = $object->getModel();
                $object->fill($input);
                
                if($object->save())
                {
                    $id = $object->id;
                    $status = true;
                }                
            }
            
        }
        
        if (is_string($id) || is_numeric($id)) {
            Log::info('save : '. $id);
        }

        return [
            'id' => $id,
            'status' => $status,
            'error' => $validator->errors(),
            'object' => $object,
            'data' => [
                'data_after' => json_encode($data_after),
                'data_before' => json_encode($data_before),
            ]
        ];
    }

    public function validation($input, $param_validator)
    {
        $validator = Validator::make($input, 
        (isset($param_validator['rules']) ? $param_validator['rules'] : $param_validator),
        (isset($param_validator['messages']) ? $param_validator['messages'] : []));

        if( isset($param_validator['remove']) )
        {
            foreach ($param_validator['remove'] as $c) 
            {
                if( array_key_exists($c, $input) ) 
                {
                    unset($input[$c]);
                }
            }
        }

        return $validator;
    }

    protected function render($viewPath, $data = [])
    {
        return \App\Util\Base\Layout::render($viewPath, $data);
    }
}

    