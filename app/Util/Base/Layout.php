<?php
namespace App\Util\Base;

use App\Util\Helpers\Util;
use Illuminate\Support\Facades\Cache;

class Layout
{
    const TEMPLATE = '';
    
    const ROW_PER_PAGE = 15;
    
    /*
	|--------------------------------------------------------------------------
	| RENDER VIEW
	|--------------------------------------------------------------------------
	|@params $viewPath : string view path
	|@params $data : array
	*/
    static function render($viewPath, $data = [])
    {
        $currentMenu = self::getCurrentMenu();
        
        $data['meta_title'] = isset($data['meta_title']) ? $data['meta_title'] : $currentMenu['title'];
        $data['meta_description'] =  isset($data['meta_description']) ? $data['meta_description'] : $currentMenu['desc'];
        $data['meta_keyword'] =  isset($data['meta_keyword']) ? $data['meta_keyword'] : str_replace(' ',',',$data['meta_description']);
        
        $data['layout_main'] = self::TEMPLATE.'.layouts.admin';
        $data['template'] = !empty(self::TEMPLATE) ? self::TEMPLATE . '.' : '' ;
        $data['theme'] = array_key_exists('theme', $_COOKIE) ? $_COOKIE['theme'] : '';
        $data['language'] = \Session::get('language');

        $data['new_user'] = FALSE;
        if( \Request::route()->getName() == 'dashboard' )
        {
            $data['new_user'] = \Auth::user()->user_lastlogin ? FALSE : TRUE;
        }

        return view($data['template'].$viewPath, $data);
    }
    
    /*
	|--------------------------------------------------------------------------
	| BUILD MENU STRUCTURED
	|--------------------------------------------------------------------------
	|@params $rowMenu : Object Menu
	*/
    static function buildRowMenu($rowMenu=[])
    {
        if(!$rowMenu) $rowMenu = \Session::get('menus');

        $rs = [
            'id_name' => [], 
            'id_url' => [], 
            'id_icon' => [],
            'id_parent' => [],
            'route_id' => [],
            'url_id' => [],
            'hierarchy' => [],
        ];

        if ($rowMenu) {
            $rs = [
                'id_name' => Util::getRowArray($rowMenu['all'], 'id', 'name'), 
                'id_url' => Util::getRowArray($rowMenu['all'], 'id', 'url'), 
                'id_icon' => Util::getRowArray($rowMenu['all'], 'id', 'icon'),
                'id_parent' => Util::getRowArray($rowMenu['all'], 'id', 'parent_id'),
                'route_id' => Util::getRowArray($rowMenu['all'], 'routes', 'id'),
                'url_id' => Util::getRowArray($rowMenu['all'], 'url', 'id'),
                'hierarchy' => $rowMenu ? $rowMenu['hierarchy']->toArray() : [],
            ];
        }
        
        return $rs;
    }

    static function getAccessMenu($level_id, $domain_id = null)
    {
        $menus = self::buildRowMenu();
        
        $rs_menu = [];
        foreach ($menus["id_menu_group"] as $menu_id => $menu_group_json) 
        {
            if( $menu_group_json == "null" ) continue;

            $menu_group = json_decode($menu_group_json);
            $menu_domain = json_decode($menus["id_menu_domain"][$menu_id]);
            if( $domain_id !== null && $menu_domain === null ) continue;

            if( is_numeric(array_search($level_id, $menu_group)) )
            {
                $rs_menu[$menu_id] = $menus["id_caption"][$menu_id];
            }

            if( $domain_id !== null && array_key_exists($menu_id, $rs_menu) && !( is_numeric(array_search($domain_id, $menu_domain)) || is_numeric(array_search("*", $menu_domain)) ) )
            {
                unset($rs_menu[$menu_id]);
            }

            if( !array_key_exists($menu_id, $rs_menu) ) continue;

            $child_id = $menu_id;
            $id_parent = $menus["id_parent"];
            $parent = [];
            while ( array_key_exists($child_id, $id_parent) ) 
            {
                $child_id = $menus["id_parent"][$child_id];
                if( $child_id == 0 ) continue;
                $parent[] = $child_id;
            }

            foreach ($parent as $parent_id) 
            {
                if(array_key_exists($parent_id, $rs_menu)) continue;
                
                $rs_menu[$parent_id] = $menus["id_caption"][$parent_id];
            }
        }

        return $rs_menu;
    }
    
    /*
	|--------------------------------------------------------------------------
	| GET ACTIVE MENU
	|--------------------------------------------------------------------------
	*/
    static function getCurrentMenu()
    {
        $menu = self::buildRowMenu();
        $route = \Request::route()->getName();
        $menuID = isset($menu['route_id'][$route]) ? $menu['route_id'][$route] : null;
        $permission = \Session::get('permission');

        if($menuID == null && $permission !== null)
        {
            $permission = $permission->pluck('menus.0.route','slug')->toArray();
            $route = isset($permission[$route]) ? $permission[$route] : null;
            $menuID = $route!==null && isset($menu['route_id'][$route]) ? $menu['route_id'][$route] : null;
        }
        
        $active_menu = [];
        if($menuID !== null)
        {
            $menu_id = $menuID;
            $active_menu[] = $menu_id;
            $id_parent = $menu["id_parent"];
            while ( array_key_exists($menu_id, $id_parent) ) 
            {
                $active_menu[] = $menu_id = $menu["id_parent"][$menu_id];
            }
        }
        
        return [
            'menu_id'=> $menuID!=null ? $menuID : "",
            'title'  => isset($menu['id_name'][$menuID]) ? $menu['id_name'][$menuID] : 'Dashboard',
            // 'desc'   => isset($menu['id_lead'][$menuID]) ? $menu['id_lead'][$menuID] : '',
            'desc'   => '',
            'parent' => isset($menu['id_parent'][$menuID]) ? $menu['id_parent'][$menuID] : '',
            'icon'   => isset($menu['id_icon'][$menuID]) ? $menu['id_icon'][$menuID] : '',
            'active_menu' => $active_menu,
        ];
    }
    
    /*
	|--------------------------------------------------------------------------
	| RENDER PAGING FOOTER
	|--------------------------------------------------------------------------
	|@params $object
	*/
    static function paging($rows)
    {
        return view(self::TEMPLATE.'.layouts.partials.paging', compact('rows'));
    }
}

    