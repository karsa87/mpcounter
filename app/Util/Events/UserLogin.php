<?php

namespace App\Util\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\User;

class UserLogin
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $user, $userGoogle;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    // public function __construct(User $user, $userGoogle)
    public function __construct(User $user)
    {
        $this->user = $user;
        
        // $this->userGoogle = $userGoogle;
    }
    
}
