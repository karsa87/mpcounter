<?php

namespace App\Util\Listeners;

use App\Models\Role;
use App\Models\Menu;
use App\Models\Setting;
use App\Models\Permission;
use App\Util\Events\UserLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRole
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        $locale = strtolower(Setting::where('key', 'DEFAULT_LANGUAGE')->first()->value);
        if ($locale != 'en' && $locale != 'id') {
            $locale = 'id';
        }

        $user = $event->user;
        $roles_id = $user->roles->pluck('id')->toArray();
        
        $menus['all'] = Menu::with('permissions:slug');
        //USER MENU
        $permissions = [];
        if( $user->isDeveloper() )
        {
            $permissions = Permission::all();
        }
        else
        {
            $permissions = $user->getPermissions();
            $menus['all']->whereHas('roles', function($q) use($roles_id){
                $q->whereIn('id', [$roles_id]);
            });
        }
        $menus['all'] = $menus['all']->get()->keyBy('id');
        
        $menus['all'] = $menus['all']->map(function($m){
            $m->routes = $m->permissions->pluck('slug')->toArray();

            return $m;
        });

        $menus['hierarchy'] = Menu::with('submenu.submenu.submenu.submenu.submenu')
                                ->firstParents()
                                ->orderBy('order', 'ASC')
                                ->get();

        $menus['hierarchy'] = $this->access($menus['hierarchy'], $menus['all']->keys()->all());
        $menus['hierarchy'] = collect($menus['hierarchy']);
        
        \Artisan::call('view:clear');
        //SAVE SESSION
        \Session::put('language', $locale);
        \Session::put('permission', $permissions);
        \Session::put('menus', $menus);
        \Session::save();
    }

    private function access($menus, $menu_ids)
    {
        $menus = is_array($menus) ? $menus : $menus->toArray();
        $result = [];
        foreach ($menus as $menu)
        {
            $submenu_new = [];
            $submenu_old = $menu['submenu'];
            unset($menu['submenu']);
            if(count($submenu_old) > 0)
            {
                $menu['submenu'] = $this->access($submenu_old, $menu_ids);
            }
            else
            {
                $menu['submenu'] = [];
            }

            if( is_numeric(array_search($menu['id'], $menu_ids)) )
            {
                $result[] = collect($menu);
            }
        }

        return $result;
    }
}
