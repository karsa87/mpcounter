<?php

namespace App\Http\Controllers\Log;

use App\Util\Base\CoreController;
use App\Models\LogBankTransaction;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\User;
use App\Models\Bank;

class LogBankTransactionController extends CoreController
{
    public function index()
    {
        $query = LogBankTransaction::with('user', 'bank', 'transaction')
                    ->orderBy('id','DESC');

        if( request('search_datetime') )
        {
            $search_datetime = Util::get('search_datetime');
            $search_datetime = explode(" - ", $search_datetime);    
            
            $query->whereBetween('log_datetime', $search_datetime);
        }

        if( request('search_table') )
        {
            $query->where('table', Util::get('table'));
        }

        if( request('search_bank_id') )
        {
            $query->where('bank_id', Util::get('search_bank_id'));
        }

        if( request('search_user_id') )
        {
            $query->where('user_id', Util::get('search_user_id'));
        }

        if( is_numeric(request('search_type')) )
        {
            $query->type(Util::get('search_type'));
        }

        $logs = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $tables = $logs->pluck('table')->unique();
        $users = User::active()->get()->pluck('name','id');
        $banks = Bank::active()->get()->pluck('name','id');

        return Layout::render('log.log_bank_transaction.index', [
            "logs" => $logs,
            "tables" => $tables,
            "users" => $users,
            "banks" => $banks,
        ]);
    }
}
