<?php

namespace App\Http\Controllers\Log;

use App\Util\Base\CoreController;
use App\Models\LogStockTransaction;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Product;
use App\Models\Branch;
use App\Models\User;

class LogStockTransactionController extends CoreController
{
    public function index()
    {
        $query = LogStockTransaction::with([
                        'user', 
                        'stock' => function($q) {
                            $q->with('product', 'branch');
                        },
                        'transaction'
                    ])
                    ->orderBy('id','DESC');

        if( request('search_datetime') )
        {
            $search_datetime = Util::get('search_datetime');
            $search_datetime = explode(" - ", $search_datetime);    
            
            $query->whereBetween('log_datetime', $search_datetime);
        }

        if( request('search_table') )
        {
            $query->where('table', Util::get('table'));
        }

        if( request('search_product_id') )
        {
            $product_id = Util::get('search_product_id');
            $query->whereHas('stock', function($q) use($product_id) {
                $q->where('product_id', $product_id);
            });
        }

        if( request('search_branch_id') )
        {
            $branch_id = Util::get('search_branch_id');
            $query->whereHas('stock', function($q) use($branch_id) {
                $q->where('branch_id', $branch_id);
            });
        }

        if( request('search_user_id') )
        {
            $query->where('user_id', Util::get('search_user_id'));
        }

        if( is_numeric(request('search_type')) )
        {
            $query->type(Util::get('search_type'));
        }

        $logs = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $tables = $logs->pluck('table')->unique();
        $users = User::active()->get()->pluck('name','id');
        $products = Product::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('log.log_stock_transaction.index', [
            "logs" => $logs,
            "tables" => $tables,
            "users" => $users,
            "products" => $products,
            "branchs" => $branchs,
        ]);
    }
}
