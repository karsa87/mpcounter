<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Supplier;

class SupplierController extends CoreController
{
    public function index()
    {
        $query = Supplier::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_snm') )
        {
            $query->whereLike('sales_name', Util::get('_snm'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        $suppliers = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('master.supplier.index', [
            "suppliers" => $suppliers
        ]);
    }

    public function edit($id)
    {
        $supplier = Supplier::find($id);
        $supplier = $supplier ? $supplier->toArray() : [];

        return response()->json([
            'url_edit' => route('master.supplier.update', $id),
            'data' => $supplier
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? Supplier::STATUS_ACTIVE : Supplier::STATUS_DEACTIVE;
        $input['has_tax'] = $input['has_tax'] == 'on' ? Supplier::HAS_TAX_YES : Supplier::HAS_TAX_NO;

        $redirect = redirect()->back();
        $query = Supplier::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "address" => 'required',
                "phone" => [
                    'required',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                "sales_phone" => [
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16',
                    'nullable'
                ],
            ],
            "messages" => trans("supplier.errors")
        ]);

        if ($result['status']) {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Supplier::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if ($status["status"]) {
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("supplier.title"), $name) );
            \DB::commit();
        } else {
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("supplier.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxSupplier()
    {
        $params = request()->all();

        $list = Supplier::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
