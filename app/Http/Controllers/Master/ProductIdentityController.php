<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Product;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Promo;
use App\Models\Stock;

class ProductIdentityController extends CoreController
{
    public function index()
    {
        $query = ProductIdentity::with([
            'branch', 'poDetail',
            'product' => function($q) {
                $q->with('category', 'brand');
            }
        ]);

        $operator = Util::get('_sop');
        $operator = is_numeric($operator) ? trans("global.array.operator.$operator") : '';
        $stock = Util::get('_stc') ?? 0;
        $branch_id = Util::get('_bc');
        
        if (!empty($operator) || is_numeric($stock)) {
            $query->where('stock', $operator, $stock);
        }

        if (!empty($branch_id)) {
            $query->where('branch_id', $branch_id);
        }

        if( request('_k') )
        {
            $keyword = Util::get('_k');
            $query->where(function($q) use ($keyword) {
                $q->whereHas('product', function($q1) use ($keyword) {
                    $q1->whereLike('name', $keyword)
                        ->orWhereLike('code', $keyword);
                })->orWhere(function($q1) use ($keyword) {
                    $q1->whereLike('identity', $keyword);
                });
            });
        }

        if( request('_bd') || request('_cg') )
        {
            $brand_id = Util::get('_bd');
            $category_id = Util::get('_cg');

            $query->whereHas('product', function($q) use($brand_id, $category_id) {
                if ($brand_id) {
                    $q->where('brand_id', $brand_id);
                }

                if ($category_id) {
                    $q->where('category_id', $category_id);
                }
            });
        }

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy)) {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else if(empty($orderBy)) {
            $query->orderBy('updated_at', 'DESC');
        }

        $products = $query->paginate(Layout::ROW_PER_PAGE)
                        ->appends( request()->except(["page"]) );
        
        $brands = Brand::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $categories = Category::active()->get()->pluck('name','id');

        return Layout::render('master.product_identity.index', [
            "products" => $products,
            'brands' => $brands,
            'branchs' => $branchs,
            'categories' => $categories,
        ]);
    }
}
