<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Menu;

class CategoryController extends CoreController
{
    public function index()
    {
        $query = Category::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( is_numeric(request('_idf')) )
        {
            $query->identifier(Util::get('_idf'));
        }

        $categories = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('master.category.index', [
            "categories" => $categories,
            'breadcrumbs' => [
                [
                    'text' => trans('global.menu.setting'),
                    'url' => '#'
                ],
                [
                    'text' => trans('category.title'),
                    'url' => route('master.category.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $category = $category ? $category->toArray() : [];

        return response()->json([
            'url_edit' => route('master.category.update', $id),
            'data' => $category
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);

        if (request('status')) {
            $input['status'] = $input['status'] == 'on' ? Category::STATUS_ACTIVE : Category::STATUS_DEACTIVE;
        }
        
        $redirect = redirect()->back();
        $query = Category::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "identifier" => 'required',
            ],
            "messages" => trans("category.errors"),
        ]);

        if (!request()->ajax()) {
            if ( $result['status'] ) {
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            } else {
                $redirect->withInput( request()->except(['_token']) )
                            ->withErrors($result['error'])
                            ->with('error', sprintf('%s', trans("global.failed_save")) );
            }
        }

        return request()->ajax() ? response()->json($result) : $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Category::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');

        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("category.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("category.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxCategory()
    {
        $params = request()->all();

        $list = Category::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
