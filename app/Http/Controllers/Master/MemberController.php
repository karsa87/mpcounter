<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Member;

class MemberController extends CoreController
{
    public function index()
    {
        $query = Member::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( is_numeric(request('_tp')) )
        {
            $query->type(Util::get('_tp'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        $members = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('master.member.index', [
            "members" => $members
        ]);
    }

    public function edit($id)
    {
        $member = Member::find($id);
        $member->birthdate = Util::formatDate($member->birthdate, Util::FORMAT_DATE_EN_SHORT);
        $member = $member ? $member->toArray() : [];

        return response()->json([
            'url_edit' => route('master.member.update', $id),
            'data' => $member
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['max_debt'] = array_key_exists('max_debt', $input) ? Util::remove_format_currency($input['max_debt']) : 0;
        $input['max_debt'] = $input['max_debt'] ?: 0;
        $input['max_day_debt'] = array_key_exists('max_day_debt', $input) ? ($input['max_day_debt'] ?: 0) : 0;
        $input['status'] = $input['status'] == 'on' ? Member::STATUS_ACTIVE : Member::STATUS_DEACTIVE;

        $redirect = redirect()->back();
        $query = Member::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "address" => 'required',
                "phone" => [
                    'required',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                "identity_type" => 'required',
                "identity" => 'required',
                "member_number" => 'required',
                "max_day_debt" => 'numeric|min:0|nullable',
                "max_debt" => 'numeric|min:0|nullable',
            ],
            "messages" => trans("member.errors")
        ]);

        if ($result['status']) {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Member::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if ($status["status"]) {
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("member.title"), $name) );
            \DB::commit();
        } else {
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("member.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxMember()
    {
        $params = request()->all();

        $list = Member::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
