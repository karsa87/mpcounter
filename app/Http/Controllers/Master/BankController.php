<?php

namespace App\Http\Controllers\Master;

use Illuminate\Validation\Rule;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Bank;

class BankController extends CoreController
{
    public function index()
    {
        $query = Bank::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('search_name') )
        {
            $query->whereLike('name', Util::get('search_name'));
        }

        if( request('_an') )
        {
            $query->whereLike('account_number', Util::get('_an'));
        }

        if( request('_nmow') )
        {
            $query->whereLike('name_owner', Util::get('_nmow'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( is_numeric(request('_df')) )
        {
            $query->default(Util::get('_df'));
        }

        $banks = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('master.bank.index', [
            "banks" => $banks,
            'breadcrumbs' => [
                [
                    'text' => trans('global.menu.bank'),
                    'url' => '#'
                ],
                [
                    'text' => trans('bank.title'),
                    'url' => route('master.bank.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        $bank = Bank::find($id);
        $bank = $bank ? $bank->toArray() : [];

        return response()->json([
            'url_edit' => route('master.bank.update', $id),
            'data' => $bank
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? Bank::STATUS_ACTIVE : Bank::STATUS_DEACTIVE;
        $input['is_default'] = $input['is_default'] == 'on' ? Bank::DEFAULT_YES : Bank::DEFAULT_NO;

        $redirect = redirect()->back();
        $query = Bank::query();

        $rule_default = '';
        if ($input['is_default']) {
            $rule_default = 'unique:bank,is_default,'.($input['id'] ?: 'NULL') .',id';
        }

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                'account_number' => 'numeric|unique:bank,account_number,'.($input['id'] ?: 'NULL') .',id',
                'is_default' => $rule_default,
                'status' => $input['is_default'] ? Rule::in([1]) : ''
            ],
            "messages" => trans("bank.errors"),
        ]);

        if( $result['status'] )
        {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Bank::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("bank.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("bank.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxBank()
    {
        $params = request()->all();

        $list = Bank::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
