<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Brand;
use App\Models\Menu;

class BrandController extends CoreController
{
    public function index()
    {
        $query = Brand::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        $brands = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('master.brand.index', [
            "brands" => $brands,
            'breadcrumbs' => [
                [
                    'text' => trans('global.menu.brand'),
                    'url' => '#'
                ],
                [
                    'text' => trans('brand.title'),
                    'url' => route('master.brand.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        $brand = Brand::find($id);
        $brand = $brand ? $brand->toArray() : [];

        return response()->json([
            'url_edit' => route('master.brand.update', $id),
            'data' => $brand
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? Brand::STATUS_ACTIVE : Brand::STATUS_DEACTIVE;

        $redirect = redirect()->back();
        $query = Brand::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255|unique:brand,name,'.($input['id'] ?: 'NULL') .',id',
            ],
            "messages" => trans("brand.errors"),
        ]);

        if (!request()->ajax()) {
            if ( $result['status'] ) {
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            } else {
                $redirect->withInput( request()->except(['_token']) )
                            ->withErrors($result['error'])
                            ->with('error', sprintf('%s', trans("global.failed_save")) );
            }
        }

        return request()->ajax() ? response()->json($result) : $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Brand::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'hard');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("brand.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("brand.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxBrand()
    {
        $params = request()->all();

        $list = Brand::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
