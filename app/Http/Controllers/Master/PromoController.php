<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Promo;

class PromoController extends CoreController
{
    public function index()
    {
        $query = Promo::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_cd') )
        {
            $query->whereLike('code', Util::get('_cd'));
        }

        if( request('_pd') )
        {
            $date = Util::get('_pd');
            $date = explode(" - ", $date);
            
            $query->where('date_start', '<=', $date[0]);
            $query->where('date_end', '>=', $date[1]);
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( is_numeric(request('_ctg')) )
        {
            $query->where('category_id', Util::get('_ctg'));
        }

        if( is_numeric(request('_bd')) )
        {
            $query->where('brand_id', Util::get('_bd'));
        }

        $promos = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $categories = Category::active()->get()->pluck('name', 'id');
        $brands = Brand::active()->get()->pluck('name', 'id');

        return Layout::render('master.promo.index', [
            "promos" => $promos,
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }

    public function products()
    {
        $query = Product::query();

        if (request("q")) { //query
            $search_q = Util::get('q');
            $query->where(function($q) use($search_q) {
                $q->where('name');
            });
        }

        if (request("op")) { //query
            $stock = request("stock") ?? 0;
            $query->where("stock", trans("global.array.operator.".Util::get('op')), $stock);
        }

        if (request("bc")) { //branch_id
            $query->where("branch_id", Util::get('bc'));
        }

        $products = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        
        return response()->json($products);
    }

    public function create()
    {
        $promo = new Promo();
        $categories = Category::select('name as text', 'id')->active()->get();
        $categories_form = $categories->pluck('text', 'id');
        $brands = Brand::select('name as text', 'id')->active()->get();
        $brands_form = $brands->pluck('text', 'id');

        return Layout::render('master.promo.form', [
            'model' => $promo,
            'categories' => $categories,
            'brands' => $brands,
            'categories_form' => $categories_form,
            'brands_form' => $brands_form,
        ]);
    }

    public function show($id)
    {
        $promo = Promo::with('products')->whereId($id)->first();
        $images = $promo->images_info();
        $categories = Category::select('name as text', 'id')->active()->get();
        $categories_form = $categories->pluck('text', 'id');
        $brands = Brand::select('name as text', 'id')->active()->get();
        $brands_form = $brands->pluck('text', 'id');

        return Layout::render('master.promo.show', [
            'model' => $promo,
            'images' => $images,
            'categories' => $categories,
            'brands' => $brands,
            'categories_form' => $categories_form,
            'brands_form' => $brands_form,
        ]);
    }

    public function edit($id)
    {
        $promo = Promo::with('products')->whereId($id)->first();
        $images = $promo->images_info();
        $categories = Category::select('name as text', 'id')->active()->get();
        $categories_form = $categories->pluck('text', 'id');
        $brands = Brand::select('name as text', 'id')->active()->get();
        $brands_form = $brands->pluck('text', 'id');

        $products = [];
        foreach ($promo->products as $product) {
            $products[] = [
                "product_id" => $product->id,
                "name" => $product->name,
                "sell_price" => $product->sell_price,
                "image" => $product->image(),
                "brand" => $product->brand->name,
                "category" => $product->category->name,
            ];
        }

        return Layout::render('master.promo.form', [
            'model' => $promo,
            'images' => $images,
            'categories' => $categories,
            'brands' => $brands,
            'categories_form' => $categories_form,
            'brands_form' => $brands_form,
            'products' => $products
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? Promo::STATUS_ACTIVE : Promo::STATUS_DEACTIVE;
        $input['discount_price'] = Util::remove_format_currency($input['discount_price']);
        $input['discount_percent'] = Util::remove_format_currency($input['discount_percent']);
        $date = $input['period'];
        $date = explode(" - ", $date);
        $input['date_start'] = count($date) > 0 ? $date[0] : '';
        $input['date_end'] = count($date) > 1 ? $date[1] : '';

        $redirect = redirect()->back();
        $query = Promo::query();

        $result = [
            'status' => FALSE,
            'error' => []
        ];
        if (!empty($input['discount_price']) && !empty($input['discount_percent']) ) {
            $result['error'][] = trans('promo.message.error_choose_one');
        } else if (empty($input['discount_price']) && empty($input['discount_percent'])) {
            $result['error'][] = trans('promo.message.error_choose_one');
        }

        if ( count($result['error']) <= 0 ) {
            $result = $this->save($query, $input,[
                "rules" => [
                    'name' => 'required|max:255',
                    'code' => 'required|unique:promo,code,'.($input['id'] ?: 'NULL') .',id',
                    'date_start' => 'date_format:Y-m-d H:i',
                    'date_end' => 'date_format:Y-m-d H:i',
                    'discount_percent' => 'numeric|' . ($input['discount_price'] ? 'nullable' : 'required|min:1|max:100'),
                    'discount_price' => 'numeric|' . ($input['discount_percent'] ? 'nullable' : 'required|min:1'),
                    'period' => 'required',
                    'products' => [
                        (empty($input['category_id']) && empty($input['brand_id'])) ? 'required' : ''
                    ]
                ],
                "messages" => trans("promo.errors"),
                "remove" => ['products', 'period']
            ]);
        }

        if( $result['status'] )
        {
            $promo = $result['object'];
            $promo->images = json_encode($promo->moveTmpImage());
            $promo->skipLog()->save();

            if (empty($input['category_id']) && empty($input['brand_id'])) {
                $promo->products()->sync(collect($input['products'])->pluck('product_id'));
            } else {
                $promo->products()->sync([]);
            }
            
            $redirect = redirect()->route('master.promo.show', [$promo->id]);
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Promo::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("promo.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("promo.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => Promo::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            Promo::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }

    public function ajaxPromo()
    {
        $params = request()->all();

        $list = Promo::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
