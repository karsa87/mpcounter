<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Branch;

class BranchController extends CoreController
{
    public function index()
    {
        $query = Branch::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_emid') )
        {
            $query->whereLike('employee_id', Util::get('_emid'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        $branchs = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $employes = Employee::active()->get()->pluck('name','id');

        return Layout::render('master.branch.index', [
            "branchs" => $branchs,
            'employes' => $employes
        ]);
    }

    public function edit($id)
    {
        $branch = Branch::find($id);
        $branch = $branch ? $branch->toArray() : [];

        return response()->json([
            'url_edit' => route('master.branch.update', $id),
            'data' => $branch
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? Branch::STATUS_ACTIVE : Branch::STATUS_DEACTIVE;

        $redirect = redirect()->back();
        $query = Branch::query();
            
        //default employee
        if (!empty(config('default.employee_id'))) {
            $input['employee_id'] = config('default.employee_id');
        }

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "address" => 'required',
                "phone" => [
                    'required',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                "employee_id" => 'nullable',
            ],
            "messages" => trans("branch.errors")
        ]);

        if( $result['status'] )
        {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Branch::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("branch.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("branch.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxBranch()
    {
        $params = request()->all();

        $list = Branch::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
