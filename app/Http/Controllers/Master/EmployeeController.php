<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Branch;
use App\Models\Role;
use App\Models\User;

class EmployeeController extends CoreController
{
    public function index()
    {
        $query = Employee::with(['user', 'branch']);

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_em') )
        {
            $query->whereLike('email', Util::get('_em'));
        }

        if( request('_idf') )
        {
            $query->whereLike('identifier', Util::get('_idf'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( request('_bc') )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        $employees = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $roles = Role::employee()->get()->pluck('name','id')->toArray();
        $branchs = Branch::active()->get()->pluck('name','id')->toArray();

        return Layout::render('master.employee.index', [
            "employees" => $employees,
            "form_roles" => $roles,
            "branchs" => $branchs,
        ]);
    }

    public function edit($id)
    {
        $employee = Employee::with([
            'user' => function($q) {
                $q->with('roles');
            }
        ])->whereId($id)->first();
        
        $roles = [];
        if( $employee->user->roles )
        {
            $roles = $employee->user->roles;
            $roles = $roles ? $roles->pluck('id')->toArray() : [];
        }
        $employee = $employee ? $employee->toArray() : [];
        $employee['user']['roles'] = $roles;

        return response()->json([
            'url_edit' => route('master.employee.update', $id),
            'data' => $employee
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? Employee::STATUS_ACTIVE : Employee::STATUS_DEACTIVE;

        $redirect = redirect()->back();
        $query = Employee::query();

        $valid_password = 'required|min:5';
        if ( $input['id'] ) {
            $valid_password = isset($input['password']) ? 'min:5' : '';
        }

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "address" => 'required',
                "phone" => [
                    'required',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                "email" => 'required|email|unique:users,email,'.($input['user_id'] ?: 'NULL') .',id',
                "username" => 'required|max:255|unique:users,username,'.($input['user_id'] ?: 'NULL') .',id',
                "password" => $valid_password,
                'user_role' => 'required',
                'branch_id' => 'required',
            ],
            "messages" => trans("employee.errors"),
            'remove' => ['username','password','user_role']
        ]);

        if( $result['status'] )
        {
            $employee = Employee::with('user')->whereId($result['id'])->first();
            $user = $employee->user_id ? $employee->user : new User();
            $user->name = $employee->name;
            $user->username = $input['username'];
            $user->password = $input['password'];
            $user->email = $employee->email;
            $user->status = $employee->status;
            $user->branch_id = $employee->branch_id;
            $user->save();

            $employee->user_id = $user->id;
            $employee->save();

            if( request('user_role') )
            {
                $roles = is_array($input['user_role']) ? $input['user_role'] : [$input['user_role']];
                $user->syncRoles($roles);
            }
            else
            {
                $user->detachAllRoles();
            }

            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Employee::find($id);
        $name = $row->name;
        $row->user->delete();

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("employee.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("employee.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxEmployee()
    {
        $params = request()->all();

        $list = Employee::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
