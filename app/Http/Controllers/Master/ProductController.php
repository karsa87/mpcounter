<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Product;
use App\Models\Branch;
use App\Models\Member;
use App\Models\Brand;
use App\Models\Promo;
use App\Models\Stock;

class ProductController extends CoreController
{
    public function index()
    {
        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        
        $operator = Util::get('_sop');
        $operator = is_numeric($operator) ? trans("global.array.operator.$operator") : '';
        $stock = Util::get('_stc');
        $branch_id = Util::get('_bc');

        $query = Product::with([
            'stock' => function($q) use ($operator, $stock, $branch_id){
                $q->select(\DB::raw('SUM(stock) as stock'), 'product_id')->groupBy('product_id');

                if (!empty($branch_id)) {
                    $q->where('branch_id', $branch_id);
                    $q->groupBy('branch_id');
                }

                if (!empty($operator) && is_numeric($stock)) {
                    $q->havingRaw("SUM(stock) $operator $stock");
                }
            }
        ]);

        $product_id = [];
        
        if (!empty($operator) || is_numeric($stock) || !empty($orderBy) || !empty($branch_id)) {
            $query->whereHas('stock', function($q) use ($operator, $stock, $branch_id){
                $q->select(\DB::raw('SUM(stock) as stock'), 'product_id')->groupBy('product_id');

                if (!empty($branch_id)) {
                    $q->where('branch_id', $branch_id);
                    $q->groupBy('branch_id');
                }
                
                if (!empty($operator) && is_numeric($stock)) {
                    $q->havingRaw("SUM(stock) $operator $stock");
                }
            });

            // $q_stock = Stock::select(\DB::raw('SUM(stock) as stock'), 'product_id')
            //             ->groupBy('product_id');

            // if ($orderBy == 'stock') {
            //     if ($sorting == 1) {
            //         $q_stock->orderByRaw('SUM(stock) DESC');
            //     } else if($sorting == 0) {
            //         $q_stock->orderByRaw('SUM(stock) ASC');
            //     }
            // }

            // if (!empty($operator) && is_numeric($stock)) {
            //     $q_stock->havingRaw("SUM(stock) $operator $stock");
            // }

            // if (!empty($branch_id)) {
            //     $q_stock->where('branch_id', $branch_id);
            //     $q_stock->groupBy('branch_id');
            // }

            // $product_id = $q_stock->get()->pluck('product_id')->toArray();
            // $product_id = array_slice($product_id, 0, Layout::ROW_PER_PAGE);
            
            // $query->whereIn('id', $product_id);
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_cd') )
        {
            $query->whereLike('code', Util::get('_cd'));
        }

        if( request('_bd') )
        {
            $query->where('brand_id', Util::get('_bd'));
        }

        if( request('_cg') )
        {
            $query->where('category_id', Util::get('_cg'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else if(empty($orderBy)) {
            $query->orderBy('updated_at', 'DESC');
        }

        $products = $query->paginate(Layout::ROW_PER_PAGE)
                        ->appends( request()->except(["page"]) );

        if ($orderBy == 'stock') {
            $sort = $products->getCollection();
            
            if ($sorting == 1) {
                $sort = $sort->sortByDesc('stock.*.stock');
            } else if($sorting == 0) {
                $sort = $sort->sortBy('stock.*.stock');
            }

            $products->setCollection($sort);
        }
        
        $brands = Brand::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $categories = Category::active()->get()->pluck('name','id');

        return Layout::render('master.product.index', [
            "products" => $products,
            'brands' => $brands,
            'branchs' => $branchs,
            'categories' => $categories,
        ]);
    }

    public function show($id)
    {
        $model = Product::with('category', 'brand', 'stock')->whereId($id)->first();
        $branchs = Branch::select('name as text', 'id')->active()->get();
        
        return Layout::render('master.product.show', [
            'model' => $model,
            'branchs' => $branchs,
        ]);
    }

    public function product_identity($id = '')
    {
        $query = ProductIdentity::with([
            'branch', 
            'product' => function($q) {
                $q->active();
                $q->with('brand', 'category');
            }
        ]);

        if ($id) {
            $query->where('product_id', $id);
        }

        if (request("q")) {
            $_q = Util::get('q');
            $query->where(function($q) use($_q) {
                $q->whereHas('product', function($qq) use($_q) {
                    $qq->whereLike("name", $_q)
                        ->orWhereLike('code', $_q);
                })
                ->orWhereLike('identity', $_q);
            });
        }

        if (request("c") || request("bd")) { //query
            $_c = Util::get('c');
            $_bd = Util::get('bd');

            $query->whereHas('product', function($q) use($_c, $_bd) {
                if ($_c) {
                    $q->where("category_id", $_c);
                }
                
                if ($_bd) {
                    $q->where("brand_id", $_bd);
                }
            });
        }

        if (request("op")) { //stock
            $stock = request("stock") ?? 0;
            $query->where("stock", trans("global.array.operator.".Util::get('op')), $stock);
        }

        if (request("bc")) { //branch_id
            $query->where("branch_id", Util::get('bc'));
        }

        $member = null;
        if (request("m") && is_numeric(request("m"))) { //branch_id
            $member = Member::find(Util::get('m'));
        }

        $products = $query->paginate(7)->appends( request()->except(["page"]) );

        $products_arr = $products->toArray();
        unset($products_arr['data']);

        $promos = Promo::with('products')->publish()->get();
        $promos = $promos->keyBy('id');
        $promo_products = $promos->pluck('products.*.id', 'id');
        $promo_categories = $promos->pluck('category_id', 'id');
        $promo_brands = $promos->pluck('brand_id', 'id');
        
        $result = [];
        foreach ($products as $product) {
            $v = $product->toArray();

            $v['product_identity_id'] = $product->id;
            $product_id = $v['product']['id'];
            $promo_id = $promo_products->filter(function ($value, $key) use($product_id) {
                return is_numeric(array_search($product_id, $value));
            });

            $promo_id = $promo_id ? $promo_id->keys()->first() : $promo_id;
            if (!is_numeric($promo_id)) {
                $promo_id = $promo_categories->search($product->product->category_id);
            }
            
            if (!is_numeric($promo_id)) {
                $promo_id = $promo_brands->search($product->product->brand_id);
            }
            
            if (is_numeric($promo_id)) {
                $promo = $promos[$promo_id];
                unset($promo['products']);
                
                $v['promo'] = $promo;
            }else{
                $v['promo'] = null;
            }

            $v['name'] = $product->product->name;
            $v['image'] = $product->product->image();
            $v['category'] = $product->product->category->name;
            $v['identifier'] = $product->product->category->identifier;
            $v['brand'] = $product->product->brand->name;
            $v['code'] = $product->product->code;
            $v['information'] = $product->product->information;
            // $v['with_expired_date'] = $product->product->with_expired_date;

            $v['sell_price'] = $product->product->sell_price;
            if ($member) {
                if ($member->isResellerOne()) {
                    $v['sell_price'] = $product->product->reseller1_sell_price;
                    $v['sell_price'] = $v['sell_price'] != "0" ? $v['sell_price'] : $product->product->sell_price;
                } elseif ($member->isResellerTwo()) {
                    $v['sell_price'] = $product->product->reseller2_sell_price;
                    $v['sell_price'] = $v['sell_price'] != "0" ? $v['sell_price'] : $product->product->reseller1_sell_price;
                    $v['sell_price'] = $v['sell_price'] != "0" ? $v['sell_price'] : $product->product->sell_price;
                } else {
                    $v['sell_price'] = $product->product->sell_price;
                }
            } else {
                $v['sell_price'] = $product->product->sell_price;
            }
            
            $v['general_sell_price'] = $product->product->sell_price;
            $v['reseller1_sell_price'] = $product->product->reseller1_sell_price;
            $v['reseller2_sell_price'] = $product->product->reseller2_sell_price;
            $v['cost_of_goods'] = $product->product->cost_of_goods;
            $v['discount'] = $v['promo'] ? $v['promo']->calculateDiscount($product->product->sell_price) : 0;
            $v['discount_promo'] = $v['discount'];
            $v['promo_id'] = $v['promo'] ? $v['promo']->id : null;
            $v['purchase_price'] = 0;
            $v['total_amount'] = 0;

            $result[] = $v;
        }

        $products_arr['data'] = $result;
        $products = $products_arr;
        
        return response()->json($products);
    }

    public function edit($id)
    {
        $model = Product::find($id);
        $brands = Brand::active()->get()->pluck('name','id');
        $categories = Category::active()->get()->pluck('name','id');

        return Layout::render('master.product.form', [
            'model' => $model,
            'brands' => $brands,
            'categories' => $categories,
        ]);
    }

    public function create()
    {
        $brands = Brand::active()->get()->pluck('name','id');
        $categories = Category::active()->get()->pluck('name','id');

        return Layout::render('master.product.form', [
            'model' => new Product(),
            'brands' => $brands,
            'categories' => $categories,
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['images'] = $input['images'] ?: '[]';
        $input['sell_price'] = Util::remove_format_currency($input['sell_price'] ?? 0 );
        $input['reseller1_sell_price'] = Util::remove_format_currency($input['reseller1_sell_price'] ?? 0 );
        $input['reseller2_sell_price'] = Util::remove_format_currency($input['reseller2_sell_price'] ?? 0 );
        $input['status'] = $input['status'] == 'on' ? Product::STATUS_ACTIVE : Product::STATUS_DEACTIVE;
        $input['with_expired_date'] = array_key_exists('with_expired_date', $input) && $input['with_expired_date'] == 'on' ? Product::EXPIRED_DATE_YES : Product::EXPIRED_DATE_NO;

        if (!$id) {
            $input['code'] = array_key_exists('code', $input) ? $input['code'] : Product::nextCodeProduct();
        }

        $redirect = redirect()->back();
        $query = Product::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "code" => 'required|max:255',
                "name" => 'required|max:255',
                "category_id" => 'required',
                "brand_id" => 'required',
                "sell_price" => 'numeric|min:0'
            ],
            "messages" => trans("product.errors")
        ]);

        if( $result['status'] )
        {
            $product = $result['object'];
            $product->images = json_encode($product->moveTmpImage());
            $product->skipLog()->save();

            if (!request()->ajax()) {
                $redirect = redirect()->route('master.product.show', $product->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
        }
        else
        {
            if (!request()->ajax()) {
                $redirect->withInput( request()->except(['_token']) )
                            ->withErrors($result['error'])
                            ->with('error', sprintf('%s', trans("global.failed_save")) );
            }
        }

        return request()->ajax() ? response()->json($result) : $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Product::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("product.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("product.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => Product::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            Product::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }

    public function ajaxProduct()
    {
        $params = request()->all();

        $list = Product::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
            $list->orWhereLike("code", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }

    public function ajaxListProduct()
    {
        $query = Product::with('category','brand')->active();

        if (request("q")) { //query
            $_q = Util::get('q');
            $query->where(function($q) use($_q) {
                $q->whereLike('name', $_q)
                    ->orWhereLike('code', $_q);
            });
        }

        if (request("bd")) { //brand_id
            $query->where("brand_id", Util::get('bd'));
        }

        if (request("c")) { //category_id
            $query->where("category_id", Util::get('c'));
        }

        if (request("bc")) { //branch_id
            // $query->where("name", Util::get('bc'));
        }

        $products = $query->paginate(7)->appends( request()->except(["page"]) );
        $data = [];
        foreach ($products as $product) {
            $data[] = [
                'product_id' => $product->id,
                'code' => $product->code,
                'name' => $product->name,
                'brand' => $product->brand->name,
                'category' => $product->category->name,
                'identifier' => $product->category->identifier,
                'information' => $product->information,
                'with_expired_date' => $product->with_expired_date,
                'images' => $product->images_arr(),
                'image' => $product->image(),
                'sell_price' => $product->sell_price,
                'cost_of_goods' => $product->cost_of_goods,
                'purchase_price' => 0,
                'total_amount' => 0,
                'qty' => 0,
                'expired_date' => '',
            ];
        }

        $result = $products->toArray();
        unset($result['data']);
        $result['data'] = $data;

        return response()->json($result);
    }
}
