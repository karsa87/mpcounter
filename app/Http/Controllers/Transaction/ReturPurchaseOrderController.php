<?php

namespace App\Http\Controllers\Transaction;

use App\Models\ReturPurchaseOrderDetail;
use App\Models\PurchaseOrderDetail;
use App\Models\ReturPurchaseOrder;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use App\Models\PurchaseOrder;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Supplier;
use App\Models\Category;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Bank;
use Exception;

class ReturPurchaseOrderController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ReturPurchaseOrder::with([
                        'purchaseOrder', 
                        'supplier', 
                        'branch', 
                        'bank'
                    ]);

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( is_numeric(request('_nrtr')) )
        {
            $query->whereLike('no_retur', Util::get('_nrtr'));
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('branch_id', auth()->user()->branch_id);
        } elseif ( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( is_numeric(request('_sp')) )
        {
            $query->where('supplier_id', Util::get('_sp'));
        }

        if( is_numeric(request('_poid')) )
        {
            $query->where('purchase_order_id', Util::get('_poid'));
        }

        $retur_purchase_orders = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $suppliers = Supplier::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $purchaseOrders = PurchaseOrder::with('bank','branch','supplier')->paid()->get()->pluck('no_invoice','id');

        return Layout::render('transaction.retur_purchase_order.index', [
            "retur_purchase_orders" => $retur_purchase_orders,
            'purchaseOrders' => $purchaseOrders,
            'suppliers' => $suppliers,
            'branchs' => $branchs,
            'banks' => $banks,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $purchaseOrders = PurchaseOrder::with('supplier','bank','branch')->paid()->get();
        $purchaseOrders_form = PurchaseOrder::select('no_invoice as text', 'id')->paid()->get();
        
        return Layout::render('transaction.retur_purchase_order.form', [
            'model' => new ReturPurchaseOrder(),
            'purchaseOrders' => $purchaseOrders,
            'purchaseOrders_form' => $purchaseOrders_form,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->store_update();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $retur_purchase_order = ReturPurchaseOrder::with([
                    'purchaseOrder', 
                    'supplier', 
                    'branch', 
                    'bank'
                ])
                ->whereId($id)
                ->first();

        return Layout::render('transaction.retur_purchase_order.show', [
            'model' => $retur_purchase_order,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $retur_purchase_order = ReturPurchaseOrder::with([
            'supplier',
            'branch',
            'bank',
            'purchaseOrder',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    }, 
                    'productIdentity'
                ]);
            },
        ])->whereId($id)->first();

        $purchaseOrders = PurchaseOrder::with('supplier','bank','branch')->paid()->get();
        $purchaseOrders_form = PurchaseOrder::select('no_invoice as text', 'id')->paid()->get();

        $details = [];
        foreach ($retur_purchase_order->details as $detail) {
            $details[] = [
                'detail_id' => $detail->id,
                'purchase_order_detail_id' => $detail->purchase_order_detail_id,
                'product_id' => $detail->product_id,
                'image' => $detail->product->image(),
                'name' => $detail->product->name,
                'qty' => $detail->qty,
                'qty_ori' => $detail->qty,
                'purchase_price' => $detail->purchase_price,
                'sell_price' => $detail->product->sell_price,
                'cost_of_goods' => $detail->cost_of_goods,
                'total_amount' => $detail->total_amount,
                // 'is_retur' => $detail->is_retur,
                'product_identity_id' => $detail->product_identity_id,
                'identity' => $detail->productIdentity->identity,
                'stock' => $detail->productIdentity->stock,
                'brand' => $detail->product->brand->name,
                'category' => $detail->product->category->name,
                'identifier' => $detail->product->category->identifier,
            ];
        }
        
        return Layout::render('transaction.retur_purchase_order.form', [
            'model' => $retur_purchase_order,
            'details' => $details,
            'purchaseOrders' => $purchaseOrders,
            'purchaseOrders_form' => $purchaseOrders_form,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $redirect = redirect()->back();
        $query = ReturPurchaseOrder::query();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        try {
            $input = request()->except(["_token"]);
            $input['images'] = $input['images'] ?: '[]';
            
            if (!array_key_exists('products', $input)) {
                $result['status'] = false;
                $result['error']->add('products', trans('retur_purchase_order.message.error_detail'));
                throw new Exception();
            }
            
            // validate 
            $input['total_amount'] = array_sum(array_map(function($item) { 
                return $item['qty'] * $item['purchase_price']; 
            }, $input['products']));

            if(!$result['status']) {
                throw new Exception();
            }
            
            if (!$id) {
                $input['no_retur'] = ReturPurchaseOrder::nextNoReturRPO();
            }

            $result = $this->save($query, $input,[
                "rules" => [
                    "date" => 'required|date_format:Y-m-d H:i',
                    "bank_id" => 'required',
                    "supplier_id" => 'required',
                    "branch_id" => 'required',
                    "purchase_order_id" => 'required',
                    "products" => 'required',
                ],
                "messages" => trans("retur_purchase_order.errors"),
                "remove" => ['products'],
            ]);

            if ($result['status']) {
                $rpo = $result['object'];
                
                $product_ids = [];
                $details_id = [];
                $total_po = 0;
                foreach ($input['products'] as $p) {
                    $p = (object) $p;
                    $rpo_detail = null;
                    if ($p->detail_id) {
                        $rpo_detail = $rpo->details()->find($p->detail_id);
                    } else {
                        //find detail
                        $rpo_detail = new ReturPurchaseOrderDetail();
                        $rpo_detail->retur_purchase_order_id = $rpo->id;
                        $rpo_detail->product_id = $p->product_id;
                    }
                    
                    $product_identity = ProductIdentity::find($p->product_identity_id);
                    
                    // create / edit product identity
                    $product_identity->identity = $p->identity;
                    if (!empty($p->detail_id)) { //edit
                        if ( $product_identity->id != $rpo_detail->product_identity_id ) {
                            $rpo_detail->productIdentity->stock += $rpo_detail->qty;
                            $rpo_detail->productIdentity->save();
                            $product_identity->stock -= $rpo_detail->qty;
                        } else {
                            $product_identity->stock += $rpo_detail->qty;
                            $product_identity->stock -= $p->qty;
                        }
                    } else { // create
                        $product_identity->stock -= $p->qty;
                    }

                    $product_identity->save();

                    // create / edit detail purchase order
                    $total_amount = $p->qty * $p->purchase_price;
                    $rpo_detail->qty = $p->qty;
                    $rpo_detail->purchase_order_detail_id = $p->purchase_order_detail_id;
                    $rpo_detail->product_id = $p->product_id;
                    $rpo_detail->product_identity_id = $product_identity->id;
                    $rpo_detail->purchase_price = $p->purchase_price;
                    // $rpo_detail->sell_price = $p->sell_price;
                    $rpo_detail->cost_of_goods = $p->cost_of_goods;
                    $rpo_detail->total_amount = $total_amount;
                    $rpo_detail->save();

                    $rpo_detail->purchaseOrderDetail->is_retur = PurchaseOrderDetail::RETUR_YES;
                    $rpo_detail->purchaseOrderDetail->save();

                    $total_po += $total_amount;

                    $details_id[] = $rpo_detail->id;
                    $product_ids[] = $rpo_detail->product_id;
                }

                //delete purchase order detail
                $delete_details = $rpo->details()
                                    ->with('productIdentity')
                                    ->whereNotIn('id', $details_id)
                                    ->get();
                
                if ($delete_details) {
                    foreach ($delete_details as $detail) {
                        $detail->productIdentity->stock += $detail->qty;
                        $detail->productIdentity->save();
                        
                        $this->delete($detail, 'hard');
                    }
                }

                if( $result['status'] ) {
                    \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);
                }
            }

            if( $result['status'] )
            {
                $rpo = $result['object'];
                $rpo->images = json_encode($rpo->moveTmpImage());
                $rpo->skipLog()->save();
                
                $rpo->debit($rpo->bank, (float) $rpo->total_amount, sprintf('%s - %s', trans("retur_purchase_order.title"), $rpo->no_retur));
                \DB::commit();

                $redirect = redirect()->route('transaction.retur.purchase.order.show', $rpo->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception $e) {
            \DB::rollback();
            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = ReturPurchaseOrder::with([
            'details' => function($q) {
                $q->with('productIdentity', 'purchaseOrderDetail');
            },
            'bank'
        ])->whereId($id)->first();
        $name = $row->no_invoice;
            
        $product_ids = [];
        foreach ($row->details as $detail) {
            $product_ids[] = $detail->product_id;
            $detail->productIdentity->stock += $detail->qty;
            $detail->productIdentity->save();

            $detail->purchaseOrderDetail->is_retur = PurchaseOrderDetail::RETUR_NO;
            $detail->purchaseOrderDetail->save();
            
            $this->delete($detail, 'soft');
        }

        $row->credit($row->bank, (float) $row->total_amount, sprintf('%s - %s', trans("retur_purchase_order.title"), $row->no_retur));
        
        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("retur_purchase_order.title"), $name) );
            
            \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("retur_purchase_order.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => ReturPurchaseOrder::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            ReturPurchaseOrder::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }
}
