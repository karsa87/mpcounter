<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\SalesOrderDetail;
use App\Models\ProductIdentity;
use Illuminate\Http\Request;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Employee;
use App\Models\Setting;
use App\Models\Member;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Bank;
use Exception;

class SalesOrderController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = SalesOrder::with('member', 'bank', 'branch', 'sales', 'retur', 'debtPayments');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_kw'))
        {
            $query->where(function($q){
                $q->whereLike('no_invoice', Util::get('_kw'))
                    ->orWhereLike('name', Util::get('_kw'));
            });
        }

        if( request('_nivc') )
        {
            $query->whereLike('no_invoice', Util::get('_nivc'));
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('branch_id', auth()->user()->branch_id);
        } elseif ( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( is_numeric(request('_sls')) )
        {
            $query->where('sales_id', Util::get('_sls'));
        }

        if( is_numeric(request('_mb')) )
        {
            $query->where('member_id', Util::get('_mb'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( is_numeric(request('_tp')) )
        {
            $query->type(Util::get('_tp'));
        }

        $sales_orders = $query->paginate(Layout::ROW_PER_PAGE)
                            ->appends( request()->except(["page"]) );

        $sales = Employee::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $members = Member::select('id')->selectRaw("CONCAT(name, ' - ', phone) as text")
                    ->active()->get()->pluck('text','id');

        return Layout::render('transaction.sales_order.index', [
            "sales_orders" => $sales_orders,
            'sales' => $sales,
            'branchs' => $branchs,
            'banks' => $banks,
            'members' => $members,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sales = Employee::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $branchs = Branch::select('name as text', 'id')->active()->get();
        $members = Member::select('name', 'phone', 'address', 'type', 'poin', 'id')
                    ->selectRaw("CONCAT(name, ' - ', phone) as text")
                    ->active()->get();
        $brands = Brand::select('name as text', 'id')->active()->get();
        $categories = Category::select('name as text', 'id')->active()->get();
        $convertion_point_rupiah = Setting::where('key', 'CONVERTION_MEMBER_POINT_TO_RUPIAH')->first()->value;
        $convertion_rupiah_point = Setting::where('key', 'CONVERTION_TOTAL_BUYING_TO_MEMBER_POINT')->first()->value;
        
        return Layout::render('transaction.sales_order.form', [
            'model' => new SalesOrder(),
            'sales' => $sales,
            'branchs' => $branchs,
            'banks' => $banks,
            'members' => $members,
            'categories' => $categories,
            'brands' => $brands,
            'convertion_point_rupiah' => $convertion_point_rupiah,
            'convertion_rupiah_point' => $convertion_rupiah_point,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->store_update();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales_order = SalesOrder::with([
            'member',
            'bank',
            'branch',
            'promo',
            'sales',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    },
                    'productIdentity'
                ]);
            },
            'retur',
            'debtPayments'
        ])->whereId($id)->first();

        $sales_order->date = Util::formatDate($sales_order->date, 'Y-m-d H:i');
        $images = $sales_order->images_info();

        return Layout::render('transaction.sales_order.show', [
            'model' => $sales_order,
            'images' => $images,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales_order = SalesOrder::with([
            'member',
            'bank',
            'branch',
            'promo',
            'sales',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    },
                    'productIdentity'
                ]);
            },
            'retur'
        ])->whereId($id)->first();
        
        if ($sales_order->retur->count() > 0) {
            return redirect()->back()->with('error', trans("sales_order.message.error_cant_edit"));
        }
        
        $details = [];
        foreach ($sales_order->details as $detail) {
            $v = [
                "detail_id" => $detail->id,
                "identifier" => $detail->product->category->identifier,
                "identity" => $detail->productIdentity->identity,
                "qty" => $detail->qty,
                "qty_ori" => $detail->qty,
                "total_amount" => $detail->total_amount_first,
                "product_id" => $detail->product_id,
                "cost_of_goods" => $detail->cost_of_goods,
                "image" => $detail->product->image(),
                "brand" => $detail->product->brand->name,
                "category" => $detail->product->category->name,
                "name" => $detail->product->name,
                "sales_price" => $detail->sales_price,
                "sell_price" => $detail->sell_price,
                "general_sell_price" => $detail->sell_price,
                "reseller1_sell_price" => $detail->product->reseller1_sell_price,
                "reseller2_sell_price" => $detail->product->reseller2_sell_price,
                "sales_price_format" => Util::format_currency($detail->sales_price),
                "discount_promo" => $detail->discount_promo,
                "discount" => $detail->discount_promo ? ($detail->discount_promo / $detail->qty) : 0,
                "promo_id" => $detail->promo_id,
                "product_identity_id" => $detail->product_identity_id,
                "id" => $detail->product_identity_id,
            ];

            $member = $sales_order->member;
            if ($member) {
                if ($member->isResellerOne()) {
                    $v['sell_price'] = $v['reseller1_sell_price'];
                    $v['sell_price'] = $v['sell_price'] != "0" ? $v['sell_price'] : $v["general_sell_price"];
                } elseif ($member->isResellerTwo()) {
                    $v['sell_price'] = $v["reseller2_sell_price"];
                    $v['sell_price'] = $v['sell_price'] != "0" ? $v['sell_price'] : $v['reseller1_sell_price'];
                    $v['sell_price'] = $v['sell_price'] != "0" ? $v['sell_price'] : $v["general_sell_price"];
                } else {
                    $v['sell_price'] = $v['general_sell_price'];
                }
            } else {
                $v['sell_price'] = $v['general_sell_price'];
            }
            
            $details[] = $v;
        }

        $sales_order->date = Util::formatDate($sales_order->date, 'Y-m-d H:i');
        $images = $sales_order->images_info();
        
        $sales = Employee::active()->get()->pluck('name','id');
        $branchs = Branch::select('name as text', 'id')->active()->get();
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $members = Member::select('name', 'phone', 'address', 'type', 'poin', 'id')
                    ->selectRaw("CONCAT(name, ' - ', phone) as text")
                    ->active()->get();
        $categories = Category::select('name as text', 'id')->active()->get();
        $brands = Brand::select('name as text', 'id')->active()->get();
        $convertion_point_rupiah = Setting::where('key', 'CONVERTION_MEMBER_POINT_TO_RUPIAH')->first()->value;
        $convertion_rupiah_point = Setting::where('key', 'CONVERTION_TOTAL_BUYING_TO_MEMBER_POINT')->first()->value;

        return Layout::render('transaction.sales_order.form', [
            'model' => $sales_order,
            'images' => $images,
            'sales' => $sales,
            'branchs' => $branchs,
            'banks' => $banks,
            'members' => $members,
            'categories' => $categories,
            'brands' => $brands,
            'details' => $details,
            'convertion_point_rupiah' => $convertion_point_rupiah,
            'convertion_rupiah_point' => $convertion_rupiah_point,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $convertion_point_rupiah = Setting::where('key', 'CONVERTION_MEMBER_POINT_TO_RUPIAH')->first()->value;

        $redirect = redirect()->back();
        $query = SalesOrder::query();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        try {
            $input = request()->except(["_token"]);
            
            //default bank
            if (!empty(config('default.bank_id')) && $input['type'] != SalesOrder::TYPE_TRANSFER) {
                $input['bank_id'] = config('default.bank_id');
            }

            $input['use_point'] = array_key_exists('use_point', $input) && $input['use_point'] == "on" ? SalesOrder::USE_POINT : SalesOrder::NOT_USE_POINT;
            $input['is_ppn'] = array_key_exists('is_ppn', $input) && $input['is_ppn'] == "on" ? SalesOrder::PPN_ENABLE : SalesOrder::PPN_DISABLE;
            $input['images'] = array_key_exists('images', $input) ? ($input['images'] ?: '[]') : '[]';
            $input['status'] = SalesOrder::STATUS_NOT_PAID;
            $input['down_payment'] = Util::remove_format_currency($input['down_payment']);
            $input['discount_sales'] = array_key_exists('discount_sales', $input) ? Util::remove_format_currency($input['discount_sales']) : 0;
            $input['shipping_cost'] = Util::remove_format_currency($input['shipping_cost']);
            $input['additional_cost'] = Util::remove_format_currency($input['additional_cost']);
            $input['point_member'] = array_key_exists('point_member', $input) ? Util::remove_format_currency($input['point_member']) : 0;
            $input['tax_bank'] = array_key_exists('tax_bank', $input) ? Util::remove_format_currency($input['tax_bank']) : 0;
            if (!array_key_exists('products', $input)) {
                $result['status'] = false;
                $result['error']->add('products', trans('sales_order.message.error_detail'));
                throw new Exception();
            }
            
            // check discount point member
            if ($input['use_point'] == SalesOrder::NOT_USE_POINT) {
                $input['point_member'] = 0;
            } elseif ($input['point_member'] <= 0) {
                $input['use_point'] = SalesOrder::NOT_USE_POINT;
            }
            
            $input['discount_poin_member'] = 0;
            if ($input['use_point'] == SalesOrder::USE_POINT) {
                $input['discount_poin_member'] = $input['point_member'] * $convertion_point_rupiah;
            }

            // validate 
            $input['total_amount'] = 0;
            $input['total_amount_first'] = 0;
            $input['total_discount_detail'] = 0;
            foreach ($input['products'] as $item) {
                $input['total_amount_first'] += $item['qty'] * $item['sell_price'];
                $dicount = isset($item['discount_promo']) ? $item['discount_promo'] : 0;
                $input['total_discount_detail'] += Util::remove_format_currency($dicount);
            }

            $input['total_amount'] = $input['total_amount_first'] + $input['shipping_cost'] + $input['additional_cost'] - $input['discount_sales'] - $input['total_discount_detail'] - $input['discount_poin_member'];

            $input['total_amount'] += ($input['total_amount'] * ($input['tax_bank'] / 100));
            
            if (SalesOrder::TYPE_CASH == $input["type"] && $input['total_amount'] > $input['down_payment']) {
                $result['status'] = false;
                $result['error']->add('down_payment', sprintf(trans('sales_order.message.error_less_down_payment'), Util::format_currency(($input['total_amount'] - $input['down_payment']))));
            } elseif ($input['down_payment'] >= $input['total_amount']) {
                // $input['down_payment'] = $input['total_amount'];
                $input['status'] = SalesOrder::STATUS_PAID;
            }
            $input['paid_off'] = $input['down_payment'] >= $input['total_amount'] ? $input['total_amount'] : $input['down_payment'];

            if(!$result['status']) {
                throw new Exception();
            }

            if (!$id) {
                $input['no_invoice'] = SalesOrder::nextNoInvoice();
            }

            if ($input['type'] != SalesOrder::TYPE_DEBT) {
                $input['date_max_payable'] = null;
            }

            if (auth()->user()->isEmployee()) {
                $input['branch_id'] = auth()->user()->branch_id;
            }
            
            $result = $this->save($query, $input,[
                "rules" => [
                    "date" => 'required|date_format:Y-m-d H:i',
                    "bank_id" => 'required',
                    "sales_id" => 'nullable',
                    "branch_id" => $id ? '' : 'required',
                    "down_payment" => 'numeric',
                    "products" => 'required',
                    "phone" => [
                        'nullable',
                        'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                        'not_regex:/[a-zA-Z]/',
                        'max:16'
                    ],
                    "address" => 'nullable',
                    "date_max_payable" => 'required_if:type,'.SalesOrder::TYPE_DEBT,
                    'tax_bank' => 'numeric'
                ],
                "messages" => trans("sales_order.errors"),
                "remove" => ['products'],
            ]);

            if ($result['status']) {
                $so = $result['object'];
                
                $product_ids = [];
                $details_id = [];
                $total_po = 0;
                foreach ($input['products'] as $p) {
                    $p = (object) $p;
                    if ($p->detail_id) {
                        $so_detail = $so->details()->with('productIdentity')->whereId($p->detail_id)->first();
                        $product_identity = $so_detail->productIdentity;
                    } else {
                        //find product identity
                        $product_identity = ProductIdentity::find($p->product_identity_id);

                        //find detail
                        $so_detail = new SalesOrderDetail();
                        $so_detail->sales_order_id = $so->id;
                        $so_detail->product_id = $p->product_id;
                    }

                    // create / edit product identity
                    if (!empty($p->detail_id)) {
                        $product_identity->stock += $so_detail->qty;
                        $product_identity->stock -= $p->qty;
                    } else {
                        $product_identity->stock -= $p->qty;
                    }
                    $product_identity->save();
                    
                    // create / edit detail sales order
                    $total_amount_first = $p->qty * $p->sell_price;
                    $so_detail->qty = $p->qty;
                    $so_detail->product_identity_id = $p->product_identity_id;
                    $so_detail->sell_price = $p->sell_price;
                    $so_detail->cost_of_goods = $p->cost_of_goods;
                    $so_detail->total_amount_first = $total_amount_first;
                    $so_detail->discount_promo = Util::remove_format_currency(isset($p->discount_promo) ? $p->discount_promo : 0);
                    $so_detail->promo_id = isset($p->promo_id) ? $p->promo_id : null;
                    $so_detail->total_amount = $so_detail->total_amount_first - $so_detail->discount_promo;
                    $so_detail->save();

                    $details_id[] = $so_detail->id;
                    $product_ids[] = $so_detail->product_id;
                }
                
                //delete sales order detail
                $delete_details = $so->details()->with('productIdentity')->whereNotIn('id', $details_id)->get();
                
                if ($delete_details) {
                    foreach ($delete_details as $detail) {
                        $detail->productIdentity->stock += $detail->qty;
                        $detail->productIdentity->save();

                        $this->delete($detail, 'hard');
                    }
                }

                if( $result['status'] ) {
                    \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);
                }
            }

            if( $result['status'] )
            {
                $so = $result['object'];
                $so->images = json_encode($so->moveTmpImage());
                $so->skipLog()->save();
                
                $so->debit($so->bank, (float) $so->paid_off, sprintf('%s - %s', trans("sales_order.title"), $so->no_invoice));
                \DB::commit();

                $redirect = redirect()->route('transaction.sales.order.show', $so->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception $e) {
            \DB::rollback();
            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = SalesOrder::with([
            'details' => function($q) {
                $q->with('productIdentity');
            },
            'bank',
            'member'
        ])->whereId($id)->first();
        $name = $row->no_invoice;
            
        $product_ids = [];
        foreach ($row->details as $detail) {
            $product_ids[] = $detail->product_id;
            $detail->productIdentity->stock += $detail->qty;
            $detail->productIdentity->save();

            $this->delete($detail, 'soft');
        }

        // divide member poin
        $member = $row->member;
        $use_point = $row->use_point;
        $total_amount = $row->total_amount;

        $row->credit($row->bank, (float) $row->paid_off, sprintf('%s - %s', trans("sales_order.title"), $row->no_invoice));
        
        $result = $this->delete($row, 'soft');
        if($result["status"]){
            if ($member && $use_point == SalesOrder::USE_POINT) {
                $convertion_rupiah_point = Setting::where('key', 'CONVERTION_TOTAL_BUYING_TO_MEMBER_POINT')->first()->value;

                $point = floor($total_amount / $convertion_rupiah_point);
                $member->poin += $point;
                $member->save();
            }

            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("sales_order.title"), $name) );

            \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("sales_order.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => SalesOrder::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            SalesOrder::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }

    public function printSmall($id)
    {
        $header = Setting::where('key', 'SALES_ORDER_HEADER_SMALL_NOTE')->first()->value;
        $footer = Setting::where('key', 'SALES_ORDER_FOOTER_SMALL_NOTE')->first()->value;

        $so = SalesOrder::with([
            'details' => function($q) {
                $q->with('productIdentity');
            }
        ])->whereId($id)->first();
        
        $view = Layout::render('transaction.sales_order.small_note', [
            'model' => $so,
            'header'=>$header,
            'footer'=>$footer
        ])->render();

        $pdf = \App::make('dompdf.wrapper');    
        $pdf->loadHtml($view);
        $pdf->setPaper([0, 0, 127.559, 566.929], 'potrait');
        
        $filename = "Sales Order - $so->no_invoice.pdf";
        return $pdf->stream($filename,["Attachment"=>FALSE]);
    }

    public function ajaxDetailSO($id)
    {
        $details = SalesOrderDetail::with([
                    'product' => function($q) {
                        $q->with('category', 'brand');
                    }, 
                    'productIdentity',
                    'returDetail'
                ])
                ->where('sales_order_id', $id)
                ->get();

        $result = [];
        foreach ($details as $detail) {
            $result[] = [
                'sales_order_detail_id' => $detail->id,
                'product_id' => $detail->product_id,
                'image' => $detail->product->image(),
                'name' => $detail->product->name,
                'qty' => $detail->qty - $detail->returDetail->sum('qty'),
                'sell_price' => $detail->sell_price,
                'cost_of_goods' => $detail->cost_of_goods,
                'total_amount' => $detail->total_amount,
                'is_retur' => $detail->is_retur,
                'product_identity_id' => $detail->product_identity_id,
                'identity' => $detail->productIdentity->identity,
                'stock' => $detail->productIdentity->stock,
                'brand' => $detail->product->brand->name,
                'category' => $detail->product->category->name,
                'identifier' => $detail->product->category->identifier,
                'promo_id' => $detail->promo_id,
                'promo' => [
                    'discount' => $detail->promo ? $detail->promo->calculateDiscount($detail->sell_price) : 0
                ],
                'discount_promo' => $detail->discount_promo,
                'total_amount_first' => $detail->total_amount_first,
                'discount' => $detail->discount,
                'discount_unit_per_item' => $detail->discount / $detail->qty,
                'total_discount' => $detail->total_discount,
            ];
        }

        return response()->json($result);
    }

    public function ajaxSalesOrder($id = '')
    {
        $query = SalesOrder::with([]);

        if ($id) {
            $query->where('id', $id);
        }

        if (request("q")) {
            $_q = Util::get('q');
            $query->where('no_invoice', 'ilike', "%$_q%");
        }

        $salesOrders = $query->limit(20)->get();

        $list = [];
        foreach ($salesOrders as $so) {
            $v = [
                'id' => $so->id,
                'text' => sprintf('%s %s', $so->no_invoice, $so->memberName),
            ];

            if ($id) {
                $v['no_invoice'] = $so->no_invoice;
                $v['name'] = $so->name;
                $v['phone'] = $so->phone;
                $v['address'] = $so->address;
                $v['address'] = $so->address;
                $v['branch'] = $so->branch->toArray();
                $v['bank'] = $so->bank->toArray();
            }

            $list[] = $v;
        }

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
