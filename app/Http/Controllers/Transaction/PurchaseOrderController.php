<?php

namespace App\Http\Controllers\Transaction;

use App\Models\PurchaseOrderDetail;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use App\Models\PurchaseOrder;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Supplier;
use App\Models\Category;
use App\Models\Product;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Bank;
use Exception;

class PurchaseOrderController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = PurchaseOrder::with('supplier', 'bank', 'branch', 'retur', 'debtPayments');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nivc') )
        {
            $query->whereLike('no_invoice', Util::get('_nivc'));
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('branch_id', auth()->user()->branch_id);
        } elseif ( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( is_numeric(request('_sp')) )
        {
            $query->where('supplier_id', Util::get('_sp'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( is_numeric(request('_tp')) )
        {
            $query->type(Util::get('_tp'));
        }

        $purchase_orders = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $suppliers = Supplier::active()->get()->pluck('name','id');

        $branchs = Branch::active();
        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $branchs->where('id', auth()->user()->branch_id);
        }
        $branchs = $branchs->get()->pluck('name','id');

        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        return Layout::render('transaction.purchase_order.index', [
            "purchase_orders" => $purchase_orders,
            'suppliers' => $suppliers,
            'branchs' => $branchs,
            'banks' => $banks,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $branchs = Branch::active();
        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $branchs->where('id', auth()->user()->branch_id);
        }
        $branchs = $branchs->get()->pluck('name','id');
        $categories = Category::select('name as text', 'id')->active()->get();
        $brands = Brand::select('name as text', 'id')->active()->get();
        
        return Layout::render('transaction.purchase_order.form', [
            'model' => new PurchaseOrder(),
            'suppliers' => $suppliers,
            'branchs' => $branchs,
            'banks' => $banks,
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->store_update();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase_order = PurchaseOrder::with([
            'supplier',
            'bank',
            'branch',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    },
                    'productIdentity'
                ]);
            },
            'retur',
            'debtPayments'
        ])->whereId($id)->first();

        $purchase_order->date = Util::formatDate($purchase_order->date, 'Y-m-d H:i');

        $images = $purchase_order->images_info();

        return Layout::render('transaction.purchase_order.show', [
            'model' => $purchase_order,
            'images' => $images,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::active()->get()->pluck('name','id');
        $branchs = Branch::active();
        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $branchs->where('id', auth()->user()->branch_id);
        }
        $branchs = $branchs->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        $purchase_order = PurchaseOrder::with([
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    },
                    'productIdentity'
                ]);
            },
            'retur'
        ])->whereId($id)->first();
        
        if ($purchase_order->retur->count() > 0) {
            return redirect()->back()->with('error', trans("purchase_order.message.error_cant_edit"));
        }
        
        $details = [];
        foreach ($purchase_order->details as $detail) {
            $details[] = [
                "detail_id" => $detail->id,
                "identifier" => $detail->product->category->identifier,
                "identity" => $detail->productIdentity->identity,
                "qty" => $detail->qty,
                "total_amount" => $detail->total_amount,
                "product_id" => $detail->product_id,
                "sell_price" => $detail->sell_price,
                "cost_of_goods" => $detail->cost_of_goods,
                "image" => $detail->product->image(),
                "brand" => $detail->product->brand->name,
                "category" => $detail->product->category->name,
                "name" => $detail->product->name,
                "purchase_price" => $detail->purchase_price,
                "purchase_price_format" => Util::format_currency($detail->purchase_price),
                "with_expired_date" => $detail->product->with_expired_date,
                "expired_date" => Util::formatDate($detail->productIdentity->expired_date, 'Y-m-d'),
            ];
        }

        $purchase_order->date = Util::formatDate($purchase_order->date, 'Y-m-d H:i');

        $images = $purchase_order->images_info();

        $categories = Category::select('name as text', 'id')->active()->get();
        $brands = Brand::select('name as text', 'id')->active()->get();
        
        return Layout::render('transaction.purchase_order.form', [
            'model' => $purchase_order,
            'images' => $images,
            'suppliers' => $suppliers,
            'branchs' => $branchs,
            'banks' => $banks,
            'categories' => $categories,
            'brands' => $brands,
            'details' => $details
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $redirect = redirect()->back();
        $query = PurchaseOrder::query();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        try {
            $input = request()->except(["_token"]);
            
            //default bank
            if (!empty(config('default.bank_id')) && $input['type'] != PurchaseOrder::TYPE_TRANSFER) {
                $input['bank_id'] = config('default.bank_id');
            }

            $input['images'] = $input['images'] ?: '[]';
            $input['status'] = PurchaseOrder::STATUS_NOT_PAID;
            $input['down_payment'] = Util::remove_format_currency($input['down_payment']);
            if (!array_key_exists('products', $input)) {
                $result['status'] = false;
                $result['error']->add('products', trans('purchase_order.message.error_detail'));
                throw new Exception();
            }
            
            // validate 
            $input['total_amount'] = array_sum(array_map(function($item) { 
                return $item['qty'] * $item['purchase_price']; 
            }, $input['products']));

            if (PurchaseOrder::TYPE_CASH == $input["type"] && $input['total_amount'] > $input['down_payment']) {
                $result['status'] = false;
                $result['error']->add('down_payment', sprintf(trans('purchase_order.message.error_less_down_payment'), Util::format_currency(($input['total_amount'] - $input['down_payment']))));
            } elseif ($input['down_payment'] >= $input['total_amount']) {
                // $input['down_payment'] = $input['total_amount'];
                $input['status'] = PurchaseOrder::STATUS_PAID;
            }
            $input['paid_off'] = $input['down_payment'] >= $input['total_amount'] ? $input['total_amount'] : $input['down_payment'];

            if(!$result['status']) {
                throw new Exception();
            }

            if (!$id) {
                $input['no_invoice'] = PurchaseOrder::nextNoInvoice();
            }

            if ($input['type'] != PurchaseOrder::TYPE_DEBT) {
                $input['date_max_payable'] = null;
            }

            $result = $this->save($query, $input,[
                "rules" => [
                    "date" => 'required|date_format:Y-m-d H:i',
                    "bank_id" => 'required',
                    "supplier_id" => 'required',
                    "branch_id" => $id ? '' : 'required',
                    "down_payment" => 'numeric',
                    "products" => 'required',
                    "date_max_payable" => 'required_if:type,'.PurchaseOrder::TYPE_DEBT
                ],
                "messages" => trans("purchase_order.errors"),
                "remove" => ['products'],
            ]);

            if ($result['status']) {
                $po = $result['object'];
                
                $product_ids = [];
                $details_id = [];
                $total_po = 0;
                foreach ($input['products'] as $p) {
                    $p = (object) $p;
                    $po_detail = null;
                    if ($p->detail_id) {
                        $po_detail = $po->details()->find($p->detail_id);
                        // $product_identity = $po_detail->productIdentity;
                    } else {
                        //find detail
                        $po_detail = new PurchaseOrderDetail();
                        $po_detail->purchase_order_id = $po->id;
                        $po_detail->product_id = $p->product_id;
                        // $po_detail->product_identity_id = $product_identity->id;
                    }

                    //find product identity
                    $where_product_identity = [
                        'branch_id' => $po->branch_id,
                        'product_id' => $p->product_id,
                    ];

                    if ($p->identifier != Category::IDENTIFIER_NO) {
                        $where_product_identity['identity'] = $p->identity;
                    }
                    $product_identity = ProductIdentity::firstOrNew($where_product_identity);
                    
                    // create / edit product identity
                    $product_identity->identity = $p->identity;
                    if (!empty($p->detail_id)) { //edit
                        if ( $product_identity->id != $po_detail->product_identity_id ) {
                            $po_detail->productIdentity->stock -= $po_detail->qty;
                            $po_detail->productIdentity->save();
                            $product_identity->stock += $po_detail->qty;
                        } else {
                            $product_identity->stock -= $po_detail->qty;
                            $product_identity->stock += $p->qty;
                        }
                    } else { // create
                        $product_identity->stock += $p->qty;
                    }

                    if ($p->with_expired_date == Product::EXPIRED_DATE_YES) {
                        $product_identity->expired_date = $p->expired_date;
                    }

                    // check duplicate identifier
                    if ($product_identity->id && $p->identifier == Category::IDENTIFIER_ONE
                        && ( 
                            $product_identity->stock > 1 || (
                                $po_detail->product_identity_id != $product_identity->id 
                                    && $product_identity->getOriginal('stock') > 0
                            )
                        )) {
                        $result['status'] = false;
                        $result['error']->add('product_identity',  sprintf('%s %s: %s', trans('purchase_order.message.error_duplicate_identity'), $p->name, $p->identity));
                    } else {
                        $product_identity->save();
                    }

                    // create / edit detail purchase order
                    $total_amount = $p->qty * $p->purchase_price;
                    $po_detail->qty = $p->qty;
                    $po_detail->product_identity_id = $product_identity->id;
                    $po_detail->purchase_price = $p->purchase_price;
                    $po_detail->sell_price = $p->sell_price;
                    $po_detail->cost_of_goods = $p->cost_of_goods;
                    $po_detail->total_amount = $total_amount;
                    $po_detail->save();

                    $total_po += $total_amount;

                    $details_id[] = $po_detail->id;
                    $product_ids[] = $po_detail->product_id;
                }

                //delete purchase order detail
                $delete_details = $po->details()->with('productIdentity')->whereNotIn('id', $details_id)->get();
                
                if ($delete_details) {
                    foreach ($delete_details as $detail) {
                        $detail->productIdentity->stock -= $detail->qty;
                        $detail->productIdentity->save();
                        

                        $this->delete($detail, 'hard');
                    }
                }

                if( $result['status'] ) {
                    \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);
                }
            }

            if( $result['status'] )
            {
                $po = $result['object'];
                $po->images = json_encode($po->moveTmpImage());
                $po->skipLog()->save();
                
                $po->credit($po->bank, (float) $po->paid_off, sprintf('%s - %s', trans("purchase_order.title"), $po->no_invoice));
                \DB::commit();

                $redirect = redirect()->route('transaction.purchase.order.show', $po->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception $e) {
            \DB::rollback();
            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = PurchaseOrder::with([
            'details' => function($q) {
                $q->with('productIdentity');
            },
            'bank'
        ])->whereId($id)->first();
        $name = $row->no_invoice;
        
        $product_ids = [];
        foreach ($row->details as $detail) {
            $product_ids[] = $detail->product_id;
            $detail->productIdentity->stock -= $detail->qty;
            $detail->productIdentity->save();

            $this->delete($detail, 'soft');
        }

        $row->debit($row->bank, (float) $row->paid_off, sprintf('%s - %s', trans("purchase_order.title"), $row->no_invoice));
        
        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("purchase_order.title"), $name) );
            
            \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("purchase_order.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => PurchaseOrder::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            PurchaseOrder::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }

    public function ajaxDetailPO($id)
    {
        $details = PurchaseOrderDetail::with([
                    'product' => function($q) {
                        $q->with('category', 'brand');
                    }, 
                    'productIdentity',
                    'returDetail'
                ])
                ->where('purchase_order_id', $id)
                ->get();

        $result = [];
        foreach ($details as $detail) {
            $result[] = [
                'purchase_order_detail_id' => $detail->id,
                'product_id' => $detail->product_id,
                'image' => $detail->product->image(),
                'name' => $detail->product->name,
                'qty' => $detail->qty - $detail->returDetail->sum('qty'),
                'purchase_price' => $detail->purchase_price,
                'sell_price' => $detail->sell_price,
                'cost_of_goods' => $detail->cost_of_goods,
                'total_amount' => $detail->total_amount,
                'is_retur' => $detail->is_retur,
                'product_identity_id' => $detail->product_identity_id,
                'identity' => $detail->productIdentity->identity,
                'stock' => $detail->productIdentity->stock,
                'brand' => $detail->product->brand->name,
                'category' => $detail->product->category->name,
                'identifier' => $detail->product->category->identifier,
            ];
        }

        return response()->json($result);
    }
}
