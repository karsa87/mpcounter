<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Validation\Rule;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Branch;
use App\Models\Pulse;
use App\Models\Bank;

class PulseController extends CoreController
{
    public function index()
    {
        $query = Pulse::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_k') )
        {
            $keyword = Util::get('_k');
            $query->where(function($q) use ($keyword) {
                $q->whereLike('no_transaction', $keyword)
                    ->orWhereLike('phone', $keyword);
            });
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);    
            
            $query->whereBetween('date', $date);
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('branch_id', auth()->user()->branch_id);
        } elseif ( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( request('_bk') )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        $pulses = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        $branchs = Branch::active();
        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $branchs->where('id', auth()->user()->branch_id);
        }
        $branchs = $branchs->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        return Layout::render('transaction.pulse.index', [
            "pulses" => $pulses,
            "branchs" => $branchs,
            "banks" => $banks,
        ]);
    }

    public function edit($id)
    {
        $pulse = Pulse::with('branch', 'bank')->whereId($id)->first();
        $pulse = $pulse ? $pulse->toArray() : [];
        $pulse['date'] = date('Y-m-d H:i', strtotime($pulse['date']));

        return response()->json([
            'url_edit' => route('transaction.pulse.update', $id),
            'data' => $pulse
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $input = request()->except(["_token"]);
        $input['total_pulse'] = Util::remove_format_currency($input['total_pulse']);
        $input['sell_price'] = Util::remove_format_currency($input['sell_price']);
        $input['capital_price'] = Util::remove_format_currency($input['capital_price']);

        if (empty($id)) {
            $input['no_transaction'] = Pulse::nextNoTransaction();
        }
            
        //default bank
        if (!empty(config('default.bank_id'))) {
            $input['bank_id'] = config('default.bank_id');
        }

        $redirect = redirect()->back();
        $query = Pulse::query();

        $result = $this->save($query, $input,[
            "rules" => [
                'no_transaction' => empty($id) ? 'required' : '',
                "date" => 'required|date_format:Y-m-d H:i',
                "bank_id" => 'required',
                "branch_id" => 'required',
                "phone" => [
                    'nullable',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                'total_pulse' => 'numeric|min:5000',
                'sell_price' => 'numeric|min:0',
                'capital_price' => 'numeric|min:0'
            ],
            "messages" => trans("pulse.errors"),
        ]);

        if( $result['status'] )
        {
            $pulse = $result['object'];
            
            $pulse->debit($pulse->bank, (float) $pulse->sell_price, sprintf('%s - %s', trans("pulse.title"), $pulse->no_transaction));
            \DB::commit();

            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            \DB::rollback();
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Pulse::find($id);
        $name = $row->no_transaction;

        $row->credit($row->bank, (float) $row->sell_price, sprintf('%s - %s', trans("pulse.title"), $row->no_transaction));

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("pulse.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("pulse.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }
}
