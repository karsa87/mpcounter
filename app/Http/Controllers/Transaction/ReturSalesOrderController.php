<?php

namespace App\Http\Controllers\Transaction;

use App\Models\ReturSalesOrderDetail;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\SalesOrderDetail;
use App\Models\ReturSalesOrder;
use App\Models\ProductIdentity;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Models\SalesOrder;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Member;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Bank;
use Exception;

class ReturSalesOrderController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ReturSalesOrder::with([
                        'salesOrder', 
                        'branch', 
                        'bank'
                    ]);

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nrtr') )
        {
            $query->whereLike('no_retur', Util::get('_nrtr'));
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('branch_id', auth()->user()->branch_id);
        } elseif ( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( request('_mnm') )
        {
            $name = Util::get('_mnm');
            $query->whereHas('salesOrder', function($q) use($name) {
                $q->whereLike('name', $name)
                    ->orWhereLike('phone', $name)
                    ->orWhereLike('address', $name);
            });
        }

        if( is_numeric(request('_soid')) )
        {
            $no_invoice = Util::get('_soid');
            $query->whereHas('salesOrder', function($q) use($no_invoice) {
                $q->whereLike('no_invoice', $no_invoice);
            });
            // $query->where('sales_order_id', Util::get('_soid'));
        }

        if( request('_kw'))
        {
            $query->where(function($q){
                $q->where('no_retur', 'ilike', "%". Util::get('_kw') ."%")
                    ->orWhere('date',  'ilike', "%". Util::get('_kw') ."%");
            });
        }

        $retur_sales_orders = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $members = Member::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        // $salesOrders = SalesOrder::paid()->get()->pluck('no_invoice','id');

        return Layout::render('transaction.retur_sales_order.index', [
            "retur_sales_orders" => $retur_sales_orders,
            // 'salesOrders' => $salesOrders,
            'members' => $members,
            'branchs' => $branchs,
            'banks' => $banks,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $salesOrders = SalesOrder::with('bank','branch')->paid()->get();
        
        return Layout::render('transaction.retur_sales_order.form', [
            'model' => new ReturSalesOrder(),
            // 'salesOrders' => $salesOrders,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->store_update();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $retur_sales_order = ReturSalesOrder::with([
                    'salesOrder', 
                    'branch', 
                    'bank'
                ])
                ->whereId($id)
                ->first();

        return Layout::render('transaction.retur_sales_order.show', [
            'model' => $retur_sales_order,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $retur_sales_order = ReturSalesOrder::with([
            'branch',
            'bank',
            'salesOrder',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    }, 
                    'productIdentity'
                ]);
            },
        ])->whereId($id)->first();

        // $salesOrders = SalesOrder::with('bank','branch')->paid()->get();

        $details = [];
        foreach ($retur_sales_order->details as $detail) {
            $details[] = [
                'detail_id' => $detail->id,
                'sales_order_detail_id' => $detail->sales_order_detail_id,
                'product_id' => $detail->product_id,
                'image' => $detail->product->image(),
                'name' => $detail->product->name,
                'qty' => $detail->qty,
                'qty_ori' => $detail->qty,
                'sell_price' => $detail->sell_price,
                'sell_price' => $detail->product->sell_price,
                'cost_of_goods' => $detail->cost_of_goods,
                'total_amount' => $detail->total_amount,
                // 'is_retur' => $detail->is_retur,
                'product_identity_id' => $detail->product_identity_id,
                'identity' => $detail->productIdentity->identity,
                'stock' => $detail->productIdentity->stock,
                'brand' => $detail->product->brand->name,
                'category' => $detail->product->category->name,
                'identifier' => $detail->product->category->identifier,
                'promo' => [
                    'discount' => $detail->promo ? $detail->promo->calculateDiscount($detail->sell_price) : 0
                ],
                'discount' => $detail->discount,
                'discount_unit_per_item' => $detail->discount / $detail->qty,
                'total_discount' => $detail->total_discount,
            ];
        }
        
        return Layout::render('transaction.retur_sales_order.form', [
            'model' => $retur_sales_order,
            'details' => $details,
            // 'salesOrders' => $salesOrders,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $redirect = redirect()->back();
        $query = ReturSalesOrder::query();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        try {
            $input = request()->except(["_token"]);
            $input['images'] = $input['images'] ?: '[]';
            
            if (!array_key_exists('products', $input)) {
                $result['status'] = false;
                $result['error']->add('products', trans('retur_sales_order.message.error_detail'));
                throw new Exception();
            }
            
            // validate 
            $input['total_amount'] = array_sum(array_map(function($item) { 
                return $item['qty'] * $item['sell_price']; 
            }, $input['products']));

            if(!$result['status']) {
                throw new Exception();
            }
            
            if (!$id) {
                $input['no_retur'] = ReturSalesOrder::nextNoReturRSO();
            }

            $result = $this->save($query, $input,[
                "rules" => [
                    "date" => 'required|date_format:Y-m-d H:i',
                    "bank_id" => 'required',
                    "branch_id" => 'required',
                    "sales_order_id" => 'required',
                    "products" => 'required',
                ],
                "messages" => trans("retur_sales_order.errors"),
                "remove" => ['products'],
            ]);

            $rso = null;
            $product_ids = [];
            if ($result['status']) {
                $rso = $result['object'];
                
                $details_id = [];
                $total_first_so = 0;
                $total_discount_detail = 0;
                foreach ($input['products'] as $p) {
                    $p = (object) $p;
                    if ($p->detail_id) {
                        $rso_detail = $rso->details()->with('productIdentity')->whereId($p->detail_id)->first();
                        $product_identity = $rso_detail->productIdentity;
                    } else {
                        //find product identity
                        $product_identity = ProductIdentity::find($p->product_identity_id);

                        //find detail
                        $rso_detail = new ReturSalesOrderDetail();
                    }

                    // create / edit product identity
                    if (!empty($p->detail_id)) {
                        $product_identity->stock -= $rso_detail->qty;
                        $product_identity->stock += $p->qty;
                    } else {
                        $product_identity->stock += $p->qty;
                    }
                    $product_identity->save();
                    
                    // create / edit detail sales order
                    $total_amount_first = $p->qty * $p->sell_price;
                    $rso_detail->qty = $p->qty;
                    $rso_detail->retur_sales_order_id = $rso->id;
                    $rso_detail->product_id = $p->product_id;
                    $rso_detail->product_identity_id = $p->product_identity_id;
                    $rso_detail->sales_order_detail_id = $p->sales_order_detail_id;
                    $rso_detail->sell_price = $p->sell_price;
                    $rso_detail->cost_of_goods = $p->cost_of_goods;
                    $rso_detail->total_amount_first = $total_amount_first;
                    $rso_detail->discount_promo = Util::remove_format_currency(isset($p->discount_promo) ? $p->discount_promo : 0);

                    $rso_detail->discount = ($total_amount_first - ($p->qty * $p->sell_price));
                    $rso_detail->total_discount = $rso_detail->discount_promo + $rso_detail->discount;

                    $rso_detail->promo_id = isset($p->promo_id) ? $p->promo_id : null;
                    $rso_detail->total_amount = $rso_detail->total_amount_first - $rso_detail->total_discount;
                    $rso_detail->save();

                    $details_id[] = $rso_detail->id;
                    $product_ids[] = $rso_detail->product_id;

                    $total_first_so += $total_amount_first;
                    $total_discount_detail += $rso_detail->total_discount;
                }
                
                $rso->total_amount_first = $total_first_so;
                $rso->total_discount_detail = $total_discount_detail;
                $rso->total_amount = $rso->total_amount_first - $rso->total_discount_detail;

                //delete sales order detail
                $delete_details = $rso->details()->with('productIdentity')->whereNotIn('id', $details_id)->get();
                
                if ($delete_details) {
                    foreach ($delete_details as $detail) {
                        $detail->productIdentity->stock += $detail->qty;
                        $detail->productIdentity->save();

                        $this->delete($detail, 'hard');
                    }
                }

                if( $result['status'] ) {
                    \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);
                }
            }

            if( $result['status'] )
            {
                $rso = $rso ?? $result['object'];
                $rso->images = json_encode($rso->moveTmpImage());
                $rso->skipLog()->save();
                
                $rso->credit($rso->bank, (float) $rso->total_amount, sprintf('%s - %s', trans("retur_sales_order.title"), $rso->no_retur));
                \DB::commit();

                $redirect = redirect()->route('transaction.retur.sales.order.show', $rso->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );

                \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);
                \App\Jobs\UpdateStockProductEcommerce::dispatch($product_ids);
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception $e) {
            \DB::rollback();
            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = ReturSalesOrder::with([
            'details' => function($q) {
                $q->with('productIdentity', 'salesOrderDetail');
            },
            'bank'
        ])->whereId($id)->first();
        $name = $row->no_invoice;
            
        $product_ids = [];
        foreach ($row->details as $detail) {
            $product_ids[] = $detail->product_id;
            $detail->productIdentity->stock -= $detail->qty;
            $detail->productIdentity->save();

            $detail->salesOrderDetail->is_retur = SalesOrderDetail::RETUR_NO;
            $detail->salesOrderDetail->save();
            
            $this->delete($detail, 'soft');
        }

        $row->debit($row->bank, (float) $row->total_amount, sprintf('%s - %s', trans("retur_sales_order.title"), $row->no_retur));
        
        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("retur_sales_order.title"), $name) );

            \App\Jobs\CalculateCostOfGoodsJob::dispatch($product_ids);

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("retur_sales_order.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => ReturSalesOrder::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            ReturSalesOrder::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }
}
