<?php

namespace App\Http\Controllers\Transaction;

use App\Models\MutationProductDetail;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use App\Models\MutationProduct;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Employee;
use App\Models\Branch;
use Exception;

class MutationProductController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = MutationProduct::with('employee', 'fromBranch', 'toBranch');

        $orderBy = Util::get('_s_ob');
        $mprting = Util::get('_s_s');
        $mprting = empty($mprting) ? 0 : $mprting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$mprting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nmp') )
        {
            $query->whereLike('no_mutation', Util::get('_nmp'));
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('from_branch_id', auth()->user()->branch_id);
        } elseif ( is_numeric(request('_fbc')) ) {
            $query->where('from_branch_id', Util::get('_fbc'));
        }

        if( is_numeric(request('_tbc')) )
        {
            $query->where('to_branch_id', Util::get('_tbc'));
        }

        if( is_numeric(request('_emp')) )
        {
            $query->where('employee_id', Util::get('_emp'));
        }

        $mutation_products = $query->paginate(Layout::ROW_PER_PAGE)
                            ->appends( request()->except(["page"]) );

        $employees = Employee::active()->get()->pluck('name','id');
        $fromBranchs = Branch::active();
        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $fromBranchs->where('id', auth()->user()->branch_id);
        }
        $fromBranchs = $fromBranchs->get()->pluck('name','id');
        $toBranchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('transaction.mutation_product.index', [
            "mutation_products" => $mutation_products,
            'employees' => $employees,
            'fromBranchs' => $fromBranchs,
            'toBranchs' => $toBranchs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::active()->get()->pluck('name','id');
        $branchs = Branch::select('name as text', 'id')->active()->get();
        $categories = Category::select('name as text', 'id')->active()->get();
        
        return Layout::render('transaction.mutation_product.form', [
            'model' => new MutationProduct(),
            'employees' => $employees,
            'branchs' => $branchs,
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->store_update();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mutation_product = MutationProduct::with([
            'employee', 'fromBranch', 'toBranch',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    },
                    'productIdentity'
                ]);
            }
        ])->whereId($id)->first();

        $mutation_product->date = Util::formatDate($mutation_product->date, 'Y-m-d H:i');
        $images = $mutation_product->images_info();

        return Layout::render('transaction.mutation_product.show', [
            'model' => $mutation_product,
            'images' => $images,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mutation_product = MutationProduct::with([
            'employee', 'fromBranch', 'toBranch',
            'details' => function($q) {
                $q->with([
                    'product' => function($qq) {
                        $qq->with('category', 'brand');
                    },
                    'productIdentity'
                ]);
            }
        ])->whereId($id)->first();
        
        $details = [];
        foreach ($mutation_product->details as $detail) {
            $details[] = [
                "detail_id" => $detail->id,
                "identifier" => $detail->product->category->identifier,
                "identity" => $detail->productIdentity->identity,
                "qty" => $detail->qty,
                "qty_ori" => $detail->qty,
                "total_amount" => $detail->total_amount_first,
                "product_id" => $detail->product_id,
                "cost_of_goods" => $detail->cost_of_goods,
                "image" => $detail->product->image(),
                "brand" => $detail->product->brand->name,
                "category" => $detail->product->category->name,
                "name" => $detail->product->name,
                "sales_price" => $detail->sales_price,
                "sell_price" => $detail->sell_price,
                "general_sell_price" => $detail->sell_price,
                "reseller1_sell_price" => $detail->product->reseller1_sell_price,
                "reseller2_sell_price" => $detail->product->reseller2_sell_price,
                "sales_price_format" => Util::format_currency($detail->sales_price),
                "discount_promo" => $detail->discount_promo,
                "discount" => $detail->discount_promo ? ($detail->discount_promo / $detail->qty) : 0,
                "promo_id" => $detail->promo_id,
                "product_identity_id" => $detail->product_identity_id,
                "id" => $detail->product_identity_id,
            ];
        }

        $mutation_product->date = Util::formatDate($mutation_product->date, 'Y-m-d H:i');
        $images = $mutation_product->images_info();
        
        $employees = Employee::active()->get()->pluck('name','id');
        $branchs = Branch::select('name as text', 'id')->active()->get();
        $categories = Category::select('name as text', 'id')->active()->get();

        return Layout::render('transaction.mutation_product.form', [
            'model' => $mutation_product,
            'images' => $images,
            'details' => $details,
            'employees' => $employees,
            'branchs' => $branchs,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();

        $redirect = redirect()->back();
        $query = MutationProduct::query();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        try {
            $input = request()->except(["_token"]);
            
            //default employee
            if (!empty(config('default.employee_id'))) {
                $input['employee_id'] = config('default.employee_id');
            }
    
            $input['images'] = array_key_exists('images' , $input) ? ($input['images'] ?: '[]') : '[]';
            $input['shipping_cost'] = Util::remove_format_currency($input['shipping_cost']);
            $input['additional_cost'] = Util::remove_format_currency($input['additional_cost']);
            
            if (!$id) {
                $input['no_mutation'] = MutationProduct::nextNoMutation();
            }
            
            $result = $this->save($query, $input,[
                "rules" => [
                    "date" => 'required|date_format:Y-m-d H:i',
                    "from_branch_id" => $id ? '':'required',
                    "to_branch_id" => $id ? '':'required',
                    "employee_id" => 'required',
                    "shipping_cost" => 'numeric',
                    "additional_cost" => 'numeric',
                ],
                "messages" => trans("mutation_product.errors"),
                "remove" => ['products'],
            ]);
            
            $total_mp = 0;
            if ($result['status']) {
                $mp = $result['object'];
                
                $details_id = [];
                foreach ($input['products'] as $p) {
                    $p = (object) $p;

                    //find product identity destination
                    $where_product_identity = [
                        'branch_id' => $mp->to_branch_id,
                        'product_id' => $p->product_id,
                    ];

                    if ($p->identifier != Category::IDENTIFIER_NO) {
                        $where_product_identity['identity'] = $p->identity;
                    }
                    $to_product_identity = ProductIdentity::firstOrNew($where_product_identity);
                    
                    if ($p->detail_id) {
                        $mp_detail = $mp->details()->with('productIdentity')->whereId($p->detail_id)->first();
                        $product_identity = $mp_detail->productIdentity;
                    } else {
                        //find product identity
                        $product_identity = ProductIdentity::find($p->product_identity_id);

                        //find detail
                        $mp_detail = new MutationProductDetail();
                        $mp_detail->product_id = $p->product_id;
                    }

                    // create / edit product identity
                    if (!empty($p->detail_id)) {
                        $product_identity->stock += $mp_detail->qty;
                        $product_identity->stock -= $p->qty;

                        $to_product_identity->stock -= $mp_detail->qty;
                        $to_product_identity->stock += $p->qty;
                    } else {
                        $product_identity->stock -= $p->qty;
                        $to_product_identity->stock += $p->qty;
                    }
                    $product_identity->save();
                    $to_product_identity->save();
                    
                    // create / edit detail sales order
                    $mp_detail->mutation_product_id = $mp->id;
                    $mp_detail->qty = $p->qty;
                    $mp_detail->product_identity_id = $product_identity->id;
                    $mp_detail->to_product_identity_id = $to_product_identity->id;
                    $mp_detail->sell_price = $p->sell_price;
                    $mp_detail->cost_of_goods = $p->cost_of_goods;
                    $mp_detail->total_amount = ($mp_detail->qty * $mp_detail->sell_price);
                    $mp_detail->save();

                    $details_id[] = $mp_detail->id;
                    $total_mp += $mp_detail->total_amount;
                }
                
                //delete sales order detail
                $delete_details = $mp->details()->with('productIdentity','toProductIdentity')->whereNotIn('id', $details_id)->get();
                
                if ($delete_details) {
                    foreach ($delete_details as $detail) {
                        $detail->productIdentity->stock += $detail->qty;
                        $detail->productIdentity->save();

                        $detail->toProductIdentity->stock -= $detail->qty;
                        $detail->toProductIdentity->save();

                        $this->delete($detail, 'hard');
                    }
                }
            }

            if( $result['status'] )
            {
                $mp = $result['object'];
                $mp->images = json_encode($mp->moveTmpImage());
                $mp->total_amount_detail = $total_mp;
                $mp->total_amount = $mp->total_amount_detail + $mp->shipping_cost + $mp->additional_cost;
                $mp->skipLog()->save();
                \DB::commit();

                $redirect = redirect()->route('transaction.mutation.product.show', $mp->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception $e) {
            \DB::rollback();
            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }
            
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = MutationProduct::with([
            'details' => function($q) {
                $q->with('productIdentity', 'toProductIdentity');
            },
        ])->whereId($id)->first();
        $name = $row->no_mutation;

        foreach ($row->details as $detail) {
            $detail->productIdentity->stock += $detail->qty;
            $detail->productIdentity->save();

            $detail->toProductIdentity->stock -= $detail->qty;
            $detail->toProductIdentity->save();

            $this->delete($detail, 'soft');
        }

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("mutation_product.title"), $name) );

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("mutation_product.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => MutationProduct::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            MutationProduct::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }
}
