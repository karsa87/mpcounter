<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use Illuminate\Http\Request;
use App\Models\StockOpname;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Branch;
use Exception;
use Excel;

class StockOpnameController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = StockOpname::with('branch');

        $orderBy = Util::get('_s_ob');
        $mprting = Util::get('_s_s');
        $mprting = empty($mprting) ? 0 : $mprting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$mprting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_m') )
        {
            $query->where('month', Util::get('_m'));
        }

        if( request('_y') )
        {
            $query->where('year', Util::get('_y'));
        }

        if( request('_ttl') )
        {
            $query->whereLike('title', Util::get('_ttl'));
        }

        if( request('_p') )
        {
            $query->where('created_by', Util::get('_p'));
        }

        if( request('_bc') )
        {
            $query->where('branch_id', Util::get('_p'));
        }

        $stock_opname = $query->paginate(Layout::ROW_PER_PAGE)
                            ->appends( request()->except(["page"]) );

        $employees = Employee::active()->get()->pluck('name','user_id');
        $fromBranchs = Branch::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('transaction.stock_opname.index', [
            "stock_opname" => $stock_opname,
            'employees' => $employees,
            'branchs' => $branchs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branchs = Branch::select('name as text', 'id')->active()->get();
        
        return Layout::render('transaction.stock_opname.form', [
            'model' => new StockOpname(),
            'branchs' => $branchs,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->store_update();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = StockOpname::with('branch', 'details')->whereId($id)->first();
        
        return Layout::render('transaction.stock_opname.show', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = StockOpname::find($id);

        if ($model->isWaiting()) {
            $model = $model ? $model->toArray() : [];

            return response()->json([
                'url_edit' => route('transaction.stock.opname.update', $id),
                'data' => $model
            ]);
        } else {
            return redirect()->route('transaction.stock.opname.show', $model->id)->with('error', sprintf('%s', trans("global.failed_save")) );;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();

        $redirect = redirect()->back();
        $query = StockOpname::query();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        try {
            $input = request()->except(["_token"]);
            $form_verified = array_key_exists('products', $input);
            if (empty($id)) {
                $input['month'] = StockOpname::nextMonth();
                $input['year'] = StockOpname::nextYear();
            }
            
            $result = $this->save($query, $input,[
                "rules" => [
                    "title" => !$form_verified ? 'required' : '',
                    "month" => !$form_verified ? 'required' : '',
                    "year" => !$form_verified ? 'required' : '',
                    "branch_id" => !$form_verified ? 'required' : '',
                ],
                "messages" => trans("stock_opname.errors"),
                "remove" => ['products'],
            ]);

            if( $result['status'] )
            {
                $stock_opname = $result['object'];
                if (array_key_exists('products', $input)) { // from form verified
                    $products = $input['products'];
                } else {
                    $products = ProductIdentity::select('product_id', 'id as product_identity_id', 'stock as stock_on_system')
                                ->where('branch_id', $stock_opname->branch_id)
                                ->get()
                                ->toArray();
                }

                $stock_opname->details()->detach();
                $stock_opname->details()->sync($products);
                \DB::commit();

                $redirect = redirect()->route('transaction.stock.opname.show', $stock_opname->id);
                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception $e) {
            \DB::rollback();
            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }
            
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = StockOpname::find($id);
        $name = $row->title;

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("stock_opname.title"), $name) );

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("stock_opname.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function ajaxDetailSOP($id)
    {
        $model = StockOpname::find($id);
        
        $rs = [];
        if ($model) {
            $query = $model->details()->with('product');
            
            if (request('rc') == 'all') {
                $datum = $query->get();
                $rs = $datum->sortBy('product.name')->toArray();
                $rs = array_values($rs);
            } else {
                if  (request('q')) {
                    $keyword = Util::get('q');
                    $query->where(function($q) use($keyword) {
                        $q->whereHas('product', function($q1) use($keyword) {
                            $q1->whereLike('name', $keyword)
                            ->orderBy('name', 'ASC');
                        })
                        ->orWhereLike('identity', $keyword);
                    });
                }
    
                $datum = $query->paginate(Layout::ROW_PER_PAGE)
                    ->appends( request()->except(["page"]) );
                $rs = $datum->toArray();
            }
        }
        
        return response()->json($rs);
    }

    public function change_waiting($id)
    {
        $redirect = redirect()->back();

        $sop = StockOpname::find($id);
        $sop->status = StockOpname::STATUS_ON_PROCESS;
        $name = $sop->title;

        if ($sop->save()) {
            $redirect->with('success', trans("stock_opname.message.success_change_waiting", [
                'name'=>$name
            ]));
        } else {
            $redirect->with('error', trans("stock_opname.message.failed_change_waiting", [
                'name'=>$name
            ]));
        }
        
        return $redirect;
    }

    public function change_process($id)
    {
        $redirect = redirect()->back();

        $sop = StockOpname::find($id);
        $sop->status = StockOpname::STATUS_VERIFIED;
        $name = $sop->title;
        
        if ($sop->save()) {
            $redirect->with('success', trans("stock_opname.message.success_change_process", [
                'name'=>$name
            ]));
        } else {
            $redirect->with('error', trans("stock_opname.message.failed_change_process", [
                'name'=>$name
            ]));
        }
        
        return $redirect;
    }

    public function revert_verified($id)
    {
        $redirect = redirect()->back();

        $sop = StockOpname::find($id);
        $sop->status = StockOpname::STATUS_ON_PROCESS;
        $name = $sop->title;
        
        if ($sop->save()) {
            $redirect->with('success', trans("stock_opname.message.success_revert_verified", [
                'name'=>$name
            ]));
        } else {
            $redirect->with('error', trans("stock_opname.message.failed_revert_verified", [
                'name'=>$name
            ]));
        }
        
        return $redirect;
    }

    public function print($id)
    {
        $redirect = redirect()->back();

        $sop = StockOpname::with([
            'branch',
            'details' => function ($q) {
                $q->with([
                    'product' => function($q1) {
                        $q1->orderBy('name', 'ASC');
                    }
                ]);
            }
        ])->whereId($id)->first();
        
        $filename = $sop->title;
        return Excel::download(new \App\Exports\StockOpnameExport($sop), "$filename.xlsx");
    }

    public function form_verified($id)
    {
        $model = StockOpname::with('branch', 'details')->whereId($id)->first();
        
        return Layout::render('transaction.stock_opname.form_verified', [
            'model' => $model
        ]);
    }
}
