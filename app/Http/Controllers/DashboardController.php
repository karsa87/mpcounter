<?php

namespace App\Http\Controllers;

use App\Models\DebtPaymentPurchaseOrder as DebtPaymentPO;
use App\Models\DebtPaymentSalesOrder as DebtPaymentSO;
use App\Models\ReturPurchaseOrderDetail;
use App\Models\ReturSalesOrderDetail;
use Illuminate\Support\Facades\Cache;
use App\Models\PurchaseOrderDetail;
use App\Models\ReturPurchaseOrder;
use App\Util\Base\CoreController;
use App\Models\SalesOrderDetail;
use App\Models\ReturSalesOrder;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Product;
use App\Models\Expend;
use App\Models\Member;
use App\Models\Stock;
use App\Models\Promo;

class DashboardController extends CoreController
{
    public function index()
    {
        [
            $this_month, $sales_order, $product, $total_income, $total_expend, $chart_so, $member, $sales_order_paid, $sales_order_not_paid, $promos
        ] = Cache::remember('view-dashboard', 1800, function () {
            $member = Member::active()->get();

            // get this month sales order
            $star_date_this_month = sprintf('%s 00:00', date('Y-m-01'));
            $end_date_this_month  = sprintf('%s 23:59', date('Y-m-t'));
            $this_month = [$star_date_this_month,$end_date_this_month];
            $sales_order = SalesOrder::whereBetween('date', $this_month)->get();
            $sales_order_paid = SalesOrder::paid()->whereBetween('date', $this_month)->get();
            $sales_order_not_paid = SalesOrder::notPaid()->whereBetween('date', $this_month)->get();

            // get count product with stock <= 1
            $s_operator = trans("global.array.operator.5");
            $s_stock = 1;

            $stock = Stock::select(\DB::raw('SUM(stock) as stock'), 'product_id')
                            ->havingRaw("SUM(stock) $s_operator $s_stock")
                            ->groupBy('product_id')
                            ->get();
            
            $product_id = $stock->pluck('product_id')->toArray();
            $product_id = array_slice($product_id, 0, Layout::ROW_PER_PAGE);
            
            $product = Product::with([
                'stock' => function($q) use ($s_operator, $s_stock){
                    $q->select(\DB::raw('SUM(stock) as stock'), 'product_id')
                        ->groupBy('product_id')
                        ->havingRaw("SUM(stock) $s_operator $s_stock");
                }
            ])->whereIn('id', $product_id)
            ->get();
            
            // get income money this month
            $debtPaymentSo = DebtPaymentSO::whereBetween('date', $this_month)->get();
            $returPO = ReturPurchaseOrder::whereBetween('date', $this_month)->get();
            $incentiveIncome = Incentive::increase()->whereBetween('date_transaction', $this_month)->get();
            $total_dpso = $debtPaymentSo->sum('paid');
            $total_rpo = $returPO ->sum('total_amount');
            $total_so = $sales_order->sum('paid_off');
            $total_incentive_income = $incentiveIncome->sum('amount');
            $total_income = $total_dpso + $total_so + $total_rpo + $total_incentive_income;
            
            // get expend money this month
            $purchase_order = PurchaseOrder::whereBetween('date', $this_month)->get();
            $debtPaymentPo = DebtPaymentPO::whereBetween('date', $this_month)->get();
            $returSO = ReturSalesOrder::whereBetween('date', $this_month)->get();
            $incentiveExpend = Incentive::decrease()->whereBetween('date_transaction', $this_month)->get();
            $expend = Expend::whereBetween('expend_date', $this_month)->get();
            $total_dppo = $debtPaymentPo->sum('paid');
            $total_rso = $returSO ->sum('total_amount');
            $total_po = $purchase_order->sum('paid_off');
            $total_incentive_expend = $incentiveExpend->sum('amount');
            $total_expend_expend = $expend->sum('amount');
            $total_expend = $total_dppo + $total_rso + $total_po + $total_incentive_expend + $total_expend_expend;
            
            // chart sales order
            $so_groupby = SalesOrder::selectRaw('COUNT(*) as count, DATE(date) as date')
                            ->whereBetween('date', $this_month)
                            ->groupByRaw('DATE(date)')
                            ->get();
            
            $chart_so = [];
            foreach ($so_groupby as $so) {
                $chart_so[date('d', strtotime($so->date))] = $so->count;
            }

            //get promo 
            $promos = Promo::publish()->limit(10)->get();

            return [
                $this_month, $sales_order, $product, $total_income, $total_expend, $chart_so, $member, $sales_order_paid, $sales_order_not_paid, $promos
            ];
        });

        return $this->render('dashboard', [
            'this_month' => $this_month,
            'sales_order' => $sales_order,
            'product' => $product,
            'total_income' => $total_income,
            'total_expend' => $total_expend,
            'chart_so' => $chart_so,
            'member' => $member,
            'sales_order_paid' => $sales_order_paid,
            'sales_order_not_paid' => $sales_order_not_paid,
            'promos' => $promos
        ]);
    }
}
