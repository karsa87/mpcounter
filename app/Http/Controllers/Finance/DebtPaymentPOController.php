<?php

namespace App\Http\Controllers\Finance;

use App\Models\DebtPaymentPurchaseOrder as DebtPayment;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\PurchaseOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Bank;

class DebtPaymentPOController extends CoreController
{
    public function index()
    {
        $query = DebtPayment::with('purchaseOrder', 'bank');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_ndb') )
        {
            $no_debt = Util::get('_ndb');
            $query->whereLike('no_debt', $no_debt);
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if( is_numeric(request('_poid')) )
        {
            $query->where('purchase_order_id', Util::get('_poid'));
        }

        $debtPayments = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        $purchase_order_id = DebtPayment::all()->pluck('purchase_order_id')->toArray();
        $purchaseOrders_all = PurchaseOrder::with('bank')
                            ->where(function($q) use($purchase_order_id) {
                                $q->whereIn('id', $purchase_order_id)
                                    ->orWhere(function($qq){
                                        $qq->notPaid()->debt();
                                    });
                            })
                            ->get();

        $purchaseOrders = $purchaseOrders_all->pluck('no_invoice','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        return Layout::render('finance.debt_payment_po.index', [
            "debtPayments" => $debtPayments,
            'purchaseOrders' => $purchaseOrders,
            'banks' => $banks,
        ]);
    }

    public function show($id)
    {
        $model = DebtPayment::with([
            'purchaseOrder' => function($q) use($id) {
                $q->with([
                    'details', 
                    'debtPayments' => function($qq) use($id) {
                        $qq->where('id', '!=', $id);
                    }
                ]);
            }, 
            'bank'
        ])->whereId($id)->first();

        return Layout::render('finance.debt_payment_po.show', [
            'model' => $model,
        ]);
    }

    public function create()
    {
        $purchaseOrders_all = PurchaseOrder::select(
                                    'id',
                                    'no_invoice',
                                    'paid_off',
                                    'total_amount',
                                    'bank_id'
                                )
                                ->with([
                                    'bank' => function($q) {
                                        $q->select('id', 'name');
                                    },
                                    'debtPayments' => function($q) {
                                        $q->select('paid','purchase_order_id');
                                    }
                                ])
                                ->notPaid()
                                ->debt();

        if ( request('_poid') ) {
            $purchaseOrders_all->whereIn('id', [Util::get('_poid')]);
        }

        $purchaseOrders_all = $purchaseOrders_all->get()->keyBy('id');

        $purchaseOrders = $purchaseOrders_all->pluck('no_invoice','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        return Layout::render('finance.debt_payment_po.form', [
            'model' => new DebtPayment(),
            'purchaseOrders_all' => $purchaseOrders_all,
            'purchaseOrders' => $purchaseOrders,
            'banks' => $banks
        ]);
    }

    public function edit($id)
    {
        $model = DebtPayment::with([
            'purchaseOrder' => function($q) {
                $q->with('details');
            }, 
            'bank'
        ])->whereId($id)->first();

        $model->date = Util::formatDate($model->date, 'Y-m-d H:i');
        $images = $model->images_info();
        
        $purchaseOrders_all = PurchaseOrder::select(
                                    'id',
                                    'no_invoice',
                                    'paid_off',
                                    'total_amount',
                                    'bank_id'
                                )->with([
                                    'bank' => function($q) {
                                        $q->select('id', 'name');
                                    },
                                    'debtPayments' => function($q) use($id) {
                                        $q->select('paid','purchase_order_id')
                                            ->where('id', '!=', $id);
                                    }
                                ])
                                ->where(function($q) use($model) {
                                    $q->whereIn('id', [$model->purchase_order_id])
                                        ->orWhere(function($qq){
                                            $qq->notPaid()->debt();
                                        });
                                })
                                ->get()
                                ->keyBy('id');
        
        $purchaseOrders = $purchaseOrders_all->pluck('no_invoice','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        
        return Layout::render('finance.debt_payment_po.form', [
            'model' => $model,
            'images' => $images,
            'purchaseOrders_all' => $purchaseOrders_all,
            'purchaseOrders' => $purchaseOrders,
            'banks' => $banks
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $redirect = redirect()->back();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        $input = request()->except(["_token"]);
        $input['images'] = $input['images'] ?: '[]';
        $input['paid'] = Util::remove_format_currency($input['paid']);

        if (empty($id)) {
            $input['no_debt'] = DebtPayment::nextNoDebtPO();
        }

        $query = DebtPayment::query();
        $result = $this->save($query, $input,[
            "rules" => [
                "date" => 'required|date_format:Y-m-d H:i',
                "bank_id" => 'required',
                "purchase_order_id" => $id ? '' : 'required',
                "no_debt" => 'required',
                "paid" => 'numeric'
            ],
            "messages" => trans("expend.errors"),
        ]);

        if( $result['status'] )
        {
            $debt = $result['object'];
            $debt->images = json_encode($debt->moveTmpImage());
            
            $debt->credit($debt->bank, (float) $debt->paid, $debt->information);

            $purchaseOrder = $debt->purchaseOrder;
            $total_paid = $purchaseOrder->debtPayments->sum('paid');
            $total_paid += $purchaseOrder->paid_off;

            if ($total_paid >= $purchaseOrder->total_amount) {
                $purchaseOrder->status = PurchaseOrder::STATUS_PAID;
            } else {
                $purchaseOrder->status = PurchaseOrder::STATUS_NOT_PAID;
            }
            $purchaseOrder->save();

            $debt->total_paid = $total_paid;
            $debt->skipLog()->save();

            $redirect = redirect()->route('finance.debt.payment.po.show', $debt->id);
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            \DB::commit();
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
            
            \DB::rollback();
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = DebtPayment::find($id);
        $name = $row->no_debt;
        $purchaseOrder = $row->purchaseOrder;

        $row->debit($row->bank, (float) $row->paid, $row->information);

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("expend.title"), $name));
            
            $total_paid = $purchaseOrder->debtPayments->sum('paid');
            $total_paid += $purchaseOrder->paid_off;

            if ($total_paid >= $purchaseOrder->total_amount) {
                $purchaseOrder->status = PurchaseOrder::STATUS_PAID;
            } else {
                $purchaseOrder->status = PurchaseOrder::STATUS_NOT_PAID;
            }
            $purchaseOrder->save();

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("expend.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => DebtPayment::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            DebtPayment::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }
}
