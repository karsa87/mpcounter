<?php

namespace App\Http\Controllers\Finance;

use App\Util\Base\CoreController;
use App\Models\CategoryIncentive;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Employee;
use App\Models\Branch;
use App\Models\Bank;

class IncentiveController extends CoreController
{
    public function index()
    {
        $query = Incentive::with('bank', 'branch');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_idt') )
        {
            $incentive_date = Util::get('_idt');
            $incentive_date = explode(" - ", $incentive_date);    
            
            $query->whereBetween('date_transaction', $incentive_date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $query->where('branch_id', auth()->user()->branch_id);
        } elseif( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( request('_ntr') )
        {
            $query->whereLike('no_transaction', Util::get('_ntr'));
        }

        if( request('_ins') )
        {
            $query->where('investor', Util::get('_ins'));
        }

        if( is_numeric(request('_tp')) )
        {
            $query->type(Util::get('_tp'));
        }

        $incentives = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $investors = $incentives->pluck('investor')->unique();
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $branchs = Branch::active();
        if (auth()->user()->isEmployee() && auth()->user()->branch_id) {
            $branchs->where('id', auth()->user()->branch_id);
        }
        $branchs = $branchs->get()->pluck('name','id');

        return Layout::render('finance.incentive.index', [
            "incentives" => $incentives,
            'investors' => $investors,
            'banks' => $banks,
            'branchs' => $branchs,
        ]);
    }

    public function edit($id)
    {
        $incentive = Incentive::find($id);
        $incentive->date_transaction = Util::formatDate($incentive->date_transaction, Util::FORMAT_DATE_EN_SHORT);
        $incentive = $incentive ? $incentive->toArray() : [];
        
        return response()->json([
            'url_edit' => route('finance.incentive.update', $id),
            'data' => $incentive
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
            
        //default bank
        if (!empty(config('default.bank_id'))) {
            $input['bank_id'] = config('default.bank_id');
        }
        
        $input['amount'] = Util::remove_format_currency($input['amount']);

        if ($id != null) {
            unset($input['type']);
        }
        $redirect = redirect()->back();

        $query = Incentive::query();
        $result = $this->save($query, $input,[
            "rules" => [
                "date_transaction" => 'required|date_format:Y-m-d',
                "bank_id" => 'required',
                "no_transaction" => 'required',
                "branch_id" => 'required',
                "amount" => 'numeric|min:0'
            ],
            "messages" => trans("incentive.errors"),
        ]);

        if( $result['status'] )
        {
            $incentive = $result['object'];

            if ($incentive->isIncrease()) {
                $incentive->debit($incentive->bank, (float) $incentive->amount, $incentive->no_transaction);
            }
            
            if ($incentive->isDecrease()) {
                $incentive->credit($incentive->bank, (float) $incentive->amount, $incentive->no_transaction);
            }
            
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Incentive::find($id);
        
        if ($row->isIncrease()) {
            $row->credit($row->bank, (float) $row->amount, $row->no_transaction);
        }
        
        if ($row->isDecrease()) {
            $row->debit($row->bank, (float) $row->amount, $row->no_transaction);
        }

        $name = $row->name;

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("incentive.title"), $name) );

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("incentive.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }
}
