<?php

namespace App\Http\Controllers\Finance;

use App\Models\DebtPaymentSalesOrder as DebtPayment;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Bank;

class DebtPaymentSOController extends CoreController
{
    public function index()
    {
        $query = DebtPayment::with('salesOrder', 'bank');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_ndb') )
        {
            $no_debt = Util::get('_ndb');
            $query->where('no_debt', $no_debt);
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if( is_numeric(request('_soid')) )
        {
            $query->where('sales_order_id', Util::get('_soid'));
        }

        $debtPayments = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        $sales_order_id = DebtPayment::all()->pluck('sales_order_id')->toArray();
        $salesOrders_all = SalesOrder::with('bank')
                            ->where(function($q) use($sales_order_id) {
                                $q->whereIn('id', $sales_order_id)
                                    ->orWhere(function($qq){
                                        $qq->notPaid()->debt();
                                    });
                            })
                            ->get();

        $salesOrders = $salesOrders_all->pluck('no_invoice','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        return Layout::render('finance.debt_payment_so.index', [
            'debtPayments' => $debtPayments,
            'salesOrders' => $salesOrders,
            'banks' => $banks,
        ]);
    }

    public function show($id)
    {
        $model = DebtPayment::with([
            'salesOrder' => function($q) use($id) {
                $q->with([
                    'details', 
                    'debtPayments' => function($qq) use($id) {
                        $qq->where('id', '!=', $id);
                    }
                ]);
            }, 
            'bank'
        ])->whereId($id)->first();

        return Layout::render('finance.debt_payment_so.show', [
            'model' => $model,
        ]);
    }

    public function create()
    {
        $salesOrders_all =  SalesOrder::select(
                                'id',
                                'no_invoice',
                                'paid_off',
                                'total_amount',
                                'bank_id'
                            )->with([
                                'bank' => function($q) {
                                    $q->select('id', 'name');
                                },
                                'debtPayments' => function($q) {
                                    $q->select('paid','sales_order_id');
                                }
                            ])
                            ->notPaid()
                            ->debt()
                            ->get()
                            ->keyBy('id');

        if ( request('_soid') ) {
            $salesOrders_all->whereIn('id', [Util::get('_soid')]);
        }
                    
        $salesOrders = $salesOrders_all->pluck('no_invoice','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');

        return Layout::render('finance.debt_payment_so.form', [
            'model' => new DebtPayment(),
            'salesOrders_all' => $salesOrders_all,
            'salesOrders' => $salesOrders,
            'banks' => $banks
        ]);
    }

    public function edit($id)
    {
        $model = DebtPayment::with([
            'salesOrder' => function($q) {
                $q->with('details');
            }, 
            'bank'
        ])->whereId($id)->first();

        $model->date = Util::formatDate($model->date, 'Y-m-d H:i');
        $images = $model->images_info();
        
        $salesOrders_all = SalesOrder::select(
                                'id',
                                'no_invoice',
                                'paid_off',
                                'total_amount',
                                'bank_id'
                            )->with([
                                'bank' => function($q) {
                                    $q->select('id', 'name');
                                },
                                'debtPayments' => function($q) use($id) {
                                    $q->select('paid','sales_order_id')
                                        ->where('id', '!=', $id);
                                }
                            ])
                            ->where(function($q) use($model) {
                                $q->whereIn('id', [$model->sales_order_id])
                                    ->orWhere(function($qq){
                                        $qq->notPaid()->debt();
                                    });
                            })
                            ->get()
                            ->keyBy('id');
        
        $salesOrders = $salesOrders_all->pluck('no_invoice','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        
        return Layout::render('finance.debt_payment_so.form', [
            'model' => $model,
            'images' => $images,
            'salesOrders_all' => $salesOrders_all,
            'salesOrders' => $salesOrders,
            'banks' => $banks
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        \DB::beginTransaction();
        $redirect = redirect()->back();
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        $input = request()->except(["_token"]);
        $input['images'] = $input['images'] ?: '[]';
        $input['paid'] = Util::remove_format_currency($input['paid']);

        if (empty($id)) {
            $input['no_debt'] = DebtPayment::nextNoDebtSO();
        }
        
        $query = DebtPayment::query();
        $result = $this->save($query, $input,[
            "rules" => [
                "date" => 'required|date_format:Y-m-d H:i',
                "bank_id" => 'required',
                "sales_order_id" =>  $id ? '' : 'required',
                "no_debt" => 'required',
                "paid" => 'numeric'
            ],
            "messages" => trans("expend.errors"),
        ]);

        if( $result['status'] )
        {
            $debt = $result['object'];
            $debt->images = json_encode($debt->moveTmpImage());
            
            $debt->debit($debt->bank, (float) $debt->paid, $debt->information);

            $salesOrder = $debt->salesOrder;
            $total_paid = $salesOrder->debtPayments->sum('paid');
            $total_paid += $salesOrder->paid_off;

            if ($total_paid >= $salesOrder->total_amount) {
                $salesOrder->status = SalesOrder::STATUS_PAID;
            } else {
                $salesOrder->status = SalesOrder::STATUS_NOT_PAID;
            }
            $salesOrder->save();

            $debt->total_paid = $total_paid;
            $debt->skipLog()->save();

            $redirect = redirect()->route('finance.debt.payment.so.show', $debt->id);
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            \DB::commit();
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
            
            \DB::rollback();
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = DebtPayment::find($id);
        $name = $row->no_debt;
        $salesOrder = $row->salesOrder;

        $row->credit($row->bank, (float) $row->paid, $row->information);

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("expend.title"), $name));

            $total_paid = $salesOrder->debtPayments->sum('paid');
            $total_paid += $salesOrder->paid_off;

            if ($total_paid >= $salesOrder->total_amount) {
                $salesOrder->status = SalesOrder::STATUS_PAID;
            } else {
                $salesOrder->status = SalesOrder::STATUS_NOT_PAID;
            }
            $salesOrder->save();
            
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("expend.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => DebtPayment::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            DebtPayment::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }
}
