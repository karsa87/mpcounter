<?php

namespace App\Http\Controllers\Finance;

use App\Util\Base\CoreController;
use App\Models\CategoryExpend;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Expend;
use App\Models\Branch;
use App\Models\Bank;

class ExpendController extends CoreController
{
    public function index()
    {
        $query = Expend::with('categoryExpend', 'bank', 'employee', 'branch');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_exdt') )
        {
            $expend_date = Util::get('_exdt');
            $expend_date = explode(" - ", $expend_date);    
            
            $query->whereBetween('expend_date', $expend_date);
        }

        if( is_numeric(request('_ntr')) )
        {
            $query->whereLike('no_transaction', Util::get('_ntr'));
        }

        if( is_numeric(request('_bk')) )
        {
            $query->where('bank_id', Util::get('_bk'));
        }

        if( is_numeric(request('_crgex')) )
        {
            $query->where('category_expend_id', Util::get('_crgex'));
        }

        if( is_numeric(request('_emp')) )
        {
            $query->where('employee_id', Util::get('_emp'));
        }

        if( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        $expends = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $employes = Employee::active()->get()->pluck('name','id');
        $category_expends = CategoryExpend::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('finance.expend.index', [
            "expends" => $expends,
            'employes' => $employes,
            'category_expends' => $category_expends,
            'banks' => $banks,
            'branchs' => $branchs,
        ]);
    }

    public function edit($id)
    {
        $expend = Expend::find($id);
        $expend->expend_date = Util::formatDate($expend->expend_date, Util::FORMAT_DATE_EN_SHORT);
        $images = $expend->images_info();

        $expend = $expend ? $expend->toArray() : [];
        $expend['images_dropzone'] = $images;
        
        return response()->json([
            'url_edit' => route('finance.expend.update', $id),
            'data' => $expend
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
            
        //default bank
        if (!empty(config('default.bank_id'))) {
            $input['bank_id'] = config('default.bank_id');
        }
            
        //default employee
        if (!empty(config('default.employee_id'))) {
            $input['employee_id'] = config('default.employee_id');
        }

        $input['images'] = $input['images'] ?: '[]';
        $input['amount'] = Util::remove_format_currency($input['amount']);
        $redirect = redirect()->back();
        $query = Expend::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "no_transaction" => 'required',
                "expend_date" => 'required|date_format:Y-m-d',
                "bank_id" => 'required',
                "branch_id" => 'required',
                "category_expend_id" => 'required',
                "amount" => 'numeric'
            ],
            "messages" => trans("expend.errors"),
        ]);

        if( $result['status'] )
        {
            $expend = $result['object'];
            $expend->images = json_encode($expend->moveTmpImage());
            $expend->skipLog()->save();
            
            $expend->credit($expend->bank, (float) $expend->amount, $expend->no_transaction);
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Expend::find($id);
        $name = $row->name;
            
        $row->debit($row->bank, (float) $row->amount, $row->no_transaction);

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("expend.title"), $name) );

            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("expend.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $result["status"]] );
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => Expend::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            Expend::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }
}
