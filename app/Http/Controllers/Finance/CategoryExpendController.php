<?php

namespace App\Http\Controllers\Finance;

use App\Util\Base\CoreController;
use App\Models\CategoryExpend;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;

class CategoryExpendController extends CoreController
{
    public function index()
    {
        $query = CategoryExpend::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        $category_expends = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('finance.category_expend.index', [
            "category_expends" => $category_expends,
        ]);
    }

    public function edit($id)
    {
        $category_expend = CategoryExpend::find($id);
        $category_expend = $category_expend ? $category_expend->toArray() : [];

        return response()->json([
            'url_edit' => route('finance.category.expend.update', $id),
            'data' => $category_expend
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == 'on' ? CategoryExpend::STATUS_ACTIVE : CategoryExpend::STATUS_DEACTIVE;

        $redirect = redirect()->back();
        $query = CategoryExpend::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255'
            ],
            "messages" => trans("category_expend.errors"),
        ]);

        if( $result['status'] )
        {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = CategoryExpend::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("category_expend.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("category_expend.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxCategoryExpend()
    {
        $params = request()->all();

        $list = CategoryExpend::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}
