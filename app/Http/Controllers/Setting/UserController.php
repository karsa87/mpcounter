<?php

namespace App\Http\Controllers\Setting;

use jeremykenedy\LaravelUsers\App\Http\Controllers\LaravelUsersController;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Permission;
// use App\Models\Domain;
use App\Models\Branch;
use App\Models\User;
use App\Models\Role;

class UserController extends CoreController
{
    public function index()
    {
        $roles = auth()->user()->getRoles();
        if ($roles->count() > 0) {
            $level = $roles->first()->level;
        } else {
            $level = Role::LEVEL_EMPLOYEE;
        }
        
        $query = User::with('employee','branch')->whereHas('roles', function($q) use ($level) {
            $q->where('level', '<=', $level);
        })->doesntHave('employee');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('search_email') )
        {
            $query->whereLike('email', Util::get('search_email'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->where('status', Util::get('_sts'));
        }
        
        $permissions = Permission::all()->pluck('name','id')->toArray();
        // $domains = Domain::active()->get()->pluck('name','id')->toArray();
        $roles = Role::whereIn('level', array_keys(Role::getListRole()))->pluck('name','id')->toArray();
        $branchs = Branch::active()->get()->pluck('name','id')->toArray();

        $users = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        return Layout::render('setting.user.index', [
            "users" => $users,
            // 'domains' => $domains,
            'roles' => $roles,
            "permissions" => $permissions,
            "branchs" => $branchs,
            'breadcrumbs' => [
                [
                    'text' => trans('global.setting'),
                    'url' => '#'
                ],
                [
                    'text' => trans('user.title'),
                    'url' => route('setting.user.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        // $_user = User::with('domains','roles','userPermissions')->where('id',$id)->first();
        $_user = User::with('roles','userPermissions')->where('id',$id)->first();
        
        $user = clone $_user;
        $user = $user ? $user->toArray() : [];
        // if( $_user->domains )
        // {
        //     $domains = $_user->domains;
        //     $user['domains'] = [];
        //     $user['domains'] = $domains ? $domains->pluck('id')->toArray() : [];
        // }

        if( $_user->roles )
        {
            $roles = $_user->roles;
            $user['roles'] = [];
            $user['roles'] = $roles ? $roles->pluck('id')->toArray() : [];
        }

        if( $_user->userPermissions )
        {
            $permissions = $_user->userPermissions;
            $user['permissions'] = [];
            $user['permissions'] = $permissions ? $permissions->pluck('id')->toArray() : [];
        }

        return response()->json([
            'url_edit' => route('setting.user.update', $id),
            'user' => $user
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }

    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null)
    {
        
        $input = request()->except(["_token"]);
        $query = User::query();

        $user = null;
        if($input["id"])
        {
            $user = User::find($input["id"]);
        }

        $input['status'] = $input['status'] == "on" ? 1 : 0;

        $valid_password = 'required|min:5';
        if ( $input['id'] ) {
            $valid_password = isset($input['password']) ? 'min:5' : '';
        }
        
        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "email" => 'max:255|email',
                "username" => 'required|max:255|unique:users,username,'.($input['id'] ?: 'NULL') .',id',
                "password" => $valid_password
            ],
            "messages" => trans("user.errors"),
            // 'remove' => ['user_domain_id','user_role'],
            'remove' => ['user_role','user_permission']
        ]);

        $redirect = redirect()->back();
        
        if( $result['status'] )
        {
            $user = $result['object'];
            if( array_key_exists('user_role', $input) && $input['user_role'] )
            {
                $roles = is_array($input['user_role']) ? $input['user_role'] : [$input['user_role']];
                $user->syncRoles($roles);
            }
            else
            {
                $user->detachAllRoles();
            }

            $user->images = json_encode($user->moveTmpImage());
            $user->skipLog()->save();

            // if( array_key_exists('user_domain_id', $input) && $input['user_domain_id'] )
            // {
            //     $domains = $input['user_domain_id'];
            //     $user->domains()->sync($domains);
            // }
            // else
            // {
            //     $user->domains()->detach();
            // }

            if( array_key_exists('user_permission', $input) && $input['user_permission'] )
            {
                $permissions = $input['user_permission'];
                $user->syncPermissions($permissions);
            }
            else
            {
                $user->detachAllPermissions();
            }

            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = User::find($id);
        $row->detachAllRoles();
        $row->detachAllPermissions();
        $name = $row->user_realname;

        $status = $this->delete($row, 'soft');

        if($status["status"])
        {
            session()->flash('success', sprintf("%s %s %s", trans("user.title"), trans("global.success_delete"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s %s", trans("user.title"), trans("global.failed_delete"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function change_status($id, $status)
    {
        $user = User::find($id);
        
        if ($user) {
            $user->status = $status;
            if ( $user->save() ) {
                session()->flash('success', sprintf(trans("user.message.success_status"), trans("user.list.status.$status")));
            } else {
                session()->flash('error', sprintf(trans("user.message.failed_status"), trans("user.list.status.$status")));
            }
        } else {
            session()->flash('error', sprintf(trans("user.message.not_found"), $name) );
        }
        
        return redirect()->back();
    }

    public function upload()
    {
        if (request()->isMethod('post')) {
            return response()->json([
                'path' => User::uploadTmpImage(request()->file('file'), TRUE)
            ]);
        } else {
            User::deleteImages(request('file'));

            return response()->json([
                'success' => TRUE
            ]);
        }
    }

    public function profile()
    {
        $user = auth()->user();
        if (request()->isMethod('post')) {
            return $this->store_update($user->id);
        } else {
            return Layout::render('setting.user.profile', [
                "model" => $user,
            ]);
        }
    }
}
