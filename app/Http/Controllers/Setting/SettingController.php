<?php

namespace App\Http\Controllers\Setting;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Setting;
use App\Models\Menu;

class SettingController extends CoreController
{
    public function index()
    {
        $query = Setting::orderBy('updated_at','DESC');

        if( request('search_name') )
        {
            $query->whereLike('name', Util::get('search_name'));
        }

        $settings = $query->paginate(Layout::ROW_PER_PAGE)
                            ->appends( request()->except(["page"]) );

        return Layout::render('setting.setting.index', [
            "settings" => $settings
        ]);
    }

    public function edit($id)
    {
        $setting = Setting::find($id);

        return response()->json([
            'url_edit' => route('setting.setting.update', $id),
            'data' => $setting
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $input['status'] = $input['status'] == "on" ? Setting::STATUS_ACTIVE : Setting::STATUS_DEACTIVE;
        if ($input['type'] == Setting::TYPE_NUMBER) {
            $input['value'] = Util::remove_format_currency($input['value_number']);
        }
        
        $redirect = redirect()->back();
        $query = Setting::query();

        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "key" => 'required|max:255|unique:setting,key,'.($input['id'] ?: 'NULL') .',id',
                "value" => 'required',
            ],
            "messages" => trans("setting.errors"),
            'remove' => ['value_number']
        ]);

        if( $result['status'] )
        {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Setting::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');

        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("setting.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("setting.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }
}
