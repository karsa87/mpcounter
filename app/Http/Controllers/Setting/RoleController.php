<?php

namespace App\Http\Controllers\Setting;

use jeremykenedy\LaravelRoles\App\Http\Controllers\LaravelRolesController;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Menu;

class RoleController extends CoreController
{
    public function index()
    {
        $roles = auth()->user()->getRoles();
        if ($roles->count() > 0) {
            $level = $roles->first()->level;
        } else {
            $level = Role::LEVEL_EMPLOYEE;
        }

        $query = Role::with('menus')->orderBy('updated_at','ASC')
                    ->levelUnder($level);

        if( request('search_name') )
        {
            $query->whereLike('name', Util::get('search_name'));
        }

        if( request('search_slug') )
        {
            $query->whereLike('slug', Util::get('search_slug'));
        }

        if( request('search_level') )
        {
            $query->where('level', Util::get('search_level'));
        }
        
        $permissions = Permission::all()->pluck('name','id')->toArray();
        $menus = Menu::with(['submenu.submenu.submenu.submenu.submenu','permissions'])
                    ->firstParents()->orderBy('order', 'ASC')
                    ->get();

        $roles = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        return Layout::render('setting.role.index', [
            "roles" => $roles,
            "permissions" => $permissions,
            "menus" => $menus,
            'breadcrumbs' => [
                [
                    'text' => trans('global.setting'),
                    'url' => '#'
                ],
                [
                    'text' => trans('role.title'),
                    'url' => route('setting.role.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        $role = Role::with(['permissions','menus'])->where('id',$id)->first();

        $permissions = [];
        if( $role && $role->permissions )
        {
            $permissions = $role->permissions->pluck('id')->toArray();
        }

        $menus = [];
        if( $role && $role->menus )
        {
            $menus = $role->menus->pluck('id')->toArray();
        }

        $role = $role ? $role->toArray() : [];

        return response()->json([
            'url_edit' => route('setting.role.update', $id),
            'role' => $role,
            'permission' => $permissions,
            'menus' => $menus,
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null)
    {
        $input = request()->except(["_token"]);
        $query = Role::query();
        
        $result = $this->save($query, $input,[
            "rules" => [
                "name" => 'required|max:255',
                "slug" => 'required|max:255|unique:roles,slug,'.($input['id'] ?: 'NULL') .',id',
                "level" => 'required|numeric',
            ],
            "messages" => trans("role.errors"),
            'remove' => ['permission']
        ]);

        $redirect = redirect()->back();
        
        if( $result['status'] )
        {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Role::find($id);
        $row->permissions()->detach();
        $row->menus()->detach();
        $name = $row->name;

        $status = $this->delete($row, 'soft');

        if($status["status"]){
            session()->flash('success', sprintf("%s %s %s", trans("role.title"), trans("global.success_delete"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s %s", trans("role.title"), trans("global.failed_delete"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function access()
    {
        $input = request()->except(["_token"]);
        $role = Role::find($input['role_id']);

        $redirect = redirect()->back();
        if( $role )
        {
            $permission = array_key_exists('permission', $input) ? array_keys($input['permission']) : [];
            if( count($permission) > 0 )
            {
                $permission_dashboard = Permission::whereIn('slug',['dashboard','login','logout'])->get();
                if( $permission_dashboard )
                {
                    $_permissions = $permission_dashboard->pluck('id')->toArray();
                    $permission = array_merge($permission, $_permissions);
                    $permission = array_unique($permission);
                }

                $role->permissions()->sync($permission);
            }

            $menu = array_key_exists('menu', $input) ? array_keys($input['menu']) : [];
            if( count($menu) > 0 )
            {
                $role->menus()->sync($menu);
            }

            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }
}
