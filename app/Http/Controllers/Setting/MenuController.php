<?php

namespace App\Http\Controllers\Setting;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Models\Permission;
use App\Models\Menu;

class MenuController extends CoreController
{
    public function index()
    {
        $query = Menu::with('submenu')->firstParents()->orderBy('order','ASC');

        if( request('name') )
        {
            $query->whereLike('name', Util::get('name'));
        }

        if( request('url') )
        {
            $query->whereLike('url', Util::get('url'));
        }

        if( request('route') )
        {
            $query->whereLike('route', Util::get('route'));
        }

        $menus = $query->get();
        $parents = Menu::parents()->get()->pluck('name','id')->toArray();
        
        return $this->render('setting.menu.index', [
            "menus" => $menus,
            "parents" => $parents,
            'breadcrumbs' => [
                [
                    'text' => trans('global.menu.setting'),
                    'url' => '#'
                ],
                [
                    'text' => trans('menu.title'),
                    'url' => route('setting.menu.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        $menu = Menu::find($id);
        $menu = $menu ? $menu->toArray() : [];

        return response()->json([
            'url_edit' => route('setting.menu.update', $id),
            'menu' => $menu,
        ]);
    }

    public function store()
    {
        return $this->store_save();
    }

    public function update($id)
    {
        return $this->store_save($id);
    }

    private function store_save($id =null)
    {
        $result = [
            'status' => FALSE,
            'error' => []
        ];
        $input = request()->except(["_token"]);

        if(array_key_exists('ordering', $input))
        {
            $this->update_order(json_decode($input['ordering'], TRUE));
            $result['status'] = TRUE;
        }
        else
        {
            if(request('is_parent'))
            {
                $input['is_parent'] = $input['is_parent'] == "on" ? 1 : 0;
            }
            else
            {
                $input['is_parent'] = 0;
            }

            $query = Menu::query();

            $input['url'] = '';
            if( \Route::has($input['route']) )
            {
                $input['url'] = str_replace(url('/') . '/', '', route($input['route']));
            }

            $result = $this->save($query, $input,[
                "rules" => [
                    "name" => 'required|max:255',
                    // "url" => 'required|max:100',
                    // "route" => 'required|max:100',
                ],
                "messages" => trans("menu.errors"),
            ]);
        }

        $redirect = redirect()->route('setting.menu.index');        
        if( $result['status'] )
        {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    private function update_order($menus, $parent_id = null)
    {
        foreach ($menus as $key => $value)
        {   
            $data = [
                'order' => $key,
                'parent_id' => NULL,
                'is_parent' => 0
            ];

            if(!empty($parent_id))
            {
                $data['parent_id'] = $parent_id;
            } elseif (empty($parent_id)) {
                $data['is_parent'] = 1;
            }

            if( array_key_exists('children', $value) )
            {
                $data['is_parent'] = 1;
                $this->update_order($value['children'], $value['id']);
            }

            Menu::where('id', $value['id'])->update($data);
        }
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Menu::find($id);
        $row->permissions()->detach();
        $name = $row->name;

        $status = $this->delete($row, 'soft');

        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("menu.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("menu.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }
}
