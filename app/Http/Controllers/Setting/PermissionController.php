<?php

namespace App\Http\Controllers\Setting;

use jeremykenedy\LaravelPermissions\App\Http\Controllers\LaravelPermissionsController;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Permission;
use App\Models\Menu;

class PermissionController extends CoreController
{
    public function index()
    {
        $this->checkDeveloper();
        $query = Permission::with('menus');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_sg') )
        {
            $query->whereLike('slug', Util::get('_sg'));
        }

        if( request('_mn') )
        {
            $menu_id = Util::get('_mn');
            $query->whereHas('menus',function($q) use($menu_id) {
                $q->where('id',$menu_id);
            });
        }

        $permissions = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        $menus = Menu::whereNotNull('route')->pluck('name','id')->toArray();

        return Layout::render('setting.permission.index', [
            "permissions" => $permissions,
            "menus" => $menus,
            'models' => Util::get_models(),
            'breadcrumbs' => [
                [
                    'text' => trans('global.menu.setting'),
                    'url' => '#'
                ],
                [
                    'text' => trans('permission.title'),
                    'url' => route('setting.permission.index')
                ]
            ],
        ]);
    }

    public function edit($id)
    {
        $this->checkDeveloper();
        $permission = Permission::with('menus')->where('id',$id)->first();
        $menu = $permission && $permission->menus->count() > 0 ? $permission->menus[0]->id : '';
        $permission = $permission ? $permission->toArray() : [];
        $permission['menu'] = $menu;

        return response()->json([
            'url_edit' => route('setting.permission.update', $id),
            'data' => $permission
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $this->checkDeveloper();
        $input = request()->except(["_token"]);
        
        $redirect = redirect()->back();
        // if( \Route::has($input['slug']) )
        // {
            $query = Permission::query();

            $result = $this->save($query, $input,[
                "rules" => [
                    "name" => 'required|max:255',
                    "slug" => 'required|max:255|unique:permissions,slug,'.($input['id'] ?: 'NULL') .',id',
                    // "menu" => 'required',
                ],
                "messages" => trans("permission.errors"),
                "remove" => ['menu']
            ]);

            if( $result['status'] )
            {
                $permission = Permission::find($result['id']);
                if( !empty($input['menu']) )
                {
                    $permission->menus()->sync([$input['menu']]);
                }

                $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            }
            else
            {
                $redirect->withInput( request()->except(['_token']) )
                            ->withErrors($result['error'])
                            ->with('error', sprintf('%s', trans("global.failed_save")) );
            }
        // }
        // else
        // {
        //     $redirect->withInput( request()->except(['_token']) )
        //                     ->withErrors([
        //                         'slug' => trans('permission.message.route_not_found')
        //                     ])
        //                     ->with('error', sprintf('%s', trans("global.failed_save")) );
        // }

        return $redirect;
    }

    public function destroy($id)
    {
        $this->checkDeveloper();
        \DB::beginTransaction();
        $row = Permission::find($id);
        $row->roles()->detach();
        $row->menus()->detach();
        $name = $row->name;

        $status = $this->delete($row, 'soft');

        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("permission.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("permission.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxPermission()
    {
        $params = request()->all();

        $list = Permission::select("id","name as text")
                        ->whereLike("name", $params["query"])
                        ->get()
                        ->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }

    private function checkDeveloper() 
    {
        $user = auth()->user();
        $user->isDeveloper() ? '' : abort(404); 
    }
}
