<?php

namespace App\Http\Controllers\Report;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Product;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Stock;
use Excel;

class StockController extends CoreController
{
    public function index()
    {
        $brands = Brand::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $categories = Category::active()->get()->pluck('name','id');

        if( request('ex') ) {
            return $this->exportExcel();
        }

        return Layout::render('report.stock.index', [
            'brands' => $brands,
            'branchs' => $branchs,
            'categories' => $categories,
        ]);
    }

    public function exportExcel()
    {
        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        
        $operator = Util::get('_sop');
        $operator = is_numeric($operator) ? trans("global.array.operator.$operator") : '';
        $stock = Util::get('_stc');
        $branch_id = Util::get('_bc');

        $query = Product::with([
            'stock' => function($q) use ($operator, $stock, $branch_id){
                $q->select(\DB::raw('SUM(stock) as stock'), 'product_id')->groupBy('product_id');

                if (!empty($branch_id)) {
                    $q->where('branch_id', $branch_id);
                    $q->groupBy('branch_id');
                }

                if (!empty($operator) && is_numeric($stock)) {
                    $q->havingRaw("SUM(stock) $operator $stock");
                }
            },
            'brand',
            'category'
        ]);

        $product_id = [];
        
        if (!empty($operator) || is_numeric($stock) || !empty($orderBy) || !empty($branch_id)) {
            $query->whereHas('stock', function($q) use ($operator, $stock, $branch_id){
                $q->select(\DB::raw('SUM(stock) as stock'), 'product_id')->groupBy('product_id');

                if (!empty($branch_id)) {
                    $q->where('branch_id', $branch_id);
                    $q->groupBy('branch_id');
                }
                
                if (!empty($operator) && is_numeric($stock)) {
                    $q->havingRaw("SUM(stock) $operator $stock");
                }
            });
            
            // $q_stock = Stock::select(\DB::raw('SUM(stock) as stock'), 'product_id')
            //             ->groupBy('product_id');

            // if ($orderBy == 'stock') {
            //     if ($sorting == 1) {
            //         $q_stock->orderByRaw('SUM(stock) DESC');
            //     } else if($sorting == 0) {
            //         $q_stock->orderByRaw('SUM(stock) ASC');
            //     }
            // }

            // if (!empty($operator) && is_numeric($stock)) {
            //     $q_stock->havingRaw("SUM(stock) $operator $stock");
            // }

            // if (!empty($branch_id)) {
            //     $q_stock->where('branch_id', $branch_id);
            //     $q_stock->groupBy('branch_id');
            // }

            // $product_id = $q_stock->get()->pluck('product_id')->toArray();
            // $product_id = array_slice($product_id, 0, Layout::ROW_PER_PAGE);
            
            // $query->whereIn('id', $product_id);
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_cd') )
        {
            $query->whereLike('code', Util::get('_cd'));
        }

        if( request('_bd') )
        {
            $query->where('brand_id', Util::get('_bd'));
        }

        if( request('_cg') )
        {
            $query->where('category_id', Util::get('_cg'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else if(empty($orderBy)) {
            $query->orderBy('updated_at', 'DESC');
        }

        $products = $query->get();

        if ($orderBy == 'stock') {
            $sort = $products->getCollection();
            
            if ($sorting == 1) {
                $sort = $sort->sortByDesc('stock.*.stock');
            } else if($sorting == 0) {
                $sort = $sort->sortBy('stock.*.stock');
            }

            $products->setCollection($sort);
        }

        $branch_name = trans('global.all');
        if (!empty($branch_id)) {
            $branch = Branch::find($branch_id);
            $branch_name = $branch ? $branch->name : $branch_name;
        }
        
        return Excel::download(new \App\Exports\StockExport($products, $branch_name), 'Stock Product.xlsx');
    }
}
