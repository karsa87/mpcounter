<?php

namespace App\Http\Controllers\Report;

use App\Models\PurchaseOrderDetail;
use App\Util\Base\CoreController;
use App\Models\SalesOrderDetail;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Supplier;
use App\Models\Employee;
use App\Models\Branch;
use App\Models\Bank;
use Excel;

class SupplierTaxController extends CoreController
{
    public function index()
    {
        $suppliers = Supplier::active()->hasTaxYes()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $banks = Bank::active()->get()->pluck('name','id');
        $employes = Employee::with('user')->active()->get()->pluck('name','user.id');

        if( request('ex') ) {
            return $this->exportExcel();
        }

        return Layout::render('report.supplier_tax.index', [
            'suppliers' => $suppliers,
            'branchs' => $branchs,
            'banks' => $banks,
            'employes' => $employes
        ]);
    }

    public function exportExcel()
    {
        $product_identity_id = PurchaseOrderDetail::select('product_identity_id')->whereHas('header', function($q){
            $q->whereHas('supplier', function($q1){
                $q1->hasTaxYes();
            });

            if( is_numeric(request('_sp')) )
            {
                $q->where('supplier_id', Util::get('_sp'));
            }
        })->groupBy('product_identity_id')->get();

        $product_identity_id = $product_identity_id->pluck('product_identity_id')->toArray();

        $products = SalesOrderDetail::with('productIdentity', 'productIdentity.product')
        ->select('product_identity_id')
        ->selectRaw('SUM(total_amount) as gross_profit, SUM(cost_of_goods * qty) as capital_price')
        ->whereIn('product_identity_id', $product_identity_id)
        ->groupBy('product_identity_id')
        ->whereHas('header', function($q) {
            if( request('_dt') )
            {
                $date = Util::get('_dt');
                $date = explode(" - ", $date);
                
                $q->whereBetween('date', $date);
            }

            if( is_numeric(request('_bk')) )
            {
                $q->where('bank_id', Util::get('_bk'));
            }

            if( is_numeric(request('_bc')) )
            {
                $q->where('branch_id', Util::get('_bc'));
            }

            if( is_numeric(request('_sts')) )
            {
                $q->status(Util::get('_sts'));
            }

            if( is_numeric(request('_tp')) )
            {
                $q->type(Util::get('_tp'));
            }
        })->get();
        

        $supplier_name = trans('global.all');
        if( is_numeric(request('_sp')) )
        {
            $supplier = Supplier::find(Util::get('_sp'));
            $supplier_name = $supplier ? $supplier->name : $supplier_name;
        }

        $filename = sprintf('%s - %s.xlsx', 'Supplier Tax', $supplier_name);
        
        return Excel::download(new \App\Exports\SupplierTaxExport($products), $filename);
    }
}
