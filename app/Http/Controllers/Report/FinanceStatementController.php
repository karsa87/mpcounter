<?php

namespace App\Http\Controllers\Report;

use App\Models\DebtPaymentPurchaseOrder;
use App\Models\ReturPurchaseOrderDetail;
use App\Models\DebtPaymentSalesOrder;
use App\Models\ReturSalesOrderDetail;
use App\Models\PurchaseOrderDetail;
use App\Models\ReturPurchaseOrder;
use App\Util\Base\CoreController;
use App\Models\SalesOrderDetail;
use App\Models\ReturSalesOrder;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Expend;
use App\Models\Branch;
use App\Models\Pulse;
use App\Models\Bank;
use Excel;

class FinanceStatementController extends CoreController
{
    public function index()
    {
        if( request('ex') ) {
            return $this->exportExcel();
        }

        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        return Layout::render('report.financial_statement.index', [
            'banks' => $banks,
            'branchs' => $branchs
        ]);
    }

    public function exportExcel()
    {
        $date = strtotime(sprintf("01-%s-%s", Util::get('_m'), Util::get('_y')));
        $star_date_this_month = sprintf('%s 00:00', date('Y-m-01', $date));
        $end_date_this_month  = sprintf('%s 23:59', date('Y-m-t', $date));
        $date = [$star_date_this_month,$end_date_this_month];

        // get query
        $q_so = SalesOrder::with('debtPayments')->whereBetween('date', $date);
        $q_po = PurchaseOrder::with('debtPayments')->whereBetween('date', $date);
        $q_rso = ReturSalesOrder::whereBetween('date', $date);
        $q_rpo = ReturPurchaseOrder::whereBetween('date', $date);
        $q_inc_increase = Incentive::increase()->whereBetween('date_transaction', $date);
        $q_inc_decrease = Incentive::decrease()->whereBetween('date_transaction', $date);
        $q_exp = Expend::with('categoryExpend')->whereBetween('expend_date', $date);
        $q_dso = DebtPaymentSalesOrder::whereBetween('date', $date);
        $q_dpo = DebtPaymentPurchaseOrder::whereBetween('date', $date);
        $q_pl = Pulse::whereBetween('date', $date);
        
        $bank_name = trans('global.all');
        if (request('_bk') || !empty(config('default.bank_id'))) {
            $id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            $bank = Bank::find($id);
            if ($bank) {
                $bank_name = $bank->name;

                $q_so->where('bank_id', $bank->id);
                $q_po->where('bank_id', $bank->id);
                $q_rso->where('bank_id', $bank->id);
                $q_rpo->where('bank_id', $bank->id);
                $q_inc_increase->where('bank_id', $bank->id);
                $q_inc_decrease->where('bank_id', $bank->id);
                $q_exp->where('bank_id', $bank->id);
                $q_dso->where('bank_id', $bank->id);
                $q_dpo->where('bank_id', $bank->id);
                $q_pl->where('bank_id', $bank->id);
            }
        }
        
        $branch_name = trans('global.all');
        if (request('_bc')) {
            $branch = Branch::find(Util::get('_bc'));
            if ($branch) {
                $branch_name = $branch->name;

                $q_so->where('branch_id', $branch->id);
                $q_po->where('branch_id', $branch->id);
                $q_rso->where('branch_id', $branch->id);
                $q_rpo->where('branch_id', $branch->id);
                $q_inc_increase->where('branch_id', $branch->id);
                $q_inc_decrease->where('branch_id', $branch->id);
                $q_exp->where('branch_id', $branch->id);
                $q_pl->where('branch_id', $branch->id);

                $q_dso->whereHas('salesOrder', function($q) use($branch) {
                    $q->where('branch_id', $branch->id);
                });

                $q_dpo->whereHas('purchaseOrder', function($q) use($branch) {
                    $q->where('branch_id', $branch->id);
                });
            }
        }

        $q_so_paid = clone $q_so;
        $q_so_paid = $q_so_paid->paid();

        $q_so_not_paid = clone $q_so;
        $q_so_not_paid = $q_so_not_paid->notPaid();
        
        $q_po_paid = clone $q_po;
        $q_po_paid = $q_po_paid->paid();

        $q_po_not_paid = clone $q_po;
        $q_po_not_paid = $q_po_not_paid->notPaid();
        
        $salesOrdersPaid = $q_so_paid->get();
        $salesOrdersNotPaid = $q_so_not_paid->get();
        $purchaseOrdersPaid = $q_po_paid->get();
        $purchaseOrdersNotPaid = $q_po_not_paid->get();
        
        $returSalesOrders = $q_rso->get();
        $returPurchaseOrders = $q_rpo->get();
        $incentiveIncrease = $q_inc_increase->get();
        $incentiveDecrease = $q_inc_decrease->get();
        $expends = $q_exp->get();

        if ($salesOrdersPaid->count() > 0 || $salesOrdersNotPaid->count() > 0) {
            $ids_paid = $salesOrdersPaid->pluck('id')->toArray();
            $ids_not_paid = $salesOrdersNotPaid->pluck('id')->toArray();
            
            $q_dso->whereNotIn('bank_id', array_merge($ids_paid, $ids_not_paid));
        }

        if ($purchaseOrdersPaid->count() > 0 || $purchaseOrdersNotPaid->count() > 0) {
            $ids_paid = $purchaseOrdersPaid->pluck('id')->toArray();
            $ids_not_paid = $purchaseOrdersNotPaid->pluck('id')->toArray();
            
            $q_dpo->whereNotIn('bank_id', array_merge($ids_paid, $ids_not_paid));
        }
        
        $debtPaymentSO = $q_dso->get();
        $debtPaymentPO = $q_dpo->get();
        $pulse = $q_pl->get();

        $view = \View::make('report.financial_statement.export', [
            'month' => Util::get('_m'),
            'year' => Util::get('_y'),
            'salesOrdersPaid' => $salesOrdersPaid,
            'salesOrdersNotPaid' => $salesOrdersNotPaid,
            'purchaseOrdersPaid' => $purchaseOrdersPaid,
            'purchaseOrdersNotPaid' => $purchaseOrdersNotPaid,
            
            'returSalesOrders' => $returSalesOrders,
            'returPurchaseOrders' => $returPurchaseOrders,
            'incentiveIncrease' => $incentiveIncrease,
            'incentiveDecrease' => $incentiveDecrease,
            'expends' => $expends,

            'debtPaymentSO' => $debtPaymentSO,
            'debtPaymentPO' => $debtPaymentPO,

            'pulse' => $pulse,

            'bank_name' => $bank_name
        ])->render(); 

        $pdf = \App::make('dompdf.wrapper');    
        $pdf->loadHtml($view);
        $pdf->setPaper('a4');
        $date = Util::formatDate(date('Y-m-d'));
        
        $filename = sprintf("%s %s %s %s.pdf", trans('financial_statement.title'), $bank_name, $date, date('H:i'));
        return $pdf->download($filename);
    }
}
