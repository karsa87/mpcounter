<?php

namespace App\Http\Controllers\Report;

use App\Models\DebtPaymentPurchaseOrder;
use App\Models\ReturPurchaseOrderDetail;
use App\Models\DebtPaymentSalesOrder;
use App\Models\ReturSalesOrderDetail;
use App\Models\PurchaseOrderDetail;
use App\Util\Base\CoreController;
use App\Models\SalesOrderDetail;
use App\Models\ReturSalesOrder;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Expend;
use App\Models\Branch;
use App\Models\Pulse;
use App\Models\Bank;
use Excel;

class LossProfitController extends CoreController
{
    public function index()
    {
        if( request('ex') ) {
            return $this->exportExcel();
        }

        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        return Layout::render('report.loss_profit.index', [
            'banks' => $banks,
            'branchs' => $branchs,
        ]);
    }

    public function exportExcel()
    {
        $date = strtotime(sprintf("01-%s-%s", Util::get('_m'), Util::get('_y')));
        $star_date_this_month = sprintf('%s 00:00', date('Y-m-01', $date));
        $end_date_this_month  = sprintf('%s 23:59', date('Y-m-t', $date));
        $date = [$star_date_this_month,$end_date_this_month];

        $bank_name = trans('global.all');
        $bank_id = null;
        if (request('_bk') || !empty(config('default.bank_id'))) {
            $bk_id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            $bank = Bank::find($bk_id);
            if ($bank) {
                $bank_id = $bank->id;
                $bank_name = $bank->name;
            }
        }
        $branch_name = trans('global.all');
        $branch_id = null;
        if (request('_bc')) {
            $branch = Branch::find(Util::get('_bc'));
            if ($branch) {
                $branch_id = $branch->id;
                $branch_name = $branch->name;
            }
        }

        // get income
        $q_sod = SalesOrderDetail::whereHas('header', function($q) use($date, $bank_id, $branch_id) {
                    $q->whereBetween('date', $date);
                    if ($bank_id) {
                        $q->where('bank_id', $bank_id);
                    }
                    if ($branch_id) {
                        $q->where('branch_id', $branch_id);
                    }
                })
                ->with([
                    'product' => function($q) {
                        $q->with('category');
                    }
                ]);

        $q_incentiveIncrease = Incentive::with('bank')->increase()->whereBetween('date_transaction', $date);
        $q_exp = Expend::with('categoryExpend', 'bank')->whereBetween('expend_date', $date);
        $q_so = SalesOrder::whereBetween('date', $date);
        $q_pl = Pulse::whereBetween('date', $date);

        if ($bank_id) {
            $q_incentiveIncrease->where('bank_id', $bank_id);
            $q_exp->where('bank_id', $bank_id);
            $q_so->where('bank_id', $bank_id);
            $q_pl->where('bank_id', $bank_id);
        }

        if ($branch_id) {
            $q_incentiveIncrease->where('branch_id', $branch_id);
            $q_exp->where('branch_id', $branch_id);
            $q_so->where('branch_id', $branch_id);
            $q_pl->where('branch_id', $branch_id);
        }

        // get total omset sales order by category
        $q_sod_omset = clone $q_sod;
        $sod_omset = $q_sod_omset->select(\DB::raw("SUM(sell_price * qty) as amount"), 'product_id')
                    ->groupBy('product_id')
                    ->get();
        $sodOmset = $sod_omset->groupBy('product.category.name');

        // get loss profit
        $q_sod_lp = clone $q_sod;
        $sod_lp = $q_sod_lp->select(\DB::raw("SUM((sell_price - cost_of_goods) * qty) as amount"), 'product_id')
                    ->groupBy('product_id')
                    ->get();
        $sodLP = $sod_lp->groupBy('product.category.name');
        
        $incentiveIncrease = $q_incentiveIncrease->get()->groupBy('bank.name');
        $salesOrders = $q_so->get();
        $expends = $q_exp->get();
        $pulse = $q_pl->get();

        $view = \View::make('report.loss_profit.export', [
            'month' => Util::get('_m'),
            'year' => Util::get('_y'),
            'salesOrders' => $salesOrders,
            'sodOmset' => $sodOmset,
            'sodLP' => $sodLP,
            'incentiveIncrease' => $incentiveIncrease,
            'expends' => $expends,
            'pulse' => $pulse,
            'bank_name' => $bank_name,
            'branch_name' => $branch_name
        ])->render(); 

        $pdf = \App::make('dompdf.wrapper');    
        $pdf->loadHtml($view);
        $pdf->setPaper('a4');
        $date = Util::formatDate(date('Y-m-d'));
        
        $filename = sprintf("%s %s %s %s.pdf", trans('loss_profit.title'), $bank_name, $date, date('H:i'));
        return $pdf->download($filename);
    }
}
