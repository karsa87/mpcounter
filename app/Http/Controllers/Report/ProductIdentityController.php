<?php

namespace App\Http\Controllers\Report;

use App\Util\Base\CoreController;
use App\Models\ProductIdentity;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Category;
use App\Models\Product;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Stock;
use Excel;

class ProductIdentityController extends CoreController
{
    public function index()
    {
        $brands = Brand::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $categories = Category::active()->get()->pluck('name','id');
        $products = Product::active()->get()->pluck('name','id');

        if( request('ex') ) {
            return $this->exportExcel();
        }

        return Layout::render('report.product_identity.index', [
            'brands' => $brands,
            'branchs' => $branchs,
            'categories' => $categories,
            'products' => $products
        ]);
    }

    public function exportExcel()
    {
        $operator = Util::get('_sop');
        $operator = is_numeric($operator) ? trans("global.array.operator.$operator") : '';
        $stock = Util::get('_stc');
        $branch_id = Util::get('_bc');

        $query = ProductIdentity::with('product', 'branch')->orderBy('product_id');

        if ($branch_id) {
            $query->where('branch_id', $branch_id);
        }

        if (!empty($operator) && is_numeric($stock)) {
            $query->where('stock', $operator, $stock);
        }

        if( request('_pid') )
        {
            $query->where('product_id', Util::get('_pid'));
        }

        if( request('_bd') || request('_cg') )
        {
            $brand_id = request('_bd');
            $category_id = request('_cg');

            $query->whereHas('product', function($q) use($brand_id, $category_id) {
                if ($brand_id) {
                    $q->where('brand_id', $brand_id);
                }

                if ($category_id) {
                    $q->where('category_id', $category_id);
                }
            });
        }

        $products = $query->get();
        // dd($products->toArray());
        $products = $products->sortBy('product.name');

        return Excel::download(new \App\Exports\ProductIdentityExport($products), trans('product.label.product_identity') . '.xlsx');
    }
}
