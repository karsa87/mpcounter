<?php

namespace App\Http\Controllers\Report;

use App\Models\DebtPaymentPurchaseOrder;
use App\Models\DebtPaymentSalesOrder;
use App\Models\ReturPurchaseOrder;
use App\Util\Base\CoreController;
use App\Models\ReturSalesOrder;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Expend;
use App\Models\Branch;
use App\Models\Pulse;
use App\Models\Bank;
use Excel;

class IncomeExpendController extends CoreController
{
    public function index()
    {
        if( request('ex') ) {
            return $this->exportExcel();
        }

        $branchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('report.income_expend.index', [
            'branchs' => $branchs
        ]);
    }

    public function exportExcel()
    {
        $income = $this->income();
        $expend = $this->expend();
        $param = array_merge($income, $expend);
        // $param['total_before'] = $this->incomeExpendBefore();

        return Excel::download(new \App\Exports\IncomeExpendExport($param), sprintf("%s - %s.xlsx", trans('income_all.title'), trans('expend_all.title')));
    }

    private function incomeExpendBefore()
    {
        $income = $this->income(true);
        $expend = $this->expend(true);
        
        return $income['total_income_all'] - $expend['total_expend_all'];
    }

    private function income($before_all = false)
    {
        $q_so = SalesOrder::with('bank');
        $q_dso = DebtPaymentSalesOrder::with('bank');
        $q_rpo = ReturPurchaseOrder::with('bank');
        $q_pl = Pulse::with('bank');
        $q_incentiveIncrease = Incentive::with('bank')->increase();

        if( request('_dt') )
        {
            if ($before_all) {
                $date = sprintf('%s 00:00', Util::get('_dt'));

                $q_so->where('date', '<=', $date);
                $q_dso->where('date', '<=', $date);
                $q_rpo->where('date', '<=', $date);
                $q_pl->where('date', '<=', $date);
                $q_incentiveIncrease->where('date_transaction', '<=', $date);
            } else {
                $date1 = sprintf('%s 00:00', Util::get('_dt'));
                $date2 = sprintf('%s 23:59', Util::get('_dt'));
                $date = [$date1, $date2];

                $q_so->whereBetween('date', $date);
                $q_dso->whereBetween('date', $date);
                $q_rpo->whereBetween('date', $date);
                $q_pl->whereBetween('date', $date);
                $q_incentiveIncrease->whereBetween('date_transaction', $date);
            }
        }

        if( request('_bc') )
        {
            $branch_id = Util::get('_bc');
            
            $q_so->whereBranchId($branch_id);
            $q_dso->whereHas('salesOrder', function($q) use($branch_id) {
                $q->whereBranchId($branch_id);
            });
            $q_rpo->whereBranchId($branch_id);
            $q_pl->whereBranchId($branch_id);
            $q_incentiveIncrease->whereBranchId($branch_id);
        }

        if( request('_bk') || !empty(config('default.bank_id')) )
        {
            $bank_id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            
            $q_so->where('bank_id', $bank_id);
            $q_dso->where('bank_id', $bank_id);
            $q_rpo->where('bank_id', $bank_id);
            $q_pl->where('bank_id', $bank_id);
            $q_incentiveIncrease->where('bank_id', $bank_id);
        }

        $sales_order =  collect([]);
        $debtPaymentSo = collect([]);
        $returPO = collect([]);
        $incentiveIncome = collect([]);
        $pulse = collect([]);
        $total_dpso = 0;
        $total_rpo = 0;
        $total_so = 0;
        $total_incentive_income = 0;
        $total_pulse = 0;
        $income_type = trans('global.all');
        if (request('_et')) {
            $income_type = trans('income_all.list.income_type.' . Util::get('_et'));
            switch (Util::get('_et')) {
                case 1:
                    $sales_order = $q_so->get();
                    $total_so = $sales_order->sum('paid_off');
                    break;
                
                case 2:
                    $debtPaymentSo = $q_dso->get();
                    $total_dpso = $debtPaymentSo->sum('paid');
                    break;
                
                case 3:
                    $returPO = $q_rpo->get();
                    $total_rpo = $returPO ->sum('total_amount');
                    break;

                case 4:
                    $incentiveIncome = $q_incentiveIncrease->get();
                    $total_incentive_income = $incentiveIncome->sum('amount');
                    break;

                case 5:
                    $pulse = $q_pl->get();
                    $total_pulse = $pulse->sum('sell_price');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $sales_order = $q_so->get();
            $debtPaymentSo = $q_dso->get();
            $returPO = $q_rpo->get();
            $incentiveIncome = $q_incentiveIncrease->get();
            $pulse = $q_pl->get();

            $total_so = $sales_order->sum('paid_off');
            $total_dpso = $debtPaymentSo->sum('paid');
            $total_rpo = $returPO ->sum('total_amount');
            $total_incentive_income = $incentiveIncome->sum('amount');
            $total_pulse = $pulse->sum('sell_price');
        }

        $total_income_all = $total_dpso + $total_so + $total_rpo + $total_incentive_income + $total_pulse;
        
        return [
            'salesOrders' => $sales_order,
            'debtPaymentSo' => $debtPaymentSo,
            'returPO' => $returPO,
            'incentiveIncome' => $incentiveIncome,
            'pulse' => $pulse,
            'total_income_all' => $total_income_all
        ];
    }

    private function expend($before_all = false)
    {
        $q_po = PurchaseOrder::with('bank');
        $q_dpo = DebtPaymentPurchaseOrder::with('bank');
        $q_rso = ReturSalesOrder::with('bank');
        $q_incentiveDecrease = Incentive::with('bank')->decrease();
        $q_exp = Expend::with('bank');

        if( request('_dt') )
        {
            if ($before_all) {
                $date = sprintf('%s 00:00', Util::get('_dt'));
                
                $q_po->where('date', '<=', $date);
                $q_dpo->where('date', '<=', $date);
                $q_rso->where('date', '<=', $date);
                $q_incentiveDecrease->where('date_transaction', '<=', $date);
                $q_exp->where('expend_date', '<=', $date);
            } else {
                $date1 = sprintf('%s 00:00', Util::get('_dt'));
                $date2 = sprintf('%s 23:59', Util::get('_dt'));
                $date = [$date1, $date2];
                
                $q_po->whereBetween('date', $date);
                $q_dpo->whereBetween('date', $date);
                $q_rso->whereBetween('date', $date);
                $q_incentiveDecrease->whereBetween('date_transaction', $date);
                $q_exp->whereBetween('expend_date', $date);
            }
        }

        if( request('_bc') )
        {
            $branch_id = Util::get('_bc');

            $q_po->whereBranchId($branch_id);
            $q_dpo->whereHas('purchaseOrder', function($q) use($branch_id) {
                $q->whereBranchId($branch_id);
            });
            $q_rso->whereBranchId($branch_id);
            $q_incentiveDecrease->whereBranchId($branch_id);
            $q_exp->whereBranchId($branch_id);
        }

        if( request('_bk') || !empty(config('default.bank_id')) )
        {
            $bank_id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            
            $q_po->where('bank_id', $bank_id);
            $q_dpo->where('bank_id', $bank_id);
            $q_rso->where('bank_id', $bank_id);
            $q_incentiveDecrease->where('bank_id', $bank_id);
            $q_exp->where('bank_id', $bank_id);
        }

        if( request('_cexp') ) {
            $category_expend_id = Util::get('_cexp');
            $q_exp->where('category_expend_id', $category_expend_id);
        }

        $purchase_order = collect([]);
        $debtPaymentPo = collect([]);
        $returSO = collect([]);
        $incentiveExpend = collect([]);
        $expend = collect([]);
        $total_dppo = 0;
        $total_rso = 0;
        $total_po = 0;
        $total_incentive_expend = 0;
        $total_expend = 0;
        $expend_type = trans('global.all');
        if (request('_et')) {
            $expend_type = trans('expend_all.list.expend_type.' . Util::get('_et'));
            switch (Util::get('_et')) {
                case 1:
                    $purchase_order = $q_po->get();
                    $total_po = $purchase_order->sum('paid_off');
                    break;
                
                case 2:
                    $debtPaymentPo = $q_dpo->get();
                    $total_dppo = $debtPaymentPo->sum('paid');
                    break;
                
                case 3:
                    $returSO = $q_rso->get();
                    $total_rso = $returSO ->sum('total_amount');
                    break;

                case 4:
                    $incentiveExpend = $q_incentiveDecrease->get();
                    $total_incentive_expend = $incentiveExpend->sum('amount');
                    break;

                case 5:
                    $expend = $q_exp->get();
                    $total_expend = $expend->sum('amount');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $purchase_order = $q_po->get();
            $debtPaymentPo = $q_dpo->get();
            $returSO = $q_rso->get();
            $incentiveExpend = $q_incentiveDecrease->get();
            $expend = $q_exp->get();

            $total_po = $purchase_order->sum('paid_off');
            $total_dppo = $debtPaymentPo->sum('paid');
            $total_rso = $returSO ->sum('total_amount');
            $total_incentive_expend = $incentiveExpend->sum('amount');
            $total_expend = $expend->sum('amount');
        }

        $total_expend_all = $total_dppo + $total_rso + $total_po + $total_incentive_expend + $total_expend;

        return [
            'purchaseOrders' => $purchase_order,
            'debtPaymentPo' => $debtPaymentPo,
            'returSO' => $returSO,
            'incentiveExpend' => $incentiveExpend,
            'expends' => $expend,
            'total_expend_all' => $total_expend_all
        ];
    }
}
