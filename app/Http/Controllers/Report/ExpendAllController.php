<?php

namespace App\Http\Controllers\Report;

use App\Models\DebtPaymentPurchaseOrder;
use App\Util\Base\CoreController;
use App\Models\ReturSalesOrder;
use App\Models\CategoryExpend;
use App\Models\PurchaseOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Expend;
use App\Models\Branch;
use App\Models\Bank;
use Excel;

class ExpendAllController extends CoreController
{
    public function index()
    {
        if( request('ex') ) {
            return $this->exportExcel();
        }

        $categoryExp = CategoryExpend::active()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('report.expend_all.index', [
            'categoryExp' => $categoryExp,
            'branchs' => $branchs
        ]);
    }

    public function exportExcel()
    {
        $q_po = PurchaseOrder::with('bank', 'details', 'details.productIdentity', 'details.product');
        $q_dpo = DebtPaymentPurchaseOrder::with('bank');
        $q_rso = ReturSalesOrder::with('bank');
        $q_incentiveDecrease = Incentive::with('bank')->decrease();
        $q_exp = Expend::with('bank');

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $q_po->whereBetween('date', $date);
            $q_dpo->whereBetween('date', $date);
            $q_rso->whereBetween('date', $date);
            $q_incentiveDecrease->whereBetween('date_transaction', $date);
            $q_exp->whereBetween('expend_date', $date);
        }

        if( request('_bc') )
        {
            $branch_id = Util::get('_bc');

            $q_po->whereBranchId($branch_id);
            $q_dpo->whereHas('purchaseOrder', function($q) use($branch_id) {
                $q->whereBranchId($branch_id);
            });
            $q_rso->whereBranchId($branch_id);
            $q_incentiveDecrease->whereBranchId($branch_id);
            $q_exp->whereBranchId($branch_id);
        }

        if( request('_bk') || !empty(config('default.bank_id')) )
        {
            $bank_id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            
            $q_po->where('bank_id', $bank_id);
            $q_dpo->where('bank_id', $bank_id);
            $q_rso->where('bank_id', $bank_id);
            $q_incentiveDecrease->where('bank_id', $bank_id);
            $q_exp->where('bank_id', $bank_id);
        }

        if( request('_cexp') ) {
            $category_expend_id = Util::get('_cexp');
            $q_exp->where('category_expend_id', $category_expend_id);
        }

        $purchase_order = collect([]);
        $debtPaymentPo = collect([]);
        $returSO = collect([]);
        $incentiveExpend = collect([]);
        $expend = collect([]);
        $total_dppo = 0;
        $total_rso = 0;
        $total_po = 0;
        $total_incentive_expend = 0;
        $total_expend = 0;
        $expend_type = trans('global.all');
        if (request('_et')) {
            $expend_type = trans('expend_all.list.expend_type.' . Util::get('_et'));
            switch (Util::get('_et')) {
                case 1:
                    $purchase_order = $q_po->get();
                    $total_po = $purchase_order->sum('paid_off');
                    break;
                
                case 2:
                    $debtPaymentPo = $q_dpo->get();
                    $total_dppo = $debtPaymentPo->sum('paid');
                    break;
                
                case 3:
                    $returSO = $q_rso->get();
                    $total_rso = $returSO ->sum('total_amount');
                    break;

                case 4:
                    $incentiveExpend = $q_incentiveDecrease->get();
                    $total_incentive_expend = $incentiveExpend->sum('amount');
                    break;

                case 5:
                    $expend = $q_exp->get();
                    $total_expend = $expend->sum('amount');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $purchase_order = $q_po->get();
            $debtPaymentPo = $q_dpo->get();
            $returSO = $q_rso->get();
            $incentiveExpend = $q_incentiveDecrease->get();
            $expend = $q_exp->get();

            $total_po = $purchase_order->sum('paid_off');
            $total_dppo = $debtPaymentPo->sum('paid');
            $total_rso = $returSO ->sum('total_amount');
            $total_incentive_expend = $incentiveExpend->sum('amount');
            $total_expend = $expend->sum('amount');
        }

        $total_expend_all = $total_dppo + $total_rso + $total_po + $total_incentive_expend + $total_expend;

        return Excel::download(new \App\Exports\ExpendAllExport([
            'purchaseOrders' => $purchase_order,
            'debtPaymentPo' => $debtPaymentPo,
            'returSO' => $returSO,
            'incentiveExpend' => $incentiveExpend,
            'expends' => $expend,
            'total_expend_all' => $total_expend_all
        ]), sprintf("%s - %s.xlsx", trans('expend_all.title'), $expend_type));
    }
}
