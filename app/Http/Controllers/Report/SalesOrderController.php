<?php

namespace App\Http\Controllers\Report;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Models\SalesOrder;
use App\Util\Base\Layout;
use App\Models\Employee;
use App\Models\Branch;
use App\Models\Member;
use App\Models\Bank;
use Excel;

class SalesOrderController extends CoreController
{
    public function index()
    {
        $sales = Employee::active()->sales()->get()->pluck('name','id');
        $branchs = Branch::active()->get()->pluck('name','id');
        $banks = Bank::active()->orderByDefault()->get()->pluck('name','id');
        $members = Member::active()->get()->pluck('name','id');
        $employes = Employee::with('user')->active()->get()->pluck('name','user.id');

        if( request('ex') ) {
            return $this->exportExcel();
        }

        return Layout::render('report.sales_order.index', [
            'sales' => $sales,
            'branchs' => $branchs,
            'banks' => $banks,
            'members' => $members,
            'employes' => $employes
        ]);
    }

    public function exportExcel()
    {
        $query = SalesOrder::with('member', 'bank', 'branch', 'sales', 'retur', 'debtPayments');

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( is_numeric(request('_nivc')) )
        {
            $query->whereLike('no_invoice', Util::get('_nivc'));
        }

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $query->whereBetween('date', $date);
        }

        if( is_numeric(request('_bk')) || !empty(config('default.bank_id')) )
        {
            $bk_id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            $query->where('bank_id', $bk_id);
        }

        if( is_numeric(request('_bc')) )
        {
            $query->where('branch_id', Util::get('_bc'));
        }

        if( is_numeric(request('_sls')) )
        {
            $query->where('sales_id', Util::get('_sls'));
        }

        if( is_numeric(request('_mb')) )
        {
            $query->where('member_id', Util::get('_mb'));
        }

        if( is_numeric(request('_sts')) )
        {
            $query->status(Util::get('_sts'));
        }

        if( is_numeric(request('_tp')) )
        {
            $query->type(Util::get('_tp'));
        }

        if( is_numeric(request('_pic')) )
        {
            $query->where('created_by', Util::get('_pic'));
        }

        $sales_orders = $query->get();
        
        return Excel::download(new \App\Exports\SalesOrderExport($sales_orders), 'Sales Order.xlsx');
    }
}
