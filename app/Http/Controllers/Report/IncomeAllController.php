<?php

namespace App\Http\Controllers\Report;

use App\Models\DebtPaymentSalesOrder;
use App\Models\ReturPurchaseOrder;
use App\Util\Base\CoreController;
use App\Models\SalesOrder;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Incentive;
use App\Models\Branch;
use App\Models\Pulse;
use App\Models\Bank;
use Excel;

class IncomeAllController extends CoreController
{
    public function index()
    {
        if( request('ex') ) {
            return $this->exportExcel();
        }

        $branchs = Branch::active()->get()->pluck('name','id');

        return Layout::render('report.income_all.index', [
            'branchs' => $branchs
        ]);
    }

    public function exportExcel()
    {
        $q_so = SalesOrder::with('bank', 'details', 'details.productIdentity', 'details.product');
        $q_dso = DebtPaymentSalesOrder::with('bank');
        $q_rpo = ReturPurchaseOrder::with('bank');
        $q_pl = Pulse::with('bank');
        $q_incentiveIncrease = Incentive::with('bank')->increase();

        if( request('_dt') )
        {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
            
            $q_so->whereBetween('date', $date);
            $q_dso->whereBetween('date', $date);
            $q_rpo->whereBetween('date', $date);
            $q_pl->whereBetween('date', $date);
            $q_incentiveIncrease->whereBetween('date_transaction', $date);
        }

        if( request('_bc') )
        {
            $branch_id = Util::get('_bc');
            
            $q_so->whereBranchId($branch_id);
            $q_dso->whereHas('salesOrder', function($q) use($branch_id) {
                $q->whereBranchId($branch_id);
            });
            $q_rpo->whereBranchId($branch_id);
            $q_pl->whereBranchId($branch_id);
            $q_incentiveIncrease->whereBranchId($branch_id);
        }

        if( request('_bk') || !empty(config('default.bank_id')) )
        {
            $bank_id = !empty(config('default.bank_id')) ? config('default.bank_id') : Util::get('_bk');
            
            $q_so->where('bank_id', $bank_id);
            $q_dso->where('bank_id', $bank_id);
            $q_rpo->where('bank_id', $bank_id);
            $q_pl->where('bank_id', $bank_id);
            $q_incentiveIncrease->where('bank_id', $bank_id);
        }

        $sales_order =  collect([]);
        $debtPaymentSo = collect([]);
        $returPO = collect([]);
        $incentiveIncome = collect([]);
        $pulse = collect([]);
        $total_dpso = 0;
        $total_rpo = 0;
        $total_so = 0;
        $total_incentive_income = 0;
        $total_pulse = 0;
        $income_type = trans('global.all');
        if (request('_et')) {
            $income_type = trans('income_all.list.income_type.' . Util::get('_et'));
            switch (Util::get('_et')) {
                case 1:
                    $sales_order = $q_so->get();
                    $total_so = $sales_order->sum('paid_off');
                    break;
                
                case 2:
                    $debtPaymentSo = $q_dso->get();
                    $total_dpso = $debtPaymentSo->sum('paid');
                    break;
                
                case 3:
                    $returPO = $q_rpo->get();
                    $total_rpo = $returPO ->sum('total_amount');
                    break;

                case 4:
                    $incentiveIncome = $q_incentiveIncrease->get();
                    $total_incentive_income = $incentiveIncome->sum('amount');
                    break;

                case 5:
                    $pulse = $q_pl->get();
                    $total_pulse = $pulse->sum('sell_price');
                    break;

                default:
                    # code...
                    break;
            }
        } else {
            $sales_order = $q_so->get();
            $debtPaymentSo = $q_dso->get();
            $returPO = $q_rpo->get();
            $incentiveIncome = $q_incentiveIncrease->get();
            $pulse = $q_pl->get();

            $total_so = $sales_order->sum('paid_off');
            $total_dpso = $debtPaymentSo->sum('paid');
            $total_rpo = $returPO ->sum('total_amount');
            $total_incentive_income = $incentiveIncome->sum('amount');
            $total_pulse = $pulse->sum('sell_price');
        }

        $total_income_all = $total_dpso + $total_so + $total_rpo + $total_incentive_income + $total_pulse;
        
        return Excel::download(new \App\Exports\IncomeAllExport([
            'salesOrders' => $sales_order,
            'debtPaymentSo' => $debtPaymentSo,
            'returPO' => $returPO,
            'incentiveIncome' => $incentiveIncome,
            'pulse' => $pulse,
            'total_income_all' => $total_income_all
        ]), sprintf("%s - %s.xlsx", trans('income_all.title'), $income_type));
    }
}
