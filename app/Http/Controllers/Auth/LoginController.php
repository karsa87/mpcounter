<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Util\Events\UserLogin;
use App\Util\Base\CoreController;

class LoginController extends CoreController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return $this->render('auth.login',[
            'meta_title' => trans('auth.meta.title'),
            'meta_description' => trans('auth.meta.description'),
            'meta_keyword' => trans('auth.meta.keyword'),
        ]);
    }

    public function login()
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required|min:5',
        ]);

        $input = request()->all();
        $user = User::where(function($q) use($input) {
            $q->where('email', $input['email'])
                ->orWhere('username',$input['email']);
        })->first();
        
        if (empty($user)) {
            return redirect('login')
                        ->withErrors([
                            'message' => trans('auth.message.username_not_found')
                        ])
                        ->withInput();
        } elseif (!\Hash::check(request('password'), $user->password)) {
            return redirect('login')
                        ->withErrors([
                            'message' => trans('auth.message.password_not_correct')
                        ])
                        ->withInput();
        } elseif (!$user->isActive()) {
            return redirect('login')
                        ->withErrors([
                            'message' => trans('auth.message.username_deactived')
                        ])
                        ->withInput();
        } else {
            \Auth::login($user, false);

            event(new UserLogin($user));

            return redirect()->route('dashboard')->with('message', 'Success login');
        }
    }

    public function logout()
    {
        \Session::flush();
        \Auth::logout();
        return redirect('login');
    }
}
