<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogStockTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class ReturSalesOrderDetail extends Model
{
    use SoftDeletes, LogHistoryTrait, LogStockTransactionTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;
    const PREFIX = 'DC-';

    protected $table = 'retur_sales_order_detail';
    protected $fillable = ['retur_sales_order_id', 'sales_order_detail_id', 'product_id', 'product_identity_id', 'promo_id', 'qty', 'sell_price', 'cost_of_goods', 'discount_promo', 'total_amount', 'total_amount_first', 'discount', 'total_discount'];

    public function header()
    {
        return $this->belongsTo(ReturSalesOrder::class, 'retur_sales_order_id')->withTrashed();
    }

    public function salesOrderDetail()
    {
        return $this->belongsTo(SalesOrderDetail::class)->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function productIdentity()
    {
        return $this->belongsTo(ProductIdentity::class);
    }

    public function promo()
    {
        return $this->belongsTo(Promo::class)->withTrashed();
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
}
