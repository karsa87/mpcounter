<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;
    
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    /**
     * 1 identity for 1 product 
     * example :
     * Imei Handphone
     * */
    const IDENTIFIER_ONE = 1;

    /**
     * many identity for 1 product 
     * example :
     * Barcde for handphone
     * */
    const IDENTIFIER_MULTI = 2;

    /**
     * no identity for 1 product 
     * example :
     * makanan donut
     * */
    const IDENTIFIER_NO = 0;

    protected $table = 'category';
    protected $fillable = ['name', 'status', 'identifier'];

    /*************
    * Relation
    ***************/
    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function promo()
    {
        return $this->hasMany(Promo::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }

    public function scopeOneIdentifier($query)
    {
        return $query->where('status', self::IDENTIFIER_ONE);
    }

    public function scopeMultiIdentifier($query)
    {
        return $query->where('status', self::IDENTIFIER_MULTI);
    }

    public function scopeNoIdentifier($query)
    {
        return $query->where('status', self::IDENTIFIER_NO);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeIdentifier($query, $identifier)
    {
        return $query->where('identifier', $identifier);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }
    
    public function oneIdentifier()
    {
        return $this->status == self::IDENTIFIER_ONE;
    }
    
    public function multiIdentifier()
    {
        return $this->status == self::IDENTIFIER_MULTI;
    }

    public function noIdentifier()
    {
        return $this->status == self::IDENTIFIER_NO;
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        }

        return parent::save($options);
    }

    protected function checkRelation()
    {
        $total = $this->product->count() + $this->promo->count();

        return $total;
    }
}
