<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\LogStockTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class MutationProductDetail extends Model
{
    use LogHistoryTrait, LogStockTransactionTrait, ScopeLike;

    protected $table = 'mutation_product_detail';
    protected $fillable = ['mutation_product_id', 'product_id', 'product_identity_id', 'to_product_identity_id', 'qty', 'cost_of_goods', 'sell_price', 'total_amount'];

    public function header()
    {
        return $this->belongsTo(MutationProduct::class, 'mutation_product_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function productIdentity()
    {
        return $this->belongsTo(ProductIdentity::class);
    }

    public function toProductIdentity()
    {
        return $this->belongsTo(ProductIdentity::class, 'to_product_identity_id')->withTrashed();
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
    
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        
        static::created(function ($detail) {
            $header = $detail->header;
            
            $detail->stockOut($detail->product_id,  $header->from_branch_id, $detail->qty, $header->no_mutation);
            $detail->stockIn($detail->product_id, $header->to_branch_id, $detail->qty, $header->no_mutation);
        });
        
        static::updated(function ($detail) {
            $header = $detail->header;
            
            $detail->stockOut($detail->product_id,  $header->from_branch_id, $detail->qty, $header->no_mutation);
            $detail->stockIn($detail->product_id, $header->to_branch_id, $detail->qty, $header->no_mutation);
        });
        
        static::deleted(function ($detail) {
            $header = $detail->header;
            
            $detail->stockIn($detail->product_id,  $header->from_branch_id, $detail->qty, $header->no_mutation);
            $detail->stockOut($detail->product_id, $header->to_branch_id, $detail->qty, $header->no_mutation);
        });
    }
}
