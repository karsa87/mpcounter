<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\ImageTrait;
use Laravel\Passport\Client;
use App\Traits\LogHistoryTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasRoleAndPermission, LogHistoryTrait, ScopeLike, ImageTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'google_id', 'email', 'images', 'nowlogin', 'loginip', 'lastlogin', 'lastloginip', 'status', 'is_api', 'username', 'email_verified_at', 'password','branch_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function domains()
    {
        return $this->belongsToMany(Domain::class, 'user_domain', 'user_id', 'domain_id');
    }

    public function log_activity()
    {
        return $this->belongsToMany(Permission::class, 'user_log_activity', 'user_id', 'permission_id')->withPivot('access_date');
    }

    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function branch()
    {
        return $this->belongsTo(branch::class)->withTrashed();
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    public function save(array $options = [])
    {
        if ($this->password && $this->password != $this->getOriginal('password')) {
            $this->password = \Hash::make($this->password);
        } else {
            unset($this->password);
        }

        return parent::save($options);
    }

    /**
     * Check if the user has all permissions.
     *
     * @param int|string|array $permission
     *
     * @return bool
     */
    public function hasOrPermissions($permission)
    {
        $result = FALSE;
        foreach ($this->getArrayFrom($permission) as $permission) {
            if ($this->checkPermission($permission)) {
                $result = TRUE;
            }
        }

        return $result;
    }

    public function has($permissions)
    {
        $has = FALSE;
        $has_permissions = \Session::get('permission');
        $has_permissions = $has_permissions->pluck('slug','id')->toArray();
        
        if( is_array($permissions) )
        {
            foreach ($permissions as $p)
            {
                if( is_numeric(array_search($p, $has_permissions)) )
                {
                    $has = TRUE;
                }
            }
        }
        elseif( is_numeric(array_search($permissions, $has_permissions)) )
        {
            $has = TRUE;
        }

        return $has;
    }

    public function underAdminWika() 
    {
        return $this->level() < Role::LEVEL_ADMINISTRATOR_WIKA;
    }

    public function isEmployee() 
    {
        return $this->level() == Role::LEVEL_EMPLOYEE;
    }

    /**********
    * SCOPE
    *********/
    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status',0);
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function getEmployeeNameAttribute()
    {
        $name = $this->employee ? $this->employee->name : null;
        $user_login = auth()->user();
        if ($user_login && $name == null) {
            $name = ($user_login->employee ? $user_login->employee->name : auth()->user()->username);
        }
        
        return $name;
    }
}
