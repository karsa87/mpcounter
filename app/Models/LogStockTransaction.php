<?php

namespace App\Models;

use App\Traits\ScopeLike;
use Illuminate\Database\Eloquent\Model;

class LogStockTransaction extends Model
{
    use ScopeLike;

    const TYPE_OUT = 0;
    const TYPE_IN = 1;

    protected $table = 'log_stock_transaction';
    protected $fillable = ['log_datetime', 'user_id', 'stock_id', 'transaction_id', 'transaction_type', 'type', 'information', 'table', 'stock_in', 'stock_out', 'stock_before', 'stock_after'];

    /**
     * Get the user record associated with the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
    
    /**
     * Get the stock record associated with the stock.
     */
    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }

    /**
     * Get the transaction record associated with the table transaction.
     */
    public function transaction()
    {
        return $this->morphTo();
    }

    /*************
    * SCOPE
    ***************/
    public function scopeTypeOut($query)
    {
        return $query->where('type', self::TYPE_OUT);
    }

    public function scopeTypeIn($query)
    {
        return $query->where('type', self::TYPE_IN);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isOut()
    {
        return $this->type == self::TYPE_OUT;
    }

    public function isIn()
    {
        return $this->type == self::TYPE_IN;
    }
}
