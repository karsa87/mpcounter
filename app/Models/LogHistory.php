<?php

namespace App\Models;

use App\Traits\ScopeLike;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogHistory extends Model
{
    use SoftDeletes, ScopeLike;

    const TRANSACTION_CREATE = 1;
    const TRANSACTION_UPDATE = 2;
    const TRANSACTION_DELETE = 3;
    
    protected $table = 'log_history';
    protected $fillable = ['log_datetime', 'user_id', 'transaction_type', 'information', 'record_id', 'data_before', 'data_after', 'record_type', 'table', 'data_change'];

    /**
     * Get the user record associated with the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the transaction record associated with the table transaction.
     */
    public function record()
    {
        return $this->morphTo();
    }

    /*************
    * SCOPE
    ***************/
    public function scopeTransactionType($query, $type)
    {
        return $query->where('transaction_type', $type);
    }
}
