<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const HAS_TAX_NO = 0;
    const HAS_TAX_YES = 1;

    protected $table = 'supplier';
    protected $fillable = ['name','address','phone','sales_name','sales_phone','information','status','has_tax'];

    /*************
    * SCOPE
    ***************/
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeActive($query)
    {
        return $query->status(self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->status(self::STATUS_DEACTIVE);
    }

    public function scopeHasTax($query, $type)
    {
        return $query->where('has_tax', $type);
    }

    public function scopeHasTaxNo($query)
    {
        return $query->hasTax(self::HAS_TAX_NO);
    }

    public function scopeHasTaxYes($query)
    {
        return $query->hasTax(self::HAS_TAX_YES);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->type == self::STATUS_ACTIVE;
    }
    
    public function isDeactive()
    {
        return $this->type == self::STATUS_DEACTIVE;
    }
}
