<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogBankTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class ReturPurchaseOrder extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ImageTrait, ScopeLike;

    const PREFIX = 'RPO-';

    public $log_pic = true;

    protected $table = 'retur_purchase_order';
    protected $fillable = ['no_retur', 'date', 'purchase_order_id', 'supplier_id', 'branch_id', 'bank_id', 'information', 'total_amount', 'images','created_by','updated_by'];

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class)->withTrashed();
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(ReturPurchaseOrderDetail::class);
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
    public function nextNoRetur()
    {
        return $this->exists ? $this->no_retur : self::nextNoReturRPO();
    }

    public static function nextNoReturRPO()
    {
        $retur_purchase_order = self::withTrashed()->latest()->first();
        if ($retur_purchase_order) {
            $next_code = str_replace(self::PREFIX, '', $retur_purchase_order->no_retur);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%04d", self::PREFIX, $next_code);
    }
}
