<?php

namespace App\Models;

use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use jeremykenedy\LaravelRoles\Models\Role as JeremyRole;
use App\Traits\ScopeLike;

class Role extends JeremyRole
{
    use LogHistoryTrait, ScopeLike;

    const LEVEL_DEVELOPER = 100;
    const LEVEL_ADMINISTRATOR_WIKA = 95;
    const LEVEL_ADMINISTRATOR = 90;
    const LEVEL_EMPLOYEE = 80;
    
    /*************
    * RELATIONSHIP
    ***************/
    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_role', 'role_id', 'menu_id');
    }

    /*************
    * SCOPE
    ***************/
    public function scopeEmployee($query)
    {
        return $query->where('level', '<', self::LEVEL_ADMINISTRATOR);
    }

    public function scopeLevelUnder($query, $level)
    {
        return $query->where('level', '<=', $level);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public static function getListRole()
    {
        $roles = auth()->user()->getRoles();
        if ($roles->count() > 0) {
            $level_user = $roles->first()->level;
        } else {
            $level_user = self::LEVEL_EMPLOYEE;
        }

        $list = [];
        foreach (trans('role.list.level') as $i => $level) {
            if ($i <= $level_user) {
                $list[$i] = $level;
            }
        }

        return $list;
    }
}
