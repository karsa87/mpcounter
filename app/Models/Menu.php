<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const IS_NOT_PARENT = 0;
    const IS_PARENT = 1;
    
    protected $table = 'menu';
    protected $fillable = ['name','order','parent_id','url','icon','route','is_parent'];

    /*************
    * RELATIONSHIP
    ***************/
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'menu_permission', 'menu_id', 'permission_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'menu_role', 'menu_id', 'role_id');
    }

    public function submenu()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }

    /*************
    * SCOPE
    ***************/
    public function scopeParents($query)
    {
        return $query->where('is_parent', self::IS_PARENT);
    }

    public function scopeChild($query)
    {
        return $query->where('is_parent', self::IS_NOT_PARENT);
    }

    public function scopeFirstParents($query)
    {
        return $query->whereNull('parent_id');
    }
}
