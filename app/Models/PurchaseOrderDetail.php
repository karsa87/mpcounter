<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogStockTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class PurchaseOrderDetail extends Model
{
    use SoftDeletes, LogHistoryTrait, LogStockTransactionTrait, ScopeLike;

    const RETUR_NO = 0;
    const RETUR_YES = 1;
    
    protected $table = 'purchase_order_detail';
    protected $fillable = ['purchase_order_id', 'product_id', 'qty', 'purchase_price', 'sell_price', 'cost_of_goods','total_amount','is_retur','product_identity_id'];

    public function header()
    {
        return $this->belongsTo(PurchaseOrder::class, 'purchase_order_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function productIdentity()
    {
        return $this->belongsTo(ProductIdentity::class);
    }

    public function returDetail()
    {
        return $this->hasMany(ReturPurchaseOrderDetail::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeReturYes($query)
    {
        return $query->where('is_retur', self::RETUR_NO);
    }

    public function scopeReturNo($query)
    {
        return $query->where('is_retur', self::RETUR_YES);
    }
    
    public function scopeRetur($query, $is_retur)
    {
        return $query->where('is_retur', $is_retur);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isReturYes()
    {
        return $this->is_retur == self::RETUR_YES;
    }

    public function isReturNo()
    {
        return $this->is_retur == self::RETUR_NO;
    }
}
