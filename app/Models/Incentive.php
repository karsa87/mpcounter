<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use App\Traits\LogBankTransactionTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incentive extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ScopeLike;

    const PREFIX = 'INC-';
    const TYPE_DECREASE = 0;
    const TYPE_INCREASE = 1;

    public $log_pic = true;

    protected $table = 'incentive';
    protected $fillable = ['bank_id', 'no_transaction', 'date_transaction', 'investor', 'type', 'information', 'amount','created_by','updated_by', 'branch_id'];

    /*************
    * Relation
    ***************/
    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    /**********
    * SCOPE
    *********/
    public function scopeIncrease($query)
    {
        return $query->where('type', self::TYPE_INCREASE);
    }

    public function scopeDecrease($query)
    {
        return $query->where('type', self::TYPE_DECREASE);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type',$type);
    }

    /**********
    * CUSTOM METHOD
    *********/
    public function isIncrease()
    {
        return $this->type == self::TYPE_INCREASE;
    }

    public function isDecrease()
    {
        return $this->type == self::TYPE_DECREASE;
    }
    
    public function nextNo()
    {
        return self::nextNoTransaction();
    }

    public static function nextNoTransaction()
    {
        $incentive = self::withTrashed()->latest()->first();
        if ($incentive) {
            $next_no_transaction = str_replace(self::PREFIX, '', $incentive->no_transaction);
        } else {
            $next_no_transaction = 0;
        }
        $next_no_transaction = (integer) $next_no_transaction + 1;

        return sprintf("%s%06d", self::PREFIX, $next_no_transaction);
    }
}
