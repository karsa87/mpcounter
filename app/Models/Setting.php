<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogHistoryTrait;
use Illuminate\Support\Str;
use App\Util\Helpers\Util;
use App\Traits\ScopeLike;

class Setting extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const TYPE_TEXT = 0;
    const TYPE_NUMBER = 1;

    protected $table = 'setting';
    protected $fillable = ['name', 'key', 'value', 'status', 'type'];

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }
    
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeNumber($query)
    {
        return $query->where('type', self::TYPE_NUMBER);
    }

    public function scopeText($query)
    {
        return $query->where('type', self::TYPE_TEXT);
    }
    
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function isNumber()
    {
        return $this->type == self::TYPE_NUMBER;
    }

    public function isText()
    {
        return $this->type == self::TYPE_TEXT;
    }

    /*************
    * CUSTOM METHOD ATTRIBUTE
    ***************/
    public function setKeyAttribute($value)
    {
        $this->attributes['key'] = strtoupper(Str::slug($value, '_'));
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = htmlspecialchars($value, ENT_QUOTES);
    }

    public function getValueAttribute($value)
    {
        $value = html_entity_decode($value, ENT_QUOTES);
        $value = $this->isNumber() ? (float) $value : $value;

        return $value;
    }
}
