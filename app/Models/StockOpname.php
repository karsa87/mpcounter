<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockOpname extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_NOT_MATCH = 0;
    const STATUS_MATCH = 1;

    const STATUS_WAITING = 0;
    const STATUS_ON_PROCESS = 1;
    const STATUS_VERIFIED = 2;

    protected $table = 'stock_opname';
    protected $fillable = ['title', 'month', 'year', 'branch_id', 'created_by', 'updated_by', 'information', 'status'];

    public $log_pic = true;
    
    /*************
    * Relation
    ***************/
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function details()
    {
        return $this->belongsToMany(ProductIdentity::class, 'stock_opname_detail', 'stock_opname_id', 'product_identity_id')->withPivot('stock_on_system', 'stock_on_hand', 'information', 'status');
    }

    /*************
    * SCOPE
    ***************/
    public function scopeWaiting($query)
    {
        return $query->where('status', self::STATUS_WAITING);
    }
    
    public function scopeProcess($query)
    {
        return $query->where('status', self::STATUS_ON_PROCESS);
    }

    public function scopeVerified($query)
    {
        return $query->where('status', self::STATUS_VERIFIED);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isWaiting()
    {
        return $this->status == self::STATUS_WAITING;
    }
    
    public function isProcess()
    {
        return $this->status == self::STATUS_ON_PROCESS;
    }

    public function isVerified()
    {
        return $this->status == self::STATUS_VERIFIED;
    }

    public function detailMatch()
    {
        return $this->details()->wherePivot('status', '=', self::STATUS_MATCH);
    }

    public function detailNotMatch()
    {
        return $this->details()->wherePivot('status', '=', self::STATUS_NOT_MATCH);
    }

    public static function nextMonth()
    {
        $stock_opname = self::latest()->first();
        
        return $stock_opname ? $stock_opname->month+1 : date('n');
    }

    public static function nextYear()
    {
        return date('Y');
    }
}
