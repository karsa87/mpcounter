<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogStockTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class SalesOrderDetail extends Model
{
    use SoftDeletes, LogHistoryTrait, LogStockTransactionTrait, ScopeLike;

    const RETUR_NO = 0;
    const RETUR_YES = 1;
    const PREFIX = 'DC-';

    protected $table = 'sales_order_detail';
    protected $fillable = ['sales_order_id', 'product_id', 'product_identity_id', 'qty', 'cost_of_goods', 'sell_price', 'promo_id', 'total_amount_first', 'discount_promo', 'total_amount', 'is_retur'];

    public function header()
    {
        return $this->belongsTo(SalesOrder::class, 'sales_order_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function promo()
    {
        return $this->belongsTo(Promo::class)->withTrashed();
    }

    public function productIdentity()
    {
        return $this->belongsTo(ProductIdentity::class);
    }

    public function returDetail()
    {
        return $this->hasMany(ReturSalesOrderDetail::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeReturYes($query)
    {
        return $query->where('is_retur', self::RETUR_NO);
    }

    public function scopeReturNo($query)
    {
        return $query->where('is_retur', self::RETUR_YES);
    }
    
    public function scopeRetur($query, $is_retur)
    {
        return $query->where('is_retur', $is_retur);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isReturYes()
    {
        return $this->is_retur == self::RETUR_YES;
    }

    public function isReturNo()
    {
        return $this->is_retur == self::RETUR_NO;
    }
}
