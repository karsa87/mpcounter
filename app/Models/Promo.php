<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class Promo extends Model
{
    use SoftDeletes, LogHistoryTrait, ImageTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;
    const PREFIX = 'DC-';

    public $log_pic = true;

    protected $table = 'promo';
    protected $fillable = ['code', 'name', 'information', 'date_start', 'date_end', 'category_id', 'discount_percent', 'discount_price', 'status', 'images', 'brand_id','created_by','updated_by'];

    public function category()
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'promo_detail')->withPivot('product_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class)->withTrashed();
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }
    
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopePublish($query)
    {
        $date = date('Y-m-d');
        
        return $query->where('status', self::STATUS_ACTIVE)
                    ->where('date_start', '<=', $date)
                    ->where('date_end', '>=', $date);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function isPercent()
    {
        return $this->discount_percent > 0;
    }

    public function isPrice()
    {
        return $this->discount_price > 0;
    }

    /**
     * Calculate discount of promo 
     * @param double $number price before discount
     * ****/
    public function calculateDiscount($number)
    {
        $discount = 0;
        if ($this->isPercent()) {
            $discount = $number * ($this->discount_percent / 100);
        } elseif ($this->isPrice()) {
            $discount = $this->discount_price;
        }

        return $discount;
    }

    public function nextCode()
    {
        return $this->exists ? $this->code : self::nextCodePromo();
    }

    public static function nextCodePromo()
    {
        $promo = self::withTrashed()->latest()->first();
        if ($promo) {
            $next_code = str_replace(self::PREFIX, '', $promo->code);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%04d", self::PREFIX, $next_code);
    }

    public function getDiscountPercentAttribute($value)
    {
        return (int) $value;
    }
}
