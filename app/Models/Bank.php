<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class Bank extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;
    
    const DEFAULT_YES = 1;
    const DEFAULT_NO = 0;

    protected $table = 'bank';
    protected $fillable = ['name', 'account_number', 'name_owner', 'status', 'is_default'];

    /*************
    * Relation
    ***************/
    public function debtPaymentPo()
    {
        return $this->hasMany(DebtPaymentPurchaseOrder::class);
    }

    public function debtPaymentSo()
    {
        return $this->hasMany(DebtPaymentSalesOrder::class);
    }

    public function expend()
    {
        return $this->hasMany(Expend::class);
    }

    public function incentive()
    {
        return $this->hasMany(Incentive::class);
    }

    public function logBank()
    {
        return $this->hasMany(LogBankTransaction::class);
    }

    public function purchaseOrder()
    {
        return $this->hasMany(PurchaseOrder::class);
    }

    public function returPurchaseOrder()
    {
        return $this->hasMany(ReturPurchaseOrder::class);
    }

    public function salesOrder()
    {
        return $this->hasMany(SalesOrder::class);
    }

    public function returSalesOrder()
    {
        return $this->hasMany(ReturSalesOrder::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }
    public function scopeDefaultYes($query)
    {
        return $query->where('is_default', self::DEFAULT_YES);
    }
    public function scopeDefaultNo($query)
    {
        return $query->where('is_default', self::DEFAULT_NO);
    }
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }
    public function scopeDefault($query, $default)
    {
        return $query->where('is_default', $default);
    }

    public function scopeOrderByDefault($query, $sorting = 'DESC')
    {
        return $query->orderBy('is_default', $sorting);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        }

        return parent::save($options);
    }

    protected function checkRelation()
    {
        $total = $this->debtPaymentPo->count() + $this->debtPaymentSo->count() + $this->expend->count() + $this->incentive->count() + $this->logBank->count() + $this->purchaseOrder->count() + $this->returPurchaseOrder->count() + $this->salesOrder->count() + $this->returSalesOrder->count();

        return $total;
    }
}
