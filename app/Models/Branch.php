<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;


class Branch extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    protected $table = 'branch';
    protected $fillable = ['name', 'address', 'phone', 'employee_id', 'status'];

    /*************
    * Relation
    ***************/
    public function productIdentity()
    {
        return $this->hasMany(ProductIdentity::class);
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function purchaseOrder()
    {
        return $this->hasMany(PurchaseOrder::class);
    }

    public function returPurchaseOrder()
    {
        return $this->hasMany(ReturPurchaseOrder::class);
    }

    public function salesOrder()
    {
        return $this->hasMany(SalesOrder::class);
    }

    public function returSalesOrder()
    {
        return $this->hasMany(ReturSalesOrder::class);
    }

    public function incentive()
    {
        return $this->hasMany(Incentive::class);
    }

    public function expend()
    {
        return $this->hasMany(Expend::class);
    }

    /**
     * Get the branch_manager record associated with the employee.
     */
    public function branch_manager()
    {
        return $this->belongsTo(Employee::class, 'employee_id')->withTrashed();
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        }

        return parent::save($options);
    }

    protected function checkRelation()
    {
        $total = $this->productIdentity->count() + $this->stock->count() + $this->purchaseOrder->count() + $this->returPurchaseOrder->count() + $this->salesOrder->count() + $this->returSalesOrder->count() + $this->incentive->count() + $this->expend->count();

        return $total;
    }
}
