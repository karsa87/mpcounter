<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    protected $table = 'brand';
    protected $fillable = ['name', 'status'];
    
    /*************
    * Relation
    ***************/
    public function productIdentity()
    {
        return $this->hasMany(ProductIdentity::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function promo()
    {
        return $this->hasMany(Promo::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        }

        return parent::save($options);
    }

    protected function checkRelation()
    {
        $total = $this->productIdentity->count() + $this->product->count() + $this->promo->count();

        return $total;
    }
}
