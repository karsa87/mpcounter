<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const IDENTIFIER_KTP = 0;
    const IDENTIFIER_SIM = 1;
    const IDENTIFIER_NPWP = 2;

    protected $table = 'employee';
    protected $fillable = ['name', 'address', 'phone', 'type_identifier', 'identifier', 'email', 'status','branch_id'];

    /**
     * Get the user record associated with the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    public function expend()
    {
        return $this->hasMany(Expend::class);
    }

    public function mutationProduct()
    {
        return $this->hasMany(MutationProduct::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeTypeIdentifier($query, $type)
    {
        return $query->where('type_identifier', $type);
    }

    public function scopeSales($query)
    {
        return $query->whereHas('user', function($q) {
            return $q->whereHas('roles', function($qq) {
                $qq->where('slug', 'sales');
            });
        });
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        } elseif ($this->deleted_at) {
            $this->user()->delete();
        }

        return parent::save($options);
    }

    protected function checkRelation()
    {
        $total = $this->expend->count() + $this->mutationProduct->count();

        return $total;
    }
}
