<?php

namespace App\Models;

use App\Traits\ScopeLike;
use Illuminate\Database\Eloquent\Model;
use jeremykenedy\LaravelRoles\Models\Permission as JeremyPermission;

class Permission extends JeremyPermission
{
    use ScopeLike;

    /*************
    * RELATIONSHIP
    ***************/
    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_permission', 'permission_id', 'menu_id');
    }
}
