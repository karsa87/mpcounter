<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ScopeLike;

class LogBankTransaction extends Model
{
    use ScopeLike;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    protected $table = 'log_bank_transaction';
    protected $fillable = ['log_datetime', 'user_id', 'bank_id', 'transaction_id', 'transaction_type', 'type', 'information', 'debit_amount', 'credit_amount', 'amount_before', 'amount_after', 'table'];

    /**
     * Get the user record associated with the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
    
    /**
     * Get the bank record associated with the bank.
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    /**
     * Get the transaction record associated with the table transaction.
     */
    public function transaction()
    {
        return $this->morphTo();
    }

    /*************
    * SCOPE
    ***************/
    public function scopeDebit($query)
    {
        return $query->where('type', self::TYPE_DEBIT);
    }

    public function scopeCredit($query)
    {
        return $query->where('type', self::TYPE_CREDIT);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isDebit()
    {
        return $this->type == self::TYPE_DEBIT;
    }

    public function isCredit()
    {
        return $this->type == self::TYPE_CREDIT;
    }
}
