<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class ProductIdentity extends Model
{
    use LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const UNIT_PCS = 0;
    const UNIT_BOX = 1;
    const UNIT_KG = 2;

    protected $table = 'product_identity';
    protected $fillable = ['product_id', 'branch_id', 'identity', 'stock', 'expired_date'];

    /**
     * Get the stock record associated with the stock.
     */
    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    /**
     * Get the category record associated with the category.
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    /**
     * Get the category record associated with the category.
     */
    public function poDetail()
    {
        return $this->hasMany(PurchaseOrderDetail::class)->withTrashed();
    }
}
