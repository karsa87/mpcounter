<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogBankTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Util\Helpers\Util;
use App\Traits\ScopeLike;

class SalesOrder extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ImageTrait, ScopeLike;

    const STATUS_PAID = 1;
    const STATUS_NOT_PAID = 0;
    
    const TYPE_CASH = 0;
    const TYPE_DEBT = 1;
    const TYPE_TRANSFER = 2;
    
    const PPN_DISABLE = 0;
    const PPN_ENABLE = 1;

    const NOT_USE_POINT = 0;
    const USE_POINT = 1;
    const PREFIX = 'SO-';

    public $log_pic = true;

    protected $table = 'sales_order';
    protected $fillable = ['no_invoice', 'date', 'member_id', 'name', 'address', 'phone', 'information', 'type', 'date_max_payable', 'branch_id', 'bank_id', 'sales_id', 'promo_id', 'total_amount_first', 'discount_promo', 'discount_sales', 'total_discount_detail', 'shipping_cost', 'additional_cost', 'total_amount', 'down_payment', 'paid_off', 'is_ppn', 'status', 'images', 'point_member', 'discount_poin_member', 'use_point','created_by','updated_by','tax_bank','tax_bank_price'];

    public function member()
    {
        return $this->belongsTo(Member::class)->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function promo()
    {
        return $this->belongsTo(Promo::class)->withTrashed();
    }

    public function sales()
    {
        return $this->belongsTo(Employee::class, 'sales_id')->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(SalesOrderDetail::class);
    }

    public function retur()
    {
        return $this->hasMany(ReturSalesOrder::class);
    }

    public function debtPayments()
    {
        return $this->hasMany(DebtPaymentSalesOrder::class);
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /*************
    * SCOPE
    ***************/
    public function scopePaid($query)
    {
        return $query->where('status', self::STATUS_PAID);
    }

    public function scopeNotPaid($query)
    {
        return $query->where('status', self::STATUS_NOT_PAID);
    }
    
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeCash($query)
    {
        return $query->where('type', self::TYPE_CASH);
    }

    public function scopeDebt($query)
    {
        return $query->where('type', self::TYPE_DEBT);
    }
    
    public function scopeTransfer($query)
    {
        return $query->where('type', self::TYPE_TRANSFER);
    }
    
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isPaid()
    {
        return $this->status == self::STATUS_PAID;
    }

    public function isNotPaid()
    {
        return $this->status == self::STATUS_NOT_PAID;
    }

    public function isCash()
    {
        return $this->type == self::TYPE_CASH;
    }

    public function isDebt()
    {
        return $this->type == self::TYPE_DEBT;
    }

    public function nextNo()
    {
        return $this->exists ? $this->no_invoice : self::nextNoInvoice();
    }

    public static function nextNoInvoice()
    {
        $sales_order = self::withTrashed()->latest()->first();
        if ($sales_order) {
            $next_code = str_replace(sprintf("%s%s/", self::PREFIX, date('ymd')), '', $sales_order->no_invoice);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%s/%04d", self::PREFIX, date('ymd'), $next_code);
    }

    public function getMemberNameAttribute()
    {
        return $this->member_id ? $this->member->name : $this->name;
    }

    public function getMemberAddressAttribute()
    {
        return $this->member_id ? $this->member->address : $this->address;
    }

    public function getMemberPhoneAttribute()
    {
        return $this->member_id ? $this->member->phone : $this->phone;
    }

    public function getTaxBankFormatAttribute($value)
    {
        return Util::format_currency($this->tax_bank, 1); 
    }

    public function getTaxBankPriceFormatAttribute($value)
    {
        return Util::format_currency($this->tax_bank_price); 
    }

    public function save(array $options = [])
    {
        $this->calculatePoinMember();

        return parent::save();
    }

    protected function calculatePoinMember()
    {
        $member_id_old = $this->getOriginal('member_id') ?? null;
        if($member_id_old != $this->member_id) {
            $member_old = Member::find($member_id_old);
            $member_new = $this->member;

            $convertion_rupiah_point = Setting::where('key', 'CONVERTION_TOTAL_BUYING_TO_MEMBER_POINT')->first()->value;
            if ($member_old) {
                $total_amount_old = $this->getOriginal('total_amount') ?? 0;
                $use_point_old = $this->getOriginal('use_point') ?? 0;
                $point_member_old = (double) $this->getOriginal('point_member') ?? 0;

                if ($use_point_old == self::USE_POINT) {
                    $member_old->poin += $point_member_old;
                } else if($use_point_old == self::NOT_USE_POINT) {
                    $point_old = floor($total_amount_old / $convertion_rupiah_point);
                    $member_old->poin -= $point_old;
                }

                $member_old->save();
            }

            if ($member_new) {
                if ($this->use_point == self::USE_POINT) {
                    $member_new->poin -= $this->point_member;
                } else if($this->use_point == self::NOT_USE_POINT) {
                    $point = floor($this->total_amount / $convertion_rupiah_point);
                    $member_new->poin += $point;
                }

                $member_new->save();
            }
        } else if (!empty($this->member_id) && $this->member_id == $member_id_old) {
            $member = $this->member;
            $convertion_rupiah_point = Setting::where('key', 'CONVERTION_TOTAL_BUYING_TO_MEMBER_POINT')->first()->value;

            $total_amount_old = $this->getOriginal('total_amount') ?? 0;
            $use_point_old = $this->getOriginal('use_point') ?? 0;

            // memotong poin karena digunakan pada penjualan ini
            $point_member_old = (double) $this->getOriginal('point_member') ?? 0;
            $point_member_now = (double) $this->point_member;
            if($point_member_old != $point_member_now) {
                $member->poin = ($member->poin + $point_member_old) - $point_member_now;
            }
            
            // menambah poin karena member melakukan pembelian pada penjualan ini
            if ($this->use_point == self::NOT_USE_POINT 
                    && $use_point_old == self::USE_POINT) {
                // menambah poin karena tidak memakai poin pada penjualan ini
                $point_old = floor($total_amount_old / $convertion_rupiah_point);
                $point_now = floor($this->total_amount / $convertion_rupiah_point);

                $member->poin += $point_now;
            } elseif ($this->use_point == self::USE_POINT 
                    && $use_point_old == self::NOT_USE_POINT) {
                // mengurangi poin karena memakai poin pada penjualan ini
                $point_old = floor($total_amount_old / $convertion_rupiah_point);
                $point_now = floor($this->total_amount / $convertion_rupiah_point);

                $member->poin -= $point_old;
            } elseif ($this->use_point == self::USE_POINT 
                    && $use_point_old == self::USE_POINT) {
                $point_old = (double) $this->getOriginal('point_member') ?? 0;
                $point_now = (double) $this->point_member;

                $member->poin = $member->poin + $point_old - $point_now;
            } elseif ($this->use_point == self::NOT_USE_POINT 
                    && $use_point_old == self::NOT_USE_POINT) {
                $point_old = floor($total_amount_old / $convertion_rupiah_point);
                $point_now = floor($this->total_amount / $convertion_rupiah_point);
                
                $member->poin = $member->poin - $point_old + $point_now;
            }
            
            $member->save();
        }
    }
}
