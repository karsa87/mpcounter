<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogBankTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class DebtPaymentSalesOrder extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ImageTrait, ScopeLike;

    const PREFIX = 'DSO-';

    public $log_pic = true;

    protected $table = 'debt_payment_sales_order';
    protected $fillable = ['no_debt', 'sales_order_id', 'bank_id', 'date', 'information', 'images', 'paid', 'total_paid', 'remaining_debt','created_by','updated_by'];

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class)->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
    public function nextNoDebt()
    {
        return $this->exists ? $this->no_debt : self::nextNoDebtSO();
    }

    public static function nextNoDebtSO()
    {
        $debt_payment_sales_order = self::withTrashed()->latest()->first();
        if ($debt_payment_sales_order) {
            $next_code = str_replace(self::PREFIX, '', $debt_payment_sales_order->no_debt);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%04d", self::PREFIX, $next_code);
    }
}
