<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class Product extends Model
{
    use SoftDeletes, LogHistoryTrait, ImageTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const EXPIRED_DATE_NO = 0;
    const EXPIRED_DATE_YES = 1;

    const UNIT_PCS = 0;
    const UNIT_BOX = 1;
    const UNIT_KG = 2;
    const PREFIX = 'PR-';

    protected $table = 'product';
    protected $fillable = ['code', 'name', 'category_id', 'brand_id', 'cost_of_goods', 'sell_price', 'images', 'information', 'status', 'unit', 'reseller1_sell_price', 'reseller2_sell_price', 'with_expired_date'];

    /**
     * Get the stock record associated with the stock.
     */
    public function stock()
    {
        return $this->hasMany(Stock::class);
    }

    public function mutationProductDetail()
    {
        return $this->hasMany(MutationProductDetail::class);
    }

    public function purchaseOrderDetail()
    {
        return $this->hasMany(PurchaseOrderDetail::class);
    }

    public function returPurchaseOrderDetail()
    {
        return $this->hasMany(ReturPurchaseOrderDetail::class);
    }

    public function salesOrderDetail()
    {
        return $this->hasMany(SalesOrderDetail::class);
    }

    public function returSalesOrderDetail()
    {
        return $this->hasMany(ReturSalesOrderDetail::class);
    }

    /**
     * Get the category record associated with the category.
     */
    public function category()
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }

    /**
     * Get the brand record associated with the brand.
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class)->withTrashed();
    }

    /**
     * Get the productIdentity record associated with the productIdentity.
     */
    public function productIdentity()
    {
        return $this->hasMany(ProductIdentity::class);
    }

    /**
     * Get the promo record associated with the promo.
     */
    public function promo()
    {
        return $this->hasMany(Promo::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeWithExpired($query)
    {
        return $query->where('with_expired_date', self::EXPIRED_DATE_YES);
    }

    public function scopeNotWithExpired($query)
    {
        return $query->where('with_expired_date', self::EXPIRED_DATE_NO);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        }

        $isNew = $this->exists;
        $res = parent::save();
        if (!$isNew) {
            $branchs = Branch::all()->pluck('id');
            
            $stock_insert = [];
            foreach ($branchs as $brand_id) {
                $stock_insert[] = [
                    'product_id' => $this->id,
                    'branch_id' => $brand_id,
                    'stock_in' => 0,
                    'stock_out' => 0,
                    'stock' => 0,
                ];
            }
            
            Stock::insert($stock_insert);
        }

        return $res;
    }

    protected function checkRelation()
    {
        $total = $this->mutationProductDetail->count() + $this->purchaseOrderDetail->count() + $this->returPurchaseOrderDetail->count() + $this->salesOrderDetail->count() + $this->returSalesOrderDetail->count() + $this->productIdentity->count();

        $total += $this->stock()->where('stock', '>', '0')->count();

        return $total;
    }
    
    public function nextCode()
    {
        return $this->exists ? $this->code : self::nextCodeProduct();
    }

    public static function nextCodeProduct()
    {
        $product = self::withTrashed()->latest()->first();
        if ($product) {
            $next_code = str_replace(self::PREFIX, '', $product->code);
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%06d", self::PREFIX, $next_code);
    }

    public static function updateCostOfGoods($ids, $date_start = '', $date_end = '')
    {
        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {
            $po = PurchaseOrderDetail::where('product_id', $id);
            $r_po = ReturPurchaseOrderDetail::where('product_id', $id);
            $so = SalesOrderDetail::where('product_id', $id);
            $r_so = ReturSalesOrderDetail::where('product_id', $id);

            if (! empty($date_start)) {
                $date = date('Y-m-d', strtotime($date_start));
                $po->whereDate('created_at', '>=', $date);
                $r_po->whereDate('created_at', '>=', $date);
                $so->whereDate('created_at', '>=', $date);
                $r_so->whereDate('created_at', '>=', $date);
            }

            if (! empty($date_end)) {
                $date = date('Y-m-d', strtotime($date_end));
                $po->whereDate('created_at', '<=', $date);
                $r_po->whereDate('created_at', '<=', $date);
                $so->whereDate('created_at', '<=', $date);
                $r_so->whereDate('created_at', '<=', $date);
            }

            $po->selectRaw('(purchase_price * qty) as total_amount, qty');
            $r_po->selectRaw('(purchase_price * qty) as total_amount, qty');
            $so->selectRaw('(cost_of_goods * qty) as total_amount, qty');
            $r_so->selectRaw('(cost_of_goods * qty) as total_amount, qty');

            $po = $po->get();
            $r_po = $r_po->get();
            $so = $so->get();
            $r_so = $r_so->get();

            // Purchase Order
            $po_stock_in = $po->sum('qty');
            $po_stock_out = 0;
            $po_total_price = $po->sum('total_amount'); // 5 * 10.000 = 50.000

            // Retur Purchase Order
            $r_po_stock_in = 0;
            $r_po_stock_out = $r_po->sum('qty');
            $r_po_total_price = $r_po->sum('total_amount'); // 0 * 0 = 0

            // Sales Order
            $so_stock_in = 0;
            $so_stock_out = $so->sum('qty');
            $so_total_price = $so->sum('total_amount'); // 0 * 0 = 0 

            // Retur Sales Order
            $r_so_stock_in = $r_so->sum('qty');
            $r_so_stock_out = 0;
            $r_so_total_price = $r_so->sum('total_amount'); // 0 * 0 = 0

            $stock_in = $po_stock_in + $r_po_stock_in + $so_stock_in + $r_so_stock_in;
            $stock_out = $po_stock_out + $r_po_stock_out + $so_stock_out + $r_so_stock_out;
            $stock = $stock_in - $stock_out;
            $total_price = $po_total_price + $r_so_total_price - $r_po_total_price - $so_total_price;

            if ($stock == 0) {
                $l_so = SalesOrderDetail::whereProductId($id)->orderBy('created_at', 'DESC')->first();
                $hpp = $l_so ? $l_so->cost_of_goods : 0;
            } else {
                $hpp = $total_price / $stock;
            }

            self::whereId($id)->update(['cost_of_goods'=>$hpp]);
        }
    }
}
