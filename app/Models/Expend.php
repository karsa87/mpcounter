<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\ImageTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\LogBankTransactionTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expend extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ImageTrait, ScopeLike;

    const PREFIX = 'EXP-';
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    public $log_pic = true;

    protected $table = 'expend';
    protected $fillable = ['expend_date', 'bank_id', 'category_expend_id', 'employee_id', 'amount', 'information', 'images', 'no_transaction','created_by','updated_by', 'branch_id'];

    /*************
    * Relation
    ***************/
    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function categoryExpend()
    {
        return $this->belongsTo(CategoryExpend::class)->withTrashed();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class)->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function nextNo()
    {
        return self::nextNoTransaction();
    }

    public static function nextNoTransaction()
    {
        $incentive = self::withTrashed()->latest()->first();
        if ($incentive) {
            $next_no_transaction = str_replace(self::PREFIX, '', $incentive->no_transaction);
        } else {
            $next_no_transaction = 0;
        }
        $next_no_transaction = (integer) $next_no_transaction + 1;

        return sprintf("%s%06d", self::PREFIX, $next_no_transaction);
    }
}
