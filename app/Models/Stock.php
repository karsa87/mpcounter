<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class Stock extends Model
{
    use LogHistoryTrait, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    protected $table = 'stock';
    protected $fillable = ['product_id', 'branch_id', 'stock_in', 'stock_out', 'stock'];

    /**
     * Get the product record associated with the product.
     */
    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }
    
    /**
     * Get the branch record associated with the branch.
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    /*************
    * SCOPE
    ***************/
    public function scopeProduct($query, $product_id)
    {
        if (is_array($product_id)) {
            $query->whereIn('product_id', $product_id);
        } elseif (is_int()) {
            $query->where('product_id', $product_id);
        }

        return $query;
    }
    
    public function scopeBranch($query, $branch_id)
    {
        if (is_array($branch_id)) {
            $query->whereIn('branch_id', $branch_id);
        } elseif (is_int()) {
            $query->where('branch_id', $branch_id);
        }

        return $query;
    }
}
