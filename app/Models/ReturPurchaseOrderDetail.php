<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogStockTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ScopeLike;

class ReturPurchaseOrderDetail extends Model
{
    use SoftDeletes, LogHistoryTrait, LogStockTransactionTrait, ScopeLike;

    protected $table = 'retur_purchase_order_detail';
    protected $fillable = ['retur_purchase_order_id', 'purchase_order_detail_id', 'product_id', 'product_identity_id', 'qty', 'cost_of_goods', 'purchase_price', 'total_amount'];

    public function header()
    {
        return $this->belongsTo(ReturPurchaseOrder::class, 'retur_purchase_order_id')->withTrashed();
    }

    public function purchaseOrderDetail()
    {
        return $this->belongsTo(PurchaseOrderDetail::class)->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function productIdentity()
    {
        return $this->belongsTo(ProductIdentity::class);
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
}
