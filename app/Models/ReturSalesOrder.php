<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogBankTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class ReturSalesOrder extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ImageTrait, ScopeLike;

    const PREFIX = 'RSO-';

    public $log_pic = true;

    protected $table = 'retur_sales_order';
    protected $fillable = ['no_retur', 'sales_order_id', 'date', 'branch_id', 'bank_id', 'promo_id', 'information', 'total_amount_first', 'total_discount_detail', 'total_amount', 'images','created_by','updated_by'];

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class)->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function promo()
    {
        return $this->belongsTo(Promo::class)->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(ReturSalesOrderDetail::class);
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
    public function nextNoRetur()
    {
        return $this->exists ? $this->no_retur : self::nextNoReturRSO();
    }

    public static function nextNoReturRSO()
    {
        $retur_sles_order = self::withTrashed()->latest()->first();
        if ($retur_sles_order) {
            $next_code = str_replace(self::PREFIX, '', $retur_sles_order->no_retur);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%04d", self::PREFIX, $next_code);
    }
}
