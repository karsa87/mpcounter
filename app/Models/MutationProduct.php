<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class MutationProduct extends Model
{
    use SoftDeletes, LogHistoryTrait, ImageTrait, ScopeLike;

    const PREFIX = 'MP-';

    protected $table = 'mutation_product';
    protected $fillable = ['no_mutation', 'date', 'employee_id', 'from_branch_id', 'to_branch_id', 'shipping_cost', 'additional_cost', 'created_by', 'updated_by'];

    public $log_pic = true;

    public function employee()
    {
        return $this->belongsTo(Employee::class)->withTrashed();
    }

    public function fromBranch()
    {
        return $this->belongsTo(Branch::class, 'from_branch_id')->withTrashed();
    }

    public function toBranch()
    {
        return $this->belongsTo(Branch::class, 'to_branch_id')->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(MutationProductDetail::class);
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
    public function nextNo()
    {
        return $this->exists ? $this->no_mutation : self::nextNoMutation();
    }

    public static function nextNoMutation()
    {
        $incentive = self::withTrashed()->latest()->first();
        if ($incentive) {
            $next_no_mutation = str_replace(self::PREFIX, '', $incentive->no_mutation);
        } else {
            $next_no_mutation = 0;
        }
        $next_no_mutation = (integer) $next_no_mutation + 1;

        return sprintf("%s%06d", self::PREFIX, $next_no_mutation);
    }
}
