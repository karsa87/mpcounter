<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogBankTransactionTrait;
use App\Traits\LogHistoryTrait;
use App\Traits\ImageTrait;
use App\Traits\ScopeLike;

class PurchaseOrder extends Model
{
    use SoftDeletes, LogHistoryTrait, LogBankTransactionTrait, ImageTrait, ScopeLike;

    const STATUS_PAID = 1;
    const STATUS_NOT_PAID = 0;
    
    const TYPE_CASH = 0;
    const TYPE_DEBT = 1;
    const TYPE_TRANSFER = 2;

    const PREFIX = 'PO-';

    public $log_pic = true;

    protected $table = 'purchase_order';
    protected $fillable = ['no_invoice', 'no_reference', 'date', 'type', 'date_max_payable','images','supplier_id','branch_id','information','bank_id','total_amount','down_payment','paid_off','status','created_by','updated_by'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(PurchaseOrderDetail::class);
    }

    public function retur()
    {
        return $this->hasMany(ReturPurchaseOrder::class);
    }

    public function debtPayments()
    {
        return $this->hasMany(DebtPaymentPurchaseOrder::class);
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /*************
    * SCOPE
    ***************/
    public function scopePaid($query)
    {
        return $query->where('status', self::STATUS_PAID);
    }

    public function scopeNotPaid($query)
    {
        return $query->where('status', self::STATUS_NOT_PAID);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }
    
    public function scopeCash($query)
    {
        return $query->where('type', self::TYPE_CASH);
    }

    public function scopeDebt($query)
    {
        return $query->where('type', self::TYPE_DEBT);
    }

    public function scopeTransfer($query)
    {
        return $query->where('type', self::TYPE_TRANSFER);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isPaid()
    {
        return $this->status == self::STATUS_PAID;
    }

    public function isNotPaid()
    {
        return $this->status == self::STATUS_NOT_PAID;
    }
    
    public function isCash()
    {
        return $this->type == self::TYPE_CASH;
    }

    public function isDebt()
    {
        return $this->type == self::TYPE_DEBT;
    }
    
    public function nextNo()
    {
        return $this->exists ? $this->no_invoice : self::nextNoInvoice();
    }

    public static function nextNoInvoice()
    {
        $purchase_order = self::withTrashed()->latest()->first();
        if ($purchase_order) {
            $next_code = str_replace(sprintf("%s%s/", self::PREFIX, date('ymd')), '', $purchase_order->no_invoice);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = $next_code + 1;

        return sprintf("%s%s/%06d", self::PREFIX, date('ymd'), $next_code);
    }
}
