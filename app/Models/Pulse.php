<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogBankTransactionTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pulse extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike, LogBankTransactionTrait;

    const PREFIX = 'PL-';

    protected $table = 'pulse';
    protected $fillable = ['no_transaction', 'date', 'phone', 'branch_id', 'bank_id', 'total_pulse', 'sell_price', 'capital_price', 'created_by', 'updated_by'];

    public $log_pic = true;
    
    /*************
    * Relation
    ***************/
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    /*************
    * SCOPE
    ***************/

    /*************
    * CUSTOM METHOD
    ***************/
    public function nextNo()
    {
        return $this->exists ? $this->no_transaction : self::nextNoTransaction();
    }

    public static function nextNoTransaction()
    {
        $pulse = self::withTrashed()->latest()->first();
        if ($pulse) {
            $next_code = str_replace(sprintf("%s%s/", self::PREFIX, date('ymd')), '', $pulse->no_transaction);

            try {
                $next_code = (integer) $next_code;
            } catch (\Throwable $th) {
                $next_code = 0;
            }
        } else {
            $next_code = 0;
        }
        $next_code = (integer) $next_code + 1;

        return sprintf("%s%s/%04d", self::PREFIX, date('ymd'), $next_code);
    }
}
