<?php

namespace App\Models;

use App\Traits\ScopeLike;
use App\Traits\LogHistoryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes, LogHistoryTrait, ScopeLike;

    const IDENTITY_STUDENT_CARD = 1;
    const IDENTITY_KTP = 2;
    const IDENTITY_SIM = 3;

    const TYPE_GENERAL = 1;
    const TYPE_RESELLER_1 = 2;
    const TYPE_RESELLER_2 = 3;

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const PREFIX = 'M-';

    protected $table = 'member';
    protected $fillable = ['name','identity_type','identity','address','phone','birthdate','poin','member_number','type','max_day_debt','max_debt','status'];

    public function salesOrder()
    {
        return $this->hasMany(SalesOrder::class);
    }

    /*************
    * SCOPE
    ***************/
    public function scopeIdentityType($query, $type)
    {
        return $query->where('identity_type', $type);
    }
    
    public function scopeStudentCard($query)
    {
        return $query->where('identity_type', self::IDENTITY_STUDENT_CARD);
    }
    
    public function scopeKtp($query)
    {
        return $query->where('identity_type', self::IDENTITY_KTP);
    }
    
    public function scopeSim($query)
    {
        return $query->where('identity_type', self::IDENTITY_SIM);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeGeneral($query)
    {
        return $query->where('type', self::TYPE_GENERAL);
    }

    public function scopeResellerOne($query)
    {
        return $query->where('type', self::TYPE_RESELLER_1);
    }

    public function scopeResellerTwo($query)
    {
        return $query->where('type', self::TYPE_RESELLER_2);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status', self::STATUS_DEACTIVE);
    }

    /*************
    * CUSTOM METHOD
    ***************/
    public function isStudentCard()
    {
        return $this->identity_type == self::IDENTITY_STUDENT_CARD;
    }
    
    public function isKtp()
    {
        return $this->identity_type == self::IDENTITY_KTP;
    }
    
    public function isSim()
    {
        return $this->identity_type == self::IDENTITY_SIM;
    }

    public function isGeneral()
    {
        return $this->type == self::TYPE_GENERAL;
    }
    
    public function isResellerOne()
    {
        return $this->type == self::TYPE_RESELLER_1;
    }
    
    public function isResellerTwo()
    {
        return $this->type == self::TYPE_RESELLER_2;
    }
    
    public function isActive()
    {
        return $this->type == self::STATUS_ACTIVE;
    }
    
    public function isDeactive()
    {
        return $this->type == self::STATUS_DEACTIVE;
    }
    
    public function nextNumber()
    {
        return self::nextMemberNumber();
    }

    public static function nextMemberNumber()
    {
        $incentive = self::withTrashed()->latest()->first();
        if ($incentive) {
            $next_no_transaction = str_replace(self::PREFIX, '', $incentive->no_transaction);
        } else {
            $next_no_transaction = 0;
        }
        $next_no_transaction = (integer) $next_no_transaction + 1;

        return sprintf("%s%s%04d", self::PREFIX, date('ydN'), $next_no_transaction);
    }

    public function save(array $options = [])
    {
        if ($this->deleted_at && $this->checkRelation() > 0) {
            return false;
        }

        return parent::save($options);
    }

    protected function checkRelation()
    {
        $total = $this->salesOrder->count();

        return $total;
    }
}
