<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait ScopeLike
{
    /**
     * Get first image.
     * 
     * @param Builder $query Query builder laravel
     * @param String $column Column search
     * @param String $value Value search
     */
    public function scopeWhereLike($query, string $column, string $value)
    {
        if (config('database.default') == 'pgsql') {
            $query->where($column, 'ilike', "%$value%");
        } else {
            $query->where($column, 'like', "%$value%");
        }

        return $query;
    }
    /**
     * Get first image.
     * 
     * @param Builder $query Query builder laravel
     * @param String $column Column search
     * @param String $value Value search
     */
    public function scopeOrWhereLike($query, string $column, string $value)
    {
        if (config('database.default') == 'pgsql') {
            $query->orWhere($column, 'ilike', "%$value%");
        } else {
            $query->orWhere($column, 'like', "%$value%");
        }

        return $query;
    }
}