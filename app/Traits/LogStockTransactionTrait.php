<?php

namespace App\Traits;

use App\Models\Stock;
use App\Models\LogStockTransaction;
use App\Observers\LogStockObserver;

trait LogStockTransactionTrait
{
    public static function bootLogStockTransactionTrait()
    {
        static::observe(new LogStockObserver);
    }

    /**
     * Get all of the owning commentable models.
     */
    public function logStock()
    {
        return $this->morphMany(LogStockTransaction::class, 'transaction');
    }

    /**
     * Create log stock_in stock_out for stock
     * @param integer $product_id model id of product
     * @param integer $branch_id model id of branch
     * @param integer $stock_in total stock in 
     * @param string $information information log transaction
     * */
    public function stockIn(int $product_id, int $branch_id, int $stock_in, $information = '')
    {
        $stock = Stock::firstOrCreate([
            'branch_id' => $branch_id,
            'product_id' => $product_id
        ], [
            'stock_in' => 0,
            'stock_out' => 0,
            'stock' => 0,
        ]);
        
        $this->createLogStock($stock, $stock_in, 0, $information, LogStockTransaction::TYPE_IN);
    }

    /**
     * Create log stock_in stock_out for stock
     * @param integer $product_id model id of product
     * @param integer $branch_id model id of branch
     * @param integer $stock_out total stock out
     * @param string $information information log transaction
     * */
    public function stockOut(int $product_id, int $branch_id, int $stock_out, $information = '')
    {
        $stock = Stock::firstOrCreate([
            'branch_id' => $branch_id,
            'product_id' => $product_id
        ], [
            'stock_in' => 0,
            'stock_out' => 0,
            'stock' => 0,
        ]);

        $this->createLogStock($stock, 0, $stock_out, $information, LogStockTransaction::TYPE_OUT);
    }

    /**
     * Create log stock_in stock_out for stock
     * @param Stock $stock model object stock
     * @param integer $stock_in stock_in amount
     * @param integer $stock_out stock_out amount
     * @param string $information information log transaction
     * @param integer $type type transaction stock_in or stock_out
     * */
    private function createLogStock(Stock $stock, int $stock_in, int $stock_out, $information = '', $type)
    {
        $log = LogStockTransaction::with('stock')
                    ->where('transaction_id', $this->id)
                    ->where('transaction_type', get_class($this))
                    ->where('table', $this->getTable())
                    ->where('stock_id', $stock->id)
                    ->orderBy('id', 'DESC')
                    ->first();

        $log = $log ?: new LogStockTransaction();

        $input = [
            'log_datetime' => date('Y-m-d H:i:s'),
            'user_id' => auth()->user()->id,
            'stock_id' => $stock->id,
            'transaction_id' => $this->id,
            'transaction_type' => get_class($this),
            'table' => $this->getTable(),
        ];

        if (!$log->exists) {
            $input['type'] = $type;
            $input['information'] = $information;
            $input['stock_in'] = $stock_in;
            $input['stock_out'] = $stock_out;
            $input['stock_before'] = $stock->stock;
            $input['stock_after'] = ($stock->stock + $stock_in - $stock_out);
            
            $log->fill($input);
            $log->save();

            $stock->stock_in = $stock->stock_in + $input['stock_in'];
            $stock->stock_out = $stock->stock_out + $input['stock_out'];
            $stock->stock = $input['stock_after'];
            $stock->save();
        } elseif ( $log->exists ) {
            if ($type == LogStockTransaction::TYPE_IN) {
                if ($stock_in == $log->stock_in) { // check edit
                    return;
                }

                $input_stock_out = $input;
                $input_stock_out['type'] = LogStockTransaction::TYPE_OUT;
                $input_stock_out['information'] = sprintf('Balancing : %s', $information);
                $input_stock_out['stock_in'] = 0;
                $input_stock_out['stock_out'] = $log->stock_in;
                $input_stock_out['stock_before'] = $stock->stock;
                $input_stock_out['stock_after'] = $input_stock_out['stock_before'] - $log->stock_in;

                $input_stock_in = $input;
                $input_stock_in['type'] = LogStockTransaction::TYPE_IN;
                $input_stock_in['information'] = sprintf('Edit : %s', $information);
                $input_stock_in['stock_in'] = $stock_in;
                $input_stock_in['stock_out'] = 0;
                $input_stock_in['stock_before'] = $input_stock_out['stock_after'];
                $input_stock_in['stock_after'] = $input_stock_in['stock_before'] + $stock_in;

                $inserts = [];
                if (!($input_stock_out['stock_in'] == 0 && $input_stock_out['stock_out'] == 0)) {
                    $inserts[] = $input_stock_out;
                }
                $inserts[] = $input_stock_in;

                LogStockTransaction::insert($inserts);

                if($log->type != $type) { // for method delete
                    // stock in = stock_in_now - $stock_in_new;
                    $stock->stock_in = $stock->stock_in - $input_stock_in['stock_out'];

                    // stock out = stock_out_now - $stock_out_new;
                    $stock->stock_out = $stock->stock_out - $input_stock_in['stock_in'];
                    $stock->stock = $stock->stock_in - $stock->stock_out;
                } else {
                    // stock in = stock_in_now - $stock_in_old + $stock_in_new;
                    $stock->stock_in = $stock->stock_in - $input_stock_out['stock_out'] + $input_stock_in['stock_in'];

                    // stock out = stock_out_now - $stock_out_old + $stock_out_new;
                    $stock->stock_out = $stock->stock_out - $input_stock_out['stock_in'] + $input_stock_in['stock_out'];
                    $stock->stock = $stock->stock_in - $stock->stock_out;
                }
                $stock->save();
            } elseif ($type == LogStockTransaction::TYPE_OUT) {
                if ($stock_out == $log->stock_out) { // check edit
                    return;
                }

                $input_stock_in = $input;
                $input_stock_in['type'] = LogStockTransaction::TYPE_IN;
                $input_stock_in['information'] = sprintf('Balancing : %s', $information);
                $input_stock_in['stock_in'] = $log->stock_out;
                $input_stock_in['stock_out'] = 0;
                $input_stock_in['stock_before'] = $stock->stock;
                $input_stock_in['stock_after'] = $input_stock_in['stock_before'] + $log->stock_out;

                $input_stock_out = $input;
                $input_stock_out['type'] = LogStockTransaction::TYPE_OUT;
                $input_stock_out['information'] = sprintf('Edit : %s', $information);
                $input_stock_out['stock_in'] = 0;
                $input_stock_out['stock_out'] = $stock_out;
                $input_stock_out['stock_before'] = $input_stock_in['stock_after'];
                $input_stock_out['stock_after'] = $input_stock_out['stock_before'] - $stock_out;

                $inserts = [];
                if (!($input_stock_in['stock_in'] == 0 && $input_stock_in['stock_out'] == 0)) {
                    $inserts[] = $input_stock_in;
                }
                $inserts[] = $input_stock_out;
                LogStockTransaction::insert($inserts);

                if ($log->type != $type) { // for delete
                    // stock in = stock_in_now - $stock_in_new;
                    $stock->stock_in = $stock->stock_in - $input_stock_out['stock_out'];

                    // stock out = stock_out_now - $stock_out_new;
                    $stock->stock_out = $stock->stock_out - $input_stock_out['stock_in'];
                    $stock->stock = $stock->stock_in - $stock->stock_out;
                } else {
                    // stock in = stock_in_now - $stock_in_old + $stock_in_new;
                    $stock->stock_in = $stock->stock_in - $input_stock_in['stock_out'] + $input_stock_out['stock_in'];

                    // stock out = stock_out_now - $stock_out_old + $stock_out_new;
                    $stock->stock_out = $stock->stock_out - $input_stock_in['stock_in'] + $input_stock_out['stock_out'];
                    $stock->stock = $stock->stock_in - $stock->stock_out;
                }
                
                $stock->save();
            }
        }
    }
}