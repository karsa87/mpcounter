<?php

namespace App\Traits;

use App\Models\Bank;
use App\Models\LogBankTransaction;

trait LogBankTransactionTrait
{
    /**
     * Get all of the owning commentable models.
     */
    public function logBank()
    {
        return $this->morphMany(LogBankTransaction::class, 'transaction');
    }

    /**
     * Create log debit credit for bank
     * @param Bank $bank model object bank
     * @param float $debit debit amount
     * @param string $information information log transaction
     * */
    public function debit(Bank $bank, float $debit, $information = '')
    {
        $this->createLogBank($bank, $debit, 0, $information, LogBankTransaction::TYPE_DEBIT);
    }

    /**
     * Create log debit credit for bank
     * @param Bank $bank model object bank
     * @param float $credit credit amount
     * @param string $information information log transaction
     * */
    public function credit(Bank $bank, float $credit, $information = '')
    {
        $this->createLogBank($bank, 0, $credit, $information, LogBankTransaction::TYPE_CREDIT);
    }

    /**
     * Create log debit credit for bank
     * @param Bank $bank model object bank
     * @param float $debit debit amount
     * @param float $credit credit amount
     * @param string $information information log transaction
     * @param integer $type type transaction debit or credit
     * */
    private function createLogBank(Bank $bank, float $debit, float $credit, $information = '', $type)
    {
        $log = LogBankTransaction::where('transaction_id', $this->id)
                    ->where('transaction_type', get_class($this))
                    ->where('table', $this->getTable())
                    ->orderBy('id', 'DESC')->first();
        $log = $log ?: new LogBankTransaction();

        $input = [
            'log_datetime' => date('Y-m-d H:i:s'),
            'user_id' => auth()->user()->id,
            'bank_id' => $bank->id,
            'transaction_id' => $this->id,
            'transaction_type' => get_class($this),
            'table' => $this->getTable(),
        ];

        if (!$log->exists) {
            $input['type'] = $type;
            $input['information'] = $information;
            $input['debit_amount'] = $debit;
            $input['credit_amount'] = $credit;
            $input['amount_before'] = $bank->saldo;
            $input['amount_after'] = ($bank->saldo + $debit - $credit);
            
            $log->fill($input);
            $log->save();

            $bank->saldo = $input['amount_after'];
            $bank->save();
        } elseif ( $log->exists ) {
            if ($type == LogBankTransaction::TYPE_DEBIT) {
                if ($debit == $log->debit_amount && $bank->id == $log->bank_id) { // check edit
                    return;
                }

                $input_credit = $input;
                $input_credit['type'] = LogBankTransaction::TYPE_DEBIT;
                $input_credit['information'] = sprintf('Balancing : %s', $information);
                $input_credit['debit_amount'] = 0;
                $input_credit['credit_amount'] = $log->debit_amount;
                
                if ($bank->id != $log->bank_id) {
                    $input_credit['bank_id'] = $log->bank->id;
                    $input_credit['amount_before'] = $log->bank->saldo;
                    $log->bank->saldo -= $log->debit_amount;
                    $input_credit['amount_after'] = $log->bank->saldo;
                    $log->bank->save();
                } else {
                    $input_credit['amount_before'] = $bank->saldo;
                    $bank->saldo -= $log->debit_amount;
                    $input_credit['amount_after'] = $bank->saldo;
                }

                $input_debit = $input;
                $input_debit['type'] = LogBankTransaction::TYPE_DEBIT;
                if ( $log->type === LogBankTransaction::TYPE_DEBIT ) {
                    $input_debit['information'] = sprintf('Edit : %s', $information);
                } else {
                    $input_debit['information'] = sprintf('Balancing : %s', $information);
                }
                $input_debit['debit_amount'] = $debit;
                $input_debit['credit_amount'] = 0;
                $input_debit['amount_before'] = $bank->saldo;
                $bank->saldo += $debit;
                $input_debit['amount_after'] = $bank->saldo;

                $inserts = [];
                if (!($input_credit['debit_amount'] == 0 && $input_credit['credit_amount'] == 0)) {
                    $inserts[] = $input_credit;
                }
                $inserts[] = $input_debit;

                LogBankTransaction::insert($inserts);
                $bank->save();
            } elseif ($type == LogBankTransaction::TYPE_CREDIT) {
                if ($credit == $log->credit_amount && $bank->id == $log->bank_id) { // check edit
                    return;
                }

                $input_debit = $input;
                $input_debit['type'] = LogBankTransaction::TYPE_DEBIT;
                $input_debit['information'] = sprintf('Balancing : %s', $information);
                $input_debit['debit_amount'] = $log->credit_amount;
                $input_debit['credit_amount'] = 0;
                
                if ($bank->id != $log->bank_id) {
                    $input_debit['bank_id'] = $log->bank->id;
                    $input_debit['amount_before'] = $log->bank->saldo;
                    $log->bank->saldo = $log->bank->saldo + $log->credit_amount;
                    $input_debit['amount_after'] = $log->bank->saldo;
                    $log->bank->save();
                } else {
                    $input_debit['amount_before'] = $bank->saldo;
                    $bank->saldo = $bank->saldo + $log->credit_amount;
                    $input_debit['amount_after'] = $bank->saldo;
                }

                $input_credit = $input;
                $input_credit['type'] = LogBankTransaction::TYPE_CREDIT;
                if ( $log->type === LogBankTransaction::TYPE_CREDIT ) {
                    $input_credit['information'] = sprintf('Edit : %s', $information);
                } else {
                    $input_credit['information'] = sprintf('Balancing : %s', $information);
                }
                $input_credit['debit_amount'] = 0;
                $input_credit['credit_amount'] = $credit;
                $input_credit['amount_before'] = $bank->saldo;
                $bank->saldo = $bank->saldo - $credit;
                $input_credit['amount_after'] = $bank->saldo;

                $inserts = [];
                if (!($input_debit['debit_amount'] == 0 && $input_debit['credit_amount'] == 0)) {
                    $inserts[] = $input_debit;
                }
                $inserts[] = $input_credit;

                LogBankTransaction::insert($inserts);
                $bank->save();
            }
        }
    }
}