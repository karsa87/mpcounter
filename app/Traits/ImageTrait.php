<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait ImageTrait
{
    /**
     * Get first image.
     */
    public function image()
    {
        $images = json_decode($this->images, TRUE);

        $url = url(config('default.img'));
        if( $images && \Storage::exists($images[0]) ) {
            $url = \Storage::url($images[0]);
        }

        return $url;
    }
    
    /**
     * Get first image.
     */
    public function images_arr($limit = 0)
    {
        $result = [];
        if (isset($this->images)) {
            foreach (json_decode($this->images, TRUE) as $link) {
                if($limit && count($result) >= $limit) {
                    break;
                }

                $result[] = \Storage::exists($link) ? \Storage::url($link) : url(config('default.img'));
            }
        }

        return $result;
    }
    
    /**
     * Get first image.
     */
    public function images_info()
    {
        $result = [];
        if (isset($this->images)) {
            foreach (json_decode($this->images, TRUE) as $image) {
                if( \Storage::exists($image) ) {
                    $result[] =  [
                        'name' => $image,
                        'size' => \Storage::size($image),
                        'url' => \Storage::url($image),
                    ];
                } else {
                    $result[] =  [
                        'name' => $image,
                        'size' => \File::size(public_path(config('default.img'))),
                        'url' => url(config('default.img')),
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * Move Image from tmp
     * **/
    public function moveTmpImage()
    {
        $images = [];
        $tmp_images = $this->images ? json_decode($this->images, TRUE) : [];

        foreach ($tmp_images as $img_path) {
            preg_match('/tmp\//', $img_path, $tmp_exists);

            if ($tmp_exists) {
                if (\Storage::exists($img_path)) {
                    preg_match('/[^\/]*$/', $img_path, $names);
                    $move_to = sprintf('%s/%s/%s', $this->getTable(), $this->id, $names[0]);
                    if (\Storage::exists($move_to)) {
                        $this->deleteImages($move_to);
                    }

                    \Storage::move($img_path, $move_to);

                    $images[] = $move_to;
                }
            } else {
                $images[] = $img_path;
            }
        }

        return $images;
    }

    /**
     * Upload Image
     * @param File $file file images
     * **/
    public static function uploadTmpImage($file)
    {
        $uploadedFile = $file;
        $filename = $uploadedFile->getClientOriginalName();
        $random = strtotime(date('Y-m-d'));
        $path_dir = sprintf( '/tmp/%s/%s', with(new static)->getTable(), $random );

        $path = \Storage::putFileAs(
            $path_dir,
            $uploadedFile,
            $filename
        );

        return $path;
    }

    /**
     * Delete image from column images object eloquent
     * **/
    public function deleteImage()
    {
        $deleted = [];
        $not_deleted = [];

        $files = $this->images ? json_decode($this->images, TRUE) : [];
        if ($files) {
            [$deleted, $not_deleted] = self::deleteImages($files);
        }

        return [
            $deleted,
            $not_deleted
        ];
    }

    /**
     * Delete Image
     * @param array|string $files path of files
     * **/
    public static function deleteImages($files)
    {
        $deleted = [];
        $not_deleted = [];

        $files = is_string($files) ? [$files] : $files;

        foreach ($files as $file) {
            if (\Storage::exists($file)) {
                \Storage::delete($file);
                $deleted[] = $file;
            } else {
                $not_deleted[] = $file;
            }
        }

        return [
            $deleted,
            $not_deleted
        ];
    }
}