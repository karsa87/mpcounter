<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;

class CalculateCostOfGoodsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $product_ids = null;
    private $date_start = '';
    private $date_end = '';

    /**
     * Create a new job instance.
     * @param array|integer $ids product_id of the product to be calculated hpp
     * @param string $date_start date start calculate hpp
     * @param string $date_end date end calculate hpp
     * 
     * @return void
     */
    public function __construct($ids, $date_start = '', $date_end = '')
    {
        $this->product_ids = $ids;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Product::updateCostOfGoods($this->product_ids, $this->date_start, $this->date_end);
    }
}
