<?php

namespace App\Observers;

class LogStockObserver
{
    /**
     * Handle the Models "created" event.
     *
     * @param  App\Models  $object
     * @return void
     */
    public function created($object)
    {
        [$product_id, $branch_id, $qty, $stock_in, $no] = $this->getInfoStock($object);

        if (!empty($product_id) && !empty($branch_id)) {
            if ($stock_in) {
                $object->stockIn($product_id, $branch_id, $qty, $no);
            } else {
                $object->stockOut($product_id,  $branch_id, $qty, $no);
            }
        }
    }

    /**
     * Handle the Models "updated" event.
     *
     * @param  App\Models  $object
     * @return void
     */
    public function updated($object)
    {
        if ($object->deleted_at != null) {
            $this->deleted($object);
        } else {
            [$product_id, $branch_id, $qty, $stock_in, $no] = $this->getInfoStock($object);
            if (!empty($product_id) && !empty($branch_id)) {
                if ($stock_in) {
                    $object->stockIn($product_id, $branch_id, $qty, $no);
                } else {
                    $object->stockOut($product_id,  $branch_id, $qty, $no);
                }
            }
        }
    }

    /**
     * Handle the Models "deleted" event.
     *
     * @param  App\Models  $object
     * @return void
     */
    public function deleted($object)
    {
        [$product_id, $branch_id, $qty, $stock_in, $no] = $this->getInfoStock($object, 'delete');

        if (!empty($product_id) && !empty($branch_id)) {
            if ($stock_in) {
                $object->stockIn($product_id, $branch_id, $qty, $no);
            } else {
                $object->stockOut($product_id,  $branch_id, $qty, $no);
            }
        }
    }

    private function getInfoStock($object, $method = '')
    {
        $table = $object->getTable();
        $header = $object->header;
        $stock_in = TRUE;
        $product_id = '';
        $branch_id = '';
        $qty = '';
        $no = '';

        switch ($table) {
            case 'purchase_order_detail':
                $stock_in = $method=='delete' ? FALSE : TRUE;
                $no = $header->no_invoice;
        
                $product_id = $object->product_id;
                $branch_id = $header->branch_id;
                $qty = $object->qty;
                break;

            case 'retur_sales_order_detail':
                $stock_in = $method=='delete' ? FALSE : TRUE;
                $no = $header->no_retur;
        
                $product_id = $object->product_id;
                $branch_id = $header->branch_id;
                $qty = $object->qty;
                break;
            
            case 'sales_order_detail':
                $stock_in = $method=='delete' ? TRUE : FALSE;
                $no = $header->no_invoice;
        
                $product_id = $object->product_id;
                $branch_id = $header->branch_id;
                $qty = $object->qty;
                break;

            case 'retur_purchase_order_detail':
                $stock_in = $method=='delete' ? TRUE : FALSE;
                $no = $header->no_retur;
        
                $product_id = $object->product_id;
                $branch_id = $header->branch_id;
                $qty = $object->qty;
                break;
            
            default:
                // do nothing
                break;
        }

        return [
            (integer) $product_id, 
            (integer) $branch_id, 
            (integer) $qty, 
            (integer) $stock_in, 
            $no
        ];
    }
}
