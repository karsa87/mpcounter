<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class IncomeAllExport implements FromView, WithEvents
{
    use Exportable;
    
    private $params = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    public function view(): View
    {
        return view('report.income_all.export', $this->params);
    }
    
    public function columnFormats(): array
    {
        return [
            // 'C' => NumberFormat::FORMAT_DATE_DATETIME,
            // 'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                // All headers - set font size to 14
                $sheet = $event->sheet->getDelegate();
                $cellRange = 'A1:W1'; 
                $sheet->getStyle($cellRange)->getFont()->setSize(14);

                $sheet->getColumnDimension('A')->setWidth(20);
                $sheet->getColumnDimension('B')->setWidth(40);
                $sheet->getColumnDimension('C')->setWidth(20);
                $sheet->getColumnDimension('D')->setWidth(30);
                $sheet->getColumnDimension('E')->setWidth(15);
                $sheet->getDefaultRowDimension()->setRowHeight(-1);

                // Set A1:D4 range to wrap text in cells
                $sheet->getStyle('A2:E' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
                $sheet->getStyle('A2:E' . $sheet->getHighestRow())->getAlignment()->setVertical('center');

            },
        ];
    }
}