<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class StockOpnameExport implements FromView, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    
    private $sop = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($sop)
    {
        $this->sop = $sop;
    }

    public function view(): View
    {
        return view('transaction.stock_opname.print', [
            'sop' => $this->sop,
            'products' => $this->sop->details->sortBy('product.name'),
        ]);
    }
    
    public function columnFormats(): array
    {
        return [
            
        ];
    }
}
