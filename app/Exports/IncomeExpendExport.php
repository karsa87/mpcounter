<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class IncomeExpendExport implements FromView, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    
    private $params = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    public function view(): View
    {
        return view('report.income_expend.export', $this->params);
    }
    
    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_DATETIME,
            // 'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}