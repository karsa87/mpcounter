<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class StockExport implements FromView, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    
    private $products = null;
    private $branch = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($products, $branch)
    {
        $this->products = $products;
        $this->branch = $branch;
    }

    public function view(): View
    {
        return view('report.stock.export', [
            'products' => $this->products,
            'branch' => $this->branch,
        ]);
    }
    
    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
