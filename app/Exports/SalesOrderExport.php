<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class SalesOrderExport implements FromView, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    
    private $salesOrders = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($salesOrders)
    {
        $this->salesOrders = $salesOrders;
    }

    public function view(): View
    {
        return view('report.sales_order.export', [
            'salesOrders' => $this->salesOrders
        ]);
    }
    
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_DATE_DATETIME,
            // 'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'N' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'O' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'P' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}
