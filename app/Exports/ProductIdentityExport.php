<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ProductIdentityExport implements FromView, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    
    private $products = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($products)
    {
        $this->products = $products;
    }

    public function view(): View
    {
        return view('report.product_identity.export', [
            'products' => $this->products
        ]);
    }
    
    public function columnFormats(): array
    {
        return [
            // 'G' => NumberFormat::FORMAT_DATE_DATETIME,
        ];
    }
}
