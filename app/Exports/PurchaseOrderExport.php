<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class PurchaseOrderExport implements FromView, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    
    private $purchaseOrders = null;

    /**
     * Create a new export instance.
     *
     * @return void
     */
    public function __construct($purchaseOrders)
    {
        $this->purchaseOrders = $purchaseOrders;
    }

    public function view(): View
    {
        return view('report.purchase_order.export', [
            'purchaseOrders' => $this->purchaseOrders
        ]);
    }
    
    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_DATETIME,
            // 'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}