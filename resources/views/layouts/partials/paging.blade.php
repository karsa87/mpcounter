@php
use App\Util\Helpers\Util;
@endphp

<div class="card-footer clearfix">
    <div class="row">
        <div class="col-lg-4">
            @php
                $first_no = (($rows->currentPage()-1)* $rows->perPage()) + 1;
                $first_no = Util::format_currency($first_no);
                $last_no = (($rows->currentPage()-1)* $rows->perPage()) + $rows->count();
                $last_no = Util::format_currency($last_no);
                $total = $rows->total();
                $total = Util::format_currency($total);
                $text = trans('global.showing');
                $text = sprintf($text, $first_no, $last_no, $total);
            @endphp
            {!! $text !!}
        </div>
        <div class="col-lg-8">
            <nav aria-label="Page navigation example">
                @if ($rows->lastPage() > 1)
                <ul class="pagination justify-content-end">
                    @php
                    $pagcurrent = $rows->currentPage();
                    $paglast = $rows->lastPage();
                    $dots = false;
                    $i = ($pagcurrent > 5) ? ($pagcurrent - 10) : $pagcurrent;
                    @endphp

                    <li class="page-item {{ ($pagcurrent == 1) ? ' disabled' : '' }}">
                        <a class="page-link" href="{{ $rows->url(1) }}" aria-label="@lang('global.first')">
                            <span aria-hidden="true">@lang('global.first')</span>
                        </a>
                    </li>
                    <li class="page-item {{ ($pagcurrent == 1) ? ' disabled' : '' }}">
                        <a class="page-link" href="{{ $rows->url($pagcurrent-1) }}" aria-label="@lang('global.prev')">
                            <span aria-hidden="true">«</span>
                            <span class="sr-only">@lang('global.prev')</span>
                        </a>
                    </li>

                    @for( $i=$i; $i<=(( $pagcurrent+2 >= $paglast-2 ? $paglast-3 : $pagcurrent+2 )); $i++ )
                        @if( $i == $pagcurrent )
                            <li class="page-item {{ ($pagcurrent == $i) ? ' active' : '' }}">
                                <a class="page-link" href="{{ $rows->url($i) }}">{{ Util::format_currency($i) }}</a>
                            </li>
                        @else
                            @if( ( $pagcurrent < 2 && $i < 2 ) || ( ( $i > $pagcurrent-4 && $i < $pagcurrent ) || $i > $pagcurrent && $i < $pagcurrent+2 ) )
                                <li class="page-item {{ ($pagcurrent == $i) ? ' active' : '' }}">
                                    <a class="page-link" href="{{ $rows->url($i) }}">{{ Util::format_currency($i) }}</a>
                                </li>
                            @else
                                @if( $i > $pagcurrent && !$dots )
                                    <li class="page-item {{ ($pagcurrent == $i) ? ' active' : '' }} disabled">
                                        <a class="page-link" href="#">...</a>
                                    </li>
                                    @php
                                        $dots = true;
                                    @endphp
                                @endif
                            @endif
                        @endif
                    @endfor
                    
                    @for( $i=($paglast>2 ? $paglast - 2 : 1); $i<=$paglast; $i++ )
                        @if( $i == $pagcurrent )
                            <li class="page-item {{ ($pagcurrent == $i) ? ' active' : '' }}">
                                <a class="page-link" href="{{ $rows->url($i) }}">{{ Util::format_currency($i) }}</a>
                            </li>
                        @else
                            @if( ( $pagcurrent < 2 && $i < 2 ) || ( ( $i > $pagcurrent-4 && $i < $pagcurrent ) || $i > $paglast-2 || $i > $pagcurrent && $i < $pagcurrent+2 ) )
                                <li class="page-item {{ ($pagcurrent == $i) ? ' active' : '' }}">
                                    <a class="page-link" href="{{ $rows->url($i) }}">{{ Util::format_currency($i) }}</a>
                                </li>
                            @endif
                        @endif
                    @endfor
                    
                    <li class="page-item {{ ($pagcurrent == $rows->lastPage()) ? ' disabled' : '' }}">
                        <a class="page-link" href="{{ $rows->url($pagcurrent+1) }}" aria-label="@lang('global.next')">
                            <span aria-hidden="true">»</span><span class="sr-only">@lang('global.next')</span>
                        </a>
                    </li>
                    <li class="page-item {{ ($pagcurrent == $rows->lastPage()) ? ' disabled' : '' }}">
                        <a class="page-link" href="{{ $rows->url($rows->lastPage()) }}" aria-label="@lang('global.last')">
                            <span aria-hidden="true">@lang('global.last')</span>
                        </a>
                    </li>
                </ul>
                @endif
            </nav>
        </div>
    </div>
</div>

