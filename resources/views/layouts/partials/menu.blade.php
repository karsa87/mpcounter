@php
$parents = collect($parents);
$parents = $parents->sortBy('order');
@endphp
@foreach( $parents as $parent )
    @if( !$parent['is_parent'] && empty($parent['route']) && empty($parent['url']) && empty($parent['icon']) )
    <li class="nav-header">@lang('global.menu.' . $parent['url'])</li>
    @else
    <li data-username="@lang('global.menu.' . $parent['url'])" class="nav-item {{ count($parent['submenu']) > 0 ? 'has-treeview' : '' }} {{ is_numeric(array_search($parent['id'], $active_menu)) && $parent['is_parent'] ? 'menu-open' : '' }}">
        <a href="{{ $parent['route'] ? route($parent['route']) : '#' }}" class="nav-link {{ $parent['is_parent'] ? 'nav-item' : '' }} {{ is_numeric(array_search($parent['id'], $active_menu)) ? 'active' : '' }}">
            @if( $parent['icon'] )
            <i class="{{ $parent['icon'] }}"></i>
            @else
            <i class="far fa-fw fa-circle "></i>
            @endif
            <p>
                @lang('global.menu.' . $parent['url'])
                @if( count($parent['submenu']) > 0 )
                <i class="fas fa-angle-left right"></i>
                @endif
            </p>
        </a>

        @if( count($parent['submenu']) > 0 )
        <ul class="nav nav-treeview">
            @include($template . 'layouts.partials.menu',['parents' => $parent['submenu'], 'active_menu' => $active_menu])
        </ul>
        @endif
    </li>
    @endif
@endforeach