@extends('layouts.main')

@section('classes_body', 'sidebar-mini layout-fixed sidebar-collapse')

@section('body')
<div class="wrapper">
    <nav class="main-header navbar navbar-expand-md navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#">
                    <i class="fas fa-bars"></i>
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </li>
            {{-- 
            <form action="#" method="get" class="form-inline ml-2 mr-2">
                <div class="input-group">
                    <input class="form-control form-control-navbar" type="search" name="q" placeholder="search" aria-label="search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            --}}
        </ul>
        <ul class="navbar-nav ml-auto ">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="http://localhost/rnd/global_pos/public/home" class="brand-link ">
            <img src="{{ asset('img/logo.png') }}" alt="{{ config('adminlte.logo_img_alt') }}" class="{{ config('adminlte.logo_img_class') }} elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light ">
                POS <small>(Point Of Sales)</small>
            </span>
        </a>
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ auth()->user()->image() ?: asset('img/user1-128x128.jpg') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="{{ route('setting.user.profile') }}" class="d-block">
                        {{ auth()->user()->name }}
                    </a>
                </div>
            </div>

            <nav class="mt-2">
                @php
                    $user = \Auth::user();
                    $current_menu = \App\Util\Base\Layout::getCurrentMenu();
                    $menus = \App\Util\Base\Layout::buildRowMenu();
                @endphp
                <ul class="nav nav-pills nav-sidebar flex-column " data-widget="treeview" role="menu">
                    <li class="nav-item ">
                        <a class="nav-link {{ empty($current_menu['menu_id']) ? 'active' : '' }}" href="{{ route('dashboard') }}">
                            <i class="fas fa-home "></i>
                            <p>
                                @lang('global.menu.dashboard')
                            </p>
                        </a>
                    </li>
                    @include($template . 'layouts.partials.menu',['parents' => $menus['hierarchy'],'active_menu' => $current_menu['active_menu']])
                </ul>
            </nav>
        </div>
    </aside>

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            {{ $current_menu['title'] }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @foreach ( array_reverse($current_menu['active_menu']) as $id)
                                @if (!empty($id))
                                    <li class="breadcrumb-item {{ $id === $current_menu['menu_id'] ? 'active' : '' }}">
                                        @if ($id === $current_menu['menu_id'])
                                        {{ $menus['id_name'][$id] }}
                                        @else
                                        <a href="{{ $menus['id_url'][$id] }}">{{ $menus['id_name'][$id] }}</a>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-header">
            <div class="container-fluid">
            </div>
        </div>

        <div class="content">
            <div class="container-fluid" id="app">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy;{{ date('Y') }} <a href="https://www.asrakit.com" target="_blank">{{ config('adminlte.title') }}</a>.</strong> All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> {{ config('app.version') }}
    </div>
</footer>
@endsection