@extends('layouts.main')

@section('classes_body', 'login-page')

@section('body')
@yield('content')
@stop