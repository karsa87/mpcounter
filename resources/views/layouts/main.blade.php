<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ isset($meta_title) ? $meta_title : trans('global.menu.dashboard') }} | {{ env('APP_NAME','POS Global') }}</title>

    <meta name="description" content="{{ isset($meta_description) ? $meta_description : trans('global.menu.dashboard') }} | {{ env('APP_NAME','POS Global') }}"/>
    <meta name="keywords" content="{{ isset($meta_keyword) ? $meta_keyword : trans('global.menu.dashboard') }}">
    <meta name="author" content="Mochamad Karsa Syaifudin Jupri" />
    <meta name="path" content="{{ explode('/',Request::path())[0] }}"/>
    <meta name="full" content="{{ Request::fullUrl() }}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base" content="{{ url('') }}"/>
    <meta name="referrer" content="no-referrer"/>
    <meta name="storage_url" content="{{ \Storage::url('/') }}"/>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}?{{ config('app.version') }}">
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}?{{ config('app.version') }}">
    @stack('css')
    @yield('css')
</head>
<body class="@yield('classes_body')">
    @yield('body')
    <script src="{{ url('js/lang.js') }}?{{ config('app.version') }}"></script>
    <script src="{{ url('js/permission.js') }}?{{ config('app.version') }}"></script>
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}?{{ config('app.version') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}?{{ config('app.version') }}"></script>
    <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}?{{ config('app.version') }}"></script>
    <script src="{{ asset('js/app.js') }}?{{ config('app.version') }}"></script>
    <script src="{{ asset('js/vendor.js') }}?{{ config('app.version') }}"></script>
    @stack('js')
    @yield('js')

    @include('layouts.partials.flash_message')
</body>
</html>
