@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-12">
        <!-- Main content -->
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4>
                        <i class="fas fa-file-invoice"></i> @lang('global.detail') @lang('mutation_product.title')
                        <small class="float-right">@lang('mutation_product.label.date'): {{ $model->date }}</small>
                    </h4>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    @lang('global.from')
                    <address>
                        <strong>{{ $model->fromBranch->name }}</strong><br>
                        {{ $model->fromBranch->address }} <br>
                        @lang('branch.label.phone'): {{ $model->fromBranch->phone }}<br>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    @lang('global.to')
                    <address>
                        <strong>{{ $model->toBranch->name }}</strong><br>
                        {{ $model->toBranch->address }} <br>
                        @lang('branch.label.phone'): {{ $model->toBranch->phone }}<br>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>@lang('mutation_product.label.no_mutation'): {{ $model->no_mutation }}</b>
                    <br>
                    <b>@lang('mutation_product.label.employee_id'):</b> {{ $model->employee->name }}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>@lang('global.no')</th>
                                <th>@lang('mutation_product.label.product_id')</th>
                                <th>@lang('mutation_product.label.identity')</th>
                                <th class="text-center">@lang('mutation_product.label.qty')</th>
                                <th class="text-right">@lang('mutation_product.label.sell_price')</th>
                                <th class="text-right">@lang('mutation_product.label.total_amount')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->details as $i => $detail)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>
                                    {{ $detail->product->name }}
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.category_id'):</strong> {{ $detail->product->category->name }}
                                        </small>
                                    </p>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.brand_id'):</strong> {{ $detail->product->brand->name }}
                                        </small>
                                    </p>
                                </td>
                                <td>{{ $detail->productIdentity->identity ?? '-' }}</td>
                                <td class="text-center">{{ $util->format_currency($detail->qty) }}</td>
                                <td class="text-right">{{ $util->format_currency($detail->sell_price) }}</td>
                                <td class="text-right">{{ $util->format_currency($detail->total_amount) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">
                                    @lang('mutation_product.label.subtotal'):
                                </th>
                                <td class="text-right">
                                    {{ $util->format_currency($model->total_amount_detail) }}
                                </td>
                            </tr>
                            <tr>
                                <th style="width:50%">
                                    @lang('mutation_product.label.shipping_cost'):
                                </th>
                                <td class="text-right">
                                    {{ $util->format_currency($model->shipping_cost) }}
                                </td>
                            </tr>
                            <tr>
                                <th style="width:50%">
                                    @lang('mutation_product.label.additional_cost'):
                                </th>
                                <td class="text-right">
                                    {{ $util->format_currency($model->additional_cost) }}
                                </td>
                            </tr>
                            <tr>
                                <th style="width:50%">
                                    @lang('mutation_product.label.total'):
                                </th>
                                <td class="text-right">
                                    {{ $util->format_currency($model->total_amount) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-12">
                    <a href="{{ route('transaction.mutation.product.index') }}" class="btn btn-danger">
                        @lang('global.back')
                    </a>

                    @hasPermission('transaction.mutation.product.edit')
                    <a href="{{ route('transaction.mutation.product.edit', $model->id) }}" class="btn btn-warning float-right">
                        @lang('global.edit')
                    </a>
                    @endhasPermission

                    <a id="btn-print" class="btn btn-default float-right mr-1">
                        @lang('mutation_product.label.print_nota')
                    </a>
                </div>
            </div>
        </div>
        <!-- /.invoice -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop