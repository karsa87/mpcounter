@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('mutation_product.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.mutation_product.search')
        </div>
    </div>
</div>
<div class="row" id="content-mutation_product">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('mutation_product.title')</h3>
                
                @hasPermission('transaction.mutation.product.create')
                <a href="{{ route('transaction.mutation.product.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
                @endhasPermission
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-stripped table-hover sorting-table" data-form="form-search-so">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="date">@lang('mutation_product.label.date')</th>
                            <th class="sorting-form" data-column="no_mutation">@lang('mutation_product.label.no_mutation')</th>
                            <th>@lang('mutation_product.label.employee_id')</th>
                            <th>@lang('mutation_product.label.information')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($mutation_products) && count($mutation_products) > 0 )
                        @foreach( $mutation_products as $i => $r)
                        <tr>
                            <td class="text-center">{{ $mutation_products->firstItem() + $i }}</td>
                            <td class="text-wrap text-center">
                                {{ $util->formatDate($r->date) }}
                            </td>
                            <td class="text-wrap">
                                {{ $r->no_mutation }}
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                {{ $r->employee->name }}
                            </td>
                            <td class="text-wrap">
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('mutation_product.label.from_branch_id'):</strong> {{ $r->fromBranch->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('mutation_product.label.to_branch_id'):</strong> {{ $r->toBranch->name }}
                                    </small>
                                </p>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('transaction.mutation.product.edit')
                                    <a title="@lang('global.edit')" href="{{ route('transaction.mutation.product.edit', $r['id']) }}" class="btn btn-sm btn-warning" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.mutation.product.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('transaction.mutation.product.show', $r['id']) }}" class="btn btn-sm btn-info" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.mutation.product.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('transaction.mutation.product.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                    
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($mutation_products) && count($mutation_products) > 0 )
            {!! \App\Util\Base\Layout::paging($mutation_products) !!}
            @endif
        </div>
    </div>
</div>
@stop