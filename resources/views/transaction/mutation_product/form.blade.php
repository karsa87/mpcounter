@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<!-- form start -->
<form 
    id="form-mutation_product" role="form" method="POST" 
    action="{{ $model->exists ? route('transaction.mutation.product.update', $model->id) : route('transaction.mutation.product.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('mutation_product.title')</h3>
            </div>
            @csrf

            <input type="hidden" name="id" value="{{ $model->id }}">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-4">
                        <div class="form-group">
                            <label for="no_mutation">@lang('mutation_product.label.no_mutation')</label>
                            <input type="text" name="no_mutation" class="form-control @classInpError('no_mutation')" placeholder="@lang('mutation_product.placeholder.no_mutation')" disabled value="{{ old('no_mutation') ?? $model->nextNo() }}">
                            
                            @inpSpanError(['column'=>'no_mutation'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-4">
                        <div class="form-group">
                            <label for="date">@lang('mutation_product.label.date')</label>
                            <input type="text" name="date" class="form-control datetimepicker @classInpError('date')" placeholder="@lang('mutation_product.placeholder.date')" autocomplete="off" value="{{ old('date') ?? $model->date }}">
                            
                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>

                    @if (empty(config('default.employee_id')))
                    <div class="col-xs-12 col-sm-12 col-lg-4">
                        <div class="form-group">
                            <label for="employee_id">@lang('mutation_product.label.employee_id')</label>
                            @php
                                $selected = old('employee_id') ?? $model->employee_id;
                            @endphp
                            <select name="employee_id"  class="form-control select2 @classInpError('employee_id')" data-placeholder="" style="width: 100%;">
                                <option value="" selected>&nbsp;</option>
                                @foreach ($employees as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @inpSpanError(['column'=>'employee_id'])
                        </div>
                    </div>
                    @endif
            
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>

    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.list') @lang('mutation_product.label.select_product')</h3>
            </div>

            <div class="card-body">
                @php
                    $input_shipping_cost = old('shipping_cost') ?? $model->shipping_cost;
                    $input_shipping_cost = $util->remove_format_currency($input_shipping_cost ?? '0');
                    $input_additional_cost = old('additional_cost') ?? $model->additional_cost;
                    $input_additional_cost = $util->remove_format_currency($input_additional_cost ?? '0');
                    
                    $input_products = old('products') ?? (isset($details) ? $details : []);
                    $input_products = json_encode($input_products);
                    
                    $input_from_branch_id = old('from_branch_id') ?? ( $model->from_branch_id ?? ($branchs->first() ? $branchs->first()->id : 0));
                    $input_to_branch_id = old('to_branch_id') ?? ( $model->to_branch_id ?? ($branchs->last() ? $branchs->last()->id : 0));
                    
                @endphp
                <list-product-mp 
                    v-bind:update="{{ $model->exists ? 1 : 0 }}"
                    v-bind:categories="{{ $categories }}"
                    v-bind:branchs="{{ $branchs }}"
                    v-bind:input_products="{{ $input_products }}"
                    v-bind:input_shipping_cost="{{ $input_shipping_cost }}"
                    v-bind:input_additional_cost="{{ $input_additional_cost }}"
                    v-bind:input_from_branch_id="{{ $input_from_branch_id }}"
                    v-bind:input_to_branch_id="{{ $input_to_branch_id }}"
                    >
                </list-product-mp>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-footer">
                <a href="{{ route('transaction.mutation.product.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/mutation_product.js') }}?{{ config('app.version') }}"></script>
@endpush