<!-- form start -->
<form role="form" id="form-search-so" action="{{ route('transaction.mutation.product.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="no_mutation">@lang('mutation_product.label.no_mutation')</label>
                    <input type="text" name="_nmp" class="form-control" placeholder="@lang('mutation_product.placeholder.no_mutation')" value="{{ request('_nmp') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="date">@lang('mutation_product.label.date')</label>
                    <input type="text" name="_dt" class="form-control datetime-rangepicker" autocomplete="off" placeholder="@lang('mutation_product.placeholder.date')" value="{{ request('_dt') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_fbc">@lang('mutation_product.label.from_branch_id')</label>
                    <select name="_fbc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($fromBranchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_fbc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_tbc">@lang('mutation_product.label.to_branch_id')</label>
                    <select name="_tbc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($toBranchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_tbc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_emp">@lang('mutation_product.label.employee_id')</label>
                    <select name="_emp"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($employees as $id => $name)
                        <option value="{{ $id }}" {{ request('_emp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.mutation.product.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>