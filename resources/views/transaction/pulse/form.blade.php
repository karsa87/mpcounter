<!-- form start -->
<form id="form-pulse" role="form" method="POST" action="{{ route('transaction.pulse.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="no_transaction">@lang('pulse.label.no_transaction')</label>
            <input type="text" name="no_transaction" class="form-control @classInpError('no_transaction')" placeholder="@lang('pulse.placeholder.no_transaction')" autocomplete="off" value="{{ old('no_transaction') ?? $model->nextNo() }}">
            
            @inpSpanError(['column'=>'no_transaction'])
        </div>
        <div class="form-group">
            <label for="date">@lang('pulse.label.date')</label>
            <input type="text" name="date" class="form-control datetimepicker @classInpError('date')" placeholder="@lang('pulse.placeholder.date')" autocomplete="off" value="{{ old('date') }}">
            
            @inpSpanError(['column'=>'date'])
        </div>
        <div class="form-group">
            <label for="phone">@lang('pulse.label.phone')</label>
            <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('pulse.placeholder.phone')" autocomplete="off" value="{{ old('phone') }}">
            
            @inpSpanError(['column'=>'phone'])
        </div>

        <div class="form-group">
            <label for="form-branch_id" class="control-label">@lang('pulse.label.branch_id')</label>
            <select name="branch_id" id="form-branch_id" class="form-control select2 @classInpError('branch_id')">
                @foreach($branchs as $id => $name)
                    <option value="{{ $id }}" {{ old('branch_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'branch_id'])
        </div>

        @if (empty(config('default.bank_id')))
        <div class="form-group">
            <label for="form-bank_id" class="control-label">@lang('pulse.label.bank_id')</label>
            <select name="bank_id" id="form-bank_id" class="form-control select2 @classInpError('bank_id')" data-placeholder="">
                @foreach($banks as $id => $name)
                    <option value="{{ $id }}" {{ old('bank_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'bank_id'])
        </div>
        @endif

        <div class="form-group">
            <label for="total_pulse">@lang('pulse.label.total_pulse')</label>
            <input type="text" name="total_pulse" class="form-control format-number @classInpError('total_pulse')" placeholder="@lang('pulse.placeholder.total_pulse')" value="{{ old('total_pulse') }}">
            
            @inpSpanError(['column'=>'total_pulse'])
        </div>

        <div class="form-group">
            <label for="sell_price">@lang('pulse.label.sell_price')</label>
            <input type="text" name="sell_price" class="form-control format-number @classInpError('sell_price')" placeholder="@lang('pulse.placeholder.sell_price')" value="{{ old('sell_price') }}">
            
            @inpSpanError(['column'=>'sell_price'])
        </div>

        <div class="form-group">
            <label for="capital_price">@lang('pulse.label.capital_price')</label>
            <input type="text" name="capital_price" class="form-control format-number @classInpError('capital_price')" placeholder="@lang('pulse.placeholder.capital_price')" value="{{ old('capital_price') }}">
            
            @inpSpanError(['column'=>'capital_price'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.pulse.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>