@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')
@inject('model', 'App\Models\Pulse')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('pulse.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.pulse.search')
        </div>
    </div>
</div>
<div class="row" id="content-pulse">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('pulse.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-pulse">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 15%;" class="sorting-form" data-column="no_transaction">@lang('pulse.label.no_transaction')</th>
                            <th style="width: 30%;" class="sorting-form" data-column="date">@lang('pulse.label.information')</th>
                            <th style="width: 20%;" class="sorting-form text-right" data-column="capital_price">@lang('pulse.label.capital_price')</th>
                            <th style="width: 20%;" class="sorting-form text-right" data-column="sell_price">@lang('pulse.label.sell_price')</th>
                            <th style="width: 10%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($pulses) && count($pulses) > 0 )
                        @foreach( $pulses as $i => $r)
                        <tr>
                            <td class="text-center">{{ $pulses->firstItem() + $i }}</td>
                            <td class="text-wrap">{{ $r->no_transaction }}</td>
                            <td class="text-wrap">
                                <h6>{{ $util->formatDate($r->date) }}</h6>
                                <p class="m-0">
                                    <small><strong>@lang('pulse.label.phone'): </strong>{{ $r->phone }}</small>
                                </p>
                                <p class="m-0">
                                    <small><strong>@lang('pulse.label.total_pulse'): </strong>{{ $util->format_currency($r->total_pulse) }}</small>
                                </p>
                                <p class="m-0">
                                    <small><strong>@lang('pulse.label.branch_id'): </strong>{{ $r->branch->name }}</small>
                                </p>
                            </td>
                            <td class="text-wrap text-right">
                                {{ $util->format_currency($r->capital_price) }}
                            </td>
                            <td class="text-wrap text-right">
                                {{ $util->format_currency($r->sell_price) }}
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('transaction.pulse.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('transaction.pulse.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-pulse btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.pulse.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('transaction.pulse.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($pulses) && count($pulses) > 0 )
            {!! \App\Util\Base\Layout::paging($pulses) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('pulse.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.pulse.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/pulse.js') }}?{{ config('app.version') }}"></script>
@endpush