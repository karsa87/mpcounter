<!-- form start -->
<form role="form" id="form-search-pulse" action="{{ route('transaction.pulse.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="_k">@lang('global.keyword')</label>
                    <input type="text" name="_k" class="form-control" placeholder="@lang('global.keyword')" value="{{ request('_k') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="date">@lang('pulse.label.date')</label>
                    <input type="text" name="_dt" class="form-control date-rangepicker" placeholder="@lang('pulse.placeholder.date')" autocomplete="off" value="{{ request('_dt') }}">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('pulse.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @if (empty(config('default.bank_id')))
            <div class="col-4">
                <div class="form-group">
                    <label for="_bk">@lang('pulse.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.pulse.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>