<!-- form start -->
<div class="card-body">
    <div class="row">
        <div class="col-4">
            <div class="form-group">
                <label for="no_invoice">@lang('purchase_order.label.no_invoice')</label>
                <input type="text" name="_nivc" class="form-control" placeholder="@lang('purchase_order.placeholder.no_invoice')" value="{{ request('_nivc') }}">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="date">@lang('purchase_order.label.date')</label>
                <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('purchase_order.placeholder.date')" value="{{ request('_dt') }}">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_bk">@lang('purchase_order.label.bank_id')</label>
                <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($banks as $id => $name)
                    <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_bc">@lang('purchase_order.label.branch_id')</label>
                <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($branchs as $id => $name)
                    <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_sp">@lang('purchase_order.label.supplier_id')</label>
                <select name="_sp"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($suppliers as $id => $name)
                    <option value="{{ $id }}" {{ request('_sp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_sts">@lang('purchase_order.label.status')</label>
                <select name="_sts"  class="form-control" style="width: 100%;">
                    <option value=""></option>
                    @foreach (trans('purchase_order.list.status') as $id => $name)
                    <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_tp">@lang('purchase_order.label.type')</label>
                <select name="_tp"  class="form-control" style="width: 100%;">
                    <option value=""></option>
                    @foreach (trans('purchase_order.list.type') as $id => $name)
                    <option value="{{ $id }}" {{ is_numeric(request('_tp')) && request('_tp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
    <a href="{{ route('transaction.purchase.order.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
    <button type="submit" class="btn btn-success btn-loader">
        @lang('global.search')
    </button>
</div>