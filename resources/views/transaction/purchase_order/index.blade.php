@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" id="form-search-po" action="{{ route('transaction.purchase.order.index') }}">
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary collapsed-card">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('purchase_order.title')</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.purchase_order.search')
        </div>
    </div>
</div>
</form>

<div class="row" id="content-purchase_order">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('purchase_order.title')</h3>
                
                @hasPermission('transaction.purchase.order.create')
                <a href="{{ route('transaction.purchase.order.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
                @endhasPermission
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-stripped table-hover sorting-table" data-form="form-search-po">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th>@lang('purchase_order.label.supplier_id')</th>
                            <th class="sorting-form" data-column="no_invoice">@lang('purchase_order.label.no_invoice')</th>
                            <th class="sorting-form" data-column="date">@lang('purchase_order.label.information')</th>
                            <th>@lang('purchase_order.label.type')</th>
                            <th>@lang('purchase_order.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($purchase_orders) && count($purchase_orders) > 0 )
                        @foreach( $purchase_orders as $i => $r)
                        <tr>
                            <td class="text-center">{{ $purchase_orders->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->supplier->name }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('supplier.label.phone'):</strong> {{ $r->supplier->phone }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('supplier.label.address'):</strong> {{ $r->supplier->address }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $r->no_invoice }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.no_reference'):</strong> {{ $r->no_reference }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                {{ $util->formatDate($r->date) }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.bank_id'):</strong> {{ $r->bank->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.branch_id'):</strong> {{ $r->branch->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.down_payment'):</strong> {{ $util->format_currency($r->paid_off) }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.total'):</strong> {{ $util->format_currency($r->total_amount) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <span class="badge {{ $r->type ? 'badge-danger' : 'badge-success' }}">@lang('purchase_order.list.type.'.$r->type)</span>
                            </td>
                            <td class="text-wrap">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('purchase_order.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @if ($r->isNotPaid())
                                    @hasPermission('finance.debt.payment.po.create')
                                    <a title="@lang('purchase_order.label.debt_payment')" href="{{ route('finance.debt.payment.po.create', ['_poid' => $r->id]) }}" class="btn btn-sm btn-primary" data-toggle="tooltip">
                                        <i class="fas fa-money-bill-alt m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @if ($r->retur->count() <= 0)
                                    @hasPermission('transaction.purchase.order.edit')
                                    <a title="@lang('global.edit')" href="{{ route('transaction.purchase.order.edit', $r['id']) }}" class="btn btn-sm btn-warning" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @hasPermission('transaction.purchase.order.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('transaction.purchase.order.show', $r['id']) }}" class="btn btn-sm btn-info" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @if ($r->retur->count() <= 0 && $r->debtPayments->count() <= 0)
                                    @hasPermission('transaction.purchase.order.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('transaction.purchase.order.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($purchase_orders) && count($purchase_orders) > 0 )
            {!! \App\Util\Base\Layout::paging($purchase_orders) !!}
            @endif
        </div>
    </div>
</div>
@stop