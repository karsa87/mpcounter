@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<!-- form start -->
<form 
    id="form-purchase_order" role="form" method="POST" 
    action="{{ $model->exists ? route('transaction.purchase.order.update', $model->id) : route('transaction.purchase.order.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('purchase_order.title')</h3>
            </div>
            @csrf

            <input type="hidden" name="id" value="{{ $model->id }}">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="no_invoice">@lang('purchase_order.label.no_invoice')</label>
                            <input type="text" name="no_invoice" class="form-control @classInpError('no_invoice')" placeholder="@lang('purchase_order.placeholder.no_invoice')" disabled value="{{ old('no_invoice') ?? $model->nextNo() }}">
                            
                            @inpSpanError(['column'=>'no_invoice'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="no_reference">@lang('purchase_order.label.no_reference')</label>
                            <input type="text" name="no_reference" class="form-control @classInpError('no_reference')" placeholder="@lang('purchase_order.placeholder.no_reference')" autocomplete="off" value="{{ old('no_reference') ?? $model->no_reference }}">
                            
                            @inpSpanError(['column'=>'no_reference'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="date">@lang('purchase_order.label.date')</label>
                            <input type="text" name="date" class="form-control datetimepicker @classInpError('date')" placeholder="@lang('purchase_order.placeholder.date')" autocomplete="off" value="{{ old('date') ?? ($model->date ?? date('Y-m-d H:i')) }}">
                            
                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="supplier_id">@lang('purchase_order.label.supplier_id')</label>
                            @php
                                $selected = old('supplier_id') ?? $model->supplier_id;
                            @endphp
                            <select name="supplier_id"  class="form-control select2 @classInpError('supplier_id')" data-placeholder="" style="width: 100%;">
                                @foreach ($suppliers as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @inpSpanError(['column'=>'supplier_id'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="branch_id">@lang('purchase_order.label.branch_id')</label>
                            @php
                                $selected = old('branch_id') ?? $model->branch_id;
                            @endphp
                            <select name="branch_id"  class="form-control select2 @classInpError('branch_id')" data-placeholder="" style="width: 100%;" {{ $model->exists ? 'disabled' : '' }}>
                                @foreach ($branchs as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @inpSpanError(['column'=>'branch_id'])
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="type">@lang('purchase_order.label.type')</label>
                            <span data-toggle="tooltip" data-placement="top" title="@lang('purchase_order.message.desc_type')">
                                <i class="fas fa-question-circle"></i>
                            </span>
                            @php
                                $selected = old('type') ?? $model->type;
                            @endphp
                            <select name="type"  class="form-control @classInpError('type')" style="width: 100%;">
                                @foreach (trans('purchase_order.list.type') as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @inpSpanError(['column'=>'type'])
                        </div>
                    </div>

                    {{-- @if (empty(config('default.bank_id'))) --}}
                    <div id="div-bank-id" class="col-xs-12 col-sm-12 col-lg-6" style="display: none;">
                        <div class="form-group">
                            <label for="bank_id">@lang('purchase_order.label.bank_id')</label>
                            @php
                                $selected = old('bank_id') ?? $model->bank_id;
                            @endphp
                            <select name="bank_id"  class="form-control select2 @classInpError('bank_id')" data-placeholder="" style="width: 100%;">
                                @foreach ($banks as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                            @inpSpanError(['column'=>'bank_id'])
                        </div>
                    </div>
                    {{-- @endif --}}
                    
                    <div id="div-date-max-payable" class="col-xs-12 col-sm-12 col-lg-6" style="display: none">
                        <div class="form-group">
                            <label for="date_max_payable">@lang('purchase_order.label.date_max_payable')</label>
                            <input type="text" name="date_max_payable" class="form-control datepicker @classInpError('date_max_payable')" placeholder="@lang('purchase_order.placeholder.date_max_payable')" autocomplete="off" value="{{ old('date_max_payable') ?? $model->date_max_payable }}">
                            
                            @inpSpanError(['column'=>'date_max_payable'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="information">@lang('purchase_order.label.information')</label>
                            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('purchase_order.placeholder.information')" >{{ old('information') ?? $model->information }}</textarea>
                            
                            @inpSpanError(['column'=>'information'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                        <label for="form-drop_files" class="control-label">@lang('purchase_order.label.images')</label>
                        <input type="hidden" name="images" id="result-upload" value="{{ old('images') ?? $model->images }}">
                        <div class="dropzone" id="files-upload" data-href="{{ route('transaction.purchase.order.upload') }}"></div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>

    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.list') @lang('purchase_order.label.buy_product')</h3>
            </div>

            <div class="card-body p-0">
                @php
                    $input_down_payment = old('down_payment') ?? $model->down_payment;
                    $input_down_payment = $util->remove_format_currency($input_down_payment ?? '0');
                    $input_products = old('products') ?? (isset($details) ? $details : []);
                    $input_products = json_encode($input_products);
                @endphp
                <list-product 
                    v-bind:categories="{{ $categories }}"
                    {{-- v-bind:branchs="{{ $branchs }}" --}}
                    v-bind:brands="{{ $brands }}"
                    v-bind:input_products="{{ $input_products }}"
                    v-bind:input_down_payment="{{ $input_down_payment }}"
                    >
                </list-product>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-footer">
                <a href="{{ route('transaction.purchase.order.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</div>
</form>

@include('master.product.modal_form')
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/purchase_order.js') }}?{{ config('app.version') }}"></script>
@endpush