@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-12">
        <!-- Main content -->
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4>
                        <i class="fas fa-file-invoice"></i> @lang('global.detail') @lang('purchase_order.title')
                        <small class="float-right">@lang('purchase_order.label.date'): {{ $model->date }}</small>
                    </h4>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    @lang('global.from')
                    <address>
                        <strong>{{ $model->supplier->name }}</strong><br>
                        {{ $model->supplier->address }} <br>
                        @lang('supplier.label.phone'): {{ $model->supplier->phone }}<br>
                        @lang('supplier.label.sales_name'): {{ $model->supplier->sales_name }}<br>
                        @lang('supplier.label.sales_phone'): {{ $model->supplier->sales_phone }}<br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    @lang('global.to')
                    <address>
                        <strong>{{ $model->branch->name }}</strong><br>
                        {{ $model->branch->address }} <br>
                        @lang('branch.label.phone'): {{ $model->branch->phone }}<br>
                    </address>
                    <strong>@lang('purchase_order.label.information')</strong>
                    <address>
                        {{ $model->information ?? '-' }}</strong><br>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>@lang('purchase_order.label.no_invoice'): {{ $model->no_invoice }}</b>
                    <br>
                    <br>
                    <b>@lang('purchase_order.label.type'):</b> @lang('purchase_order.list.type.'.$model->type) - {{ $model->bank->name }}
                    <br>
                    <b>@lang('purchase_order.label.date_max_payable'):</b> {{ $util->formatDate($model->date_max_payable, 'Y-m-d') ?? '-' }}
                    <br>
                    <b>@lang('purchase_order.label.status'):</b> <span class="badge {{ $model->status ? 'badge-success' : 'badge-danger' }}">@lang('purchase_order.list.status.'.$model->status)</span>
                    <br>
                    <b>@lang('purchase_order.label.pic'):</b> {{ $model->createdBy ? $model->createdBy->employeeName : '' }}
                    
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>@lang('global.no')</th>
                                <th>@lang('purchase_order.label.product_id')</th>
                                <th>@lang('purchase_order.label.identity')</th>
                                <th class="text-center">@lang('purchase_order.label.purchase_price')</th>
                                <th class="text-center">@lang('purchase_order.label.qty')</th>
                                <th class="text-center">@lang('purchase_order.label.sub_total')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->details as $i => $detail)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>
                                    {{ $detail->product->name }}
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.category_id'):</strong> {{ $detail->product->category->name }}
                                        </small>
                                    </p>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.brand_id'):</strong> {{ $detail->product->brand->name }}
                                        </small>
                                    </p>
                                </td>
                                <td>{{ $detail->productIdentity->identity ?? '-' }}</td>
                                <td class="text-right">{{ $util->format_currency($detail->purchase_price) }}</td>
                                <td class="text-center">{{ $util->format_currency($detail->qty) }}</td>
                                <td class="text-right">{{ $util->format_currency($detail->total_amount) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <div class="table-responsive">
                        <table class="table">
                            @php
                                $total_paid = $model->debtPayments->sum('paid');
                            @endphp
                            <tr>
                                <th style="width:50%">@lang('purchase_order.label.total'):</th>
                                <td class="text-right">{{ $util->format_currency($model->total_amount) }}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">
                                    @lang('debt_payment_po.label.total_paid')
                                </th>
                                <th class="text-right">
                                    <span class="badge badge-success text-md">{{ $util->format_currency($total_paid + $model->paid_off) }}</span>
                                    
                                </th>
                            </tr>
                            <tr>
                                <th style="width:50%">
                                    @lang('purchase_order.label.remaining_debt')
                                </th>
                                <th class="text-right">
                                    <span class="badge badge-danger text-md">{{ $util->format_currency($model->total_amount - ($total_paid + $model->paid_off)) }}</span>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-12">
                    <a href="{{ route('transaction.purchase.order.index') }}" class="btn btn-danger">
                        @lang('global.back')
                    </a>

                    @if ($model->retur->count() <= 0)
                    @hasPermission('transaction.purchase.order.edit')
                    <a href="{{ route('transaction.purchase.order.edit', $model->id) }}" class="btn btn-warning float-right">
                        @lang('global.edit')
                    </a>
                    @endhasPermission
                    @endif

                    @if ($model->isNotPaid())
                    @hasPermission('finance.debt.payment.po.create')
                    <a href="{{ route('finance.debt.payment.po.create', ['_poid' => $model->id]) }}" class="btn btn-primary float-right mr-1">
                        @lang('purchase_order.label.debt_payment')
                    </a>
                    @endhasPermission
                    @endif

                    <a id="btn-print" class="btn btn-default float-right mr-1">
                        @lang('purchase_order.label.print_nota')
                    </a>
                </div>
            </div>
        </div>
        <!-- /.invoice -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row" id="tab-detail">
    <div class="col-12">
        <!-- Custom Tabs -->
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills mr-auto p-2">
                    <li class="nav-item"><a class="nav-link active" href="#tab_images" data-toggle="tab">@lang('purchase_order.label.images')</a></li>
                    <li class="nav-item"><a class="nav-link" href="#tab_retur" data-toggle="tab">@lang('retur_purchase_order.title')</a></li>
                    <li class="nav-item"><a class="nav-link" href="#tab_debt_payment" data-toggle="tab">@lang('debt_payment_po.title')</a></li>
                </ul>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_images">
                        <div class="row p-3">
                            <div class="col-lg-12">
                            @foreach ($model->images_arr() as $i => $image)
                                <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                    <img src="{{ $image }}" class="img-fluid m-2 img-lg center-cropped" alt="Images {{ $i+1 }}"/>
                                </a>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_retur">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>@lang('global.no')</th>
                                    <th>@lang('retur_purchase_order.label.product_id')</th>
                                    <th>@lang('retur_purchase_order.label.identity')</th>
                                    <th class="text-center">@lang('retur_purchase_order.label.qty')</th>
                                    <th class="text-right">@lang('retur_purchase_order.label.purchase_price')</th>
                                    <th class="text-right">@lang('retur_purchase_order.label.sub_total')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no=1;
                                    $total_amount = 0;
                                @endphp
                                @if( $model->retur->count() > 0 )
                                @foreach ($model->retur as $retur)
                                    @php
                                        $total_amount += $retur->details->sum('total_amount');
                                    @endphp
                                    @foreach ($retur->details as $i => $detail)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            {{ $detail->product->name }} 

                                            @hasPermission('transaction.retur.purchase.order.show')
                                            <a class="btn btn-info btn-xs" target="_blank" href="{{ route('transaction.retur.purchase.order.show', $retur->id) }}">
                                                <i class="fas fa-external-link-square-alt m-0"></i>
                                            </a>
                                            @endhasPermission

                                            <p class="m-0">
                                                <small>
                                                    <strong>@lang('product.label.category_id'):</strong> {{ $detail->product->category->name }}
                                                </small>
                                            </p>
                                            <p class="m-0">
                                                <small>
                                                    <strong>@lang('product.label.brand_id'):</strong> {{ $detail->product->brand->name }}
                                                </small>
                                            </p>
                                        </td>
                                        <td>{{ $detail->productIdentity->identity ?? '-' }}</td>
                                        <td class="text-center">{{ $util->format_currency($detail->qty) }}</td>
                                        <td class="text-right">{{ $util->format_currency($detail->purchase_price) }}</td>
                                        <td class="text-right">{{ $util->format_currency($detail->total_amount) }}</td>
                                    </tr>
                                    @endforeach
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                                </tr>
                                @endif
                            </tbody>
                            <thead>
                                <tr>
                                    <th class="text-right" colspan="5">@lang('retur_purchase_order.label.total')</th>
                                    <th class="text-right">{{ $util->format_currency($total_amount) }}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_debt_payment">
                        <table class="table table-stripped table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                                    <th style="width: 10%;">@lang('debt_payment_po.label.no_debt')</th>
                                    <th style="width: 20%;">@lang('debt_payment_po.label.date')</th>
                                    <th style="width: 25%;">@lang('debt_payment_po.label.information')</th>
                                    <th style="width: 30%;" class="text-center">@lang('debt_payment_po.label.images')</th>
                                    <th style="width: 10%;" class="text-center">@lang('debt_payment_po.label.paid')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $no = 1;
                                $total_paid = 0;
                            @endphp
                                @if( $model->debtPayments->count() > 0 )
                                @foreach( $model->debtPayments as $i => $r)
                                @php
                                    $total_paid += $r->paid;
                                @endphp
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-wrap">
                                        <h6 class="mb-1">
                                            {{ $r->no_debt }}
                                        </h6>

                                        @hasPermission('finance.debt.payment.po.show')
                                        <a class="btn btn-info btn-xs" target="_blank" href="{{ route('finance.debt.payment.po.show', $r->id) }}">
                                            <i class="fas fa-external-link-square-alt m-0"></i>
                                        </a>
                                        @endhasPermission
                                    </td>
                                    <td class="text-wrap">
                                        <h6 class="mb-1">
                                            {{ $util->formatDate($r->date) }}
                                        </h6>
                                    </td>
                                    <td class="text-wrap">
                                        {{ $r->information }}
                                        <p class="m-0">
                                            <small>
                                                <strong>@lang('debt_payment_po.label.bank_id'):</strong> {{ $r->bank->name }}
                                            </small>
                                        </p>
                                    </td>
                                    <td class="text-wrap text-center">
                                        <div class="row">
                                            @foreach ($r->images_arr(3) as $i => $image)
                                            <div class="col-sm-6">
                                                <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                                <img src="{{ $image }}" class="img-fluid mb-2" alt="Images {{ $i+1 }}"/>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        {{ $util->format_currency($r->paid) }}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                                </tr>
                                @endif
                            </tbody>
                            <thead class="bg-gray">
                                <tr>
                                    <th colspan="5" class="text-right">
                                        @lang('debt_payment_po.label.total')
                                    </th>
                                    <th class="text-right">
                                        {{ $util->format_currency($total_paid) }}
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="5" class="text-right">
                                        @lang('purchase_order.label.down_payment')
                                    </th>
                                    <th class="text-right">
                                        {{ $util->format_currency($model->paid_off) }}
                                    </th>
                                </tr>
                                <tr class="bg-green">
                                    <th colspan="5" class="text-right">
                                        @lang('debt_payment_po.label.total_paid')
                                    </th>
                                    <th class="text-right">
                                        {{ $util->format_currency($total_paid + $model->paid_off) }}
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.card-body -->
        </div>
        <!-- ./card -->
    </div>
    <!-- /.col -->
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/purchase_order.js') }}?{{ config('app.version') }}"></script>
@endpush