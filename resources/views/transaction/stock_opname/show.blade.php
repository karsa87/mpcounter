@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-12 col-sm-6">
        <div class="card card-solid">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-text-width"></i>
                    @lang('stock_opname.label.information')
                </h3>
            </div>
            <div class="card-body">
                <dl>
                    <dt>@lang('stock_opname.label.title')</dt>
                    <dd>{{ $model->title }}</dd>
                    <dt>@lang('stock_opname.label.month')</dt>
                    <dd>@lang('global.array.months.' . $model->month)</dd>
                    <dt>@lang('stock_opname.label.year')</dt>
                    <dd>{{ $model->year }}</dd>
                    <dt>@lang('stock_opname.label.branch_id')</dt>
                    <dd>{{ $model->branch->name }}</dd>
                    <dt>@lang('stock_opname.label.information')</dt>
                    <dd>{{ $model->information }}</dd>
                </dl>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="{{ route('transaction.stock.opname.index') }}" class="btn btn-danger btn-lg">
                    @lang('global.back')
                </a>

                @if( $model->isProcess() )
                @hasPermission('transaction.stock.opname.change.process')
                <a title="@lang('stock_opname.label.change_process')" href="{{ route('transaction.stock.opname.change.process', $model['id']) }}" class="btn btn-lg btn-success btn-loader float-right ml-2" data-toggle="tooltip">
                    @lang('stock_opname.label.change_process')
                </a>
                @endhasPermission

                @hasPermission('transaction.stock.opname.form.verified')
                <a title="@lang('stock_opname.label.input_stock')" href="{{ route('transaction.stock.opname.form.verified', $model['id']) }}" class="btn btn-warning btn-lg btn-loader float-right ml-2" data-toggle="tooltip">
                    @lang('stock_opname.label.input_stock')
                </a>
                @endhasPermission
                @endif
    
                @hasPermission('transaction.stock.opname.print')
                <a href="{{ route('transaction.stock.opname.print', $model->id) }}" class="btn btn-default btn-lg float-right" target="_blank">
                    @lang('stock_opname.label.print')
                </a>
                @endhasPermission
            </div>
        </div>
        <!-- /.card -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <list-product-identity-stock-opname
            v-bind:stock_opname_id="{{ $model->id }}"
        ></list-product-identity-stock-opname>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/stock_opname.js') }}?{{ config('app.version') }}"></script>
@endpush