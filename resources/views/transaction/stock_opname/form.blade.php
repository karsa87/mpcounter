@inject('model', 'App\Models\StockOpname')

<!-- form start -->
<form id="form-stock_opname" role="form" method="POST" action="{{ route('transaction.stock.opname.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="title">@lang('stock_opname.label.title')</label>
            <input type="text" name="title" class="form-control @classInpError('title')" placeholder="@lang('stock_opname.placeholder.title')" value="{{ old('title') }}">
            
            @inpSpanError(['column'=>'title'])
        </div>
        <div class="form-group">
            <label for="month">@lang('stock_opname.label.month')</label>
            <input type="text" name="month" class="form-control @classInpError('month')" placeholder="@lang('stock_opname.placeholder.month')" value="{{ old('month') ?? $model->nextMonth() }}" readonly>
            
            @inpSpanError(['column'=>'month'])
        </div>
        <div class="form-group">
            <label for="year">@lang('stock_opname.label.year')</label>
            <input type="text" name="year" class="form-control @classInpError('year')" placeholder="@lang('stock_opname.placeholder.year')" value="{{ old('year') ?? $model->nextYear() }}" readonly>
            
            @inpSpanError(['column'=>'year'])
        </div>
        <div class="form-group">
            <label for="form-branch_id" class="control-label">@lang('stock_opname.label.branch_id')</label>
            
            <select name="branch_id" id="form-branch_id" class="form-control select2 @classInpError('branch_id')">
                @foreach($branchs as $id => $name)
                    <option value="{{ $id }}" {{ old('branch_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'branch_id'])
        </div>
        <div class="form-group">
            <label for="information">@lang('stock_opname.label.information')</label>
            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('stock_opname.placeholder.information')" >{{ old('information') }}</textarea>
            
            @inpSpanError(['column'=>'information'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.stock.opname.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>