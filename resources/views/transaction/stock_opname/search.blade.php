<!-- form start -->
<form role="form" id="form-search-stock_opname" action="{{ route('transaction.stock.opname.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="title">@lang('stock_opname.label.title')</label>
                    <input type="text" name="_ttl" class="form-control" placeholder="@lang('stock_opname.placeholder.title')" value="{{ request('_ttl') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_m">@lang('stock_opname.label.month')</label>
                    <select name="_m"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('global.array.months') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_m')) && request('_m') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_y">@lang('stock_opname.label.year')</label>
                    @php
                        $years = array_combine(range(date("Y"), 2000), range(date("Y"), 2000));
                    @endphp
                    <select name="_y"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($years as $id => $name)
                        <option value="{{ $id }}" {{ request('_y') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('stock_opname.label.branch_id')</label>
                    <select name="_bc"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sts">@lang('stock_opname.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('stock_opname.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.stock.opname.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>