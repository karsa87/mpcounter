@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('stock_opname.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.stock_opname.search')
        </div>
    </div>
</div>
<div class="row" id="content-stock_opname">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('stock_opname.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-stock_opname">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="title">@lang('stock_opname.label.title')</th>
                            <th class="text-center">@lang('stock_opname.label.branch_id')</th>
                            <th class="text-center">@lang('stock_opname.label.information')</th>
                            <th class="text-center">@lang('stock_opname.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($stock_opname) && count($stock_opname) > 0 )
                        @foreach( $stock_opname as $i => $r)
                        <tr>
                            <td class="text-center">{{ $stock_opname->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->title }}

                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap text-center">
                                {{ $r->branch->name }}
                            </td>
                            <td class="text-wrap text-center">
                                @lang('global.array.months.' . $r->month), {{ $r->year }}
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('stock_opname.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @if ($r->isWaiting())
                                    @hasPermission('transaction.stock.opname.change.waiting')
                                    <a title="@lang('stock_opname.label.change_waiting')" href="{{ route('transaction.stock.opname.change.waiting', $r['id']) }}" class="btn btn-sm btn-secondary btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-chalkboard-teacher m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @if ($r->isProcess())
                                    @hasPermission('transaction.stock.opname.change.process')
                                    <a title="@lang('stock_opname.label.change_process')" href="{{ route('transaction.stock.opname.change.process', $r['id']) }}" class="btn btn-sm btn-success btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-chalkboard-teacher m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @if ($r->isVerified())
                                    @hasPermission('transaction.stock.opname.revert.verified')
                                    <a title="@lang('stock_opname.label.revert_verified')" href="{{ route('transaction.stock.opname.revert.verified', $r['id']) }}" class="btn btn-sm btn-danger btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-chalkboard-teacher m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @hasPermission('transaction.stock.opname.print')
                                    <a title="@lang('stock_opname.label.print')" href="{{ route('transaction.stock.opname.print', $r['id']) }}" class="btn btn-sm btn-default" data-toggle="tooltip" target="_blank">
                                        <i class="fas fa-print m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.stock.opname.show')
                                    <a title="@lang('global.detail')" href="{{ route('transaction.stock.opname.show', $r['id']) }}" class="btn btn-sm btn-info btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @if( $r->isProcess() )
                                    @hasPermission('transaction.stock.opname.form.verified')
                                    <a title="@lang('stock_opname.label.input_stock')" href="{{ route('transaction.stock.opname.form.verified', $r['id']) }}" class="btn btn-sm btn-warning btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-keyboard m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @if( $r->isWaiting() )
                                    @hasPermission('transaction.stock.opname.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('transaction.stock.opname.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-stock_opname btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @hasPermission('transaction.stock.opname.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('transaction.stock.opname.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($stock_opnames) && count($stock_opnames) > 0 )
            {!! \App\Util\Base\Layout::paging($stock_opnames) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('stock_opname.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.stock_opname.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/stock_opname.js') }}?{{ config('app.version') }}"></script>
@endpush