<table>
    <tbody>
        <tr>
            <td><b>@lang('stock_opname.label.title')</b></td>
            <td colspan="5">{{ $sop->title }}</td>
        </tr>
        <tr>
            <td><b>@lang('stock_opname.label.branch_id')</b></td>
            <td colspan="5">{{ $sop->branch->name }}</td>
        </tr>
        <tr>
            <td><b>@lang('stock_opname.label.month')</b></td>
            <td colspan="5">@lang('global.array.months.' . $sop->month)</td>
        </tr>
        <tr>
            <td><b>@lang('stock_opname.label.year')</b></td>
            <td colspan="5">{{ $sop->year }}</td>
        </tr>
        <tr>
            <td><b>@lang('stock_opname.label.status')</b></td>
            <td colspan="5">@lang('stock_opname.list.status.' . $sop->status)</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
    </tbody>
    <thead>
    <tr>
        <th><b>@lang('stock_opname.label.product_id')</b></th>
        <th><b>@lang('stock_opname.label.product_identity_id')</b></th>
        <th><b>@lang('stock_opname.label.stock_on_system')</b></th>
        <th><b>@lang('stock_opname.label.stock_on_hand')</b></th>
        <th><b>@lang('stock_opname.label.information')</b></th>
        @if ($sop->isVerified())
        <th><b>@lang('stock_opname.label.status')</b></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->product->name }}</td>
            <td>{{ $product->identity }}</td>
            <td>{{ $product->pivot->stock_on_system }}</td>
            <td>{{ $sop->isVerified() ? $product->pivot->stock_on_hand : '' }}</td>
            <td>{{ $product->pivot->information }}</td>
            @if ($sop->isVerified())
            <td>@lang('stock_opname.list.match_status.' . $product->pivot->status)</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>