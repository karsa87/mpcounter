@extends('layouts.admin')

@section('content')
<!-- form start -->
<form id="form-stock_opname" role="form" method="POST" action="{{ route('transaction.stock.opname.update', $model->id) }}">
    <input type="hidden" name="_method" value="PUT">

<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('stock_opname.title')</h3>
            </div>

            @csrf
            <input type="hidden" name="id" value="{{ $model->id }}">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-lg-4">
                        <label for="title">@lang('stock_opname.label.title')</label>
                        <input type="text" name="title" class="form-control @classInpError('title')" placeholder="@lang('stock_opname.placeholder.title')" value="{{ $model->title }}" disabled>
                        
                        @inpSpanError(['column'=>'title'])
                    </div>
                    <div class="form-group col-xs-6 col-sm-6 col-lg-2">
                        <label for="month">@lang('stock_opname.label.month')</label>
                        <input type="text" name="month" class="form-control @classInpError('month')" placeholder="@lang('stock_opname.placeholder.month')" value="{{ trans('global.array.months.' . $model->month) }}" disabled>
                        
                        @inpSpanError(['column'=>'month'])
                    </div>
                    <div class="form-group col-xs-6 col-sm-6 col-lg-2">
                        <label for="year">@lang('stock_opname.label.year')</label>
                        <input type="text" name="year" class="form-control @classInpError('year')" placeholder="@lang('stock_opname.placeholder.year')" value="{{ $model->year }}" readonly>
                        
                        @inpSpanError(['column'=>'year'])
                    </div>
                    <div class="form-group col-xs-12 col-sm-12 col-lg-4">
                        <label for="branch_id">@lang('stock_opname.label.branch_id')</label>
                        <input type="text" name="branch_name" class="form-control @classInpError('branch_id')" placeholder="@lang('stock_opname.placeholder.branch_id')" value="{{ $model->branch->name }}" disabled>
                        <input type="hidden" value="{{ $model->branch_id }}" />
                        
                        @inpSpanError(['column'=>'branch_id'])
                    </div>
                    <div class="form-group col-xs-12 col-sm-12 col-lg-8">
                        <label for="information">@lang('stock_opname.label.information')</label>
                        <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('stock_opname.placeholder.information')" >{{ old('information') }}</textarea>
                        
                        @inpSpanError(['column'=>'information'])
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('transaction.stock.opname.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <list-form-product-identity-stock-opname
            v-bind:stock_opname_id="{{ $model->id }}"
        ></list-form-product-identity-stock-opname>
    </div>
</div>
</form>
@stop
