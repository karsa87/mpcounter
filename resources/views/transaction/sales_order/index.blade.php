@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" id="form-search-so" action="{{ route('transaction.sales.order.index') }}">
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary collapsed-card">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('sales_order.title')</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.sales_order.search')
        </div>
    </div>
</div>
</form>

<div class="row" id="content-sales_order">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('sales_order.title')</h3>

                <div class="card-tools">
                    <div class="input-group">
                        <input type="text" name="search_kw" class="form-control float-right" placeholder="@lang('global.keyword')" value="{{ request('_kw') }}">
                
                        <div class="input-group-append">
                            <button type="submit" id="btn-search" class="btn btn-default"><i class="fas fa-search"></i></button>
                            @hasPermission('transaction.sales.order.create')
                            <a href="{{ route('transaction.sales.order.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
                            @endhasPermission
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-stripped table-hover sorting-table" data-form="form-search-so">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="no_nvoice">@lang('sales_order.label.no_invoice')</th>
                            <th>@lang('sales_order.label.member')</th>
                            <th class="sorting-form" data-column="date">@lang('sales_order.label.date')</th>
                            <th>@lang('sales_order.label.information')</th>
                            <th>@lang('sales_order.label.type')</th>
                            <th>@lang('sales_order.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($sales_orders) && count($sales_orders) > 0 )
                        @foreach( $sales_orders as $i => $r)
                        <tr>
                            <td class="text-center">{{ $sales_orders->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->no_invoice }}
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                {{ $r->memberName }}
                                <p class="m-0">
                                    <small>
                                        {{ $r->memberAddress }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        {{ $r->memberPhone }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-center">
                                {{ $util->formatDate($r->date) }}
                            </td>
                            <td class="text-wrap">
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('sales_order.label.sales_id'):</strong> {{ $r->sales_id ? $r->sales->name : '' }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('sales_order.label.bank_id'):</strong> {{ $r->bank->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('sales_order.label.branch_id'):</strong> {{ $r->branch->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('sales_order.label.paid_off'):</strong> {{ $util->format_currency($r->paid_off) }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('sales_order.label.total_amount'):</strong> {{ $util->format_currency($r->total_amount) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <span class="badge {{ $r->type ? 'badge-danger' : 'badge-success' }}">@lang('sales_order.list.type.'.$r->type)</span>
                            </td>
                            <td class="text-wrap">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('sales_order.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('transaction.sales.order.note.small')
                                    <a href="{{ route('transaction.sales.order.note.small', $r->id) }}" class="btn btn-sm btn-default" target="_blank" title="@lang('sales_order.label.print_nota')"  data-toggle="tooltip">
                                        <i class="fas fa-print m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @if ($r->isNotPaid())
                                    @hasPermission('finance.debt.payment.so.create')
                                    <a title="@lang('sales_order.label.debt_payment')" href="{{ route('finance.debt.payment.so.create', ['_soid' => $r->id]) }}" class="btn btn-sm btn-primary" data-toggle="tooltip">
                                        <i class="fas fa-money-bill-alt m-0"></i>
                                    </a>
                                    @endhasPermission
                                    @endif

                                    @hasPermission('transaction.sales.order.edit')
                                    @if ($r->retur->count() <= 0)
                                    <a title="@lang('global.edit')" href="{{ route('transaction.sales.order.edit', $r['id']) }}" class="btn btn-sm btn-warning" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endif
                                    @endhasPermission

                                    @hasPermission('transaction.sales.order.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('transaction.sales.order.show', $r['id']) }}" class="btn btn-sm btn-info" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.sales.order.destroy')
                                    @if ($r->retur->count() <= 0 && $r->debtPayments->count() <= 0)
                                    <button title="@lang('global.delete')" data-href="{{ route('transaction.sales.order.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endif
                                    @endhasPermission
                                    
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($sales_orders) && count($sales_orders) > 0 )
            {!! \App\Util\Base\Layout::paging($sales_orders) !!}
            @endif
        </div>
    </div>
</div>
@stop

@push('js')
<script type="text/javascript">
    $("input[name='search_kw']").keypress(function(e){
        if (e.keyCode == 13) {
            $("input[name='_kw']").val($(this).val());
            $('#form-search-so').submit();
        }
    });

    $("#btn-search").click(function() {
        $("input[name='_kw']").val($("input[name='search_kw']").val());
        $('#form-search-so').submit();
    });
</script>
@endpush