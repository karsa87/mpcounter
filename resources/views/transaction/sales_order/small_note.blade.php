<?php
use App\Models\SalesOrder;
use App\Util\Helpers\Util;

/* @var $model SalesOrder */
?>

<html>
<head>
    <title>@lang('sales_order.title')</title>
    <style type="text/css">
        .row{
            width: 100%;
            padding: 5px;
            clear: both;
            font-size: 5pt;
        }
        body{
            padding: 5px 0;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 5pt;
        }
        h3{
            margin: 3px;
        }
        table {
            width: 100%;
        }
        .border-top{
            border-top:1px solid;
        }
        .border-bottom{
            border-bottom:1px solid;
        }
        .text-wrap {
            word-wrap: break-word;
        }
        @page { 
            margin: 0px 0px 0px -2px;
        }
    </style>
</head>
<body>
    <div class="header">
        {!! $header !!}
        <h1 style="margin: -10px 2px 0px 2px; text-align: center;">{{ $model->branch->name }}</h1>
        <p align=left style="margin: 2px 2px; text-align: center;">{{ $model->branch->address }}</p>
        <p style="margin: 2px 2px; text-align: center;">{{ $model->branch->phone }}</p>
    </div>
    <div class="row" style="font-size: 6pt;">
        <hr>
        <table>
            <tr>
                <td width="15%">@lang('global.no')</td>
                <td width="5%">:</td>
                <td width="30%">{{ $model->no_invoice }}</td>
                <td colspan="3">@lang('sales_order.label.payment'):</td>
            </tr>

            <tr>
                <td width="15%">@lang('sales_order.label.date')</td>
                <td width="5%">:</td>
                <td width="30%">{{ Util::formatDate(date('Y-m-d H:i'), 'd/m/Y H:i') }}</td>
                <td colspan="3">
                    @lang("sales_order.list.type.$model->type") {{ $model->bank_id ? '(' . $model->bank->name . ')' : '' }}
                </td>
            </tr>
        </table>

        <hr style="border-style:dotted">
        <table>
            <tr>
                <td width="15%">@lang('sales_order.label.name')</td>
                <td width="5%">:</td>
                <td width="30%">{{ $model->name }}</td>
                <td width="15%">@lang('sales_order.label.phone')</td>
                <td width="5%">:</td>
                <td width="30%">{{ $model->phone }}</td>
            </tr>
            <tr>
                <td width="15%" >@lang('sales_order.label.address')</td>
                <td colspan="2" >:</td>
                <td width="15%" valign="top">@lang('sales_order.label.sales_id')</td>
                <td width="5%" valign="top">:</td>
                <td width="30%" valign="top">{{ $model->sales_id ? $model->sales->nama : '-' }}</td>
            </tr>
            <tr>
                <td colspan="3" >{{ $model->address }}</td>
                <td colspan="3"></td>
            </tr>
        </table>
    </div>
    <div class="row" style="font-size: 5pt;">
        <table cellpadding='2' border="0">
            <thead class="border-top border-bottom">
                <tr>
                    <th width="40%" align="left">@lang('sales_order.label.product_id')</th>
                    <th width="10%">@lang('sales_order.label.qty')</th>
                    <th width="5%" align="right"></th>
                    <th width="35%" align="right">@lang('sales_order.label.sub_total')</th>
                </tr>
            </thead>
            <tbody style="border: none;">
                @php
                    $total = $ttl_qty = 0;
                @endphp
                @foreach ($model->details as $d)
                    <tr>
                        <td colspan="3" class="text-wrap">
                            {{ $d->product->name }}
                        </td>
                        <td colspan="1"></td>
                    </tr>
                    
                    <tr>
                        <td class="text-wrap">ID: {{ $d->productIdentity->identity ?? '-' }}</td>
                        <td align="center">x{{ Util::format_currency($d->qty) }}</td>
                        {{-- <td align='right'>
                            Rp. {{ Util::format_currency($d->discount_promo) }}
                        </td> --}}
                        <td>Rp.</td>
                        <td align='right'>{{ Util::format_currency($d->total_amount) }}</td>
                    </tr>
                @endforeach
            </tbody>
            <thead class="border-top">
                @php
                    $subtotal = $model->total_amount_first - $model->total_discount_detail;
                @endphp
                @if($model->is_ppn)
                    @php
                        $subtotal = $subtotal * 100 / 110;
                        $ppn = (($subtotal * 10) / 100);
                    @endphp
                @endif
                <tr>
                    <td width="40%" align='right'>@lang('sales_order.label.subtotal')</td>
                    <td width="10%">{{ Util::format_currency($model->details->sum('qty')) }}</td>
                    <td width="5%">Rp.</td>
                    <td width="35%" align='right'>{{ Util::format_currency($subtotal) }}</td>
                </tr>
                <tr>
                    <td align='right'>@lang('sales_order.label.shipping_cost')</td>
                    <td>+</td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($model->shipping_cost) }}</td>
                </tr>
            </thead>
            <thead>
                <tr>
                    <td align='right'>@lang('sales_order.label.discount_sales')</td>
                    <td>-</td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($model->discount_sales) }}</td>
                </tr>
                <tr>
                    <td align='right'>@lang('sales_order.label.pay_point') <small>({{ $model->point_member }})</small></td>
                    <td>-</td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($model->discount_poin_member) }}</td>
                </tr>
                <tr>
                    <td align='right'>@lang('sales_order.label.tax_bank') <small>({{ $model->tax_bank }}%)</small></td>
                    <td>+</td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($model->tax_bank_price) }}</td>
                </tr>
                @if($model->is_ppn)
                <tr>
                    <td align='right'>@lang('sales_order.label.ppn')</td>
                    <td>+</td>
                    <td></td>
                    <td align='right'>
                        {{ Util::format_currency($ppn) }}
                    </td>
                </tr>
                @endif
            </thead>
            @php
                $total_paid = $model->debtPayments->sum('paid');
                $total_paid_off = $model->paid_off + $total_paid - $model->discount_poin_member;
                $remaining_debt = $total_paid_off - $model->total_amount;
            @endphp
            <tbody class="border-top" style="font-size: 7pt; font-weight: bold;">
                <tr>
                    <td align='right'>@lang('sales_order.label.grand_total')</td>
                    <td></td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($model->total_amount) }}</td>
                </tr>
                <tr>
                    <td align='right'>@lang('sales_order.label.paid_off')</td>
                    <td></td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($total_paid_off) }}</td>
                </tr>
            </tbody>
            <thead class="border-top" style="font-size: 8pt; font-weight: bold;">
                <tr>
                    <td align='right'>@lang('sales_order.label.money_changes')</td>
                    <td></td>
                    <td>Rp.</td>
                    <td align='right'>{{ Util::format_currency($remaining_debt) }}</td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="row">
        <table cellpadding="2">
            <tr>
                <td style="border-top:0.5px solid; width: 30%; padding-top:20px" valign="top">
                    <strong>@lang('sales_order.label.cashier')</strong>
                    <br>
                    {{ $model->createdBy ? $model->createdBy->employeeName : '-' }}
                    <br>
                </td>
                <td style="width: 70%; font-size: 4pt;" valign="top">
                    <ul>
                        <li>BARANG YANG SUDAH DIBERLI TIDAK DAPAT DITUKAR ATAU DIKEMBALIKAN</li>
                        <li>GARANSI REPLACE KERUSAKAN 1X24 JAM SETELAH BARANG DITERIMA</li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
    <div class="footer">
        {!! $footer !!}
    </div>
</body>
</html>