@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<!-- form start -->
<form 
    id="form-sales_order" role="form" method="POST" 
    action="{{ $model->exists ? route('transaction.sales.order.update', $model->id) : route('transaction.sales.order.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('sales_order.title')</h3>
            </div>
            @csrf

            <input type="hidden" name="id" value="{{ $model->id }}">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="no_invoice">@lang('sales_order.label.no_invoice')</label>
                                            <input type="text" name="no_invoice" class="form-control @classInpError('no_invoice')" placeholder="@lang('sales_order.placeholder.no_invoice')" disabled value="{{ old('no_invoice') ?? $model->nextNo() }}">
                                            
                                            @inpSpanError(['column'=>'no_invoice'])
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="date">@lang('sales_order.label.date')</label>
                                            <input type="text" name="date" class="form-control datetimepicker @classInpError('date')" placeholder="@lang('sales_order.placeholder.date')" autocomplete="off" value="{{ old('date') ?? ($model->date ?? date('Y-m-d H:i')) }}">
                                            
                                            @inpSpanError(['column'=>'date'])
                                        </div>
                                    </div>
                                    {{-- <div class="col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="sales_id">@lang('sales_order.label.sales_id')</label>
                                            @php
                                                $selected = old('sales_id') ?? $model->sales_id;
                                            @endphp
                                            <select name="sales_id"  class="form-control select2 @classInpError('sales_id')" data-placeholder="" style="width: 100%;">
                                                <option value="" selected>&nbsp;</option>
                                                @foreach ($sales as $id => $name)
                                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                            @inpSpanError(['column'=>'sales_id'])
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="type">@lang('sales_order.label.type')</label>
                                            <span data-toggle="tooltip" data-placement="top" title="@lang('purchase_order.message.desc_type')">
                                                <i class="fas fa-question-circle"></i>
                                            </span>
                                            @php
                                                $selected = old('type') ?? $model->type;
                                            @endphp
                                            <select name="type"  class="form-control @classInpError('type')" style="width: 100%;">
                                                @foreach (trans('sales_order.list.type') as $id => $name)
                                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                            @inpSpanError(['column'=>'type'])
                                        </div>
                                    </div>
                                    <div id="div-date-max-payable" class="col-xs-12 col-sm-12 col-lg-4" style="display: none">
                                        <div class="form-group">
                                            <label for="date_max_payable">@lang('sales_order.label.date_max_payable')</label>
                                            <input type="text" name="date_max_payable" class="form-control datepicker @classInpError('date_max_payable')" placeholder="@lang('sales_order.placeholder.date_max_payable')" autocomplete="off" value="{{ old('date_max_payable') ?? $model->date_max_payable }}">
                                            
                                            @inpSpanError(['column'=>'date_max_payable'])
                                        </div>
                                    </div>

                                    {{-- @if (empty(config('default.bank_id'))) --}}
                                    <div id="div-bank-id" class="col-xs-12 col-sm-12 col-lg-4" style="display: none;">
                                        <div class="form-group">
                                            <label for="bank_id">@lang('sales_order.label.bank_id')</label>
                                            @php
                                                $selected = old('bank_id') ?? $model->bank_id;
                                            @endphp
                                            <select name="bank_id"  class="form-control select2 @classInpError('bank_id')" data-placeholder="" style="width: 100%;">
                                                @foreach ($banks as $id => $name)
                                                <option value="{{ $id }}" {{ is_numeric($selected) && $selected == $id ? "selected"  : ""}}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                            @inpSpanError(['column'=>'bank_id'])
                                        </div>
                                    </div>
                                    {{-- @endif --}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @php
                    $input_down_payment = old('down_payment') ?? $model->down_payment;
                    $input_down_payment = $util->remove_format_currency($input_down_payment ?? '0');
                    $input_discount_sales = old('discount_sales') ?? $model->discount_sales;
                    $input_discount_sales = $util->remove_format_currency($input_discount_sales ?? '0');
                    $input_shipping_cost = old('shipping_cost') ?? $model->shipping_cost;
                    $input_shipping_cost = $util->remove_format_currency($input_shipping_cost ?? '0');
                    $input_additional_cost = old('additional_cost') ?? $model->additional_cost;
                    $input_additional_cost = $util->remove_format_currency($input_additional_cost ?? '0');
                    $input_tax_bank = old('tax_bank') ?? $model->tax_bank;
                    $input_tax_bank = $input_tax_bank ?? '0';
                    $input_point_member = old('point_member') ?? $model->point_member;
                    $input_point_member = $util->remove_format_currency($input_point_member ?? '0');
                    $input_is_ppn = old('is_ppn') ?? ($model->is_ppn ?? '0');
                    $input_is_ppn = $input_is_ppn === 'on' ? '1' : $input_is_ppn;
                    $input_use_point = old('use_point') ?? ($model->use_point ?? 0);
                    $input_use_point = $input_use_point === 'on' ? '1' : $input_use_point;
                    
                    $input_branch_id = 0;
                    $is_employee = 0;
                    if (auth()->user()->isEmployee()) {
                        $input_branch_id = auth()->user()->branch_id ?? $input_branch_id;
                        $is_employee = 1;
                    } else {
                        $input_branch_id = $branchs->first() ? $branchs->first()->id : $input_branch_id;
                    }

                    $input_branch_id = old('branch_id') ?? ($model->branch_id ?? $input_branch_id);
                    
                    $input_products = old('products') ?? (isset($details) ? $details : []);
                    $input_products = json_encode($input_products);
                @endphp
                <list-product-so 
                    v-bind:update="{{ $model->exists ? 1 : 0 }}"
                    v-bind:categories="{{ $categories }}"
                    v-bind:branchs="{{ $branchs }}"
                    v-bind:brands="{{ $brands }}"
                    v-bind:input_branch_id="{{ $input_branch_id }}"
                    v-bind:input_is_employee="{{ $is_employee }}"
                    v-bind:input_products="{{ $input_products }}"
                    v-bind:input_down_payment="{{ $input_down_payment }}"
                    v-bind:input_discount_sales="{{ $input_discount_sales }}"
                    v-bind:input_shipping_cost="{{ $input_shipping_cost }}"
                    v-bind:input_additional_cost="{{ $input_additional_cost }}"
                    v-bind:input_tax_bank="{{ $input_tax_bank }}"
                    v-bind:input_is_ppn="{{ $input_is_ppn }}"
                    v-bind:input_use_point="{{ $input_use_point }}"
                    v-bind:input_point_member="{{ $input_point_member }}"
                    v-bind:members="{{ $members }}"
                    v-bind:convertion_point_rupiah="{{ $convertion_point_rupiah }}"
                    v-bind:convertion_rupiah_point="{{ $convertion_rupiah_point }}"
                    v-bind:input_name="'{{ old('name') ?? $model->name }}'"
                    v-bind:input_phone="'{{ old('phone') ?? $model->phone }}'"
                    v-bind:input_address="'{{ old('address') ?? $model->address }}'"
                    v-bind:input_member_id="'{{ old('member_id') ?? $model->member_id }}'"
                    >
                </list-product-so>
                
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-footer">
                <a href="{{ route('transaction.sales.order.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@push('js')
    <script>
        var members = JSON.parse('{!! $members->toJson() !!}');
        var convertion_point_rupiah = parseFloat('{{ $convertion_point_rupiah }}');
    </script>
    <script type="text/javascript" src="{{ asset('js/admin/transaction/sales_order.js') }}?{{ config('app.version') }}"></script>
@endpush