<!-- form start -->

<div class="card-body">
    <div class="row">
        <div class="col-4" style="display: none;">
            <div class="form-group">
                <label for="no_invoice">@lang('global.keyword')</label>
                <input type="text" name="_kw" class="form-control" placeholder="@lang('global.keyword')" value="{{ request('_kw') }}">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="date">@lang('sales_order.label.date')</label>
                <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('sales_order.placeholder.date')" value="{{ request('_dt') }}">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_bk">@lang('sales_order.label.bank_id')</label>
                <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($banks as $id => $name)
                    <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_bc">@lang('sales_order.label.branch_id')</label>
                <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($branchs as $id => $name)
                    <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_sls">@lang('sales_order.label.sales_id')</label>
                <select name="_sls"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($sales as $id => $name)
                    <option value="{{ $id }}" {{ request('_sls') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_mb">@lang('sales_order.label.member_id')</label>
                <select name="_mb"  class="form-control select2" data-placeholder="" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($members as $id => $name)
                    <option value="{{ $id }}" {{ request('_mb') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_sts">@lang('sales_order.label.status')</label>
                <select name="_sts"  class="form-control" style="width: 100%;">
                    <option value=""></option>
                    @foreach (trans('sales_order.list.status') as $id => $name)
                    <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="_tp">@lang('sales_order.label.type')</label>
                <select name="_tp"  class="form-control" style="width: 100%;">
                    <option value=""></option>
                    @foreach (trans('sales_order.list.type') as $id => $name)
                    <option value="{{ $id }}" {{ is_numeric(request('_tp')) && request('_tp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
    <a href="{{ route('transaction.sales.order.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
    <button type="submit" class="btn btn-success btn-loader">
        @lang('global.search')
    </button>
</div>
