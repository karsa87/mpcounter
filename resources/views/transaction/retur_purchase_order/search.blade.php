<!-- form start -->
<form role="form" id="form-search-rpo" action="{{ route('transaction.retur.purchase.order.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="no_retur">@lang('retur_purchase_order.label.no_retur')</label>
                    <input type="text" name="_nrtr" class="form-control" placeholder="@lang('retur_purchase_order.placeholder.no_retur')" value="{{ request('_nrtr') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="date">@lang('retur_purchase_order.label.date')</label>
                    <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('retur_purchase_order.placeholder.date')" value="{{ request('_dt') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bk">@lang('retur_purchase_order.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('retur_purchase_order.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sp">@lang('retur_purchase_order.label.supplier_id')</label>
                    <select name="_sp"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($suppliers as $id => $name)
                        <option value="{{ $id }}" {{ request('_sp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_poid">@lang('retur_purchase_order.label.purchase_order_id')</label>
                    <select name="_poid"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($purchaseOrders as $id => $name)
                        <option value="{{ $id }}" {{ request('_poid') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.retur.purchase.order.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>