@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<!-- form start -->
<form 
    id="form-retur_purchase_order" role="form" method="POST" 
    action="{{ $model->exists ? route('transaction.retur.purchase.order.update', $model->id) : route('transaction.retur.purchase.order.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('retur_purchase_order.title')</h3>
            </div>
            @csrf

            <input type="hidden" name="id" value="{{ $model->id }}">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="no_retur">@lang('retur_purchase_order.label.no_retur')</label>
                            <input type="text" name="no_retur" class="form-control @classInpError('no_retur')" placeholder="@lang('retur_purchase_order.placeholder.no_retur')" disabled value="{{ old('no_retur') ?? $model->nextNoRetur() }}">
                            
                            @inpSpanError(['column'=>'no_retur'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="date">@lang('retur_purchase_order.label.date')</label>
                            <input type="text" name="date" class="form-control datetimepicker @classInpError('date')" placeholder="@lang('retur_purchase_order.placeholder.date')" autocomplete="off" value="{{ old('date') ?? date('Y-m-d H:i', strtotime($model->date ?? '0days'))  }}">
                            
                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="purchase_order_id">@lang('retur_purchase_order.label.purchase_order_id')</label>
                            <input type="hidden" name="purchase_order_id" id="purchase-order-id" value="{{ old('purchase_order_id') ?? $model->purchase_order_id }}">
                            <br>
                            <label for="purchase_order_id" id="purchase-order-invoice">@lang('retur_purchase_order.placeholder.purchase_order_id')</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="supplier_id">@lang('retur_purchase_order.label.supplier_id')</label>
                            <input type="hidden" name="supplier_id" id="supplier-id" value="{{ old('supplier_id') ?? $model->supplier_id }}">
                            <br>
                            <label for="supplier_id" id="supplier-name">@lang('retur_purchase_order.placeholder.supplier_id')</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="branch_id">@lang('retur_purchase_order.label.branch_id')</label>
                            <input type="hidden" name="branch_id" id="branch-id" value="{{ old('branch_id') ?? $model->branch_id }}">
                            <br>
                            <label for="branch_id" id="branch-name">@lang('retur_purchase_order.placeholder.branch_id')</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="bank_id">@lang('retur_purchase_order.label.bank_id')</label>
                            <input type="hidden" name="bank_id" id="bank-id" value="{{ old('bank_id') ?? $model->bank_id }}">
                            <br>
                            <label for="bank_id" id="bank-name">@lang('retur_purchase_order.placeholder.bank_id')</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="information">@lang('retur_purchase_order.label.information')</label>
                            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('retur_purchase_order.placeholder.information')" >{{ old('information') ?? $model->information }}</textarea>
                            
                            @inpSpanError(['column'=>'information'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                        <label for="form-drop_files" class="control-label">@lang('retur_purchase_order.label.images')</label>
                        <input type="hidden" name="images" id="result-upload" value="{{ old('images') ?? $model->images }}">
                        <div class="dropzone" id="files-upload" data-href="{{ route('transaction.retur.purchase.order.upload') }}"></div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>

    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.list') @lang('retur_purchase_order.label.retur_product')</h3>
            </div>
            <div class="card-body">
                @php
                    $input_products = old('products') ?? (isset($details) ? $details : []);
                    $input_products = json_encode($input_products);
                
                    $input_purchase_order_id = old('purchase_order_id') ?? ($model->purchase_order_id ?? 0);
                @endphp
                <list-product-rpo
                    v-bind:update="{{ $model->exists ? 1 : 0 }}"
                    v-bind:input_products="{{ $input_products }}"
                    v-bind:purchase_orders="{{ $purchaseOrders->keyBy('id') }}"
                    v-bind:purchase_orders_list="{{ $purchaseOrders_form }}"
                    v-bind:input_purchase_order_id="{{ $input_purchase_order_id }}"
                    >
                </list-product-rpo>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-footer">
                <a href="{{ route('transaction.retur.purchase.order.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/retur_purchase_order.js') }}?{{ config('app.version') }}"></script>
@endpush