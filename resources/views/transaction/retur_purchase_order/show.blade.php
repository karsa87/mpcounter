@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-12">
        <!-- Main content -->
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4>
                        <i class="fas fa-file-invoice"></i> @lang('global.detail') @lang('retur_purchase_order.title')
                        <small class="float-right">@lang('retur_purchase_order.label.date'): {{ $model->date }}</small>
                    </h4>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    @lang('global.from')
                    <address>
                        <strong>{{ $model->branch->name }}</strong><br>
                        {{ $model->branch->address }} <br>
                        @lang('branch.label.phone'): {{ $model->branch->phone }}<br>
                        <strong>@lang('purchase_order.label.no_invoice'):</strong> {{ $model->purchaseOrder->no_invoice }}<br>
                    </address>
                    <strong>@lang('purchase_order.label.information')</strong>
                    <address>
                        {{ $model->purchaseOrder->information ?? '-' }}</strong><br>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    @lang('global.to')
                    <address>
                        <strong>{{ $model->supplier->name }}</strong><br>
                        {{ $model->supplier->address }} <br>
                        @lang('supplier.label.phone'): {{ $model->supplier->phone }}<br>
                        @lang('supplier.label.sales_name'): {{ $model->supplier->sales_name }}<br>
                        @lang('supplier.label.sales_phone'): {{ $model->supplier->sales_phone }}<br>
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>@lang('retur_purchase_order.label.no_retur'): {{ $model->no_retur }}</b>
                    <br>
                    <b>@lang('retur_purchase_order.label.bank_id'):</b> {{ $model->bank->name }}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>@lang('global.no')</th>
                                <th>@lang('retur_purchase_order.label.product_id')</th>
                                <th>@lang('retur_purchase_order.label.identity')</th>
                                <th class="text-center">@lang('retur_purchase_order.label.purchase_price')</th>
                                <th class="text-center">@lang('retur_purchase_order.label.qty')</th>
                                <th class="text-center">@lang('retur_purchase_order.label.sub_total')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->details as $i => $detail)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>
                                    {{ $detail->product->name }}
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.category_id'):</strong> {{ $detail->product->category->name }}
                                        </small>
                                    </p>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.brand_id'):</strong> {{ $detail->product->brand->name }}
                                        </small>
                                    </p>
                                </td>
                                <td>{{ $detail->productIdentity->identity ?? '-' }}</td>
                                <td class="text-right">{{ $util->format_currency($detail->purchase_price) }}</td>
                                <td class="text-center">{{ $util->format_currency($detail->qty) }}</td>
                                <td class="text-right">{{ $util->format_currency($detail->total_amount) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">@lang('retur_purchase_order.label.total'):</th>
                                <td class="text-right">{{ $util->format_currency($model->total_amount) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-12">
                    <a href="{{ route('transaction.retur.purchase.order.index') }}" class="btn btn-danger">
                        @lang('global.back')
                    </a>

                    @hasPermission('transaction.retur.purchase.order.edit')
                    <a href="{{ route('transaction.retur.purchase.order.edit', $model->id) }}" class="btn btn-warning float-right">
                        @lang('global.edit')
                    </a>
                    @endhasPermission

                    <a id="btn-print" class="btn btn-default float-right mr-1">
                        @lang('retur_purchase_order.label.print_nota')
                    </a>
                </div>
            </div>
        </div>
        <!-- /.invoice -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row" id="tab-detail">
    <div class="col-12">
        <!-- Custom Tabs -->
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills mr-auto p-2">
                    <li class="nav-item"><a class="nav-link active" href="#tab_images" data-toggle="tab">@lang('purchase_order.label.images')</a></li>
                </ul>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_images">
                        <div class="row p-3">
                            <div class="col-lg-12">
                            @foreach ($model->images_arr() as $i => $image)
                                <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                    <img src="{{ $image }}" class="img-fluid m-2 img-lg center-cropped" alt="Images {{ $i+1 }}"/>
                                </a>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.card-body -->
        </div>
        <!-- ./card -->
    </div>
    <!-- /.col -->
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/transaction/retur_purchase_order.js') }}?{{ config('app.version') }}"></script>
@endpush