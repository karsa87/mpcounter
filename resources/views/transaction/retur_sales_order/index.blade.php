@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('retur_sales_order.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'transaction.retur_sales_order.search')
        </div>
    </div>
</div>
<div class="row" id="content-retur_sales_order">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('retur_sales_order.title')</h3>
                
                @hasPermission('transaction.retur.sales.order.create')
                <a href="{{ route('transaction.retur.sales.order.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
                @endhasPermission
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-stripped table-hover sorting-table" data-form="form-search-rso">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="no_retur">@lang('retur_sales_order.label.no_retur')</th>
                            <th>@lang('retur_sales_order.label.sales_order_id')</th>
                            <th class="sorting-form" data-column="date">@lang('retur_sales_order.label.information')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($retur_sales_orders) && count($retur_sales_orders) > 0 )
                        @foreach( $retur_sales_orders as $i => $r)
                        <tr>
                            <td class="text-center">{{ $retur_sales_orders->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->no_retur }}
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                {{ $r->salesOrder->no_invoice }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('retur_sales_order.label.date'):</strong> {{ $util->formatDate($r->salesOrder->date) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $util->formatDate($r->date) }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('retur_sales_order.label.member_id'):</strong> {{ $r->salesOrder->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('retur_sales_order.label.bank_id'):</strong> {{ $r->bank->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('retur_sales_order.label.branch_id'):</strong> {{ $r->branch->name }}
                                    </small>
                                </p>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('transaction.retur.sales.order.edit')
                                    <a title="@lang('global.edit')" href="{{ route('transaction.retur.sales.order.edit', $r['id']) }}" class="btn btn-sm btn-warning" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.retur.sales.order.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('transaction.retur.sales.order.show', $r['id']) }}" class="btn btn-sm btn-info" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('transaction.retur.sales.order.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('transaction.retur.sales.order.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($retur_sales_orders) && count($retur_sales_orders) > 0 )
            {!! \App\Util\Base\Layout::paging($retur_sales_orders) !!}
            @endif
        </div>
    </div>
</div>
@stop