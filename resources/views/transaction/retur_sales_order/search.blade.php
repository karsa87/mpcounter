<!-- form start -->
<form role="form" id="form-search-rso" action="{{ route('transaction.retur.sales.order.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="no_retur">@lang('retur_sales_order.label.no_retur')</label>
                    <input type="text" name="_nrtr" class="form-control" placeholder="@lang('retur_sales_order.placeholder.no_retur')" value="{{ request('_nrtr') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="date">@lang('retur_sales_order.label.date')</label>
                    <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('retur_sales_order.placeholder.date')" value="{{ request('_dt') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bk">@lang('retur_sales_order.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('retur_sales_order.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_member_id">@lang('retur_sales_order.label.member_id')</label>
                    <input type="text" name="_mnm" class="form-control" placeholder="@lang('retur_sales_order.placeholder.member_id')" value="{{ request('_mnm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_soid">@lang('retur_sales_order.label.sales_order_id')</label>
                    <input type="text" name="_soid" class="form-control" placeholder="@lang('retur_sales_order.placeholder.sales_order_id')" value="{{ request('_soid') }}">
                    {{-- <select name="_soid"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($salesOrders as $id => $name)
                        <option value="{{ $id }}" {{ request('_soid') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('transaction.retur.sales.order.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>