{{-- @extends('adminlte::login') --}}
@extends('layouts.login')

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('dashboard') }}">{!! config('adminlte.logo') !!}</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">@lang('auth.login')</p>
            
            <form action="{{ route('login') }}" method="post" id="form-login" role="form">
                @csrf
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" name="email" class="form-control @classInpError('email')" value="" placeholder="Email" id="inp-email" autofocus>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @inpSpanError(['column'=>'email'])
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control @classInpError('email')" placeholder="Password" id="inp-password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @inpSpanError(['column'=>'password'])
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            @lang('global.sign_in')
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
{{-- <script src="{{ asset('js/admin/auth/login.js') }}?{{ config('app.version') }}"></script> --}}
@stop