@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('brand.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.brand.search')
        </div>
    </div>
</div>
<div class="row" id="content-brand">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('brand.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-brand">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('brand.label.name')</th>
                            <th>@lang('brand.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($brands) && count($brands) > 0 )
                        @foreach( $brands as $i => $r)
                        <tr>
                            <td class="text-center">{{ $brands->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->name }}
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('brand.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.brand.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.brand.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-brand btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.brand.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.brand.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($brands) && count($brands) > 0 )
            {!! \App\Util\Base\Layout::paging($brands) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('brand.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.brand.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/brand.js') }}?{{ config('app.version') }}"></script>
@endpush