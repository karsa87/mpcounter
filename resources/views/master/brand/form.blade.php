<!-- form start -->
<form id="form-brand" role="form" method="POST" action="{{ route('master.brand.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('brand.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('brand.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="brand-status" {{ ( empty(old('status')) || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="brand-status">@lang('brand.label.status')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                    <i class="fas fa-question-circle"></i>
                </span>
                @inpSpanError(['column'=>'status'])
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        @if (!isset($modal))
        <a href="{{ route('master.brand.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        @endif
        <button type="submit" class="btn btn-success float-right {{ !isset($modal) ? 'btn-loader' : '' }}">@lang('global.save')</button>
    </div>
</form>