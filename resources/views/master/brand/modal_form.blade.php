
<div class="modal fade" id="modal-brand">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('global.add') @lang('brand.title')</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('master.brand.form', ['modal'=>true])
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $('#form-brand').submit(function(e){
        e.preventDefault();

        var data = $(this).serialize();

        var msg_failed = "";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': CONFIG.token
            },
            url: $(this).attr('action'), 
            type: 'POST', 
            data: data,
            success: function(result){
                if(result.status == true){
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#modal-brand').modal('hide');
                } else {
                    $.each(result.error, function(k, v) {
                        msg_failed += '</li><li>';
                        msg_failed += v.join('</li><li>');
                    });
                    
                }
            },
            beforeSend: function( xhr ) {
                showProgress();
            }
        })
        .always(function(result) {
            Swal.close();
            if (msg_failed !== "") {
                showAlert("<ul>" + msg_failed + "</ul>", 'error');
            }
        })
        .fail(function() {
            swal_confirm.fire(
                "Can't delete",
                msg_failed,
                'error'
            )
        });

        return false;
    });
</script>
@endpush