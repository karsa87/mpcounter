<!-- form start -->
<form role="form" id="form-search-employee" action="{{ route('master.employee.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('employee.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('employee.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="email">@lang('employee.label.email')</label>
                    <input type="text" name="_em" class="form-control" placeholder="@lang('employee.placeholder.email')" value="{{ request('_em') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="identifier">@lang('employee.label.identifier')</label>
                    <input type="text" name="_idf" class="form-control" placeholder="@lang('employee.placeholder.identifier')" value="{{ request('_idf') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sts">@lang('employee.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('employee.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('employee.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" style="width: 100%;" data-placeholder="">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_bc')) && request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.employee.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>