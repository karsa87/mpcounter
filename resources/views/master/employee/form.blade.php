<!-- form start -->
<form id="form-employee" role="form" method="POST" action="{{ route('master.employee.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('employee.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('employee.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="email">@lang('employee.label.email')</label>
            <input type="text" name="email" class="form-control @classInpError('email')" placeholder="@lang('employee.placeholder.email')" value="{{ old('email') }}">
            
            @inpSpanError(['column'=>'email'])
        </div>
        <div class="form-group">
            <label for="phone">@lang('employee.label.phone')</label>
            <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('employee.placeholder.phone')" value="{{ old('phone') }}">
            
            @inpSpanError(['column'=>'phone'])
        </div>
        <div class="form-group">
            <label for="address">@lang('employee.label.address')</label>
            <textarea name="address" class="form-control @classInpError('address')" rows="3" placeholder="@lang('employee.placeholder.address')" >{{ old('address') }}</textarea>
            
            @inpSpanError(['column'=>'address'])
        </div>
        
        <div class="form-group">
            <label for="username">@lang('user.label.username')</label>
            <input type="hidden" name="user_id"  value="{{ old('user_id') }}">
            <input type="text" name="username" class="form-control @classInpError('username')" placeholder="@lang('user.placeholder.username')" value="{{ old('username') }}">

            @inpSpanError(['column'=>'username'])
        </div>
        <div class="form-group">
            <label for="password">@lang('user.label.password')</label>
            <div class="input-group mb-3">
                <input type="password" name="password" class="form-control @classInpError('password')" value="{{ old('password') }}" placeholder="@lang('user.placeholder.password')">
                <div class="input-group-append">
                    <button type="button" class="btn btn-default btn-flat" id="btn-show-password">
                        <i class="fas fa-eye"></i>
                    </button>
                </div>

                @inpSpanError(['column'=>'password'])
            </div>
        </div>
        <div class="form-group">
            <label for="form-branch_id" class="control-label">@lang('employee.label.branch_id')</label>
            
            <select name="branch_id" id="form-branch_id" class="form-control select2 @classInpError('branch_id')" data-placeholder="">
                @foreach($branchs as $id => $name)
                    <option value="{{ $id }}" {{ old('branch_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'branch_id'])
        </div>
        <div class="form-group">
            <label for="form-user_role" class="control-label">@lang('user.placeholder.role')</label>

            <span data-toggle="tooltip" data-placement="top" title="@lang('employee.message.desc_user_role')">
                <i class="fas fa-question-circle"></i>
            </span>
            
            <select name="user_role" id="form-user_role" class="form-control select2 @classInpError('user_role')" data-placeholder="">
                @foreach($form_roles as $id => $name)
                    <option value="{{ $id }}" {{ old('user_role') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'user_role'])
        </div>
        
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="employee-status" {{ ( old('status') || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="employee-status">@lang('employee.label.status')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                    <i class="fas fa-question-circle"></i>
                </span>
                @inpSpanError(['column'=>'status'])
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.employee.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>