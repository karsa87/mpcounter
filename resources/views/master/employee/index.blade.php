@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('employee.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.employee.search')
        </div>
    </div>
</div>
<div class="row" id="content-employee">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('employee.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-employee">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('employee.label.name')</th>
                            <th class="sorting-form" data-column="email">@lang('employee.label.information')</th>
                            <th class="sorting-form" data-column="address">@lang('employee.label.address')</th>
                            <th class="text-center">@lang('employee.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($employees) && count($employees) > 0 )
                        @foreach( $employees as $i => $r)
                        <tr>
                            <td class="text-center">{{ $employees->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->name }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('employee.label.username'):</strong> {{ $r->user->username }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->email }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('employee.label.phone'):</strong> {{ $r->phone }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('employee.label.role'):</strong> 
                                        @php
                                            $roles = $r->user->getRoles();
                                        @endphp
                                        {{ $roles->count() > 0 ? $roles->first()->name : '' }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('employee.label.branch_id'):</strong> {{ $r->branch_id ? $r->branch->name : '' }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $r->address }}
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('employee.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.employee.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.employee.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-employee btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.employee.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.employee.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($employees) && count($employees) > 0 )
            {!! \App\Util\Base\Layout::paging($employees) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('employee.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.employee.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/employee.js') }}?{{ config('app.version') }}"></script>
@endpush