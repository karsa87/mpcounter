<!-- form start -->
<form id="form-bank" role="form" method="POST" action="{{ route('master.bank.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('bank.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('bank.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="account_number">@lang('bank.label.account_number')</label>
            <input type="text" name="account_number" class="form-control @classInpError('account_number')" placeholder="@lang('bank.placeholder.account_number')" value="{{ old('account_number') }}">
            
            @inpSpanError(['column'=>'account_number'])
        </div>
        <div class="form-group">
            <label for="name_owner">@lang('bank.label.name_owner')</label>
            <input type="text" name="name_owner" class="form-control @classInpError('name_owner')" placeholder="@lang('bank.placeholder.name_owner')" value="{{ old('name_owner') }}">
            
            @inpSpanError(['column'=>'name_owner'])
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="is_default" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('is_default')" name="is_default"  id="bank-is_default" {{ ( old('is_default') || old('is_default') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="bank-is_default">@lang('bank.label.is_default')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('bank.message.description_is_default')">
                    <i class="fas fa-question-circle"></i>
                </span>

                @inpSpanError(['column'=>'is_default'])
            </div>
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="bank-status" {{ ( old('status') || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="bank-status">@lang('bank.label.status')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                    <i class="fas fa-question-circle"></i>
                </span>

                @inpSpanError(['column'=>'status'])
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.bank.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>