<!-- form start -->
<form role="form" id="form-search-bank" action="{{ route('master.bank.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('bank.label.name')</label>
                    <input type="text" name="search_name" class="form-control" placeholder="@lang('bank.placeholder.name')" value="{{ request('search_name') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="account_number">@lang('bank.label.account_number')</label>
                    <input type="text" name="_an" class="form-control" placeholder="@lang('bank.placeholder.account_number')" value="{{ request('_an') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="name_owner">@lang('bank.label.name_owner')</label>
                    <input type="text" name="_nmow" class="form-control" placeholder="@lang('bank.placeholder.name_owner')" value="{{ request('_nmow') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_df">@lang('bank.label.is_default')</label>
                    <select name="_df"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('bank.list.default') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_df')) && request('_df') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sts">@lang('bank.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('bank.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.bank.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>