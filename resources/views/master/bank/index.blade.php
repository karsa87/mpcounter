@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('bank.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.bank.search')
        </div>
    </div>
</div>
<div class="row" id="content-bank">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('bank.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-bank">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('bank.label.name')</th>
                            <th>@lang('bank.label.account_bank')</th>
                            <th class="text-right sorting-form" data-column="saldo">@lang('bank.label.saldo')</th>
                            <th class="text-center">@lang('bank.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($banks) && count($banks) > 0 )
                        @foreach( $banks as $i => $r)
                        <tr>
                            <td class="text-center">{{ $banks->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('bank.label.is_default'):</strong>
                                        <span class="badge {{ $r->is_default ? 'badge-success' : 'badge-danger' }}">@lang('bank.list.default.'.$r->is_default)</span>
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name_owner }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('bank.label.account_number'):</strong> {{ $r->account_number }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-right">
                                {{ $util->format_currency($r->saldo) }}
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('bank.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.bank.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.bank.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-bank btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.bank.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.bank.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($banks) && count($banks) > 0 )
            {!! \App\Util\Base\Layout::paging($banks) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('bank.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.bank.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/bank.js') }}?{{ config('app.version') }}"></script>
@endpush