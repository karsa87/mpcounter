@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('branch.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.branch.search')
        </div>
    </div>
</div>
<div class="row" id="content-branch">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('branch.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-branch">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('branch.label.name')</th>
                            <th class="sorting-form" data-column="address">@lang('branch.label.address')</th>
                            <th>@lang('branch.label.employee_id')</th>
                            <th class="text-center">@lang('branch.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($branchs) && count($branchs) > 0 )
                        @foreach( $branchs as $i => $r)
                        <tr>
                            <td class="text-center">{{ $branchs->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->name }}
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->address }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('branch.label.phone'):</strong> {{ $r->phone }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->employee_id ? $r->branch_manager->name : '' }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('branch.label.phone'):</strong> {{ $r->employee_id ? $r->branch_manager->phone : '' }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('branch.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.branch.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.branch.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-branch btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.branch.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.branch.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($branchs) && count($branchs) > 0 )
            {!! \App\Util\Base\Layout::paging($branchs) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('branch.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.branch.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/branch.js') }}?{{ config('app.version') }}"></script>
@endpush