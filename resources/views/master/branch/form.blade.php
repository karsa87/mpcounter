<!-- form start -->
<form id="form-branch" role="form" method="POST" action="{{ route('master.branch.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('branch.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('branch.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="phone">@lang('branch.label.phone')</label>
            <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('branch.placeholder.phone')" value="{{ old('phone') }}">
            
            @inpSpanError(['column'=>'phone'])
        </div>
        <div class="form-group">
            <label for="address">@lang('branch.label.address')</label>
            <textarea name="address" class="form-control @classInpError('address')" rows="3" placeholder="@lang('branch.placeholder.address')" >{{ old('address') }}</textarea>
            
            @inpSpanError(['column'=>'address'])
        </div>
        <div class="form-group" style="display: none;">
            <label for="form-employee_id" class="control-label">@lang('branch.label.employee_id')</label>

            <span data-toggle="tooltip" data-placement="top" title="@lang('branch.message.desc_employee_id')">
                <i class="fas fa-question-circle"></i>
            </span>
            <select name="employee_id" id="form-employee_id" class="form-control select2 @classInpError('employee_id')" data-placeholder="">
                @foreach($employes as $id => $name)
                    <option value="{{ $id }}" {{ old('employee_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'employee_id'])
        </div>
        
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="branch-status" {{ ( empty(old('status')) || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="branch-status">@lang('branch.label.status')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                    <i class="fas fa-question-circle"></i>
                </span>
                @inpSpanError(['column'=>'status'])
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.branch.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>