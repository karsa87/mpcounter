@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="card card-solid">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h3 class="d-inline-block d-sm-none">{{ $model->name }}</h3>
                <div class="col-12">
                    <img src="{{ $model->image() }}" class="product-image" alt="Images 1" id="preview-product-image" style="height: 250px;background-position: center center;background-repeat: no-repeat;">
                </div>
                <div class="col-12 product-image-thumbs">
                    @foreach ($model->images_arr() as $i => $image)
                    <div class="product-image-thumb {{ $i == 0 ? 'active' : '' }}">
                        <img src="{{ $image }}" alt="Images {{ $i+1 }}">
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <h3 class="my-3">{{ $model->name }}</h3>
                <p>{{ $model->information }}</p>

                <hr>
                <h4>@lang('product.label.code')</h4>
                <p>{{ $model->code }}</p>

                <hr>
                <h4>@lang('product.label.category_id')</h4>
                <p>{{ $model->category->name }}</p>

                <hr>
                <h4>@lang('product.label.brand_id')</h4>
                <p>{{ $model->brand->name }}</p>

                <hr>
                <h4>
                    @lang('product.label.available_stock')
                    <span class="float-right badge bg-success">{{ $model->productIdentity->sum('stock') }}</span>
                </h4>

                <hr>
                <h4>
                    @lang('product.label.status')
                    <span class="float-right badge {{ $model->status ? 'bg-success' : 'bg-danger' }}">@lang('product.list.status.'.$model->status)</span>
                </h4>
                {{-- 
                <hr>
                <h4>
                    @lang('product.label.with_expired_date')
                    <span class="float-right badge {{ $model->with_expired_date ? 'bg-success' : 'bg-danger' }}">@lang('product.list.expired.'.$model->with_expired_date)</span>
                </h4> 
                --}}
                <div class="bg-gray p-3 mt-5">
                    @hasPermission('master.product.show.hpp')
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('product.label.cost_of_goods')
                            <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                                <i class="fas fa-question-circle"></i>
                            </span>
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($model->cost_of_goods) }}
                            </span>
                        </p>
                    </div>
                    @endhasPermission
                    <div class="bg-green d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('product.label.sell_price')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($model->sell_price) }}
                            </span>
                        </p>
                    </div>
                    <div class="bg-green d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('product.label.reseller1_sell_price')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($model->reseller1_sell_price) }}
                            </span>
                        </p>
                    </div>
                    <div class="bg-green d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('product.label.reseller2_sell_price')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($model->reseller2_sell_price) }}
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-4">
            <div class="col-lg-12">
                <a href="{{ route('master.product.index') }}" class="btn btn-danger btn-lg">
                    @lang('global.back')
                </a>

                @hasPermission('master.product.edit')
                <a href="{{ route('master.product.edit', $model->id) }}" class="btn btn-warning btn-lg float-right">
                    @lang('global.edit')
                </a>
                @endhasPermission
            </div>
        </div>
        <div class="row mt-4">
            <nav class="w-100">
                <div class="nav nav-tabs" id="product-tab" role="tablist">
                    <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">@lang('product.label.product_identity')</a>
                </div>
            </nav>
            <div class="tab-content p-3 w-100" id="nav-tabContent">
                <div class="tab-pane fade show active" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab">
                    <list-product-identity
                        v-bind:branchs="{{ $branchs }}"
                        v-bind:product_id="{{ $model->id }}"
                    ></list-product-identity>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/product.js') }}?{{ config('app.version') }}"></script>
@endpush