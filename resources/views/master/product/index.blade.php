@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('product.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.product.search')
        </div>
    </div>
</div>
<div class="row" id="content-product">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('product.title')</h3>
                <a href="{{ route('master.product.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-hover sorting-table" data-form="form-search-product">
                    <thead>
                        <tr>
                            <th style="width: 5%" class="text-center">@lang('global.no')</th>
                            <th style="width: 20%" class="sorting-form" data-column="name">@lang('product.label.name')</th>
                            <th style="width: 20%">@lang('product.label.information')</th>
                            <th style="width: 10%" class="text-center sorting-form"  data-column="stock">@lang('product.label.stock')</th>
                            <th style="width: 10%">@lang('product.label.images')</th>
                            <th style="width: 15%" class="sorting-form" data-column="sell_price">@lang('product.label.sell_price')</th>
                            <th style="width: 5%" class="text-center">@lang('product.label.status')</th>
                            <th style="width: 5%">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($products) && count($products) > 0 )
                        @foreach( $products as $i => $r)
                        <tr>
                            <td class="text-center">{{ $products->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.code'):</strong> {{ $r->code }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.category_id'):</strong> {{ $r->category->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.brand_id'):</strong> {{ $r->brand->name }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-center">
                                @php
                                    $stock_now = $r->stock->sum('stock');
                                @endphp
                                <h3>
                                    <span class="badge {{ ($stock_now <= 1) ? 'badge-danger' : 'badge-success' }} ">{{ $stock_now }}</span>
                                </h3>
                            </td>
                            <td class="text-wrap">
                                @php
                                    $image = $r->image();
                                @endphp
                                <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                <img src="{{ $image }}" class="img-fluid mb-2" alt="Images {{ $i+1 }}"/>
                                </a>
                            </td>
                            <td class="text-wrap">
                                <h3 class="mb-1">
                                    {{ $util->format_currency($r->sell_price) }}
                                </h3>
                                @hasPermission('master.product.show.hpp')
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.cost_of_goods'):</strong> {{ $util->format_currency($r->cost_of_goods) }}
                                    </small>
                                </p>
                                @endhasPermission
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('product.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.product.edit')
                                    <a title="@lang('global.edit')" href="{{ route('master.product.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-product btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.product.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('master.product.show', $r['id']) }}" class="btn btn-sm btn-info" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.product.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.product.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($products) && count($products) > 0 )
            {!! \App\Util\Base\Layout::paging($products) !!}
            @endif
        </div>
    </div>
</div>
@stop