@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')
@inject('product', 'App\Models\Product')

@section('content')
<!-- form start -->
<form 
    id="form-product" role="form" method="POST" 
    action="{{ $model->exists ? route('master.product.update', $model->id) : route('master.product.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
    @csrf
    <input type="hidden" name="id" value="{{ old('id') ?? $model->id }}">
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('product.title')</h3>
            </div>

            <div class="card-body">
                <div class="form-group">
                    <label for="code">@lang('product.label.code')</label>
                    <input type="text" name="code" class="form-control @classInpError('code')" placeholder="@lang('product.placeholder.code')" value="{{ old('code') ?? $product->nextCode() }}">
                    
                    @inpSpanError(['column'=>'code'])
                </div>
                <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="hidden" name="status" value="0" />
                        <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="product-status" {{ ( $model->status || old('status') == 'on' || !$model->exists ) ? 'checked' : '' }}>
                        <label class="custom-control-label" for="product-status">@lang('product.label.status')</label>

                        <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                            <i class="fas fa-question-circle"></i>
                        </span>
                        @inpSpanError(['column'=>'status'])
                    </div>
                </div>
                {{--                 
                <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="hidden" name="with_expired_date" value="0" />
                        <input type="checkbox" class="custom-control-input @classInpError('with_expired_date')" name="with_expired_date"  id="product-with_expired_date" {{ ( $model->with_expired_date || old('with_expired_date') == 'on' ) ? 'checked' : '' }}>
                        <label class="custom-control-label" for="product-with_expired_date">@lang('product.label.with_expired_date')</label>

                        <span data-toggle="tooltip" data-placement="top" title="@lang('product.message.desc_with_expired_date')">
                            <i class="fas fa-question-circle"></i>
                        </span>
                        @inpSpanError(['column'=>'with_expired_date'])
                    </div>
                </div>
                 --}}
                <div class="form-group">
                    <label for="name">@lang('product.label.name')</label>
                    <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('product.placeholder.name')" value="{{ old('name') ?? $model->name }}">
                    
                    @inpSpanError(['column'=>'name'])
                </div>
                <div class="form-group">
                    <label for="form-category_id" class="control-label">
                        @lang('product.label.category_id')
                        @hasPermission('master.category.create')
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal-category">
                            <i class="fas fa-plus"></i>
                        </button>
                        @endhasPermission
                    </label>
                    <select name="category_id" id="form-category_id" class="form-control ajax-select2 @classInpError('category_id')" data-url="{{ route('ajax.category') }}" data-placeholder="">
                        @if (old('category_id') || $model->exists)
                        @php
                            $model->category_id = old('category_id') ?? $model->category_id;
                        @endphp
                        <option value="{{ $model->category_id }}" selected>
                            {{ $model->category->name }}
                        </option>
                        @endif
                        {{-- @foreach($categories as $id => $name)
                            <option value="{{ $id }}" {{ $model->category_id == $id || old('category_id') == $id ? "selected"  : ""}} >
                                {{ $name }}
                            </option>
                        @endforeach --}}
                    </select>

                    @inpSpanError(['column'=>'category_id'])
                </div>
                <div class="form-group">
                    <label for="form-brand_id" class="control-label">
                        @lang('product.label.brand_id')
                        @hasPermission('master.brand.create')
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal-brand">
                            <i class="fas fa-plus"></i>
                        </button>
                        @endhasPermission
                    </label>
                    <select name="brand_id" id="form-brand_id" class="form-control ajax-select2 @classInpError('brand_id')" data-url="{{ route('ajax.brand') }}" data-placeholder="">
                        @if (old('brand_id') || $model->exists)
                        @php
                            $model->brand_id = old('brand_id') ?? $model->brand_id;
                        @endphp
                        <option value="{{ $model->brand_id }}" selected>
                            {{ $model->brand->name }}
                        </option>
                        @endif
                        {{-- @foreach($brands as $id => $name)
                            <option value="{{ $id }}" {{ $model->brand_id == $id || old('brand_id') == $id ? "selected"  : ""}} >
                                {{ $name }}
                            </option>
                        @endforeach --}}
                    </select>

                    @inpSpanError(['column'=>'brand_id'])
                </div>
                <div class="form-group">
                    <label for="sell_price">@lang('product.label.sell_price')</label>
                    <input type="text" name="sell_price" class="form-control format-number @classInpError('sell_price')" placeholder="@lang('product.placeholder.sell_price')" value="{{ old('sell_price') ?? $model->sell_price }}">
                    
                    @inpSpanError(['column'=>'sell_price'])
                </div>
                <div class="form-group">
                    <label for="reseller1_sell_price">@lang('product.label.reseller1_sell_price')</label>
                    <input type="text" name="reseller1_sell_price" class="form-control format-number @classInpError('reseller1_sell_price')" placeholder="@lang('product.placeholder.reseller1_sell_price')" value="{{ old('reseller1_sell_price') ?? $model->reseller1_sell_price }}">
                    
                    @inpSpanError(['column'=>'reseller1_sell_price'])
                </div>
                <div class="form-group">
                    <label for="reseller2_sell_price">@lang('product.label.reseller2_sell_price')</label>
                    <input type="text" name="reseller2_sell_price" class="form-control format-number @classInpError('reseller2_sell_price')" placeholder="@lang('product.placeholder.reseller2_sell_price')" value="{{ old('reseller2_sell_price') ?? $model->reseller2_sell_price }}">
                    
                    @inpSpanError(['column'=>'reseller2_sell_price'])
                </div>
                <div class="form-group">
                    <label for="information">@lang('product.label.information')</label>
                    <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('product.placeholder.information')" >{{ old('information') ?? $model->information }}</textarea>
                    
                    @inpSpanError(['column'=>'information'])
                </div>
            </div>
        </div>
    </div> 


    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('product.label.images')</h3>
            </div>
            <div class="card-body">
                <input type="hidden" name="images" id="result-upload" value="{{ old('images') ?? $model->images }}">
                    <div class="dropzone" id="files-upload" data-href="{{ route('master.product.upload') }}"></div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                @if (!isset($modal))
                <a href="{{ route('master.product.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                @endif
                <button type="submit" class="btn btn-success float-right {{ !isset($modal) ? 'btn-loader' : '' }}">@lang('global.save')</button>
            </div>
        </div>
    </div> 
</div>
</form>

@include('master.category.modal_form')
@include('master.brand.modal_form')
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/product.js') }}?{{ config('app.version') }}"></script>
@endpush