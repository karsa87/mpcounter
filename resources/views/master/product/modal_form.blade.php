
<div class="modal fade" id="modal-product">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('global.add') @lang('product.title')</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form 
                    id="form-product" role="form" method="POST" 
                    action="{{ route('master.product.store') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ old('id') ?? $model->id }}">
                    <input type="hidden" name="status" value="on">
                    <input type="hidden" name="images" value="[]">

                    <div class="row">
                        <div class="col-xs-12 xol-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="name">@lang('product.label.name')</label>
                                <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('product.placeholder.name')" value="{{ old('name') ?? $model->name }}">
                                
                                @inpSpanError(['column'=>'name'])
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="form-category_id" class="control-label">
                                    @lang('product.label.category_id')
                                </label>
                                <select name="category_id" id="form-category_id" class="form-control ajax-select2 @classInpError('category_id')" data-url="{{ route('ajax.category') }}" data-placeholder="" style="width: 100%;">
                                </select>

                                @inpSpanError(['column'=>'category_id'])
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="form-brand_id" class="control-label">
                                    @lang('product.label.brand_id')
                                </label>
                                <select name="brand_id" id="form-brand_id" class="form-control ajax-select2 @classInpError('brand_id')" data-url="{{ route('ajax.brand') }}" data-placeholder="" style="width: 100%;">
                                </select>

                                @inpSpanError(['column'=>'brand_id'])
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="sell_price">@lang('product.label.sell_price')</label>
                                <input type="text" name="sell_price" class="form-control format-number @classInpError('sell_price')" placeholder="@lang('product.placeholder.sell_price')" value="{{ old('sell_price') ?? $model->sell_price }}">
                                
                                @inpSpanError(['column'=>'sell_price'])
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="reseller1_sell_price">@lang('product.label.reseller1_sell_price')</label>
                                <input type="text" name="reseller1_sell_price" class="form-control format-number @classInpError('reseller1_sell_price')" placeholder="@lang('product.placeholder.reseller1_sell_price')" value="{{ old('reseller1_sell_price') ?? $model->reseller1_sell_price }}">
                                
                                @inpSpanError(['column'=>'reseller1_sell_price'])
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="reseller2_sell_price">@lang('product.label.reseller2_sell_price')</label>
                                <input type="text" name="reseller2_sell_price" class="form-control format-number @classInpError('reseller2_sell_price')" placeholder="@lang('product.placeholder.reseller2_sell_price')" value="{{ old('reseller2_sell_price') ?? $model->reseller2_sell_price }}">
                                
                                @inpSpanError(['column'=>'reseller2_sell_price'])
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-12 col-lg-12">
                            <div class="form-group">
                                <label for="information">@lang('product.label.information')</label>
                                <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('product.placeholder.information')" >{{ old('information') ?? $model->information }}</textarea>
                                
                                @inpSpanError(['column'=>'information'])
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success float-right">@lang('global.save')</button>
                    </div>
                </form>

                @include('master.category.modal_form')
                @include('master.brand.modal_form')
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $('#form-product').submit(function(e){
        e.preventDefault();

        var data = $(this).serialize();

        var msg_failed = "";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': CONFIG.token
            },
            url: $(this).attr('action'), 
            type: 'POST', 
            data: data,
            success: function(result){
                if(result.status == true){
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#modal-product').modal('hide');
                } else {
                    $.each(result.error, function(k, v) {
                        msg_failed += '</li><li>';
                        msg_failed += v.join('</li><li>');
                    });
                    
                }
            },
            beforeSend: function( xhr ) {
                showProgress();
            }
        })
        .always(function(result) {
            Swal.close();
            if (msg_failed !== "") {
                showAlert("<ul>" + msg_failed + "</ul>", 'error');
            }
        })
        .fail(function() {
            swal_confirm.fire(
                "Can't delete",
                msg_failed,
                'error'
            )
        });

        return false;
    });
</script>
@endpush