@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')
@inject('model', 'App\Models\Member')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('supplier.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.supplier.search')
        </div>
    </div>
</div>
<div class="row" id="content-supplier">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('supplier.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-supplier">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('supplier.label.name')</th>
                            <th class="sorting-form" data-column="sales_name">@lang('supplier.label.sales_name')</th>
                            <th>@lang('supplier.label.information')</th>
                            <th class="text-center">@lang('supplier.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($suppliers) && count($suppliers) > 0 )
                        @foreach( $suppliers as $i => $r)
                        <tr>
                            <td class="text-center">{{ $suppliers->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('supplier.label.address'):</strong> {{ $r->address }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('supplier.label.phone'):</strong> {{ $r->phone }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->sales_name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('supplier.label.sales_phone'):</strong> {{ $r->sales_phone }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $r->information }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('supplier.label.has_tax'):</strong> 
                                        <span class="badge {{ $r->has_tax ? 'badge-success' : 'badge-danger' }}">@lang('supplier.list.has_tax.'.$r->has_tax)</span>
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('supplier.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.supplier.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.supplier.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-supplier btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.supplier.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.supplier.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($suppliers) && count($suppliers) > 0 )
            {!! \App\Util\Base\Layout::paging($suppliers) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('supplier.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.supplier.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/supplier.js') }}?{{ config('app.version') }}"></script>
@endpush