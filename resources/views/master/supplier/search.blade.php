<!-- form start -->
<form role="form" id="form-search-supplier" action="{{ route('master.supplier.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="_nm">@lang('supplier.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('supplier.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_snm">@lang('supplier.label.sales_name')</label>
                    <input type="text" name="_snm" class="form-control" placeholder="@lang('supplier.placeholder.sales_name')" value="{{ request('_snm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sts">@lang('supplier.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value="" selected></option>
                        @foreach (trans('supplier.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.supplier.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>