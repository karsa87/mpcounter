<!-- form start -->
<form id="form-supplier" role="form" method="POST" action="{{ route('master.supplier.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('supplier.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('supplier.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="phone">@lang('supplier.label.phone')</label>
            <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('supplier.placeholder.phone')" value="{{ old('phone') }}">
            
            @inpSpanError(['column'=>'phone'])
        </div>
        <div class="form-group">
            <label for="address">@lang('supplier.label.address')</label>
            <textarea name="address" class="form-control @classInpError('address')" rows="3" placeholder="@lang('supplier.placeholder.address')" >{{ old('address') }}</textarea>
            
            @inpSpanError(['column'=>'address'])
        </div>
        <div class="form-group">
            <label for="sales_name">@lang('supplier.label.sales_name')</label>
            <input type="text" name="sales_name" class="form-control @classInpError('sales_name')" placeholder="@lang('supplier.placeholder.sales_name')" value="{{ old('sales_name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="sales_phone">@lang('supplier.label.sales_phone')</label>
            <input type="text" name="sales_phone" class="form-control @classInpError('sales_phone')" placeholder="@lang('supplier.placeholder.sales_phone')" value="{{ old('sales_phone') }}">
            
            @inpSpanError(['column'=>'sales_phone'])
        </div>
        <div class="form-group">
            <label for="information">@lang('supplier.label.information')</label>
            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('supplier.placeholder.information')" >{{ old('information') }}</textarea>
            
            @inpSpanError(['column'=>'information'])
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="has_tax" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('has_tax')" name="has_tax"  id="supplier-has_tax" {{ ( old('has_tax') || old('has_tax') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="supplier-has_tax">@lang('supplier.label.has_tax')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('supplier.message.desc_has_tax')">
                    <i class="fas fa-question-circle"></i>
                </span>
                @inpSpanError(['column'=>'has_tax'])
            </div>
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="supplier-status" {{ ( empty(old('status')) || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="supplier-status">@lang('supplier.label.status')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                    <i class="fas fa-question-circle"></i>
                </span>
                @inpSpanError(['column'=>'status'])
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.supplier.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>