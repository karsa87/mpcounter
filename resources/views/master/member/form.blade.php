<!-- form start -->
<form id="form-member" role="form" method="POST" action="{{ route('master.member.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('member.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('member.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="phone">@lang('member.label.phone')</label>
            <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('member.placeholder.phone')" value="{{ old('phone') }}">
            
            @inpSpanError(['column'=>'phone'])
        </div>
        <div class="form-group">
            <label for="address">@lang('member.label.address')</label>
            <textarea name="address" class="form-control @classInpError('address')" rows="3" placeholder="@lang('member.placeholder.address')" >{{ old('address') }}</textarea>
            
            @inpSpanError(['column'=>'address'])
        </div>
        <div class="form-group">
            <label for="form-identity_type" class="control-label">@lang('member.label.identity_type')</label>
            <select name="identity_type" id="form-identity_type" class="form-control select2 @classInpError('identity_type')" data-placeholder="">
                @foreach(trans('member.list.identity_type') as $id => $name)
                    <option value="{{ $id }}" {{ old('identity_type') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'identity_type'])
        </div>
        <div class="form-group">
            <label for="identity">@lang('member.label.identity')</label>
            <input type="text" name="identity" class="form-control @classInpError('identity')" placeholder="@lang('member.placeholder.identity')" value="{{ old('identity') }}">
            
            @inpSpanError(['column'=>'identity'])
        </div>
        <div class="form-group">
            <label for="birthdate">@lang('member.label.birthdate')</label>
            <input type="text" name="birthdate" class="form-control datepicker @classInpError('birthdate')" placeholder="@lang('member.placeholder.birthdate')" autocomplete="off" value="{{ old('birthdate') }}">
            
            @inpSpanError(['column'=>'birthdate'])
        </div>
        <div class="form-group">
            <label for="member_number">@lang('member.label.member_number')</label>
            <input type="text" name="member_number" class="form-control @classInpError('member_number')" placeholder="@lang('member.placeholder.member_number')" value="{{ old('member_number') ?: $model->nextNumber() }}">
            
            @inpSpanError(['column'=>'member_number'])
        </div>

        <div class="form-group">
            <label for="form-type" class="control-label">@lang('member.label.type')</label>
            <select name="type" id="form-type" class="form-control select2 @classInpError('type')" data-placeholder="">
                @foreach(trans('member.list.type') as $id => $name)
                    <option value="{{ $id }}" {{ old('type') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'type'])
        </div>

        {{--
        <div class="form-group">
            <label for="max_day_debt">@lang('member.label.max_day_debt')</label>
            <input type="text" name="max_day_debt" class="form-control @classInpError('max_day_debt')" placeholder="@lang('member.placeholder.max_day_debt')" value="{{ old('max_day_debt') }}">
            
            @inpSpanError(['column'=>'max_day_debt'])
        </div>
        <div class="form-group">
            <label for="max_debt">@lang('member.label.max_debt')</label>
            <input type="text" name="max_debt" class="form-control format-number @classInpError('max_debt')" placeholder="@lang('member.placeholder.max_debt')" value="{{ old('max_debt') }}">
            
            @inpSpanError(['column'=>'max_debt'])
        </div>
        --}}
        
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input @classInpError('status')" name="status"  id="member-status" {{ ( old('status') || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="member-status">@lang('member.label.status')</label>

                <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                    <i class="fas fa-question-circle"></i>
                </span>
                @inpSpanError(['column'=>'status'])
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.member.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>