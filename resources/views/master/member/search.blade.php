<!-- form start -->
<form role="form" id="form-search-member" action="{{ route('master.member.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('member.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('member.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_tp">@lang('member.label.type')</label>
                    <select name="_tp"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('member.list.type') as $id => $name)
                        <option value="{{ $id }}" {{ request('_tp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_status">@lang('member.label.status')</label>
                    <select name="search_status"  class="form-control" style="width: 100%;">
                        <option value="" selected></option>
                        @foreach (trans('member.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('search_status')) && request('search_status') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.member.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>