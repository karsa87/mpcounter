@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')
@inject('model', 'App\Models\Member')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('member.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.member.search')
        </div>
    </div>
</div>
<div class="row" id="content-member">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('member.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-member">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('member.label.name')</th>
                            <th class="sorting-form" data-column="address">@lang('member.label.address')</th>
                            <th class="sorting-form" data-column="member_number">@lang('member.label.member_number')</th>
                            <th class="text-center">@lang('member.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($members) && count($members) > 0 )
                        @foreach( $members as $i => $r)
                        <tr>
                            <td class="text-center">{{ $members->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                    @if (!auth()->user()->underAdminWika())
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('global.id'):{{ $r->id }}</strong>
                                        </small>
                                    </p>
                                    @endif
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('member.list.identity_type.'.$r->identity_type):</strong> {{ $r->identity }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('member.label.poin'):</strong> {{ $r->poin }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->address }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('member.label.phone'):</strong> {{ $r->phone }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->member_number }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('member.label.type'):</strong> @lang('member.list.type.'.$r->type)
                                    </small>
                                </p>

                                {{-- 
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('member.label.max_day_debt'):</strong> {{ $r->max_day_debt }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('member.label.max_debt'):</strong> {{ $util->format_currency($r->max_debt) }}
                                    </small>
                                </p> 
                                --}}
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('member.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.member.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.member.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-member btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.member.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.member.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($members) && count($members) > 0 )
            {!! \App\Util\Base\Layout::paging($members) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('member.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.member.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/member.js') }}?{{ config('app.version') }}"></script>
@endpush