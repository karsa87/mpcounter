<!-- form start -->
<form role="form" id="form-search-product" action="{{ route('master.product.identity.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('global.keyword')</label>
                    <input type="text" name="_k" class="form-control" placeholder="@lang('product.placeholder.name')" value="{{ request('_k') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bd">@lang('product.label.brand_id')</label>
                    <select name="_bd"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($brands as $id => $name)
                        <option value="{{ $id }}" {{ request('_bd') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_cg">@lang('product.label.category_id')</label>
                    <select name="_cg"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($categories as $id => $name)
                        <option value="{{ $id }}" {{ request('_cg') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="_stc">@lang('product.label.stock')</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <select name="_bc"  class="form-control" style="width: 100%;">
                                <option value="">@lang('global.all')</option>
                                @foreach ($branchs as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric(request('_bc')) && request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group-prepend">
                            <select name="_sop"  class="form-control" style="width: 100%;">
                                @foreach (trans('global.array.operator') as $id => $name)
                                <option value="{{ $id }}" {{ is_numeric(request('_sop')) && request('_sop') == $id ? "selected"  : ""}}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" name="_stc" class="form-control" value="{{ request('_stc') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.product.identity.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>