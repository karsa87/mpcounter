@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('product.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.product_identity.search')
        </div>
    </div>
</div>
<div class="row" id="content-product">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('product.title')</h3>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-hover sorting-table" data-form="form-search-product">
                    <thead>
                        <tr>
                            <th style="width: 5%" class="text-center">@lang('global.no')</th>
                            <th style="width: 10%" class="text-center sorting-form"  data-column="stock">@lang('product.label.stock')</th>
                            <th style="width: 20%" class="sorting-form" data-column="name">@lang('product.label.name')</th>
                            <th style="width: 25%">@lang('product.label.information')</th>
                            <th style="width: 20%">@lang('product.label.sell_price')</th>
                            <th style="width: 20%">@lang('purchase_order.label.purchase_price')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($products) && count($products) > 0 )
                        @foreach( $products as $i => $r)
                        <tr>
                            <td class="text-center">{{ $products->firstItem() + $i }}</td>
                            <td class="text-wrap text-center">
                                @php
                                    $stock_now = $r->stock;
                                @endphp
                                <h3>
                                    <span class="badge {{ ($stock_now <= 1) ? 'badge-danger' : 'badge-success' }} ">{{ $stock_now }}</span>
                                </h3>
                                <p class="m-0">
                                    <small>
                                        {{ $r->branch->name }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->product->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.code'):</strong> {{ $r->product->code }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                {{ $r->identity ?? '-' }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.category_id'):</strong> {{ $r->product->category->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.brand_id'):</strong> {{ $r->product->brand->name }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h3 class="mb-1">
                                    {{ $util->format_currency($r->product->sell_price) }}
                                </h3>
                                @hasPermission('master.product.show.hpp')
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('product.label.cost_of_goods'):</strong> {{ $util->format_currency($r->product->cost_of_goods) }}
                                    </small>
                                </p>
                                @endhasPermission
                            </td>
                            <td class="text-wrap">
                                <h3 class="mb-1">
                                    {{ $r->poDetail->last() ? $util->format_currency($r->poDetail->last()->purchase_price) : 0 }}
                                </h3>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($products) && count($products) > 0 )
            {!! \App\Util\Base\Layout::paging($products) !!}
            @endif
        </div>
    </div>
</div>
@stop