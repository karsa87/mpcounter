
<div class="modal fade" id="modal-category">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('global.add') @lang('category.title')</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('master.category.form', ['modal'=>true])
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $('#form-category').submit(function(e){
        e.preventDefault();

        var data = $(this).serialize();

        var msg_failed = "";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': CONFIG.token
            },
            url: $(this).attr('action'), 
            type: 'POST', 
            data: data,
            success: function(result){
                if(result.status == true){
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#modal-category').modal('hide');
                } else {
                    $.each(result.error, function(k, v) {
                        msg_failed += '</li><li>';
                        msg_failed += v.join('</li><li>');
                    });
                    
                }
            },
            beforeSend: function( xhr ) {
                showProgress();
            }
        })
        .always(function(result) {
            Swal.close();
            if (msg_failed !== "") {
                showAlert("<ul>" + msg_failed + "</ul>", 'error');
            }
        })
        .fail(function() {
            swal_confirm.fire(
                "Can't delete",
                msg_failed,
                'error'
            )
        });

        return false;
    });
</script>
@endpush