<!-- form start -->
<form id="form-category" role="form" method="POST" action="{{ route('master.category.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('category.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('category.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="form-identifier" class="control-label">@lang('category.label.identifier')</label>
            
            <a href="javascript:void(0)" id="btn-identifier-info" data-toggle="tooltip" data-placement="top" title="@lang('global.click')">
                <i class="fas fa-question-circle"></i>
            </a>
            
            <select name="identifier" id="form-identifier" class="form-control select2 @classInpError('identifier')" data-placeholder="" style="width: 100%;">
                @foreach(trans('category.list.identifier') as $id => $name)
                    <option value="{{ $id }}" {{ old('identifier') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'identifier'])
        </div>
        
        <div class="form-group">
            <div class="custom-control custom-switch @classInpError('status')">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input" name="status"  id="category-status" {{ ( empty(old('status')) || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="category-status">@lang('category.label.status')</label>
            </div>
            @inpSpanError(['column'=>'status'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        @if (!isset($modal))
        <a href="{{ route('master.category.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        @endif
        <button type="submit" class="btn btn-success float-right {{ !isset($modal) ? 'btn-loader' : '' }}">@lang('global.save')</button>
    </div>
</form>