@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('category.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.category.search')
        </div>
    </div>
</div>
<div class="row" id="content-category">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('category.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-category">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('category.label.name')</th>
                            <th class="text-center">@lang('category.label.status')</th>
                            <th class="text-center">@lang('category.label.identifier')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($categories) && count($categories) > 0 )
                        @foreach( $categories as $i => $r)
                        <tr>
                            <td class="text-center">{{ $categories->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->name }}

                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('category.list.status.'.$r->status)</span>
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->identifier ? 'badge-success' : 'badge-danger' }}">@lang('category.list.identifier.'.$r->identifier)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.category.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.category.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-category btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.category.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.category.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($categorys) && count($categorys) > 0 )
            {!! \App\Util\Base\Layout::paging($categorys) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('category.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.category.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/category.js') }}?{{ config('app.version') }}"></script>
@endpush