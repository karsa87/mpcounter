<!-- form start -->
<form role="form" id="form-search-promo" action="{{ route('master.promo.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="code">@lang('promo.label.code')</label>
                    <input type="text" name="_cd" class="form-control" placeholder="@lang('promo.placeholder.code')" value="{{ request('_cd') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('promo.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('promo.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_ctg">@lang('promo.label.category_id')</label>
                    <select name="_ctg"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($categories as $id => $name)
                        <option value="{{ $id }}" {{ request('_ctg') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bd">@lang('promo.label.brand_id')</label>
                    <select name="_bd"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($brands as $id => $name)
                        <option value="{{ $id }}" {{ request('_bd') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="period">@lang('promo.label.period')</label>
                    <input type="text" name="_pd" class="form-control datetime-rangepicker" placeholder="@lang('promo.placeholder.period')" value="{{ request('_pd') }}" autocomplete="off">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sts">@lang('promo.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value="" selected></option>
                        @foreach (trans('promo.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.promo.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>