@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<!-- form start -->
<form 
    id="form-promo" role="form" method="POST" 
    action="{{ $model->exists ? route('master.promo.update', $model->id) : route('master.promo.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
    @csrf
    <input type="hidden" name="id" value="{{ old('id') ?? $model->id }}">
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('promo.title')</h3>
                
                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success float-right">
                    <input type="hidden" name="status" value="1" />
                    <input type="checkbox" class="custom-control-input" name="status"  id="promo-status" {{ ( $model->status || old('status') == 'on' || (!$model->exists && empty(old('status'))) ) ? 'checked' : '' }}>
                    <label class="custom-control-label" for="promo-status">@lang('promo.label.status')</label>

                    <span data-toggle="tooltip" data-placement="top" title="@lang('global.message.desc_status')">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="code">@lang('promo.label.code')</label>
                            <input type="text" name="code" class="form-control @classInpError('code')" placeholder="@lang('promo.placeholder.code')" value="{{ old('code') ?? $model->nextCode() }}">
                            
                            @inpSpanError(['column'=>'code'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="name">@lang('promo.label.name')</label>
                            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('promo.placeholder.name')" value="{{ old('name') ?? $model->name }}">
                            
                            @inpSpanError(['column'=>'name'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="period">@lang('promo.label.period')</label>
                            <input type="text" name="period" autocomplete="off" class="form-control datetime-rangepicker @classInpError('period')" placeholder="@lang('promo.placeholder.period')" value="{{ old('period') ?? ( $model->exists ? ($util->formatDate($model->date_start, 'Y-m-d H:i') . ' - '. $util->formatDate($model->date_end, 'Y-m-d H:i')) : '' ) }}">
                            
                            @inpSpanError(['column'=>'period'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="form-category_id" class="control-label">@lang('promo.label.category_id')</label>

                            <span data-toggle="tooltip" data-placement="top" title="@lang('promo.message.desc_category_id')">
                                <i class="fas fa-question-circle"></i>
                            </span>
                            
                            <select name="category_id" id="form-category_id" class="form-control select2 @classInpError('category_id')" data-placeholder="">
                                <option value=''>&nbsp</option>
                                @foreach($categories_form as $id => $name)
                                    <option value="{{ $id }}" {{ $model->category_id == $id || old('category_id') == $id ? "selected"  : ""}} >
                                        {{ $name }}
                                    </option>
                                @endforeach
                            </select>
                            
                            @inpSpanError(['column'=>'category_id'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="form-brand_id" class="control-label">@lang('promo.label.brand_id')</label>

                            <span data-toggle="tooltip" data-placement="top" title="@lang('promo.message.desc_brand_id')">
                                <i class="fas fa-question-circle"></i>
                            </span>

                            <select name="brand_id" id="form-brand_id" class="form-control select2 @classInpError('brand_id')" data-placeholder="">
                                <option value=''>&nbsp</option>
                                @foreach($brands_form as $id => $name)
                                    <option value="{{ $id }}" {{ $model->brand_id == $id || old('brand_id') == $id ? "selected"  : ""}} >
                                        {{ $name }}
                                    </option>
                                @endforeach
                            </select>
                            @inpSpanError(['column'=>'brand_id'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="discount_percent">@lang('promo.label.discount') @lang('promo.label.discount_percent')</label>
                            <input type="number" min="0" max="100" name="discount_percent" class="form-control @classInpError('discount_percent')" placeholder="@lang('promo.placeholder.discount_percent')" value="{{ old('discount_percent') ?? ($model->discount_percent ?? 0) }}">
                            
                            @inpSpanError(['column'=>'discount_percent'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="discount_price">@lang('promo.label.discount') @lang('promo.label.discount_price')</label>
                            <input type="text" name="discount_price" class="form-control format-number @classInpError('discount_price')" placeholder="@lang('promo.placeholder.discount_price')" value="{{ old('discount_price') ?? ($model->discount_price ?? 0) }}">
                            
                            @inpSpanError(['column'=>'discount_price'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="form-group">
                            <label for="information">@lang('promo.label.information')</label>
                            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('promo.placeholder.information')" >{{ old('information') ?? $model->information }}</textarea>
                            
                            @inpSpanError(['column'=>'information'])
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                        <input type="hidden" name="images" id="result-upload" value="{{ old('images') ?? $model->images }}">
                        <div class="dropzone" id="files-upload" data-href="{{ route('master.promo.upload') }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary" id="div-product">
            <div class="card-header">
                <h3 class="card-title">@lang('global.list') @lang('promo.label.selected_product')</h3>

                <span data-toggle="tooltip" data-placement="top" title="@lang('promo.message.desc_select_product')" class="float-right">
                    <i class="fas fa-question-circle"></i>
                </span>
            </div>

            <div class="card-body">
                @php
                    $input_products = old('products') ?? (isset($products) ? $products : []);
                    $input_products = json_encode($input_products);
                @endphp
                <list-product-promo 
                    v-bind:categories="{{ $categories }}"
                    v-bind:brands="{{ $brands }}"
                    v-bind:input_products="{{ $input_products }}"
                    >
                </list-product-promo>
            </div>
        </div>
    </div>

    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary">
            <div class="card-footer">
                <a href="{{ route('master.promo.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/promo.js') }}?{{ config('app.version') }}"></script>
@endpush