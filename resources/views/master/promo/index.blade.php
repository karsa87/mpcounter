@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('promo.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.promo.search')
        </div>
    </div>
</div>
<div class="row" id="content-promo">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('promo.title')</h3>
                <a href="{{ route('master.promo.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-promo">
                    <thead>
                        <tr>
                            <th style="width:5%;" class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name" style="width:20%;">@lang('promo.label.name')</th>
                            <th class="sorting-form" data-column="date_start" style="width:20%;">@lang('promo.label.information')</th>
                            <th class="sorting-form" data-column="discount_percent|discount_price" style="width:10%;">@lang('promo.label.discount')</th>
                            <th style="width:25%;">@lang('promo.label.images')</th>
                            <th style="width:10%;" class="text-center">@lang('promo.label.status')</th>
                            <th style="width:10%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($promos) && count($promos) > 0 )
                        @foreach( $promos as $i => $r)
                        <tr>
                            <td class="text-center">{{ $promos->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('promo.label.code'):</strong> {{ $r->code }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->information }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('promo.label.category_id'):</strong> {{ $r->category_id ? $r->category->name : '' }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('promo.label.period'):</strong> <br>
                                        {{ $util->formatDate($r->date_start, 'Y-m-d H:i') }} - {{ $util->formatDate($r->date_end, 'Y-m-d H:i') }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('promo.label.discount_percent'):</strong> {{ $util->format_currency($r->discount_percent) }}%
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('promo.label.discount_price'):</strong> {{ $util->format_currency($r->discount_price) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <div class="row">
                                    @foreach ($r->images_arr(2) as $i => $image)
                                    <div class="col-sm-4">
                                        <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                        <img src="{{ $image }}" class="img-fluid mb-2" alt="Images {{ $i+1 }}"/>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                            <td class="text-wrap text-center">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('promo.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.promo.edit')
                                    <a title="@lang('global.edit')" href="{{ route('master.promo.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-promo btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.promo.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('master.promo.show', $r['id']) }}" class="btn btn-sm btn-info" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.promo.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.promo.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($promos) && count($promos) > 0 )
            {!! \App\Util\Base\Layout::paging($promos) !!}
            @endif
        </div>
    </div>
</div>
@stop