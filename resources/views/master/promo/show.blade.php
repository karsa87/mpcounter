@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="card card-solid">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h3 class="d-inline-block d-sm-none">{{ $model->name }}</h3>
                <div class="col-12">
                    <img src="{{ $model->image() }}" class="product-image" alt="Images 1" id="preview-product-image" style="height: 250px;background-position: center center;background-repeat: no-repeat;">
                </div>
                <div class="col-12 product-image-thumbs">
                    @foreach ($model->images_arr() as $i => $image)
                    <div class="product-image-thumb {{ $i == 0 ? 'active' : '' }}">
                        <img src="{{ $image }}" alt="Images {{ $i+1 }}">
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <h3 class="my-3">{{ $model->name }} - {{ $model->code }}</h3>
                <hr>
                <p>{{ $model->information }}</p>

                <hr>
                <h4>@lang('promo.label.period')</h4>
                <p>@lang('promo.label.date_start'): {{ $util->formatDate($model->date_start) }}</p>
                <p>@lang('promo.label.date_end'): {{ $util->formatDate($model->date_end) }}</p>

                <hr>
                <h4>@lang('promo.label.discount')</h4>
                <p><strong>@lang('promo.label.discount_percent'):</strong> {{ $model->discount_percent }}</p>
                <p><strong>@lang('promo.label.discount_price'):</strong> {{ $util->format_currency($model->discount_price) }}</p>

                <hr>
                <h4>@lang('promo.label.category_id')</h4>
                <p>{{ $model->category_id ? $model->category->name : '' }}</p>

                <hr>
                <h4>@lang('promo.label.brand_id')</h4>
                <p>{{ $model->brand_id ? $model->brand->name : '' }}</p>

                <hr>
                <h4>@lang('promo.label.status'): <span class="badge {{ $model->status ? 'badge-success' : 'badge-danger' }}">@lang('promo.list.status.'.$model->status)</span></h4>
            </div>
        </div>
        <div class="mt-4">
            <div class="col-lg-12">
                <a href="{{ route('master.promo.index') }}" class="btn btn-danger btn-lg">
                    @lang('global.back')
                </a>

                @hasPermission('master.promo.edit')
                <a href="{{ route('master.promo.edit', $model->id) }}" class="btn btn-warning btn-lg float-right">
                    @lang('global.edit')
                </a>
                @endhasPermission
            </div>
        </div>
        <div class="row mt-4">
            <nav class="w-100">
                <div class="nav nav-tabs" id="promo-tab" role="tablist">
                    <a class="nav-item nav-link active" id="promo-desc-tab" data-toggle="tab" href="#promo-desc" role="tab" aria-controls="promo-desc" aria-selected="true">@lang('promo.label.product')</a>
                </div>
            </nav>
            <div class="tab-content p-3 w-100" id="nav-tabContent">
                <div class="tab-pane fade show active" id="promo-desc" role="tabpanel" aria-labelledby="promo-desc-tab">
                    @php
                        $products = $model->products;
                    @endphp
                    <table class="table table-head-fixed">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%">@lang('global.no')</th>
                                <th style="width: 20%">@lang('product.label.name')</th>
                                <th style="width: 20%">@lang('product.label.information')</th>
                                <th style="width: 15%">@lang('product.label.sell_price')</th>
                                <th style="width: 20%">@lang('product.label.images')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if( isset($products) && count($products) > 0 )
                            @foreach( $products as $i => $r)
                            <tr>
                                <td class="text-center">{{ $i+1 }}</td>
                                <td class="text-wrap">
                                    <h6 class="mb-1">
                                        {{ $r->name }}
                                    </h6>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.code'):</strong> {{ $r->code }}
                                        </small>
                                    </p>
                                </td>
                                <td class="text-wrap">
                                    <h6 class="mb-1">
                                        {{ $r->information }}
                                    </h6>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.category_id'):</strong> {{ $r->category->name }}
                                        </small>
                                    </p>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('product.label.brand_id'):</strong> {{ $r->brand->name }}
                                        </small>
                                    </p>
                                </td>
                                <td class="text-wrap">
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('global.before'):</strong> 
                                            {{ $util->format_currency($r->sell_price) }}
                                        </small>
                                    </p>
                                    <p class="m-0">
                                        <small>
                                            <strong>@lang('global.after'):</strong> 
                                            @php
                                                $price = $r->sell_price;
                                                $discount_percent = $model->discount_percent;
                                                $discount_price = $model->discount_percent;

                                                if ($discount_percent > 0) {
                                                    $price = $price - ($price * $discount_percent / 100);
                                                } elseif ($discount_price > 0) {
                                                    $price = $price - $discount_price;
                                                }
                                            @endphp
                                            {{ $util->format_currency($price) }}
                                        </small>
                                    </p>
                                </td>
                                <td class="text-wrap">
                                    <div class="row">
                                        @foreach ($r->images_arr() as $i => $image)
                                        <div class="col-sm-4">
                                            <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                            <img src="{{ $image }}" class="img-fluid mb-2" alt="Images {{ $i+1 }}"/>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="7" class="text-center">@lang('global.data_not_available')</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/promo.js') }}?{{ config('app.version') }}"></script>
@endpush