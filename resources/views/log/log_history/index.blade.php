@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('log_history.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'log.log_history.search')
        </div>
    </div>
</div>
<div class="row" id="content-log_history">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('log_history.title')</h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-stripped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th>@lang('log_history.label.log_datetime')</th>
                            <th>@lang('log_history.label.transaction_type')</th>
                            <th>@lang('log_history.label.table')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($logs) && count($logs) > 0 )
                        @foreach( $logs as $i => $r)
                        <tr>
                            <td class="text-center">{{ $logs->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->user->name }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_history.label.information'):</strong> {{ $util->formatDate($r->log_datetime, $util->FORMAT_DATETIME_ID_LONG_DAY) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    @lang('log_history.list.transaction_type.'.$r->transaction_type)
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_history.label.information'):</strong> {{ $r->information }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_history.label.record_id'):</strong> {{ $r->record ? $r->record->defaultName() : $r->record_id }}
                                    </small>
                                </p>
                            </td>
                            <td class="">
                                <h6 class="mb-1">
                                    {{ $r->table }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_history.label.record_type'):</strong> {{ $r->record_type }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <a title="@lang('global.edit')"class="btn btn-sm btn-info" data-toggle="collapse" href="#detail-{{$i}}" class="accordion-toggle">
                                    <i class="fas fa-eye m-0"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" class="hiddenRow">
                                <div id="detail-{{$i}}" class="accordian-body collapse">
                                    <table class="table table-stripped table-hover">
                                        <thead>
                                            <th>@lang('global.column')</th>
                                            <th>@lang('log_history.label.data_before')</th>
                                            <th>@lang('log_history.label.data_change')</th>
                                        </thead>
                                        <tbody>
                                            @php
                                                $data_before = json_decode($r->data_before, TRUE);
                                                if(strtotime($r->log_datetime) < strtotime('2020-05-18')){
                                                    $data_before = json_decode($r->data_after, TRUE);
                                                }
                                            @endphp
                                            @foreach (json_decode($r->data_change, TRUE) as $col => $value)
                                                <tr>
                                                    <td>{{ $col }}</td>
                                                    <td>
                                                        @if (array_key_exists($col, $data_before))
                                                            {!! $data_before[$col] !!}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>{!! $value !!}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($logs) && count($logs) > 0 )
            {!! \App\Util\Base\Layout::paging($logs) !!}
            @endif
        </div>
    </div>
</div>
@stop

@push('css')
    <style>
        .hiddenRow {
            padding: 0 50px !important;
        }
    </style>
@endpush
@push('js')
    {{-- <script type="text/javascript" src="{{ asset('js/admin/finance/log_history.js') }}?{{ config('app.version') }}"></script> --}}
@endpush