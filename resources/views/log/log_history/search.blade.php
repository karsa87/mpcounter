<!-- form start -->
<form role="form" action="{{ route('log.log.history.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="_ri">@lang('log_history.label.record_id')</label>
                    <input type="text" name="_ri" class="form-control" value="{{ request('_ri') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_datetime">@lang('log_history.label.log_datetime')</label>
                    <input type="text" name="search_datetime" class="form-control date-rangepicker" value="{{ request('search_datetime') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_table">@lang('log_history.label.table')</label>
                    <select name="search_table"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($tables as $name)
                        <option value="{{ $name }}" {{ request('search_table') == $name ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_transaction_type">@lang('log_history.label.transaction_type')</label>
                    <select name="search_transaction_type"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('log_history.list.transaction_type') as $name)
                        <option value="{{ $name }}" {{ request('search_transaction_type') == $name ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_user_id">@lang('log_history.label.user_id')</label>
                    <select name="search_user_id"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($users as $id => $name)
                        <option value="{{ $id }}" {{ request('search_user_id') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('log.log.history.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>