@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('log_stock_transaction.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'log.log_stock_transaction.search')
        </div>
    </div>
</div>
<div class="row" id="content-log_stock_transaction">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('log_stock_transaction.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th>@lang('log_stock_transaction.label.user_id')</th>
                            <th>@lang('log_stock_transaction.label.information')</th>
                            <th>@lang('log_stock_transaction.label.product_id')</th>
                            <th class="text-center">@lang('log_stock_transaction.label.stock_out')</th>
                            <th class="text-center">@lang('log_stock_transaction.label.stock_in')</th>
                            <th class="text-center">@lang('log_stock_transaction.label.stock_before')</th>
                            <th class="text-center">@lang('log_stock_transaction.label.stock_after')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($logs) && count($logs) > 0 )
                        @foreach( $logs as $i => $r)
                        <tr>
                            <td class="text-center">{{ $logs->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->user->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        {{ $util->formatDate($r->log_datetime, $util->FORMAT_DATETIME_ID_LONG_DAY) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->information }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_stock_transaction.label.transaction_id'):</strong> {{ $r->transaction_id }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_stock_transaction.label.table'):</strong> {{ $r->table }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $r->stock->product->name }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_stock_transaction.label.branch_id'):</strong> {{ $r->stock->branch->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('log_stock_transaction.label.type'):</strong> @lang('log_stock_transaction.list.type.'.$r->type)
                                    </small>
                                </p>
                            </td>
                            <td class="text-right">
                                {{ $util->format_currency($r->stock_out) }}
                            </td>
                            <td class="text-right">
                                {{ $util->format_currency($r->stock_in) }}
                            </td>
                            <td class="text-right">
                                {{ $util->format_currency($r->stock_before) }}
                            </td>
                            <td class="text-right">
                                {{ $util->format_currency($r->stock_after) }}
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($logs) && count($logs) > 0 )
            {!! \App\Util\Base\Layout::paging($logs) !!}
            @endif
        </div>
    </div>
</div>
@stop