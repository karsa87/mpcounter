<!-- form start -->
<form role="form" action="{{ route('log.log.stock.transaction.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="search_datetime">@lang('log_stock_transaction.label.log_datetime')</label>
                    <input type="text" name="search_datetime" class="form-control date-rangepicker" value="{{ request('search_datetime') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_table">@lang('log_stock_transaction.label.table')</label>
                    <select name="search_table"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($tables as $name)
                        <option value="{{ $name }}" {{ request('search_table') == $name ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_type">@lang('log_stock_transaction.label.type')</label>
                    <select name="search_type"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('log_stock_transaction.list.type') as $name)
                        <option value="{{ $name }}" {{ request('search_type') == $name ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_user_id">@lang('log_stock_transaction.label.user_id')</label>
                    <select name="search_user_id"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($users as $id => $name)
                        <option value="{{ $id }}" {{ request('search_user_id') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_product_id">@lang('log_stock_transaction.label.product_id')</label>
                    <select name="search_product_id"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($products as $id => $name)
                        <option value="{{ $id }}" {{ request('search_product_id') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_branch_id">@lang('log_stock_transaction.label.branch_id')</label>
                    <select name="search_branch_id"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('search_branch_id') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('log.log.stock.transaction.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>