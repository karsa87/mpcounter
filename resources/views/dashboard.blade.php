{{-- @extends('adminlte::page') --}}
@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-info"></i> @lang('dashboard.label.welcome')</h5>
    @lang('dashboard.message.welcome') <strong>{{ auth()->user()->name }}</strong>
</div>

@if ($promos->count() > 0)
<div class="row mb-3">
    <div class="col-md-12">
        <div id="promo-slide" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach ($promos->chunk(4) as $i => $promos)
                <div class="carousel-item {{ $i <= 0 ? 'active' : '' }}">
                    <div class="row">
                    @foreach ($promos as $promo)
                        <div class="col-sm m-1" style="background: #dedede; padding: 10px;">
                            <img class="d-block" src="{{ $promo->image() }}" alt="{{ $promo->name }}" style="width: 200px; height: 200px; margin: auto;">
                        </div>
                    @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#promo-slide" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#promo-slide" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
@endif

<div class="row">
    {{-- <div class="col-sm-12 col-md-6 col-lg-3">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{ $util->format_currency($member->count()) }}</h3>

                <p>@lang('dashboard.label.member')</p>
            </div>
            <div class="icon">
                <i class="fas fa-users"></i>
            </div>
            <div class="small-box-footer">
                <div class="btn-group col-12">
                    <a href="{{ route('transaction.sales.order.create') }}" class="btn btn-success col-6">
                        @lang('dashboard.label.create') 
                        <i class="fas fa-plus"></i>
                    </a>
                    @php
                        $_dt = sprintf('%s 00:00 - %s 23:59', date('Y-01-m'), date('Y-m-t'))
                    @endphp
                    <a href="{{ route('transaction.sales.order.index', ['_dt'=>$_dt]) }}" class="btn btn-info col-6 float-right">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="col-sm-12 col-md-6 col-lg-3">
        <div class="small-box bg-info card-1">
            <div class="inner">
                <h3>{{ $util->format_currency($sales_order_paid->count() + $sales_order_not_paid->count()) }}</h3>

                <p>@lang('dashboard.label.sales_order')</p>
            </div>
            <div class="icon">
                <i class="fas fa-users"></i>
            </div>
            <div class="small-box-footer">
                <div class="btn-group col-12">
                    <a href="{{ route('transaction.sales.order.create') }}" class="btn btn-success col-6">
                        @lang('dashboard.label.create') 
                        <i class="fas fa-plus"></i>
                    </a>
                    @php
                        $_dt = sprintf('%s 00:00 - %s 23:59', date('Y-01-m'), date('Y-m-t'))
                    @endphp
                    <a href="{{ route('transaction.sales.order.index', ['_dt'=>$_dt]) }}" class="btn btn-info col-6 float-right">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-sm-12 col-md-6 col-lg-3">
        <!-- small box -->
        <div class="small-box bg-purple card-2">
            <div class="inner">
                <h3>{{ $util->format_currency($product->count()) }}</h3>

                <p>@lang('dashboard.label.stock_product')</p>
            </div>
            <div class="icon">
                <i class="fas fa-boxes"></i>
            </div>
            <div class="small-box-footer">
                <div class="btn-group col-12">
                    <a href="{{ route('transaction.purchase.order.create') }}" class="btn btn-success col-6">
                        @lang('dashboard.label.create') 
                        <i class="fas fa-plus"></i>
                    </a>
                    <a href="{{ route('master.product.index', ['_sop'=>5, '_stc'=>1]) }}" class="btn btn-info col-6 float-right">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-sm-12 col-md-6 col-lg-3">
        <!-- small box -->
        <div class="small-box bg-success card-3">
            <div class="inner">
                <h3>{{ $util->format_currency($total_income) }}</h3>

                <p>@lang('dashboard.label.income')</p>
            </div>
            <div class="icon">
                <i class="fas fa-money-bill-alt"></i>
            </div>
            <div class="small-box-footer">
                <div class="btn-group col-12">
                    @php
                        $_dt = sprintf('%s 00:00 - %s 23:59', date('Y-01-m'), date('Y-m-t'))
                    @endphp
                    <a href="{{ route('report.income.all.index', ['_dt'=>$_dt]) }}" class="btn btn-info col-12">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-sm-12 col-md-6 col-lg-3">
        <!-- small box -->
        <div class="small-box bg-danger card-4">
            <div class="inner">
                <h3>{{ $util->format_currency($total_expend) }}</h3>

                <p>@lang('dashboard.label.expend')</p>
            </div>
            <div class="icon">
                <i class="fas fa-money-bill-wave"></i>
            </div>

            <div class="small-box-footer">
                <div class="btn-group col-12">
                    @php
                        $_dt = sprintf('%s 00:00 - %s 23:59', date('Y-01-m'), date('Y-m-t'))
                    @endphp
                    <a href="{{ route('report.expend.all.index', ['_dt'=>$_dt]) }}" class="btn btn-info col-12 float-right">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-md-9">
        <!-- AREA CHART -->
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">
                    @lang('dashboard.label.sales_order') @lang('dashboard.label.chart') :
                    {{ $util->formatDate($this_month[0], 'd-m-Y') }} - {{ $util->formatDate($this_month[1], 'd-m-Y') }}
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="chart">
                    <canvas id="profitLoss" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-3">
        <!-- small box -->
        <div class="small-box bg-success card-5">
            <div class="inner">
                <h3>{{ $util->format_currency($sales_order_paid->count()) }}</h3>

                <p>@lang('dashboard.label.sales_order_paid')</p>
            </div>
            <div class="icon">
                <i class="fas fa-shopping-basket"></i>
            </div>
            <div class="small-box-footer">
                <div class="btn-group col-12">
                    <a href="{{ route('transaction.sales.order.create') }}" class="btn btn-success col-6">
                        @lang('dashboard.label.create') 
                        <i class="fas fa-plus"></i>
                    </a>
                    <a href="{{ route('transaction.sales.order.index', ['_sts'=>1]) }}" class="btn btn-info col-6 float-right">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- small box -->
        <div class="small-box bg-danger card-6">
            <div class="inner">
                <h3>{{ $util->format_currency($sales_order_not_paid->count()) }}</h3>

                <p>@lang('dashboard.label.sales_order_not_paid')</p>
            </div>
            <div class="icon">
                <i class="fas fa-shopping-basket"></i>
            </div>

            <div class="small-box-footer">
                <div class="btn-group col-12">
                    <a href="{{ route('transaction.sales.order.index', ['_sts'=>0]) }}" class="btn btn-info col-12 float-right">
                        @lang('dashboard.label.more') 
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('js')
    <script>
        var start_date = new Date("{{ $util->formatDate($this_month[0], 'Y-m-d') }}");
        var end_date = new Date("{{ $util->formatDate($this_month[1], 'Y-m-d') }}");
        var chart_so = JSON.parse('{!! json_encode($chart_so) !!}');
    </script>
    <script type="text/javascript" src="{{ asset('js/admin/dashboard.js') }}?{{ config('app.version') }}"></script>
@endpush

@push('css')
    <style>
        .card-1{
            background-color: #2ba2f2 !important;
        }
        .card-2{
            background-color: #9b5ab6 !important;
        }
        .card-3{
            background-color: #2ecd70 !important;
        }
        .card-4{
            background-color: #e64d3d !important;
        }
        .card-5{
            background-color: #20c576 !important;
        }
        .card-6{
            background-color: #fe5677 !important;
        }
    </style>
@endpush