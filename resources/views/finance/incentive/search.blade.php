<!-- form start -->
<form role="form" id="form-search-incentive" action="{{ route('finance.incentive.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="no_transaction">@lang('incentive.label.no_transaction')</label>
                    <input type="text" name="_ntr" class="form-control" placeholder="@lang('incentive.placeholder.no_transaction')" value="{{ request('_ntr') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="incentive_date">@lang('incentive.label.date_transaction')</label>
                    <input type="text" name="_idt" class="form-control date-rangepicker" placeholder="@lang('incentive.placeholder.date_transaction')" value="{{ request('_idt') }}">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('incentive.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @if (empty(config('default.bank_id')))
            <div class="col-4">
                <div class="form-group">
                    <label for="_bk">@lang('incentive.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif

            <div class="col-4">
                <div class="form-group">
                    <label for="_ins">@lang('incentive.label.investor')</label>
                    <select name="_ins"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($investors as $name)
                        <option value="{{ $name }}" {{ request('_ins') == $name ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_tp">@lang('incentive.label.type')</label>
                    <select name="_tp"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('incentive.list.type') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_tp')) && request('_tp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('finance.incentive.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>