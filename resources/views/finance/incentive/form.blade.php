<!-- form start -->
<form id="form-incentive" role="form" method="POST" action="{{ route('finance.incentive.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="no_transaction">@lang('incentive.label.no_transaction')</label>
            <input type="text" name="no_transaction" class="form-control @classInpError('no_transaction')" placeholder="@lang('incentive.placeholder.no_transaction')" autocomplete="off" value="{{ old('no_transaction') ?: $model->nextNo() }}">
            
            @inpSpanError(['column'=>'no_transaction'])
        </div>
        <div class="form-group">
            <label for="information">@lang('incentive.label.information')</label>
            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('incentive.placeholder.information')" >{{ old('information') }}</textarea>
            
            @inpSpanError(['column'=>'information'])
        </div>
        <div class="form-group">
            <label for="date_transaction">@lang('incentive.label.date_transaction')</label>
            <input type="text" name="date_transaction" class="form-control datepicker @classInpError('date_transaction')" placeholder="@lang('incentive.placeholder.date_transaction')" autocomplete="off" value="{{ old('date_transaction') }}">
            
            @inpSpanError(['column'=>'date_transaction'])
        </div>

        <div class="form-group">
            <label for="form-branch_id" class="control-label">@lang('incentive.label.branch_id')</label>
            <select name="branch_id" id="form-branch_id" class="form-control select2 @classInpError('branch_id')">
                @foreach($branchs as $id => $name)
                    <option value="{{ $id }}" {{ old('branch_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'branch_id'])
        </div>

        @if (empty(config('default.bank_id')))
        <div class="form-group">
            <label for="form-bank_id" class="control-label">@lang('incentive.label.bank_id')</label>
            <select name="bank_id" id="form-bank_id" class="form-control select2 @classInpError('bank_id')" data-placeholder="">
                @foreach($banks as $id => $name)
                    <option value="{{ $id }}" {{ old('bank_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'bank_id'])
        </div>
        @endif

        <div class="form-group">
            <label for="investor">@lang('incentive.label.investor')</label>
            <input type="text" name="investor" class="form-control @classInpError('investor')" placeholder="@lang('incentive.placeholder.investor')" autocomplete="off" value="{{ old('investor') }}">
            
            @inpSpanError(['column'=>'investor'])
        </div>
        <div class="form-group">
            <label for="form-type" class="control-label">@lang('incentive.label.type')</label>
            <span data-toggle="tooltip" data-placement="top" title="@lang('incentive.message.desc_type')">
                <i class="fas fa-question-circle"></i>
            </span>
            <select name="type" id="form-type" class="form-control select2 @classInpError('type')" data-placeholder="">
                @foreach(trans('incentive.list.type') as $id => $name)
                    <option value="{{ $id }}" {{ old('type') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'type'])
        </div>
        <div class="form-group">
            <label for="amount">@lang('incentive.label.amount')</label>
            <input type="text" name="amount" class="form-control format-number @classInpError('amount')" placeholder="@lang('incentive.placeholder.amount')" value="{{ old('amount') }}">
            
            @inpSpanError(['column'=>'amount'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('finance.incentive.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>