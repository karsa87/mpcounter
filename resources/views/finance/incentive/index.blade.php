@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')
@inject('model', 'App\Models\Incentive')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('incentive.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'finance.incentive.search')
        </div>
    </div>
</div>
<div class="row" id="content-incentive">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('incentive.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-incentive">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="no_transaction">@lang('incentive.label.no_transaction')</th>
                            <th>@lang('incentive.label.branch_id')</th>

                            @if (empty(config('default.bank_id')))
                            <th>@lang('incentive.label.bank_id')</th>
                            @endif

                            <th class="sorting-form" data-column="information">@lang('incentive.label.information')</th>
                            <th class="text-center sorting-form" data-column="amount">@lang('incentive.label.amount')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($incentives) && count($incentives) > 0 )
                        @foreach( $incentives as $i => $r)
                        <tr>
                            <td class="text-center">{{ $incentives->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->no_transaction }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('incentive.label.date_transaction'):</strong> {{ $util->formatDate($r->date_transaction) }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>

                            <td class="text-wrap">
                                {{ $r->branch ? $r->branch->name : '' }}
                            </td>

                            @if (empty(config('default.bank_id')))
                            <td class="text-wrap">
                                {{ $r->bank->name }}
                            </td>
                            @endif

                            <td class="text-wrap">
                                {{ $r->information }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('incentive.label.type'):</strong> @lang('incentive.list.type.'.$r->type)
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-right">
                                {{ $util->format_currency($r->amount) }}
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('finance.incentive.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('finance.incentive.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-incentive btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('finance.incentive.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('finance.incentive.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($incentives) && count($incentives) > 0 )
            {!! \App\Util\Base\Layout::paging($incentives) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('incentive.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'finance.incentive.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/finance/incentive.js') }}?{{ config('app.version') }}"></script>
@endpush