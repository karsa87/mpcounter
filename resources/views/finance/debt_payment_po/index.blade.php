@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('debt_payment_po.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'finance.debt_payment_po.search')
        </div>
    </div>
</div>
<div class="row" id="content-expend">
    <div class="col-xs-12 xol-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('debt_payment_po.title')</h3>

                <a href="{{ route('finance.debt.payment.po.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-stripped table-hover sorting-table" data-form="form-search-debt-payment-po">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 15%;" class="sorting-form" data-column="no_debt">@lang('debt_payment_po.label.no_debt')</th>
                            <th style="width: 25%;">@lang('debt_payment_po.label.purchase_order_id')</th>
                            <th style="width: 20%;" class="sorting-form" data-column="date">@lang('debt_payment_po.label.information')</th>
                            <th style="width: 30%;" class="text-center">@lang('debt_payment_po.label.images')</th>
                            <th style="width: 5%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($debtPayments) && count($debtPayments) > 0 )
                        @foreach( $debtPayments as $i => $r)
                        <tr>
                            <td class="text-center">{{ $debtPayments->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->no_debt }}
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->purchaseOrder->no_invoice }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.supplier_id'):</strong> {{ $r->purchaseOrder->supplier->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('purchase_order.label.date'):</strong> {{ $util->formatDate($r->purchaseOrder->date) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $util->formatDate($r->date) }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('debt_payment_po.label.bank_id'):</strong> {{ $r->bank->name }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('debt_payment_po.label.paid'):</strong> {{ $util->format_currency($r->paid) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-center">
                                <div class="row">
                                    @foreach ($r->images_arr(3) as $i => $image)
                                    <div class="col-sm-6">
                                        <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                        <img src="{{ $image }}" class="img-fluid mb-2" alt="Images {{ $i+1 }}"/>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('finance.debt.payment.po.edit')
                                    <a title="@lang('global.edit')" href="{{ route('finance.debt.payment.po.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-expend btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('finance.debt.payment.po.show')
                                    <a title="@lang('global.show')" href="{{ route('finance.debt.payment.po.show', $r['id']) }}" class="btn btn-sm btn-info btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('finance.debt.payment.po.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('finance.debt.payment.po.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($debtPayments) && count($debtPayments) > 0 )
            {!! \App\Util\Base\Layout::paging($debtPayments) !!}
            @endif
        </div>
    </div>
</div>
@stop