<!-- form start -->
<form role="form" id="form-search-debt-payment-po" action="{{ route('finance.debt.payment.po.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="no_debt">@lang('debt_payment_po.label.no_debt')</label>
                    <input type="text" name="_ndb" class="form-control" placeholder="@lang('debt_payment_po.placeholder.no_debt')" value="{{ request('_ndb') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="date">@lang('debt_payment_po.label.date')</label>
                    <input type="text" name="_dt" class="form-control date-rangepicker" placeholder="@lang('debt_payment_po.placeholder.date')" value="{{ request('_dt') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_bk">@lang('debt_payment_po.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_poid">@lang('debt_payment_po.label.purchase_order_id')</label>
                    <select name="_poid"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($purchaseOrders as $id => $name)
                        <option value="{{ $id }}" {{ request('_poid') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('finance.debt.payment.po.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>