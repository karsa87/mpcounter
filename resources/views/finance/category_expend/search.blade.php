<!-- form start -->
<form role="form" id="form-search-category-ex" action="{{ route('finance.category.expend.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('category_expend.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('category_expend.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_sts">@lang('category_expend.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('category_expend.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('finance.category.expend.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>