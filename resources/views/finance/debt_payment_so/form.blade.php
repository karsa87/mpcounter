@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<!-- form start -->
<form 
    id="form-debt-payment" role="form" method="POST" 
    action="{{ $model->exists ? route('finance.debt.payment.so.update', $model->id) : route('finance.debt.payment.so.store') }}">
@if ($model->exists)
    <input type="hidden" name="_method" value="PUT">
@endif
    @csrf
    <input type="hidden" name="id" value="{{ old('id') ?? $model->id }}">
<div class="row">
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('debt_payment_so.title')</h3>
            </div>

            <div class="card-body">
                <div class="form-group">
                    <label for="no_debt">@lang('debt_payment_so.label.no_debt')</label>
                    <input type="text" name="no_debt" readonly class="form-control @classInpError('no_debt')" placeholder="@lang('debt_payment_so.placeholder.no_debt')" value="{{ old('no_debt') ?? $model->nextNoDebtSO() }}">
                    
                    @inpSpanError(['column'=>'no_debt'])
                </div>
                <div class="form-group">
                    <label for="date">@lang('debt_payment_so.label.date')</label>
                    <input type="text" name="date" class="form-control datetimepicker @classInpError('date')" placeholder="@lang('debt_payment_so.placeholder.date')" autocomplete="off" value="{{ old('date') ?? $util->formatDate(($model->date ?? date('Y-m-d H:i')), 'Y-m-d H:i') }}">
                    
                    @inpSpanError(['column'=>'date'])
                </div>
                <div class="form-group">
                    @php
                        $soid = request('_soid');
                        if (empty($soid)) {
                            $selected = old('purchase_order_id') ?? $model->purchase_order_id;
                        } else {
                            $selected = $soid;
                        }
                    @endphp

                    <label for="form-sales_order_id" class="control-label">@lang('debt_payment_so.label.sales_order_id')</label>
                    <select name="sales_order_id" id="form-sales_order_id" class="form-control select2 @classInpError('sales_order_id')" data-placeholder="">
                        @foreach($salesOrders as $id => $name)
                            <option value="{{ $id }}" {{ $selected == $id ? "selected"  : ""}} >
                                {{ $name }}
                            </option>
                        @endforeach
                    </select>

                    @inpSpanError(['column'=>'sales_order_id'])
                </div>
                <div class="form-group">
                    <label for="form-bank_id" class="control-label">@lang('debt_payment_so.label.bank_id')</label>
                    <input type="hidden" name="bank_id" value="{{ old('bank_id') ?? $model->bank_id }}">
                    <br>
                    <label id="form-bank_id" class="control-label">@lang('debt_payment_so.placeholder.bank_id')</label>
                    @inpSpanError(['column'=>'bank_id'])
                </div>
                <div class="form-group">
                    <label for="form-remaining_debt" class="control-label">@lang('debt_payment_so.label.remaining_debt')</label>
                    <input type="hidden" name="remaining_debt" value="{{ old('remaining_debt') ?? $model->remaining_debt }}">
                    <br>
                    <label id="form-remaining_debt" class="control-label">@lang('debt_payment_so.placeholder.remaining_debt')</label>
                    
                    @inpSpanError(['column'=>'remaining_debt'])
                </div>
                <div class="form-group">
                    <label for="paid">@lang('debt_payment_so.label.paid')</label>
                    <input type="text" name="paid" class="form-control format-number @classInpError('paid')" placeholder="@lang('debt_payment_so.placeholder.paid')" value="{{ old('paid') ?? $model->paid }}">
                    
                    @inpSpanError(['column'=>'paid'])
                </div>
                <div class="form-group">
                    <label for="information">@lang('debt_payment_so.label.information')</label>
                    <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('debt_payment_so.placeholder.information')" >{{ old('information') ?? $model->information }}</textarea>
                    
                    @inpSpanError(['column'=>'information'])
                </div>
            </div>
        </div>
    </div> 
    <!-- /.card-body -->

    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('debt_payment_so.label.images')</h3>
            </div>
            <div class="card-body">
                <input type="hidden" name="images" id="result-upload" value="{{ old('images') ?? $model->images }}">
                <div class="dropzone" id="files-upload" data-href="{{ route('finance.debt.payment.so.upload') }}" data-thumbnail="{{ asset('img/icon-csv.png') }}"></div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('finance.debt.payment.so.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
            </div>
        </div>
    </div>
</form>
@stop

@push('js')
    <script type="text/javascript">
        var salesOrders = JSON.parse('{!! $salesOrders_all->toJson() !!}');
    </script>
    <script type="text/javascript" src="{{ asset('js/admin/finance/debt_payment_so.js') }}?{{ config('app.version') }}"></script>
@endpush