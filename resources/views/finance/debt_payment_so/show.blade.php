@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="card card-solid">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h3 class="d-inline-block d-sm-none">{{ $model->name }}</h3>
                <div class="col-12">
                    <img src="{{ $model->image() }}" class="product-image" alt="Images 1" id="preview-product-image" style="height: 250px;background-position: center center;background-repeat: no-repeat;">
                </div>
                <div class="col-12 product-image-thumbs">
                    @foreach ($model->images_arr() as $i => $image)
                    <div class="product-image-thumb {{ $i == 0 ? 'active' : '' }}">
                        <img src="{{ $image }}" alt="Images {{ $i+1 }}">
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <h3 class="my-3">{{ $model->no_debt }}</h3>
                <hr>
                <p>{{ $model->information }}</p>

                <hr>
                <h4>@lang('debt_payment_so.label.date')</h4>
                <p>{{ $util->formatDate($model->date) }}</p>

                <hr>
                <h4>@lang('debt_payment_so.label.bank_id')</h4>
                <p>{{ $model->bank_id ? $model->bank->name : '' }}</p>

                <hr>
                <h4>@lang('debt_payment_so.label.sales_order_id')</h4>
                <p>{{ $model->sales_order_id ? $model->salesOrder->no_invoice : '' }}</p>
                
                @php
                    $another_paid = $model->sales_order_id ? $model->salesOrder->debtPayments->sum('paid') : 0;
                    $total_paid = $another_paid + ($model->sales_order_id ? $model->salesOrder->paid_off : 0);
                    
                    $total_amount = $model->sales_order_id ? $model->salesOrder->total_amount : 0;
                    
                    $remaining_debt = $total_amount - $total_paid - $model->paid;
                @endphp
                <div class="bg-gray p-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('sales_order.label.total_amount')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($total_amount) }}
                            </span>
                        </p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('debt_payment_po.label.total_paid')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($total_paid) }}
                            </span>
                        </p>
                    </div>
                    <div class="bg-green d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('debt_payment_po.label.paid')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($model->paid) }}
                            </span>
                        </p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="text-lg m-2">
                            @lang('debt_payment_po.label.remaining_debt')
                        </p>
                        <p class="d-flex flex-column text-right m-2">
                            <span class="font-weight-bold">
                                <i class="ion ion-android-arrow-up text-success"></i> 
                                {{ $util->format_currency($remaining_debt) }}
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-4">
            <div class="col-lg-12">
                <a href="{{ route('finance.debt.payment.so.index') }}" class="btn btn-danger btn-lg">
                    @lang('global.back')
                </a>

                @hasPermission('finance.debt.payment.so.edit')
                <a href="{{ route('finance.debt.payment.so.edit', $model->id) }}" class="btn btn-warning btn-lg float-right">
                    @lang('global.edit')
                </a>
                @endhasPermission
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/finance/debt_payment_so.js') }}?{{ config('app.version') }}"></script>
@endpush