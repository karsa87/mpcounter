@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')
@inject('model', 'App\Models\Expend')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('expend.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'finance.expend.search')
        </div>
    </div>
</div>
<div class="row" id="content-expend">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('expend.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-expend">
                    <thead>
                        <tr>
                            <th style="width: 10%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 10%;" class="sorting-form" data-column="no_transaction">@lang('expend.label.no_transaction')</th>
                            <th style="width: 10%;" class="sorting-form" data-column="date">@lang('expend.label.date')</th>
                            <th style="width: 25%;">@lang('expend.label.category_expend_id')</th>
                            <th style="width: 25%;" class="sorting-form" data-column="information">@lang('expend.label.information')</th>
                            <th style="width: 30%;" class="text-center">@lang('expend.label.images')</th>
                            <th style="width: 10%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($expends) && count($expends) > 0 )
                        @foreach( $expends as $i => $r)
                        <tr>
                            <td class="text-center">{{ $expends->firstItem() + $i }}</td>
                            <td class="text-wrap">{{ $r->no_transaction }}</td>
                            <td class="text-wrap">{{ $util->formatDate($r->expend_date) }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->categoryExpend->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('expend.label.employee_id'):</strong> {{ $r->employee_id ? $r->employee->name : '' }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                {{ $r->information }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('expend.label.branch_id'):</strong> {{ $r->branch ? $r->branch->name : '' }}
                                    </small>
                                </p>
                                @if (empty(config('default.bank_id')))
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('expend.label.bank_id'):</strong> {{ $r->bank->name }}
                                    </small>
                                </p>
                                @endif
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('expend.label.amount'):</strong> {{ $util->format_currency($r->amount) }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-wrap text-center">
                                <div class="row">
                                    @foreach ($r->images_arr(2) as $i => $image)
                                    <div class="col-sm-6">
                                        <a href="{{ $image }}" data-toggle="lightbox" data-title="Images {{ $i+1 }}" data-gallery="gallery">
                                            <img src="{{ $image }}" class="img-fluid mb-2" alt="Images {{ $i+1 }}"/>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('finance.expend.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('finance.expend.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-expend btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('finance.expend.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('finance.expend.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($expends) && count($expends) > 0 )
            {!! \App\Util\Base\Layout::paging($expends) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('expend.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'finance.expend.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/finance/expend.js') }}?{{ config('app.version') }}"></script>
@endpush