<!-- form start -->
<form role="form" id="form-search-expend" action="{{ route('finance.expend.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="no_transaction">@lang('expend.label.no_transaction')</label>
                    <input type="text" name="_ntr" class="form-control" placeholder="@lang('expend.placeholder.no_transaction')" value="{{ request('_ntr') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="expend_date">@lang('expend.label.expend_date')</label>
                    <input type="text" name="_exdt" class="form-control date-rangepicker" placeholder="@lang('expend.placeholder.expend_date')" value="{{ request('_exdt') }}">
                </div>
            </div>

            <div class="col-4">
                <div class="form-group">
                    <label for="_bc">@lang('expend.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}" {{ request('_bc') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @if (empty(config('default.bank_id')))
            <div class="col-4">
                <div class="form-group">
                    <label for="_bk">@lang('expend.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}" {{ request('_bk') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif

            <div class="col-4">
                <div class="form-group">
                    <label for="_crgex">@lang('expend.label.category_expend_id')</label>
                    <select name="_crgex"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($category_expends as $id => $name)
                        <option value="{{ $id }}" {{ request('_crgex') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_emp">@lang('expend.label.employee_id')</label>
                    <select name="_emp"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($employes as $id => $name)
                        <option value="{{ $id }}" {{ request('_emp') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('finance.expend.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>