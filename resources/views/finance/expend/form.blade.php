<!-- form start -->
<form id="form-expend" role="form" method="POST" action="{{ route('finance.expend.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="no_transaction">@lang('expend.label.no_transaction')</label>
            <input type="text" name="no_transaction" class="form-control @classInpError('no_transaction')" placeholder="@lang('expend.placeholder.no_transaction')" autocomplete="off" value="{{ old('no_transaction') ?? $model->nextNo() }}">
            
            @inpSpanError(['column'=>'no_transaction'])
        </div>
        <div class="form-group">
            <label for="information">@lang('expend.label.information')</label>
            <textarea name="information" class="form-control @classInpError('information')" rows="3" placeholder="@lang('expend.placeholder.information')" >{{ old('information') }}</textarea>
            
            @inpSpanError(['column'=>'information'])
        </div>
        <div class="form-group">
            <label for="expend_date">@lang('expend.label.expend_date')</label>
            <input type="text" name="expend_date" class="form-control datetimepicker @classInpError('expend_date')" placeholder="@lang('expend.placeholder.expend_date')" autocomplete="off" value="{{ old('expend_date') }}">
            
            @inpSpanError(['column'=>'expend_date'])
        </div>

        <div class="form-group">
            <label for="form-branch_id" class="control-label">@lang('expend.label.branch_id')</label>
            <select name="branch_id" id="form-branch_id" class="form-control select2 @classInpError('branch_id')">
                @foreach($branchs as $id => $name)
                    <option value="{{ $id }}" {{ old('branch_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'branch_id'])
        </div>

        @if (empty(config('default.bank_id')))
        <div class="form-group">
            <label for="form-bank_id" class="control-label">@lang('expend.label.bank_id')</label>
            <select name="bank_id" id="form-bank_id" class="form-control select2 @classInpError('bank_id')" data-placeholder="">
                @foreach($banks as $id => $name)
                    <option value="{{ $id }}" {{ old('bank_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'bank_id'])
        </div>
        @endif

        <div class="form-group">
            <label for="form-category_expend_id" class="control-label">@lang('expend.label.category_expend_id')</label>
            <select name="category_expend_id" id="form-category_expend_id" class="form-control select2 @classInpError('category_expend_id')" data-placeholder="">
                @foreach($category_expends as $id => $name)
                    <option value="{{ $id }}" {{ old('category_expend_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'category_expend_id'])
        </div>

        @if (empty(config('default.employee_id')))
        <div class="form-group">
            <label for="form-employee_id" class="control-label">@lang('expend.label.employee_id')</label>
            <select name="employee_id" id="form-employee_id" class="form-control select2 @classInpError('employee_id')" data-placeholder="">
                @foreach($employes as $id => $name)
                    <option value="{{ $id }}" {{ old('employee_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'employee_id'])
        </div>
        @endif

        <div class="form-group">
            <label for="amount">@lang('expend.label.amount')</label>
            <input type="text" name="amount" class="form-control format-number @classInpError('amount')" placeholder="@lang('expend.placeholder.amount')" value="{{ old('amount') }}">
            
            @inpSpanError(['column'=>'amount'])
        </div>
        <div>
            <label for="form-drop_files" class="control-label">@lang('expend.label.images')</label>
            <input type="hidden" name="images" id="result-upload" value="{{ old('images') }}">
            <div class="dropzone" id="files-upload" data-href="{{ route('finance.expend.upload') }}" data-thumbnail="{{ asset('img/icon-csv.png') }}"></div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('finance.expend.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>