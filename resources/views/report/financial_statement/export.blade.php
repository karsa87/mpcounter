@php
    use App\Util\Helpers\Util;
@endphp

<html>
    <head>
        <title>@lang('global.report') @lang('financial_statement.title')</title>
    </head>
    <body>
        <h1><b><center>@lang('global.report') @lang('financial_statement.title')</center></b></h1>
        <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td>@lang('financial_statement.label.period')</td>
                <td>:</td>
                <td>@lang('global.array.months.'.$month), {{ $year }}</td>
            </tr>
            <tr>
                <td>@lang('financial_statement.label.print_date')</td>
                <td>:</td>
                <td><?= Util::formatDate(date('Y-m-d')) ?></td>
            </tr>
            <tr>
                <td>@lang('financial_statement.label.bank_id')</td>
                <td>:</td>
                <td>{{ $bank_name }}</td>
            </tr>
        </table>
        
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.sales_order')</b></h1>
        @php
            $total_so_paid = $salesOrdersPaid->sum('total_amount_first');
            $total_so_not_paid = 0;

            foreach ($salesOrdersNotPaid as $so) {
                $paid = $so->debtPayments->sum('paid');
                $total_paid = $paid + $so->paid_off;
                $total_so_not_paid += ($so->total_amount_first - $total_paid);
                $total_so_paid += $total_paid;
            }
            
            $total_so_all = $total_so_paid+$total_so_not_paid;
            $total_so_after = $total_so_paid-$total_so_not_paid;
            $no = 1;
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_so_first')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_so_all) }}</td>
            </tr>
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_so_not_paid')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_so_not_paid) }}</td>
            </tr>
            <tr>
                <td colspan="2"><h3><b>@lang('financial_statement.label.total_so') (+)</b></h3></td>
                <td> : Rp. </td>
                <td align="right"><b>{{ Util::format_currency($total_so_after) }}</b></td>
            </tr>
            </table>
        </div>

        {{-- START DETAIL ADD COST --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.add_cost')</b></h1>
        @php
            $total_shipping_cost = $salesOrdersPaid->sum('shipping_cost');
            $total_additional_cost = $salesOrdersPaid->sum('additional_cost');
            $total_add_cost = $total_shipping_cost + $total_additional_cost;
        @endphp

        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_shipping_cost')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_shipping_cost) }}</td>
            </tr>
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_additional_cost')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_additional_cost) }}</td>
            </tr>
            <tr>
                <td colspan="2"><h3><b>@lang('financial_statement.label.total_add_cost') (+)</b></h3></td>
                <td> : Rp. </td>
                <td align="right"><b>{{ Util::format_currency($total_add_cost) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL ADD COST --}}

        {{-- START DETAIL RETURN PURCHASE ORDER --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.return_po')</b></h1>
        @php
            $total_return_po = $returPurchaseOrders->sum('total_amount');
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.total_return_po') (+)</b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_return_po) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL RETURN SALES ORDER --}}

        {{-- START DETAIL DEBT PAYMENT SALES ORDER --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.dso')</b></h1>
        @php
            $total_dso = $debtPaymentSO->sum('amount');
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.dso') (+)</b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_dso) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL DEBT PAYMENT SALES ORDER --}}

        {{-- START DETAIL INCENTIVE INCREASE --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.incentive')</b></h1>
        @php
            $total_incentive_increase = $incentiveIncrease->sum('amount');
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.incentive') @lang('financial_statement.label.increase') (+)</b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_incentive_increase) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL INCENTIVE INCREASE --}}

        {{-- START DETAIL PULSE --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.pulse')</b></h1>
        @php
            $total_pulse = $pulse->sum('sell_price');
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.pulse') (+) </b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_pulse) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL PULSE --}}


        {{-- START DETAIL DISCOUNT --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.discount')</b></h1>
        @php
            // $total_discount_promo = $salesOrdersPaid->sum('discount_promo');
            $total_discount_sales = $salesOrdersPaid->sum('discount_sales');
            // $total_discount_detail = $salesOrdersPaid->sum('total_discount_detail');
            // $total_discount = $total_discount_promo + $total_discount_sales + $total_discount_detail;
            $total_discount = $total_discount_sales;
        @endphp

        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            {{-- 
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_discount_promo')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_discount_promo) }}</td>
            </tr>
            --}}
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_discount_sales')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_discount_sales) }}</td>
            </tr>
            {{-- 
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_discount_detail')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_discount_detail) }}</td>
            </tr>
            --}}
            <tr>
                <td colspan="2"><h3><b>@lang('financial_statement.label.total_discount') (-)</b></h3></td>
                <td> : Rp. </td>
                <td align="right"><b>{{ Util::format_currency($total_discount) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL DISCOUNT --}}

        {{-- START DETAIL PURCHASE ORDER --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.purchase_order')</b></h1>
        @php
            $total_po_paid = $purchaseOrdersPaid->sum('total_amount');
            $total_po_not_paid = 0;

            foreach ($purchaseOrdersNotPaid as $po) {
                $paid = $po->debtPayments->sum('paid');
                $total_paid = $paid + $po->paid_off;
                $total_po_not_paid += ($po->total_amount - $total_paid);
                $total_po_paid += $total_paid;
            }
            
            $total_po_all = $total_po_paid+$total_po_not_paid;
            $total_po_after = $total_po_paid-$total_po_not_paid;
            $no = 1;
        @endphp

        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_po_first')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_po_all) }}</td>
            </tr>
            <tr>
                <td width="5%">-</td>
                <td width="25%">@lang('financial_statement.label.total_po_not_paid')</td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right">{{ Util::format_currency($total_po_not_paid) }}</td>
            </tr>
            <tr>
                <td colspan="2"><h3><b>@lang('financial_statement.label.total_po') (-)</b></h3></td>
                <td> : Rp. </td>
                <td align="right"><b>{{ Util::format_currency($total_po_after) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL PURCHASE ORDER --}}

        {{-- START DETAIL RETURN SALES ORDER --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.return_so')</b></h1>
        @php
            $total_return_so_first = $returSalesOrders->sum('total_amount_first');
            $total_return_so_discount = $returSalesOrders->sum('total_discount_detail');
            $total_return_so = $total_return_so_first - $total_return_so_discount;
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.total_return_so') (-)</b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_return_so) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL RETURN SALES ORDER --}}

        {{-- START DETAIL DEBT PAYMENT PURCHASE ORDER --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.dpo')</b></h1>
        @php
            $total_dpo = $debtPaymentPO->sum('amount');
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.dpo') (-)</b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_dpo) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL DEBT PAYMENT PURCHASE ORDER --}}

        {{-- START DETAIL INCENTIVE DECREASE --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.incentive')</b></h1>
        @php
            $total_incentive_decrease = $incentiveDecrease->sum('amount');
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td width="30%"><h3><b>@lang('financial_statement.label.incentive') @lang('financial_statement.label.decrease') (-) </b></h3></td>
                <td width="10%"> : Rp. </td>
                <td width="20%" align="right"><b>{{ Util::format_currency($total_incentive_decrease) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL INCENTIVE DECREASE --}}

        {{-- START DETAIL EXPEND --}}
        @php
            $total_expend = 0;
        @endphp
        {{-- 
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('financial_statement.label.expend')</b></h1>

        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            @foreach ($expends as $expend)
                @php
                    $total = $expend->sum('amount');
                    $total_expend += $total;
                @endphp
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">{{ $expend->categoryExpend->name }}</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($total) }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="2"><h3><b>@lang('financial_statement.label.total_expend') (-)</b></h3></td>
                <td> : Rp. </td>
                <td align="right"><b>{{ Util::format_currency($total_expend) }}</b></td>
            </tr>
            </table>
        </div>
        --}}
        {{-- END DETAIL EXPEND --}}

        @php
            $total_income = $total_so_after + $total_return_po + $total_dso + $total_incentive_increase + $total_add_cost;
            $total_expend = $total_discount + $total_po_after + $total_return_so + $total_dpo + $total_incentive_decrease + $total_expend;

            $total_all = $total_income - $total_expend;
        @endphp
        
        <hr style="margin-bottom: 1px;">
        <hr style="height: 2px; background-color: black; margin: 0px">
        <center><h1><b>@lang('financial_statement.label.current_balance') @lang('global.array.months.'.$month), {{ $year }}</b></h1></center>
        <center><h1><b>@lang('financial_statement.label.bank_id') {{ $bank_name }}</b></h1></center>
        <center><h1><b>Rp. {{ Util::format_currency($total_all) }}</b></h1></center>
    </body>
</html>
        