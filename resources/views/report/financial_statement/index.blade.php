@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" action="{{ route('report.financial.statement.index') }}">
<input type="hidden" name="ex" value="1" />
<div class="row">
    <div class="col-lg-6">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.report') @lang('financial_statement.title')</h3>
            </div>
            <!-- form start -->

            <div class="card-body">
                <div class="form-group">
                    <label for="period">@lang('financial_statement.label.period')</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend col-6">
                            <select name="_m"  class="form-control" style="width: 100%;">
                                @foreach (trans('global.array.months') as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /btn-group -->
                        @php
                            $years = array_combine(range(date("Y"), 2000), range(date("Y"), 2000));
                        @endphp
                        <div class="input-group-prepend col-6">
                            <select name="_y"  class="form-control" style="width: 100%;">
                                <option value="">@lang('global.all')</option>
                                @foreach ($years as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="_bc">@lang('expend_all.label.branch_id')</label>
                    <select name="_bc"  class="form-control" style="width: 100%;">
                        <option value="">@lang('global.all')</option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                @if (empty(config('default.bank_id')))
                <div class="form-group">
                    <label for="_bk">@lang('financial_statement.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" style="width: 100%;" data-placeholder="">
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('report.financial.statement.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right">
                    @lang('global.export')
                </button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/report/financial_statement.js') }}?{{ config('app.version') }}"></script>
@endpush