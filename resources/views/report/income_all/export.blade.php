@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('income_all.label.no_transaction')</b></th>
        <th><b>@lang('income_all.label.income_type')</b></th>
        <th><b>@lang('income_all.label.date')</b></th>
        <th><b>@lang('income_all.label.information')</b></th>
        <th><b>@lang('income_all.label.amount')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($salesOrders as $salesOrder)
        <tr>
            <td style="font-weight: bold;">{{ $salesOrder->no_invoice }}</td>
            <td style="font-weight: bold;">@lang('income_all.list.income_type.1')</td>
            <td style="font-weight: bold;">{{ Util::formatDate($salesOrder->date, 'd/m/Y H:i') }}</td>
            <td style="font-weight: bold;">
                {{ $salesOrder->information }}
                @php
                    $info = "";
                    if ($salesOrder->discount_sales) {
                        $info .= trans('sales_order.label.discount_sales') . ' (-)' . Util::format_currency($salesOrder->discount_sales);
                    }

                    if ($salesOrder->shipping_cost) {
                        $info .= ' ; '. trans('sales_order.label.shipping_cost') . ' (+)' . Util::format_currency($salesOrder->shipping_cost);
                    }

                    if ($salesOrder->additional_cost) {
                        $info .= ' ; '. trans('sales_order.label.additional_cost') . ' (+)' . Util::format_currency($salesOrder->additional_cost);
                    }

                    echo $info;
                @endphp
            </td>
            <td style="font-weight: bold;" data-format="#,##0_-">{{ $salesOrder->paid_off }}</td>
        </tr>

        @foreach ($salesOrder->details as $detail)
        <tr>
            <td></td>
            <td>{{ $detail->product->name }}</td>
            <td align="left">{{ (string) $detail->productIdentity->identity }}</td>
            <td align="center">x{{ $detail->qty }}</td>
            <td data-format="#,##0_-">{{ $detail->total_amount }}</td>
        </tr>
        @endforeach
    @endforeach

    @if ($debtPaymentSo->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($debtPaymentSo as $debt)
        <tr>
            <td>{{ $debt->no_debt }}</td>
            <td>@lang('income_all.list.income_type.2')</td>
            <td>{{ Util::formatDate($debt->date, 'd/m/Y H:i') }}</td>
            <td>{{ $debt->information }}</td>
            <td data-format="#,##0_-">{{ $debt->paid }}</td>
        </tr>
    @endforeach

    @if ($returPO->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($returPO as $rpo)
        <tr>
            <td>{{ $rpo->no_retur }}</td>
            <td>@lang('income_all.list.income_type.3')</td>
            <td>{{ Util::formatDate($rpo->date, 'd/m/Y H:i') }}</td>
            <td>{{ $rpo->information }}</td>
            <td data-format="#,##0_-">{{ $rpo->total_amount }}</td>
        </tr>
    @endforeach

    @if ($incentiveIncome->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($incentiveIncome as $incentive)
        <tr>
            <td>{{ $incentive->no_transaction }}</td>
            <td>@lang('income_all.list.income_type.4')</td>
            <td>{{ Util::formatDate($incentive->date_transaction, 'd/m/Y H:i') }}</td>
            <td>{{ $incentive->information }}</td>
            <td data-format="#,##0_-">{{ $incentive->amount }}</td>
        </tr>
    @endforeach

    @if ($pulse->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($pulse as $pl)
        <tr>
            <td>{{ $pl->no_transaction }}</td>
            <td>@lang('income_all.list.income_type.5')</td>
            <td>{{ Util::formatDate($pl->date, 'd/m/Y H:i') }}</td>
            <td>{{ $pl->information }}</td>
            <td data-format="#,##0_-">{{ $pl->sell_price }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4"><b>@lang('income_all.label.total')</b></td>
        <td data-format="#,##0_-">{{ $total_income_all }}</td>
    </tr>
    </tbody>
</table>