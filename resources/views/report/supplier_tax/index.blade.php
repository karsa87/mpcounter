@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" action="{{ route('report.supplier.tax.index') }}">
<input type="hidden" name="ex" value="1" />
<div class="row">
    <div class="col-lg-6">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.report') @lang('purchase_order.title')</h3>
            </div>
            <!-- form start -->

            <div class="card-body">
                <div class="form-group">
                    <label for="date">@lang('purchase_order.label.date')</label>
                    <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('purchase_order.placeholder.date')" autocomplete="off">
                </div>

                @if (empty(config('default.bank_id')))
                <div class="form-group">
                    <label for="_bk">@lang('purchase_order.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif

                <div class="form-group">
                    <label for="_bc">@lang('purchase_order.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_sp">@lang('purchase_order.label.supplier_id')</label>
                    <select name="_sp"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($suppliers as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('report.supplier.tax.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right">
                    @lang('global.export')
                </button>
            </div>
        </div>
    </div>
</div>
</form>
@stop