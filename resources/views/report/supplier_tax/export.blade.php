@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('product.label.code')</b></th>
        <th><b>@lang('product.label.name')</b></th>
        <th><b>@lang('product.label.branch_id')</b></th>
        <th><b>@lang('product.label.category_id')</b></th>
        <th><b>@lang('product.label.brand_id')</b></th>
        <th><b>@lang('product.label.product_identity')</b></th>
        <th><b>@lang('product.label.gross_profit')</b></th>
        <th><b>@lang('product.label.net_profit')</b></th>
        <th><b>@lang('product.label.tax')</b></th>
    </tr>
    </thead>
    <tbody>
        @php
            $total_tax = 0;
        @endphp
    @foreach($products as $row)
    @php
        $productIdentity = $row->productIdentity;
    @endphp
        <tr>
            <td>{{ $productIdentity->product->code }}</td>
            <td>{{ $productIdentity->product->name }}</td>
            <td>{{ $productIdentity->branch->name }}</td>
            <td>{{ $productIdentity->product->category->name }}</td>
            <td>{{ $productIdentity->product->brand->name }}</td>
            <td>{{ $productIdentity->identity }}</td>
            <td data-format="#,##0_-">{{ $row->gross_profit }}</td>
            @php
                $net_profit = $row->gross_profit - $row->capital_price;
                $tax = $net_profit * 25 / 100;
                $total_tax += $tax;
            @endphp
            <td data-format="#,##0_-">{{ $net_profit }}</td>
            <td data-format="#,##0_-">{{ $tax }}</td>
        </tr>
    @endforeach
    </tbody>
    <thead>
        <tr>
            <td colspan="8"><b>@lang('global.total')</b></td>
            <td data-format="#,##0_-">{{ $total_tax }}</td>
        </tr>
    </thead>
</table>