@php
    use App\Util\Helpers\Util;
@endphp

<html>
    <head>
        <title>@lang('loss_profit.title') @lang('global.report')</title>
    </head>
    <body>
        <h1><b><center>@lang('loss_profit.title') @lang('global.report')</center></b></h1>
        <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            <tr>
                <td>@lang('loss_profit.label.period')</td>
                <td>:</td>
                <td>@lang('global.array.months.'.$month), {{ $year }}</td>
            </tr>
            <tr>
                <td>@lang('loss_profit.label.print_date')</td>
                <td>:</td>
                <td><?= Util::formatDate(date('Y-m-d')) ?></td>
            </tr>
            <tr>
                <td>@lang('loss_profit.label.bank_id')</td>
                <td>:</td>
                <td>{{ $bank_name }}</td>
            </tr>
            <tr>
                <td>@lang('loss_profit.label.branch_id')</td>
                <td>:</td>
                <td>{{ $branch_name }}</td>
            </tr>
        </table>
        
        {{-- START DETAIL OMSET --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('loss_profit.label.sales_order')</b></h1>
        @php
            $total_so_omset = 0;
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
                @foreach ($sodOmset as $category => $so)
                @php
                    $total = $so->sum('amount');
                    $total_so_omset += $total;
                @endphp
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">{{ $category }}</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($total) }}</td>
                </tr>
                @endforeach
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">@lang('pulse.title')</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($pulse->sum('sell_price')) }}</td>
                </tr>
                <tr>
                    <td colspan="2"><h3><b>@lang('loss_profit.label.total_so')</b></h3></td>
                    <td> : Rp. </td>
                    <td align="right"><b>{{ Util::format_currency($total_so_omset) }}</b></td>
                </tr>
            </table>
        </div>
        {{-- END DETAIL OMSET --}}

        {{-- START DETAIL LOSS PROFIT --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('loss_profit.label.gross_profit')</b></h1>
        @php
            $total_so_gross_profit = 0;
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
                @foreach ($sodLP as $category => $so)
                @php
                    $total = $so->sum('amount');
                    $total_so_gross_profit += $total;
                @endphp
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">{{ $category }}</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($total) }}</td>
                </tr>
                @endforeach
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">@lang('pulse.title')</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">
                        @php
                            $total_profit_pulse = $pulse->sum('sell_price') - $pulse->sum('capital_price');
                            $total_so_gross_profit += $total_profit_pulse;
                        @endphp
                        {{ Util::format_currency($total_profit_pulse) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h3><b>@lang('loss_profit.label.total_gross_profit')</b></h3></td>
                    <td> : Rp. </td>
                    <td align="right"><b>{{ Util::format_currency($total_so_gross_profit) }}</b></td>
                </tr>
            </table>
        </div>
        {{-- END DETAIL LOSS PROFIT --}}

        {{-- START DETAIL INCENTIVE INCREASE --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('loss_profit.label.incentive')</b></h1>
        @php
            $total_incentive_increase = 0;
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
                @foreach ($incentiveIncrease as $category => $incentive)
                @php
                    $total = $incentive->sum('amount');
                    $total_incentive_increase += $total;
                @endphp
                <tr>
                    <td width="5%">-</td>
                    <td width="25%"><h3><b>{{ $category }} </b></h3></td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right"><b>{{ Util::format_currency($total) }}</b></td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2"><h3><b>@lang('loss_profit.label.total_incentive_increase')</b></h3></td>
                    <td> : Rp. </td>
                    <td align="right"><b>{{ Util::format_currency($total_incentive_increase) }}</b></td>
                </tr>
            </table>
        </div>
        {{-- END DETAIL INCENTIVE INCREASE --}}

        {{-- START DETAIL DISCOUNT --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('loss_profit.label.discount')</b></h1>
        @php
            $total_discount_promo = $salesOrders->sum('total_discount_detail');
            $total_discount_sales = $salesOrders->sum('discount_sales');
            $total_discount = $total_discount_promo + $total_discount_sales;
        @endphp
        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">@lang('loss_profit.label.total_discount_promo')</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($total_discount_promo) }}</td>
                </tr>
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">@lang('loss_profit.label.total_discount_sales')</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($total_discount_sales) }}</td>
                </tr>
                <tr>
                    <td colspan="2"><h3><b>@lang('loss_profit.label.total_discount')</b></h3></td>
                    <td> : Rp. </td>
                    <td align="right"><b>{{ Util::format_currency($total_discount) }}</b></td>
                </tr>
            </table>
        </div>
        {{-- END DETAIL DISCOUNT --}}

        {{-- START DETAIL EXPEND --}}
        <hr style="height: 2px; background-color: black;">
        <h1><b>@lang('loss_profit.label.expend')</b></h1>
        @php
            $total_expend = 0;
        @endphp

        <div style="margin-left: 20px">
            <table border="0" cellspacing="0" cellpadding="5" width='100%'>
            @foreach ($expends as $expend)
                @php
                    $total = $expend->sum('amount');
                    $total_expend += $total;
                @endphp
                <tr>
                    <td width="5%">-</td>
                    <td width="25%">{{ $expend->categoryExpend->name }}</td>
                    <td width="10%"> : Rp. </td>
                    <td width="20%" align="right">{{ Util::format_currency($total) }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="2"><h3><b>@lang('loss_profit.label.total_expend')</b></h3></td>
                <td> : Rp. </td>
                <td align="right"><b>{{ Util::format_currency($total_expend) }}</b></td>
            </tr>
            </table>
        </div>
        {{-- END DETAIL EXPEND --}}

        @php
            $total_all = $total_so_gross_profit + $total_incentive_increase - $total_discount - $total_expend;
        @endphp
        
        <hr style="margin-bottom: 1px;">
        <hr style="height: 2px; background-color: black; margin: 0px">
        <center>
            <h1>
                <b>
                    @lang('loss_profit.label.current_balance') @lang('global.array.months.'.$month), {{ $year }}
                </b>
            </h1>
        </center>
        <center><h1><b>Rp. {{ Util::format_currency($total_all) }}</b></h1></center>
    </body>
</html>
        