@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" action="{{ route('report.product.identity.index') }}">
<input type="hidden" name="ex" value="1" />
<div class="row">
    <div class="col-lg-6">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.report') @lang('product.label.product_identity')</h3>
            </div>
            <!-- form start -->

            <div class="card-body">
                <div class="form-group">
                    <label>@lang('purchase_order.label.product_id')</label>
                    <select name="_pid"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($products as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>@lang('product.label.category_id')</label>
                    <select name="_cg"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($categories as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>@lang('product.label.brand_id')</label>
                    <select name="_bd"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($brands as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>@lang('product.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_stc">@lang('product.label.stock')</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <select name="_sop"  class="form-control" style="width: 100%;">
                                @foreach (trans('global.array.operator') as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" name="_stc" class="form-control">
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('report.product.identity.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right">
                    @lang('global.export')
                </button>
            </div>
        </div>
    </div>
</div>
</form>
@stop