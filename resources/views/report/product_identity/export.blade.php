@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('product.label.code')</b></th>
        <th><b>@lang('product.label.name')</b></th>
        <th><b>@lang('product.label.branch_id')</b></th>
        <th><b>@lang('product.label.category_id')</b></th>
        <th><b>@lang('product.label.brand_id')</b></th>
        <th><b>@lang('product.label.product_identity')</b></th>
        {{-- <th><b>@lang('product.label.expired_date')</b></th> --}}
        <th><b>@lang('product.label.cost_of_goods')</b></th>
        <th><b>@lang('product.label.sell_price')</b></th>
        <th><b>@lang('product.label.stock')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $productIdentity)
        <tr>
            <td>{{ $productIdentity->product->code }}</td>
            <td>{{ $productIdentity->product->name }}</td>
            <td>{{ $productIdentity->branch->name }}</td>
            <td>{{ $productIdentity->product->category->name }}</td>
            <td>{{ $productIdentity->product->brand->name }}</td>
            <td>{{ sprintf('%s', $productIdentity->identity) }}</td>
            {{-- <td>{{ Util::formatDate($productIdentity->expired_date, 'd/m/Y') }}</td> --}}
            <td data-format="#,##0_-">{{ $productIdentity->product->cost_of_goods }}</td>
            <td data-format="#,##0_-">{{ $productIdentity->product->sell_price }}</td>
            <td>{{ $productIdentity->stock }}</td>
        </tr>
    @endforeach
    </tbody>
</table>