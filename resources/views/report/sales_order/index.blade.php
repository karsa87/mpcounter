@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" action="{{ route('report.sales.order.index') }}">
<input type="hidden" name="ex" value="1" />
<div class="row">
    <div class="col-lg-6">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.report') @lang('sales_order.title')</h3>
            </div>
            <!-- form start -->

            <div class="card-body">
                <div class="form-group">
                    <label for="date">@lang('sales_order.label.date')</label>
                    <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('sales_order.placeholder.date')" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="_bk">@lang('sales_order.label.bank_id')</label>
                    <select name="_bk"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($banks as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_bc">@lang('sales_order.label.branch_id')</label>
                    <select name="_bc"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_sls">@lang('sales_order.label.sales_id')</label>
                    <select name="_sls" class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($sales as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_mb">@lang('sales_order.label.member_id')</label>
                    <select name="_mb"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($members as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_pic">@lang('sales_order.label.pic')</label>
                    <select name="_pic" class="form-control select2" style="width: 100%;" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($employes as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_sts">@lang('sales_order.label.status')</label>
                    <select name="_sts"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('sales_order.list.status') as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_tp">@lang('sales_order.label.type')</label>
                    <select name="_tp"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('sales_order.list.type') as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('report.sales.order.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right">
                    @lang('global.export')
                </button>
            </div>
        </div>
    </div>
</div>
</form>
@stop