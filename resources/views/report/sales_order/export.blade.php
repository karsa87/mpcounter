@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('sales_order.label.no_invoice')</b></th>
        <th><b>@lang('sales_order.label.date')</b></th>
        <th><b>@lang('sales_order.label.name')</b></th>
        <th><b>@lang('sales_order.label.phone')</b></th>
        <th><b>@lang('sales_order.label.address')</b></th>
        <th><b>@lang('sales_order.label.sales_id')</b></th>
        <th><b>@lang('sales_order.label.bank_id')</b></th>
        <th><b>@lang('sales_order.label.branch_id')</b></th>
        <th><b>@lang('sales_order.label.pic')</b></th>
        <th><b>@lang('sales_order.label.type')</b></th>
        <th><b>@lang('sales_order.label.status')</b></th>
        <th><b>@lang('sales_order.label.total_amount_first')</b></th>
        <th><b>@lang('sales_order.label.discount')</b></th>
        <th><b>@lang('sales_order.label.add_cost')</b></th>
        <th><b>@lang('sales_order.label.total_amount')</b></th>
        <th><b>@lang('sales_order.label.paid_off')</b></th>
        <th><b>@lang('sales_order.label.remaining_debt')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($salesOrders as $salesOrder)
        <tr>
            <td>{{ $salesOrder->no_invoice }}</td>
            <td>{{ Util::formatDate($salesOrder->date, 'd/m/Y H:i') }}</td>
            <td>{{ $salesOrder->name }}</td>
            <td>{{ $salesOrder->phone }}</td>
            <td>{{ $salesOrder->address }}</td>
            <td>{{ $salesOrder->sales ? $salesOrder->sales->name : '-' }}</td>
            <td>{{ $salesOrder->bank->name }}</td>
            <td>{{ $salesOrder->branch->name }}</td>
            <td>{{ $salesOrder->createdBy ? $salesOrder->createdBy->name : '-' }}</td>
            <td>@lang('sales_order.list.type.' . $salesOrder->type)</td>
            <td>@lang('sales_order.list.status.' . $salesOrder->status)</td>
            <td data-format="#,##0_-">{{ $salesOrder->total_amount_first }}</td>
            <td data-format="#,##0_-">{{ $salesOrder->discount_promo + $salesOrder->discount_sales + $salesOrder->total_discount_detail }}</td>
            <td data-format="#,##0_-">{{ $salesOrder->shipping_cost + $salesOrder->additional_cost }}</td>
            <td data-format="#,##0_-">{{ $salesOrder->total_amount }}</td>
            <td data-format="#,##0_-">{{ $salesOrder->paid_off + $salesOrder->debtPayments->sum('paid') }}</td>
            <td data-format="#,##0_-">{{ $salesOrder->total_amount - ($salesOrder->paid_off + $salesOrder->debtPayments->sum('paid')) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>