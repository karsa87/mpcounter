@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('expend_all.label.no_transaction')</b></th>
        <th><b>@lang('expend_all.label.expend_type')</b></th>
        <th><b>@lang('expend_all.label.date')</b></th>
        <th><b>@lang('expend_all.label.information')</b></th>
        <th><b>@lang('expend_all.label.amount')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($purchaseOrders as $purchaseOrder)
        <tr>
            <td style="font-weight: bold;">{{ $purchaseOrder->no_invoice }}</td>
            <td style="font-weight: bold;">@lang('expend_all.list.expend_type.1')</td>
            <td style="font-weight: bold;">{{ Util::formatDate($purchaseOrder->date, 'd/m/Y H:i') }}</td>
            <td style="font-weight: bold;">{{ $purchaseOrder->information }}</td>
            <td style="font-weight: bold;" data-format="#,##0_-">{{ $purchaseOrder->paid_off }}</td>
        </tr>

        @foreach ($purchaseOrder->details as $detail)
        <tr>
            <td></td>
            <td>{{ $detail->product->name }}</td>
            <td align="left">{{ (string) $detail->productIdentity->identity }}</td>
            <td align="center">x{{ $detail->qty }}</td>
            <td>{{ $detail->total_amount }}</td>
        </tr>
        @endforeach
        <tr><td colspan="5"></td></tr>
    @endforeach

    @if ($debtPaymentPo->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($debtPaymentPo as $debt)
        <tr>
            <td>{{ $debt->no_debt }}</td>
            <td>@lang('expend_all.list.expend_type.2')</td>
            <td>{{ Util::formatDate($debt->date, 'd/m/Y H:i') }}</td>
            <td>{{ $debt->information }}</td>
            <td>{{ $debt->paid }}</td>
        </tr>
    @endforeach

    @if ($returSO->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($returSO as $rso)
        <tr>
            <td>{{ $rso->no_retur }}</td>
            <td>@lang('expend_all.list.expend_type.3')</td>
            <td>{{ Util::formatDate($rso->date, 'd/m/Y H:i') }}</td>
            <td>{{ $rso->information }}</td>
            <td>{{ $rso->total_amount }}</td>
        </tr>
    @endforeach

    @if ($incentiveExpend->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($incentiveExpend as $incentive)
        <tr>
            <td>{{ $incentive->no_transaction }}</td>
            <td>@lang('expend_all.list.expend_type.4')</td>
            <td>{{ Util::formatDate($incentive->date_transaction, 'd/m/Y H:i') }}</td>
            <td>{{ $incentive->information }}</td>
            <td>{{ $incentive->amount }}</td>
        </tr>
    @endforeach

    @if ($expends->count() > 0)
        <tr><td colspan="5"></td></tr>
    @endif
    @foreach($expends as $expend)
        <tr>
            <td>{{ $expend->no_retur }}</td>
            <td>@lang('expend_all.list.expend_type.5')</td>
            <td>{{ Util::formatDate($expend->expend_date, 'd/m/Y H:i') }}</td>
            <td>{{ $expend->information }}</td>
            <td>{{ $expend->amount }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4"><b>@lang('expend_all.label.total')</b></td>
        <td>{{ $total_expend_all }}</td>
    </tr>
    </tbody>
</table>