@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<form role="form" action="{{ route('report.expend.all.index') }}">
<input type="hidden" name="ex" value="1" />
<div class="row">
    <div class="col-lg-6">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.report') @lang('expend_all.title')</h3>
            </div>
            <!-- form start -->

            <div class="card-body">
                <div class="form-group">
                    <label for="date">@lang('expend_all.label.date')</label>
                    <input type="text" name="_dt" class="form-control datetime-rangepicker" placeholder="@lang('expend_all.placeholder.date')" autocomplete="off" value="{{ request('_dt') ? request('_dt') : '' }}">
                </div>
                <div class="form-group">
                    <label for="_bc">@lang('expend_all.label.branch_id')</label>
                    <select name="_bc"  class="form-control" style="width: 100%;">
                        <option value="">@lang('global.all')</option>
                        @foreach ($branchs as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="_et">@lang('expend_all.label.expend_type')</label>
                    <select name="_et" id="form-expend_type" class="form-control" style="width: 100%;">
                        @foreach (trans('expend_all.list.expend_type') as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="div-category-expend" style="display: none;">
                    <label for="_cexp">@lang('expend_all.label.category_expend_id')</label>
                    <select name="_cexp" id="form-category_expend_id" class="form-control select2" style="width: 100%;" data-placeholder="">
                        <option value="">@lang('global.all')</option>
                        @foreach ($categoryExp as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a href="{{ route('report.expend.all.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                <button type="submit" class="btn btn-success float-right">
                    @lang('global.export')
                </button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@push('js')
<script type="text/javascript">
    $(function(){
        $("#form-expend_type").on('change', function(e){
            if ($(this).val() == 5) {
                $("#div-category-expend").show();
            } else {
                $("#div-category-expend").hide();
                $("#form-category_expend_id").val('').trigger('change');
            }
        });
    })
</script>
@endpush