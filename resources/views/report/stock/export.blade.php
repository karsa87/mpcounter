<table>
    <thead>
    <tr>
        <th><b>@lang('product.label.code')</b></th>
        <th><b>@lang('product.label.name')</b></th>
        <th><b>@lang('product.label.branch_id')</b></th>
        <th><b>@lang('product.label.category_id')</b></th>
        <th><b>@lang('product.label.brand_id')</b></th>
        <th><b>@lang('product.label.cost_of_goods')</b></th>
        <th><b>@lang('product.label.sell_price')</b></th>
        <th><b>@lang('product.label.stock')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->code }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $branch }}</td>
            <td>{{ $product->category->name }}</td>
            <td>{{ $product->brand->name }}</td>
            <td data-format="#,##0_-">{{ $product->cost_of_goods }}</td>
            <td data-format="#,##0_-">{{ $product->sell_price }}</td>
            <td>{{ $product->stock->sum('stock') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>