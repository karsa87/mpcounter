@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('income_all.label.no_transaction')</b></th>
        <th><b>@lang('income_all.label.date')</b></th>
        <th><b>@lang('income_all.label.information')</b></th>
        <th><b>@lang('income_all.label.income_type')</b></th>
        <th><b>@lang('income_all.label.amount')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($salesOrders as $salesOrder)
        <tr>
            <td>{{ $salesOrder->no_invoice }}</td>
            <td>{{ Util::formatDate($salesOrder->date, 'd/m/Y H:i') }}</td>
            <td>{{ $salesOrder->information }}</td>
            <td>@lang('income_all.list.income_type.1')</td>
            <td data-format="#,##0_-">{{ $salesOrder->paid_off }}</td>
        </tr>
    @endforeach
    @foreach($debtPaymentSo as $debt)
        <tr>
            <td>{{ $debt->no_debt }}</td>
            <td>{{ Util::formatDate($debt->date, 'd/m/Y H:i') }}</td>
            <td>{{ $debt->information }}</td>
            <td>@lang('income_all.list.income_type.2')</td>
            <td data-format="#,##0_-">{{ $debt->paid }}</td>
        </tr>
    @endforeach
    @foreach($returPO as $rpo)
        <tr>
            <td>{{ $rpo->no_retur }}</td>
            <td>{{ Util::formatDate($rpo->date, 'd/m/Y H:i') }}</td>
            <td>{{ $rpo->information }}</td>
            <td>@lang('income_all.list.income_type.3')</td>
            <td data-format="#,##0_-">{{ $rpo->total_amount }}</td>
        </tr>
    @endforeach
    @foreach($incentiveIncome as $incentive)
        <tr>
            <td>{{ $incentive->no_transaction }}</td>
            <td>{{ Util::formatDate($incentive->date_transaction, 'd/m/Y H:i') }}</td>
            <td>{{ $incentive->information }}</td>
            <td>@lang('income_all.list.income_type.4')</td>
            <td data-format="#,##0_-">{{ $incentive->amount }}</td>
        </tr>
    @endforeach
    @foreach($pulse as $pl)
        <tr>
            <td>{{ $pl->no_transaction }}</td>
            <td>{{ Util::formatDate($pl->date, 'd/m/Y H:i') }}</td>
            <td>{{ $pl->information }}</td>
            <td>@lang('income_all.list.income_type.5')</td>
            <td data-format="#,##0_-">{{ $pl->sell_price }}</td>
        </tr>
    @endforeach
    @foreach($purchaseOrders as $purchaseOrder)
        <tr>
            <td>{{ $purchaseOrder->no_invoice }}</td>
            <td>{{ Util::formatDate($purchaseOrder->date, 'd/m/Y H:i') }}</td>
            <td>{{ $purchaseOrder->information }}</td>
            <td>@lang('expend_all.list.expend_type.1')</td>
            <td data-format="#,##0_-">{{ $purchaseOrder->paid_off }}</td>
        </tr>
    @endforeach
    @foreach($debtPaymentPo as $debt)
        <tr>
            <td>{{ $debt->no_debt }}</td>
            <td>{{ Util::formatDate($debt->date, 'd/m/Y H:i') }}</td>
            <td>{{ $debt->information }}</td>
            <td>@lang('expend_all.list.expend_type.2')</td>
            <td data-format="#,##0_-">{{ $debt->paid }}</td>
        </tr>
    @endforeach
    @foreach($returSO as $rso)
        <tr>
            <td>{{ $rso->no_retur }}</td>
            <td>{{ Util::formatDate($rso->date, 'd/m/Y H:i') }}</td>
            <td>{{ $rso->information }}</td>
            <td>@lang('expend_all.list.expend_type.3')</td>
            <td data-format="#,##0_-">{{ $rso->total_amount }}</td>
        </tr>
    @endforeach
    @foreach($incentiveExpend as $incentive)
        <tr>
            <td>{{ $incentive->no_transaction }}</td>
            <td>{{ Util::formatDate($incentive->date_transaction, 'd/m/Y H:i') }}</td>
            <td>{{ $incentive->information }}</td>
            <td>@lang('expend_all.list.expend_type.4')</td>
            <td data-format="#,##0_-">{{ $incentive->amount }}</td>
        </tr>
    @endforeach
    @foreach($expends as $expend)
        <tr>
            <td>{{ $expend->no_retur }}</td>
            <td>{{ Util::formatDate($expend->expend_date, 'd/m/Y H:i') }}</td>
            <td>{{ $expend->information }}</td>
            <td>@lang('expend_all.list.expend_type.5')</td>
            <td data-format="#,##0_-">{{ $expend->amount }}</td>
        </tr>
    @endforeach

    <tr>
        <td colspan="4"><b>@lang('income_all.label.total') @lang('income_all.title')</b></td>
        <td data-format="#,##0_-">{{ $total_income_all }}</td>
    </tr>
    <tr>
        <td colspan="4"><b>@lang('expend_all.label.total') @lang('expend_all.title')</b></td>
        <td data-format="#,##0_-">{{ $total_expend_all }}</td>
    </tr>
    <tr>
        <td colspan="4"><b>@lang('expend_all.label.total')</b></td>
        {{-- <td data-format="#,##0_-">{{ $total_before - $total_income_all - $total_expend_all }}</td> --}}
        <td data-format="#,##0_-">{{ $total_income_all - $total_expend_all }}</td>
    </tr>
    </tbody>
</table>