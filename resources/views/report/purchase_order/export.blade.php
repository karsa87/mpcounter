@php
use App\Util\Helpers\Util;
@endphp

<table>
    <thead>
    <tr>
        <th><b>@lang('purchase_order.label.no_invoice')</b></th>
        <th><b>@lang('purchase_order.label.no_reference')</b></th>
        <th><b>@lang('purchase_order.label.date')</b></th>
        <th><b>@lang('purchase_order.label.supplier_id')</b></th>
        <th><b>@lang('purchase_order.label.bank_id')</b></th>
        <th><b>@lang('purchase_order.label.branch_id')</b></th>
        <th><b>@lang('sales_order.label.pic')</b></th>
        <th><b>@lang('purchase_order.label.type')</b></th>
        <th><b>@lang('purchase_order.label.status')</b></th>
        <th><b>@lang('purchase_order.label.total_amount')</b></th>
        <th><b>@lang('purchase_order.label.paid_off')</b></th>
        <th><b>@lang('purchase_order.label.remaining_debt')</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($purchaseOrders as $purchaseOrder)
        <tr>
            <td>{{ $purchaseOrder->no_invoice }}</td>
            <td>{{ $purchaseOrder->no_reference }}</td>
            <td>{{ Util::formatDate($purchaseOrder->date, 'd/m/Y H:i') }}</td>
            <td>{{ $purchaseOrder->supplier ? $purchaseOrder->supplier->name : '-' }}</td>
            <td>{{ $purchaseOrder->bank->name }}</td>
            <td>{{ $purchaseOrder->branch->name }}</td>
            <td>{{ $purchaseOrder->createdBy ? $purchaseOrder->createdBy->name : '-' }}</td>
            <td>@lang('purchase_order.list.type.' . $purchaseOrder->type)</td>
            <td>@lang('purchase_order.list.status.' . $purchaseOrder->status)</td>
            <td data-format="#,##0_-">{{ $purchaseOrder->total_amount }}</td>
            <td data-format="#,##0_-">{{ $purchaseOrder->paid_off + $purchaseOrder->debtPayments->sum('paid') }}</td>
            <td data-format="#,##0_-">{{ $purchaseOrder->total_amount - ($purchaseOrder->paid_off + $purchaseOrder->debtPayments->sum('paid')) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>