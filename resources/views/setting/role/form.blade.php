<!-- form start -->
<form id="form-role" role="form" method="POST" action="{{ route('setting.role.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('role.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('role.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="slug">@lang('role.label.slug')</label>
            <input type="text" name="slug" class="form-control @classInpError('slug')" placeholder="@lang('role.placeholder.slug')" value="{{ old('slug') }}">

            @inpSpanError(['column'=>'slug'])
        </div>
        <div class="form-group">
            <label for="icon">@lang('role.label.description')</label>
            <textarea name="description" class="form-control @classInpError('description')" rows="3" placeholder="@lang('role.placeholder.description')" >{{ old('description') }}</textarea>

            @inpSpanError(['column'=>'description'])
        </div>
        <div class="form-group">
            <label for="level">@lang('role.label.level')</label>
            <select name="level" id="form-level" class="form-control @classInpError('level')">
                @foreach ($role->getListRole() as $id => $name)
                <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'description'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.role.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>