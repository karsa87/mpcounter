@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('user.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.user.search')
        </div>
    </div>
</div>
<div class="row" id="content-user">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('user.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-user">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('user.label.name')</th>
                            <th class="sorting-form" data-column="lastlogin">@lang('user.label.lastlogin')</th>
                            <th class="text-center">@lang('user.label.status')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($users) && count($users) > 0 )
                        @foreach( $users as $i => $r)
                        <tr>
                            <td class="text-center">{{ $users->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('user.label.email'):</strong> {{ $r->email }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('user.label.username'):</strong> {{ $r->username }}
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('user.label.branch_id'):</strong> {{ $r->branch_id ? $r->branch->name : '' }}
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    @if( $r->lastlogin )
                                    {{ date('D, d-m-Y H:i', strtotime($r->lastlogin)) }}
                                    @else
                                    -
                                    @endif
                                </h6>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('user.label.lastloginip'):</strong> {{ $r->lastloginip }}
                                    </small>
                                </p>
                            </td>
                            <td class="text-center text-wrap">
                                <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('user.list.status.'.$r->status)</span>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    
                                    @hasPermission('setting.user.changestatus')
                                    <a title="@lang('user.label.change_status')" href="{{ route('setting.user.changestatus', [$r->id, ($r->status ? 0 : 1)]) }}" class="btn btn-sm {{ $r->status ? 'btn-danger' : 'btn-success' }}" data-toggle="tooltip">
                                        @if($r->status)
                                        <i class="fas fa-window-close m-0"></i>
                                        @else
                                        <i class="fas fa-check-square m-0"></i>
                                        @endif
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.user.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.user.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-user btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission
                                    
                                    @hasPermission('setting.user.destroy')
                                    @if(!$r->employee)
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.user.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endif
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($users) && count($users) > 0 )
            {!! \App\Util\Base\Layout::paging($users) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('user.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.user.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/user.js') }}?{{ config('app.version') }}"></script>
@endpush