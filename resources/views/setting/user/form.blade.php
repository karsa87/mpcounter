<!-- form start -->
<form id="form-user" role="form" method="POST" action="{{ route('setting.user.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('user.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('user.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="username">@lang('user.label.username')</label>
            <input type="text" name="username" class="form-control @classInpError('username')" placeholder="@lang('user.placeholder.username')" value="{{ old('username') }}">

            @inpSpanError(['column'=>'username'])
        </div>
        <div class="form-group">
            <label for="password">@lang('user.label.password')</label>
            <div class="input-group mb-3">
                <input type="password" name="password" class="form-control @classInpError('password')" value="{{ old('password') }}" placeholder="@lang('user.placeholder.password')">
                <div class="input-group-append">
                    <button type="button" class="btn btn-default btn-flat" id="btn-show-password">
                        <i class="fas fa-eye"></i>
                    </button>
                </div>

                @inpSpanError(['column'=>'password'])
            </div>
        </div>
        <div class="form-group">
            <label for="email">@lang('user.label.email')</label>
            <input type="email" name="email" class="form-control @classInpError('email')" placeholder="@lang('user.placeholder.email')" value="{{ old('email') }}">

            @inpSpanError(['column'=>'email'])
        </div>
        <div class="form-group">
            <label for="form-branch_id" class="control-label">@lang('user.label.branch_id')</label>
            
            <select name="branch_id" id="form-branch_id" class="form-control select2 @classInpError('branch_id')" data-placeholder="">
                <option value="" selected></option>
                @foreach($branchs as $id => $name)
                    <option value="{{ $id }}" {{ old('branch_id') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'branch_id'])
        </div>
        <div class="form-group">
            <label for="form-user_role" class="control-label">@lang('user.placeholder.role')</label>
            <select name="user_role" id="form-user_role" class="form-control select2 @classInpError('user_role')" data-placeholder="">
                @foreach($roles as $id => $name)
                    <option value="{{ $id }}" {{ old('user_role') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'user_role'])
        </div>
        <div class="form-group">
            <label for="permission" class="control-label">@lang('user.label.permission')</label>
            <select name="user_permission[]" id="form-user_permission" class="form-control select2  @classInpError('user_permission')" data-placeholder="" multiple="">
                @foreach($permissions as $id => $name)
                    <option value="{{ $id }}" {{ old('user_permission') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'user_permission'])
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch @classInpError('status')">
                <input type="hidden" name="status" value="0" />
                <input type="checkbox" class="custom-control-input" name="status"  id="user-status" {{ ( old('status') || old('status') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="user-status">@lang('user.label.status')</label>
            </div>
            @inpSpanError(['column'=>'status'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.user.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>