@extends('layouts.admin')
@inject('util', 'App\Util\Helpers\Util')
@section('content')
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="{{ $model->image() }}" alt="{{ $model->name }}" />
                </div>

                <h3 class="profile-username text-center">{{ $model->name }}</h3>

                <p class="text-muted text-center">{{ $model->roles->first()->name }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item"><b>@lang('user.label.lastlogin')</b> <a class="float-right">{{ $util->formatDate($model->lastlogin, $util->FORMAT_DATETIME_ID_LONG) }}</a></li>
                    <li class="list-group-item"><b>@lang('user.label.lastloginip')</b> <a class="float-right">{{ $model->lastloginip }}</a></li>
                </ul>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="card">
            <div class="card-header p-2">
                Settings
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form id="form-user" role="form" method="POST" action="{{ route('setting.user.store') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ old('id') ?? $model->id }}">
                    <input type="hidden" name="status" value="{{ $model->status == 1 ? 'on' : 'off' }}">
                    <input type="hidden" name="branch_id" value="{{ $model->branch_id }}">
                    <input type="hidden" name="user_role" value="{{ $model->roles->first()->id }}">
                    
                    @if (!empty($model->userPermissions))
                        @foreach ($model->userPermissions as $k => $v)
                        <input type="hidden" name="user_permission[{{$k}}]" value="{{ $v->id }}">
                        @endforeach
                    @else
                    <input type="hidden" name="user_permission" value="">
                    @endif

                    
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">@lang('user.label.name')</label>
                            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('user.placeholder.name')" value="{{ old('name') ?? $model->name }}">
                            
                            @inpSpanError(['column'=>'name'])
                        </div>
                        <div class="form-group">
                            <label for="username">@lang('user.label.username')</label>
                            <input type="text" name="username" class="form-control @classInpError('username')" placeholder="@lang('user.placeholder.username')" value="{{ old('username') ?? $model->username }}">
                
                            @inpSpanError(['column'=>'username'])
                        </div>
                        <div class="form-group">
                            <label for="password">@lang('user.label.password')</label>
                            <div class="input-group mb-3">
                                <input type="password" name="password" class="form-control @classInpError('password')" value="{{ old('password') }}" placeholder="@lang('user.placeholder.password')">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-default btn-flat" id="btn-show-password">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </div>
                
                                @inpSpanError(['column'=>'password'])
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('user.label.email')</label>
                            <input type="email" name="email" class="form-control @classInpError('email')" placeholder="@lang('user.placeholder.email')" value="{{ old('email') ?? $model->email }}">
                
                            @inpSpanError(['column'=>'email'])
                        </div>
                        <div class="form-group">
                            <label for="images">@lang('user.label.images')</label>
                            <input type="hidden" name="images" id="result-upload" value="{{ old('images') ?? $model->images }}">
                            <div class="dropzone" id="files-upload" data-href="{{ route('setting.user.upload') }}"></div>
                
                            @inpSpanError(['column'=>'images'])
                        </div>
                    </div>
                    <!-- /.card-body -->
                
                    <div class="card-footer">
                        <a href="{{ route('setting.user.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/user.js') }}?{{ config('app.version') }}"></script>
@endpush