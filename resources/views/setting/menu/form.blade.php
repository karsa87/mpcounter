<!-- form start -->
<form id="form-menu" role="form" method="POST" action="{{ route('setting.menu.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('menu.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('menu.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch @classInpError('is_parent')">
                <input type="hidden" name="is_parent" value="0" />
                <input type="checkbox" class="custom-control-input" name="is_parent"  id="menu-is_parent" {{ ( old('is_parent') || old('is_parent') == 'on' ) ? 'checked' : '' }}>
                <label class="custom-control-label" for="menu-is_parent">@lang('menu.label.is_parent')</label>
            </div>
            @inpSpanError(['column'=>'is_parent'])
        </div>
        <div class="form-group" style="display: none;">
            <label for="url">@lang('menu.label.url')</label>
            <input type="text" name="url" class="form-control @classInpError('url')" placeholder="@lang('menu.placeholder.url')" value="{{ old('url') }}">

            @inpSpanError(['column'=>'url'])
        </div>
        <div class="form-group">
            <label for="route">@lang('menu.label.route')</label>
            <input type="text" name="route" class="form-control @classInpError('route')" placeholder="@lang('menu.placeholder.route')" value="{{ old('route') }}">

            @inpSpanError(['column'=>'route'])
        </div>
        <div class="form-group">
            <label for="icon">@lang('menu.label.icon')</label>
            <input type="text" name="icon" class="form-control @classInpError('icon')" placeholder="@lang('menu.placeholder.icon')" value="{{ old('icon') }}">

            @inpSpanError(['column'=>'icon'])
        </div>
        
        <div class="form-group">
            <label for="parent_id">@lang('menu.label.parent_id')</label>
            <select name="parent_id"  id="menu-parent_id" class="form-control select2 @classInpError('parent_id')" data-placeholder="" style="width: 100%;">
                <option value=""></option>
                @foreach ($parents as $id => $name)
                <option value="{{ $id }}" {{ old('parent_id') == $id ? "selected"  : ""}}>{{ $name }}</option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'parent_id'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.menu.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>