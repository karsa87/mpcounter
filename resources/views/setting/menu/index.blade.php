@extends('layouts.admin')

@section('content')
<form method="post" id="menu-order-form" action="{{ route('setting.menu.store') }}" novalidate="novalidate">
    @csrf
    <input type="hidden" name="ordering" id="inp-order">
</form>

<div class="row" id="content-menu">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h5 class="m-0">
                    @lang('global.list') @lang('menu.title')
                </h5>
            </div>
            <div class="card-body">
                <div class="form-row div-footer-btn" style="display: none;">
                    <div class="form-group col-md-12">
                        <a href="{{ route('setting.menu.index') }}" class="btn btn-danger">
                            <i class="fas fa-redo-alt"></i> @lang('global.cancel')
                        </a>
                        <button type="submit" class="btn btn-success float-right m-0 btn-update">
                            <i class="fa fa-save"></i> @lang('global.update')
                        </button>
                    </div>
                </div>

                <div class="dd" id="nestable">
                    @include($template . '.setting.menu.menu',['parents' => $menus])
                </div>
            </div>

            <div class="card-footer div-footer-btn" style="display: none;">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <a href="{{ route('setting.menu.index') }}" class="btn btn-danger">
                            <i class="fas fa-redo-alt"></i> @lang('global.cancel')
                        </a>
                        <button type="submit" class="btn btn-success float-right m-0 btn-update">
                            <i class="fa fa-save"></i> @lang('global.update')
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('menu.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.menu.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/menu.js') }}?{{ config('app.version') }}"></script>
@endpush