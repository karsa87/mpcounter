<!-- form start -->
<form role="form" id="form-search-permission" action="{{ route('setting.permission.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('permission.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('permission.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="slug">@lang('permission.label.slug')</label>
                    <input type="text" name="_sg" class="form-control" placeholder="@lang('permission.placeholder.slug')" value="{{ request('_sg') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_mn">@lang('permission.label.menu')</label>
                    <select name="_mn"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($menus as $id => $name)
                        <option value="{{ $id }}" {{ request('_mn') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.permission.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>