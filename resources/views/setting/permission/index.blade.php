@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.search') @lang('permission.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.permission.search')
        </div>
    </div>
</div>
<div class="row" id="content-permission">
    <div class="col-xs-12 xol-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('permission.title')</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed sorting-table" data-form="form-search-permission">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 35%;" class="sorting-form" data-column="name">@lang('permission.label.name')</th>
                            <th style="width: 25%;" class="sorting-form" data-column="slug">@lang('permission.label.slug')</th>
                            <th style="width: 20%;" class="text-center">@lang('permission.label.menu')</th>
                            <th style="width: 15%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($permissions) && count($permissions) > 0 )
                        @foreach( $permissions as $i => $r)
                        <tr>
                            <td class="text-center">{{ $permissions->firstItem() + $i }}</td>
                            <td class="text-wrap">{{ $r->name }}</td>
                            <td class="text-wrap">{{ $r->slug }}</td>
                            <td class="text-center text-wrap">
                                {{ $r->menus->count() > 0 ? $r->menus->first()->name : '-' }}
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('setting.permission.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.permission.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-permission btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.permission.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.permission.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($permissions) && count($permissions) > 0 )
            {!! \App\Util\Base\Layout::paging($permissions) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('permission.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.permission.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/permission.js') }}?{{ config('app.version') }}"></script>
@endpush