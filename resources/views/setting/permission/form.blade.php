<!-- form start -->
<form id="form-permission" role="form" method="POST" action="{{ route('setting.permission.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('permission.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('permission.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="slug">@lang('permission.label.slug')</label>
            <input type="text" name="slug" class="form-control @classInpError('slug')" placeholder="@lang('permission.placeholder.slug')" value="{{ old('slug') }}">

            @inpSpanError(['column'=>'slug'])
        </div>
        <div class="form-group">
            <label for="icon">@lang('permission.label.description')</label>
            <textarea name="description" class="form-control @classInpError('description')" rows="3" placeholder="@lang('permission.placeholder.description')" >{{ old('description') }}</textarea>

            @inpSpanError(['column'=>'description'])
        </div>
        
        <div class="form-group">
            <label for="menu">@lang('permission.label.menu')</label>
            <select name="menu"  id="permission-menu" class="form-control select2 @classInpError('menu')" data-placeholder="" style="width: 100%;">
                <option value=""></option>
                @foreach ($menus as $id => $name)
                <option value="{{ $id }}" {{ old('menu') == $id ? "selected"  : ""}}>{{ $name }}</option>
                @endforeach
            </select>
            @inpSpanError(['column'=>'menu'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.permission.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>