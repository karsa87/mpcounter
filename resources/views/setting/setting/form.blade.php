<!-- form start -->
<form id="form-setting" role="form" method="POST" action="{{ route('setting.setting.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('setting.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('setting.placeholder.name')" value="{{ old('name') }}">
            
            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="key">@lang('setting.label.key')</label>
            <input type="text" name="key" class="form-control @classInpError('key')" placeholder="@lang('setting.placeholder.key')" value="{{ old('key') }}">

            @inpSpanError(['column'=>'key'])
        </div>
        <div class="form-group">
            <label for="form-type" class="control-label">@lang('setting.label.type')</label>
            <select name="type" id="form-type" class="form-control @classInpError('type')">
                @foreach(trans('setting.list.type') as $id => $name)
                    <option value="{{ $id }}" {{ old('type') == $id ? "selected"  : ""}} >
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            @inpSpanError(['column'=>'type'])
        </div>
        <div class="form-group">
            <label for="value">@lang('setting.label.value')</label>
            <textarea id="form_value" name="value" class="form-control @classInpError('value')">{{ old('value') }}</textarea>

            <input type="text" id="form_value_number" name="value_number" class="form-control @classInpError('value') format-number" placeholder="@lang('setting.placeholder.value')" value="{{ old('value_number') }}">
            
            @inpSpanError(['column'=>'value'])
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch @classInpError('status')">
                <input type="hidden" name="status" value="0" />
                @php
                    $checked = 'checked';
                    if (old('status')) {
                        if (old('status') == 'on') {
                            $checked = 'checked';
                        } else {
                            $checked = '';
                        }
                    }
                @endphp
                <input type="checkbox" class="custom-control-input" name="status"  id="setting-status" {{ $checked }}>
                <label class="custom-control-label" for="setting-status">@lang('setting.label.status')</label>
            </div>
            @inpSpanError(['column'=>'status'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.setting.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>