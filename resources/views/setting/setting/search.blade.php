<!-- form start -->
<form role="form" action="{{ route('setting.setting.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('setting.label.name')</label>
                    <input type="text" name="search_name" class="form-control" placeholder="@lang('setting.placeholder.name')" value="{{ request('search_name') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="search_status">@lang('setting.label.status')</label>
                    <select name="search_status"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('setting.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('search_status')) && request('search_status') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.setting.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>