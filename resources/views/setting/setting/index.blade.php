@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row" id="content-setting">
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('setting.title')</h3>
                
                <div class="card-tools">
                    <form role="form" action="{{ route('setting.setting.index') }}">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="search_name" class="form-control float-right" placeholder="@lang('global.search')">
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body table-responsive p-0" >
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th style="width: 5%" class="text-center">@lang('global.no')</th>
                            <th style="width: 30%">@lang('setting.label.name')</th>
                            <th style="width: 60%; max-width:100px;" class="text-center">@lang('setting.label.value')</th>
                            <th style="width: 5%">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($settings) && count($settings) > 0 )
                        @foreach( $settings as $i => $r)
                        <tr>
                            <td class="text-center">{{ $settings->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->name }}
                                @if (auth()->user()->isDeveloper())
                                <p>
                                    <small>{{ $r->key }}</small>
                                </p>
                                @endif
                                <p>
                                    <small>
                                        <span class="badge {{ $r->status ? 'badge-success' : 'badge-danger' }}">@lang('setting.list.status.'.$r->status)</span>
                                    </small>
                                </p>
                                @if (!auth()->user()->underAdminWika())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-center text-wrap" style="max-width:100px;">
                                @if ($r->isNumber())
                                    {!! $util->format_currency($r->value) !!}
                                @else
                                    {!! $r->value !!}
                                @endif
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('setting.setting.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.setting.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-edit-setting btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission
                                    
                                    @if (auth()->user()->isDeveloper())
                                    @hasPermission('setting.setting.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.setting.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($settings) && count($settings) > 0 )
            {!! \App\Util\Base\Layout::paging($settings) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 xol-sm-12 col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@lang('global.form') @lang('setting.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.setting.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}?{{ config('app.version') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/setting/setting.js') }}?{{ config('app.version') }}"></script>
@endpush