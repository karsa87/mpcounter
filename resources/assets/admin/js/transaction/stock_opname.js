$('.btn-edit-stock_opname').click(function(e){
    e.preventDefault();

    var btn = $(this);
    
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="title"]').val(result.data.title);
                $('input[name="month"]').val(result.data.month);
                $('input[name="year"]').val(result.data.year);
                $('textarea[name="information"]').val(result.data.information);
                $("#form-branch_id").val(result.data.branch_id).trigger('change');
                
                $('#form-stock_opname').attr('action', result.url_edit);
                $('#form-stock_opname').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});