if($("#form-debt-payment").length > 0) {
    var image_uploaded = [];
    var dropfile = $("div#files-upload");
    var href = dropfile.attr("data-href");

    var dropzone_upload = new Dropzone("div#files-upload", {
        url: href,
        acceptedFiles: "image/*",
        maxFilesize: 2,
        maxFiles: 5,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': CONFIG.token
        },
        init: function() { /* event listeners for removed files from dropzone*/
            var result_upload = $("#result-upload").val();
            if(result_upload !== "" && result_upload !== undefined && result_upload !== null){
                datum = JSON.parse(result_upload);
                var dropzone = this;
                datum.forEach(function(img_path, index){
                    var mockFile = { name: img_path, size: 32000 };
                    dropzone.options.addedfile.call(dropzone, mockFile);
                    dropzone.options.thumbnail.call(dropzone, mockFile, CONFIG.storage_url + img_path);
                    mockFile.previewElement.classList.add('dz-success');
                    mockFile.previewElement.classList.add('dz-complete');
                    image_uploaded.push(img_path);
                });
            }

            this.on("removedfile", function(file) {
                if ((file.new_name != undefined && file.new_name != '') 
                        || (file.name != undefined && file.name != '')){
                    var name = (file.new_name != undefined && file.new_name != '') ? file.new_name : file.name;
                    var index = image_uploaded.indexOf(name);
                    if (index > -1) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': CONFIG.token
                            },
                            type: 'DELETE',
                            url: href,
                            data: {
                                file: name,
                            },
                            dataType: 'json',
                            success: function(result){
                                if (typeof result == 'string')
                                    result = $.parseJSON(result);
                                if (result.success){
                                    image_uploaded.splice(index, 1);
                                    
                                    $("#result-upload").val(JSON.stringify(image_uploaded));
                                }
                            }
                        });
                    }
                }
            });
            this.on('processing', function(file){
                showAlert("Processing upload file", "info");
                return true;
            });
            this.on('queuecomplete', function(){
                return true;
            });
            this.on('success',function(file, result){
                // changing src of preview element
                file.previewElement.querySelector("img").src = CONFIG.storage_url + result.path;

                showAlert("Complete upload file", "success");

                image_uploaded.push(result.path);
                $("#result-upload").val( JSON.stringify(image_uploaded) );
                
                return true;
            });
        }
    });

    var total_paid = 0;
    var total_amount = 0;
    var remaining_debt = 0;
    $('#form-sales_order_id').on('change', function(e){
        var sales_order_id = $(this).val();
        var so = salesOrders[sales_order_id];
        if (so !== undefined) {
            total_paid = 0;
            if (so.debt_payments.length > 0) {
                total_paid = so.debt_payments.map(paid => (typeof paid.paid == 'string' ? paid.paid.parseFloatId() : paid.paid)).reduce((acc, paid) => paid + acc);
            }
            total_paid += (typeof so.paid_off == 'string' ? so.paid_off.parseFloatId() : so.paid_off);
            var paid = $('input[name="paid"').val();
            total_amount = (typeof so.total_amount == 'string' ? so.total_amount.parseFloatId() : so.total_amount);
            remaining_debt = total_amount - total_paid - paid.parseFloatId();
            
            $("#form-bank_id").text(so.bank.name);
            $("input[name='bank_id']").val(so.bank_id);
            $("input[name='remaining_debt']").val(remaining_debt);
            $("#form-remaining_debt").text( remaining_debt.format() );
        } else {
            $("#form-bank_id").text(trans('debt_payment_so.placeholder.bank_id'));
            $("#form-remaining_debt").text(trans('debt_payment_so.placeholder.remaining_debt'));
            $("input[name='bank_id']").val('');
        }
    });

    $('#form-sales_order_id').change();

    $('input[name="paid"').keyup(function() {
        var paid = $(this).val();
        if (paid != '') {
            remaining_debt = total_amount - total_paid - paid.parseFloatId();
        } else {
            remaining_debt = total_amount - total_paid;
        }

        if (remaining_debt < 0) {
            paid = paid.parseFloatId();
            $(this).val((paid + remaining_debt).format());

            remaining_debt = 0;
        }

        $("input[name='remaining_debt']").val(remaining_debt);
        $("#form-remaining_debt").text( remaining_debt.format() );
    });
}