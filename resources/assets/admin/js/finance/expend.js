var image_uploaded = [];
var dropfile = $("div#files-upload");
var href = dropfile.attr("data-href");

var dropzone_upload = new Dropzone("div#files-upload", {
    url: href,
    acceptedFiles: "image/*",
    maxFilesize: 2,
    maxFiles: 5,
    addRemoveLinks: true,
    headers: {
        'X-CSRF-TOKEN': CONFIG.token
    },
    init: function() { /* event listeners for removed files from dropzone*/
        var result_upload = $("#result-upload").val();
        if(result_upload !== "" && result_upload !== undefined && result_upload !== null){
            datum = JSON.parse(result_upload);
            var dropzone = this;
            datum.forEach(function(img_path, index){
                var mockFile = { name: img_path, size: 32000 };
                dropzone.options.addedfile.call(dropzone, mockFile);
                dropzone.options.thumbnail.call(dropzone, mockFile, CONFIG.storage_url + img_path);
                mockFile.previewElement.classList.add('dz-success');
                mockFile.previewElement.classList.add('dz-complete');
                image_uploaded.push(img_path);
            });
        }

        this.on("removedfile", function(file) {
            if ((file.new_name != undefined && file.new_name != '') 
                    || (file.name != undefined && file.name != '')){
                var name = (file.new_name != undefined && file.new_name != '') ? file.new_name : file.name;
                var index = image_uploaded.indexOf(name);
                if (index > -1) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': CONFIG.token
                        },
                        type: 'DELETE',
                        url: href,
                        data: {
                            file: name,
                        },
                        dataType: 'json',
                        success: function(result){
                            if (typeof result == 'string')
                                result = $.parseJSON(result);
                            if (result.success){
                                image_uploaded.splice(index, 1);
                                
                                $("#result-upload").val(JSON.stringify(image_uploaded));
                            }
                        }
                    });
                }
            }
        });
        this.on('processing', function(file){
            showAlert("Processing upload file", "info");
            return true;
        });
        this.on('queuecomplete', function(){
            return true;
        });
        this.on('success',function(file, result){
            // changing src of preview element
            file.previewElement.querySelector("img").src = CONFIG.storage_url + result.path;

            showAlert("Complete upload file", "success");

            image_uploaded.push(result.path);
            $("#result-upload").val( JSON.stringify(image_uploaded) );
            
            return true;
        });
    }
});

$('.btn-edit-expend').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="no_transaction"]').val(result.data.no_transaction);
                $('input[name="expend_date"]').val(result.data.expend_date);
                $('input[name="amount"]').val(result.data.amount.format());
                $('input[name="images"]').val(result.data.images);
                $('textarea[name="information"]').val(result.data.information);
                $("#form-bank_id").val(result.data.bank_id).trigger('change');
                $("#form-branch_id").val(result.data.branch_id).trigger('change');
                $("#form-employee_id").val(result.data.employee_id).trigger('change');
                $("#form-category_expend_id").val(result.data.category_expend_id).trigger('change');

                $.each(result.data.images_dropzone, function(i,v){
                    var mockFile = {name: v.name, size: v.size};
                    dropzone_upload.options.addedfile.call(dropzone_upload, mockFile);
                    dropzone_upload.options.thumbnail.call(dropzone_upload, mockFile, v.url);
                    mockFile.previewElement.classList.add('dz-success');
                    mockFile.previewElement.classList.add('dz-complete');
                    image_uploaded.push(v.name);
                });
            
                // dropzone_upload.init();
                
                $('#form-expend').attr('action', result.url_edit);
                $('#form-expend').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});