$('.btn-edit-supplier').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);
                $('textarea[name="address"]').val(result.data.address);
                $('input[name="phone"]').val(result.data.phone);
                $('input[name="sales_phone"]').val(result.data.sales_phone);
                $('input[name="sales_name"]').val(result.data.sales_name);
                $('textarea[name="information"]').val(result.data.information);
                $('#supplier-status').prop('checked', result.data.status ? true : false);
                $('#supplier-has_tax').prop('checked', result.data.has_tax ? true : false);
                
                $('#form-supplier').attr('action', result.url_edit);
                $('#form-supplier').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});