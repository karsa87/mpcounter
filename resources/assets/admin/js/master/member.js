$('.btn-edit-member').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);
                $('textarea[name="address"]').val(result.data.address);
                $('input[name="phone"]').val(result.data.phone);
                $('input[name="identity"]').val(result.data.identity);
                $('input[name="birthdate"]').val(result.data.birthdate);
                $('input[name="member_number"]').val(result.data.member_number);
                $('input[name="max_day_debt"]').val(result.data.max_day_debt);
                $('input[name="max_debt"]').val(result.data.max_debt);
                $('#member-status').prop('checked', result.data.status ? true : false);
                $("#form-identity_type").val(result.data.identity_type).trigger('change');
                $("#form-type").val(result.data.type).trigger('change');
                
                $('#form-member').attr('action', result.url_edit);
                $('#form-member').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});