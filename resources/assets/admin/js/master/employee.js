$('.btn-edit-employee').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);
                $('textarea[name="address"]').val(result.data.address);
                $('input[name="phone"]').val(result.data.phone);
                $('input[name="identifier"]').val(result.data.employee_manager);
                $('input[name="email"]').val(result.data.email);
                $('input[name="username"]').val(result.data.user.username);
                $('input[name="user_id"]').val(result.data.user.id);
                $("#employee-type_identifier").val(result.data.type_identifier).trigger('change');
                $('#employee-status').prop('checked', result.data.status ? true : false);
                $("#form-user_role").val(result.data.user.roles).trigger('change');
                $("#form-branch_id").val(result.data.branch_id).trigger('change');
                
                $('#form-employee').attr('action', result.url_edit);
                $('#form-employee').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

$('#btn-show-password').click(function(){
    if( $(this).hasClass('btn-secondary') ) {
        $(this).addClass('btn-default');
        $(this).removeClass('btn-secondary');

        $('input[name="password"]').attr('type','password');
    }else{
        $(this).addClass('btn-secondary');
        $(this).removeClass('btn-default');
        $('input[name="password"]').attr('type','text');
    }
});