$('.btn-edit-bank').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);
                $('input[name="account_number"]').val(result.data.account_number);
                $('input[name="name_owner"]').val(result.data.name_owner);
                $('#bank-status').prop('checked', result.data.status ? true : false);
                $('#bank-is_default').prop('checked', result.data.is_default ? true : false);
                
                $('#form-bank').attr('action', result.url_edit);
                $('#form-bank').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});