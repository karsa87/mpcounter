if($("div#files-upload").length > 0) {
    var image_uploaded = [];
    var dropfile = $("div#files-upload");
    var href = dropfile.attr("data-href");

    var dropzone_upload = new Dropzone("div#files-upload", {
        url: href,
        acceptedFiles: "image/*",
        maxFilesize: 2,
        maxFiles: 5,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': CONFIG.token
        },
        init: function() { /* event listeners for removed files from dropzone*/
            var result_upload = $("#result-upload").val();
            if(result_upload !== "" && result_upload !== undefined && result_upload !== null){
                datum = JSON.parse(result_upload);
                var dropzone = this;
                datum.forEach(function(img_path, index){
                    var mockFile = { name: img_path, size: 32000 };
                    dropzone.options.addedfile.call(dropzone, mockFile);
                    dropzone.options.thumbnail.call(dropzone, mockFile, CONFIG.storage_url + img_path);
                    mockFile.previewElement.classList.add('dz-success');
                    mockFile.previewElement.classList.add('dz-complete');
                    image_uploaded.push(img_path);
                });
            }

            this.on("removedfile", function(file) {
                if ((file.new_name != undefined && file.new_name != '') 
                        || (file.name != undefined && file.name != '')){
                    var name = (file.new_name != undefined && file.new_name != '') ? file.new_name : file.name;
                    var index = image_uploaded.indexOf(name);
                    if (index > -1) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': CONFIG.token
                            },
                            type: 'DELETE',
                            url: href,
                            data: {
                                file: name,
                            },
                            dataType: 'json',
                            success: function(result){
                                if (typeof result == 'string')
                                    result = $.parseJSON(result);
                                if (result.success){
                                    image_uploaded.splice(index, 1);
                                    
                                    $("#result-upload").val(JSON.stringify(image_uploaded));
                                }
                            }
                        });
                    }
                }
            });
            this.on('processing', function(file){
                showAlert("Processing upload file", "info");
                return true;
            });
            this.on('queuecomplete', function(){
                return true;
            });
            this.on('success',function(file, result){
                // changing src of preview element
                file.previewElement.querySelector("img").src = CONFIG.storage_url + result.path;

                showAlert("Complete upload file", "success");

                image_uploaded.push(result.path);
                $("#result-upload").val( JSON.stringify(image_uploaded) );
                
                return true;
            });
        }
    });
}

$('.btn-edit-user').click(function(e){
    e.preventDefault();

    var btn = $(this);
    
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.user.id !== undefined)
            {
                $('input[name="id"]').val(result.user.id);
                $('input[name="name"]').val(result.user.name);
                $('input[name="username"]').val(result.user.username);
                $('input[name="email"]').val(result.user.email);

                // $("#form-user_domain_id").val(result.user.domains).trigger('change');
                
                $('#user-status').prop('checked', result.user.status ? true : false);
                $("#form-user_role").val(result.user.roles).trigger('change');
                $("#form-user_permission").val(result.user.permissions).trigger('change');
                $("#form-branch_id").val(result.user.branch_id).trigger('change');
                
                $('#form-user').attr('action', result.url_edit);
                $('#form-user').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

$('#btn-show-password').click(function(){
    if( $(this).hasClass('btn-secondary') ) {
        $(this).addClass('btn-default');
        $(this).removeClass('btn-secondary');

        $('input[name="password"]').attr('type','password');
    }else{
        $(this).addClass('btn-secondary');
        $(this).removeClass('btn-default');
        $('input[name="password"]').attr('type','text');
    }
});