$('.btn-edit-setting').click(function(e){
    e.preventDefault();

    var btn = $(this);
    
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);

                if ( $('input[name="key"]').length > 0 ) {
                    $('input[name="key"]').val(result.data.key);
                }

                if ( result.data.type == 1 ) {
                    $('#form_value_number').val(result.data.value);
                } else {
                    $('#form_value').val(result.data.value);
                    tinyMCE.activeEditor.setContent(result.data.value);
                }
                showValue(result.data.type);

                if ( $("#form-type").length > 0 ) {
                    $("#form-type").val(result.data.type);
                }

                $('#setting-status').prop('checked', result.data.status ? true : false);
                
                $('#form-setting').attr('action', result.url_edit);
                $('#form-setting').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

tinymce.init({
    selector: 'textarea#form_value',
    height: 200,
    menubar: false,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | fontsizeselect |' +
    'bold italic underline strikethrough | alignleft aligncenter ' +
    'alignright alignjustify ' +
    'removeformat | help',
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : '',  
    content_css: '//www.tiny.cloud/css/codepen.min.css',
    fontsize_formats: "6pt 8pt 10pt 12pt 14pt 18pt 24pt 36pt"
});

$("#form-type").change(function() {
    showValue($(this).val());
});

$("#form-type").change();

function showValue(type){
    var el = tinymce.get('form_value');
    if (type == 1) {
        if (el != null) {
            el.hide();
            $('#form_value').hide();
        }

        $('#form_value_number').show();
    } else {
        if (el != null) {
            el.show();
        }

        $('#form_value_number').hide();
    }
}