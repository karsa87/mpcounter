window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.$ = window.jQuery = require('jquery');
window.moment = require('moment');

dropzone = Dropzone = require('dropzone');
Dropzone.autoDiscover = false;

require('daterangepicker');
require('bootstrap-datepicker');
require('bootstrap-datetime-picker');
require('../../vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle');
require('overlayscrollbars');
require('../../vendor/almasaeed2010/adminlte/plugins/select2/js/select2.full');
window.Swal = require('../../vendor/almasaeed2010/adminlte/plugins/sweetalert2/sweetalert2.all');
require('../../vendor/almasaeed2010/adminlte/plugins/chart.js/Chart.min');
require('../../vendor/almasaeed2010/adminlte/dist/js/adminlte');
require('../../vendor/almasaeed2010/adminlte/dist/js/demo');
require('../../vendor/almasaeed2010/adminlte/plugins/ekko-lightbox/ekko-lightbox.min');

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });

(function($, window, document, undefined)
{
    $.fn.formatNumber = function(){
        var _multi = this;
        _multi.unbind('keyup.formatNumber');
        var f = _multi.bind('keyup.formatNumber',function(){
            var val = $(this).val().parseFloatId();
            if( val ) {
                val = val.format();
            }
            $(this).val(val);
        });
        _multi.keyup();
        return f;
    };
})(window.jQuery || window.Zepto, window, document);

Number.prototype.format = function (fix)
{
    return this.toFixed(fix ? fix : 0).replace(/\d(?=(\d{3})+$)/g, '$&.');
};

String.prototype.parseFloatId = function()
{
    var string = this.concat().replace(/[^-0-9\,\.]/g, '');
    if (!string) {
        return '';
    }
    return typeof string === 'string' && string !== '' && string !== '-' ? parseFloat(string.replace(/[,.]/g, '')) : string;
};

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}