require('./bootstrap');

window.CONFIG = {
    path: $('meta[name=path]').attr("content"),
    full_url: $('meta[name=full]').attr("content"),
    token: $('meta[name=csrf-token]').attr("content"),
    base_url: $('meta[name=base]').attr("content"),
    referrer: $('meta[name=referrer]').attr("content"),
    storage_url: $('meta[name=storage_url]').attr("content"),
};

window.Vue = require('vue');
window.trans = string => _.get(window.i18n, string);
window.has_permission = string => _.get(window.posp, string);
Vue.prototype.trans = string => _.get(window.i18n, string);
Vue.prototype.has_permission = string => _.get(window.posp, string);
Vue.component('ListProduct', require('./components/transaction/purchase_order/LIST_PRODUCT.vue').default);
Vue.component('ListProductSo', require('./components/transaction/sales_order/LIST_PRODUCT.vue').default);
Vue.component('ListProductMp', require('./components/transaction/mutation_product/LIST_PRODUCT.vue').default);
Vue.component('ListProductRpo', require('./components/transaction/retur_purchase_order/LIST_PRODUCT_PO.vue').default);
Vue.component('ListProductRso', require('./components/transaction/retur_sales_order/LIST_PRODUCT_SO.vue').default);
Vue.component('ListProductPromo', require('./components/master/promo/LIST_PRODUCT.vue').default);
Vue.component('ListProductIdentity', require('./components/master/product/LIST_PRODUCT_IDENTITY.vue').default);
Vue.component('ListProductIdentityStockOpname', require('./components/transaction/stock_opname/LIST_PRODUCT_IDENTITY.vue').default);
Vue.component('ListFormProductIdentityStockOpname', require('./components/transaction/stock_opname/LIST_FORM_PRODUCT_IDENTITY.vue').default);
Vue.component('Pagination', require('./components/plugins/PAGINATION.vue').default);
Vue.component('Select2', require('./components/plugins/select2.vue').default);
// require('./components/plugins/select2.vue');

const app = new Vue({
    el: '#app',
});

//Initialize Select2 Elements
$('.select2').select2({
    allowClear: true
});

$(".ajax-select2").each(function(i) {
    $(this).select2({
        ajax: {
            headers: {
                'X-CSRF-TOKEN': CONFIG.token
            },
            url: $(this).data('url'),
            processResults: function(data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: data.items
                };
            }
        }
    });
});

//sweetalert
window.showProgress = function()
{
    let timerInterval
    Swal.fire({
        title: 'Wait!!!',
        // timer: 2000,
        timerProgressBar: true,
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                        b.textContent = Swal.getTimerLeft()
                    }
                }
            }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            // console.log('I was closed by the timer')
        }
    })
}

$('.btn-delete').click(function(){
    showSweetAlert($(this));
})

function showSweetAlert(el)
{
    var url = el.attr('href');
    if(url == undefined || url == null || url == ""){
        url = el.attr('data-href');
    }

    var title = el.attr('data-title');
    if(title == undefined || title == null || title == ""){
        title = "Are you sure?";
    }

    var description = el.attr('data-description');
    if(description == undefined || description == null || description == ""){
        description = "";
    }

    var icon = el.attr('data-icon');
    if(icon == undefined || icon == null || icon == ""){
        icon = "warning";
    }

    var msg_success = el.attr('data-message-success');
    if(msg_success == undefined || msg_success == null || msg_success == ""){
        msg_success = "Success delete";
    }

    var msg_failed = el.attr('data-message-failed');
    if(msg_failed == undefined || msg_failed == null || msg_failed == ""){
        msg_failed = "Failed delete";
    }

    var csrf_token = el.attr('data-csrf-token');
    if(csrf_token == undefined || csrf_token == null || csrf_token == ""){
        csrf_token = "";
    }

    var method = el.attr('data-method');
    if(method == undefined || method == null || method == ""){
        method = "DELETE";
    }

    var param = el.attr('data-param');
    if(param == undefined || param == null || param == ""){
        param = "{}";
    }

    const swal_confirm = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success ml-1',
          cancelButton: 'btn btn-danger mr-1'
        },
        buttonsStyling: false
    })

    swal_confirm.fire({
        title: title,
        text: description,
        type: icon,      
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        showLoaderOnConfirm: true,
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': CONFIG.token
                },
                url: url, 
                type: method, 
                data: JSON.parse(param),
                success: function(result){
                    if(result.status == "1" || result.status == 1){
                        swal_confirm.fire({
                            title: 'Deleted!',
                            type: 'success',
                            onClose: () => {
                                location.reload();
                            }
                        })
                    } else {
                        location.reload();
                    }
                },
                beforeSend: function( xhr ) {
                    showProgress();
                }
            })
            .always(function(result) {
                Swal.close();
            })
            .fail(function() {
                swal_confirm.fire(
                    "Can't delete",
                    msg_failed,
                    'error'
                )
            });
        }
    })
}

window.Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.showAlert = function(message, type)
{
    Toast.fire({
        type: type,
        title: message
    })
}

$('button.btn-loader').mouseup(function(e){
    e.stopPropagation();
    // $(this).click();
    showProgress();
});

$('a.btn-loader').mouseup(function(e){
    e.stopPropagation();
    // $(this).click();
    showProgress();
});

$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayBtn: "linked",
    todayHighlight: true
});

$('.datetimepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    todayHighlight: true
});

$('.format-number').formatNumber();

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
        alwaysShowClose: true
    });
});

$('.date-rangepicker').daterangepicker({
    autoUpdateInput: false,
    locale: {
        format: 'YYYY-MM-DD',
        cancelLabel: 'Clear'
    }
});

$('.date-rangepicker').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
});

$('.date-rangepicker').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
});

$('.datetime-rangepicker').daterangepicker({
    autoUpdateInput: false,
    timePicker24Hour: true,
    timePicker: true,
    locale: {
        format: 'YYYY-MM-DD HH:mm',
        cancelLabel: 'Clear'
    }
});

$('.datetime-rangepicker').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm') + ' - ' + picker.endDate.format('YYYY-MM-DD HH:mm'));
});

$('.datetime-rangepicker').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
});

$(".sorting-table").each(function(i) {
    var form = $(this).data('form');
    if (form !== undefined) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        var orderBy = urlParams.get('_s_ob');
        orderBy = orderBy != null ? orderBy : '';
        var sorting = urlParams.get('_s_s');
        sorting = sorting != null ? sorting : '';

        $("#" + form).append('<input type="hidden" name="_s_ob" id="'+ form +'-ob" value="'+ orderBy +'"><input type="hidden" name="_s_s"  id="'+ form +'-s" value="'+ sorting +'">');
    }
});

$(".sorting-form").each(function(i, el){
    var form = $(this).closest(".sorting-table").data('form');
    var column = $(this).data('column');
    
    if(form !== undefined && column !== undefined) 
    {
        var orderBy = $("#"+ form +"-ob").val();
        var sorting = $("#"+ form +"-s").val();
        
        if(orderBy == column) {
            if (sorting == 1) {
                $(this).prepend("<i class='fas fa-sort-down m-1'></i>");
            } else {
                $(this).prepend("<i class='fas fa-sort-up m-1'></i>");
            }
        } else {
            $(this).prepend("<i class='fas fa-sort m-1'></i>");
        }
    }
});

$(".sorting-form").click(function() {
    var form = $(this).closest(".sorting-table").data('form');
    var column = $(this).data('column');

    if(form !== undefined && column !== undefined) 
    {
        var elemt_i = $(this).find( "i" );
        if (elemt_i.length > 0) {
            elemt_i.remove();
        }

        if (elemt_i.hasClass('fa-sort-up')) { //DESC
            $("#"+ form +"-s").val(1);
        } else {
            $("#"+ form +"-s").val(0);
        }

        $("#"+ form +"-ob").val(column);
        $("#"+ form).submit();
    }
});

$('[data-toggle="tooltip"]').tooltip()