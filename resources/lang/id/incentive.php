<?php

$lang = [
    'title' => 'Insentif',
    
    'label' => [
        'bank_id' => 'Bank', 
        'branch_id' => 'Cabang', 
        'no_transaction' => 'No Transaksi', 
        'date_transaction' => 'Tanggal', 
        'investor' => 'Penanggung Jawab', 
        'type' => 'Tipe', 
        'information' => 'Keterangan', 
        'amount' => 'Total Uang'
    ],
    
    'placeholder' => [
        'no_transaction' => 'ex: INC-0001', 
        'date_transaction' => 'ex: 2020-03-05', 
        'investor' => 'ex: Agus', 
        'information' => 'ex: Penambahan modal', 
        'amount' => 'ex: 1,000,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>incentive</code>',
        'description_list' => 'Daftar <code>incentive</code>',
        'desc_type' => 'Penguarangan: kurangi saldo bank, Penambahan: tambah saldo bank'
    ],

    'list' => [
        'type' => ['Pengurangan', 'Penambahan']
    ]
];

$lang['errors'] = [
    'date_transaction.required' => sprintf('%s harap diisi', $lang["label"]["date_transaction"]),
    'date_transaction.date_format' => sprintf('%s format tanggal salah', $lang["label"]["date_transaction"]),
    'bank_id.required' => sprintf('%s harap diisi', $lang["label"]["bank_id"]),
    'branch_id.required' => sprintf('%s harap diisi', $lang["label"]["branch_id"]),
    'no_transaction.required' => sprintf('%s harap diisi', $lang["label"]["no_transaction"]),
    'amount.numeric' => sprintf('%s format nomor salah', $lang["label"]["amount"]),
    'amount.min' => sprintf('%s minmal 0', $lang["label"]["amount"]),
];

return $lang;