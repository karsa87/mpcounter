<?php

$lang = [
    'title' => 'Kategori Pengeluaran',
    
    'label' => [
        'name' => 'Nama',
        'status' => 'Status',
    ],
    
    'placeholder' => [
        'name' => 'ex: Operasional',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>kategori pengeluaran</code>',
        'description_list' => 'Daftar <code>kategori pengeluaran</code>',
    ],

    'list' => [
        'status' => ['Tidak aktif', 'Aktif'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
];

return $lang;