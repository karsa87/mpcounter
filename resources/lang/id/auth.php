<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Masuk untuk memulai sesi Anda',
    'failed' => 'Kredensial ini tidak cocok dengan catatan kami.',
    'throttle' => 'Terlalu banyak upaya masuk. Silakan coba lagi dalam :seconds detik.',

    'message' => [
        'password_not_correct' => 'Kata sandi salah',
        'username_not_found' => 'Username tidak ditemukan',
        'username_deactived' => 'Username tidak aktif',
    ],
    
    'validation' => [
        'email' => [
            'required' => 'Silahkan masukkan kata sandi'
        ],
        'password' => [
            'required' => 'Harap berikan kata sandi'
        ],
    ],

    'meta' => [
        'title' => 'Login',
        'description' => 'Global POS is system retail management',
        'keyword' => 'pos, point of sales, point, of, sales, mochamad karsa syaifudin jupri, mochamad, karsa, syaifudin, jupri, wikacell, system, retail, management, system retail management'
    ]
];
