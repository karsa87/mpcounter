<?php

$lang = [
    'title' => 'Merek',
    
    'label' => [
        'name' => 'Nama',
        'status' => 'Status',
    ],
    
    'placeholder' => [
        'name' => 'ex : Samsung',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>merek</code>',
        'description_list' => 'Daftar <code>merek</code>',
    ],

    'list' => [
        'status' => ['Tidak aktif', 'Aktif'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
];

return $lang;