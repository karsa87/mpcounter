<?php

$lang = [
    'title' => 'Pegawai',
    
    'label' => [
        'name' => 'Nama',
        'contact' => 'Kontak',
        'email' => 'Email',
        'phone' => 'Telepon',
        'address' => 'Alamat',
        'type_identifier' => 'Jenis Identifier',
        'identifier' => 'Identifier',
        'status' => 'Status',
        'username' => 'Username',
        'password' => 'Kata Sandi',
        'information' => 'Keterangan',
        'role' => 'Role',
        'branch_id' => 'Cabang',
    ],
    
    'placeholder' => [
        'name' => 'ex: Agus',
        'email' => 'ex: agus@gmail.com',
        'phone' => 'ex 08983939393',
        'address' => 'ex: Jl. margonda',
        'identifier' => 'ex: 129487274820234',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>employee</code>',
        'description_list' => 'Daftar <code>employee</code>',
        'desc_user_role' => 'Hak akses untuk karyawan yang masuk'
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'identifier' => ['KTP', 'SIM', 'NPWP'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'name.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["name"]),
    'address.required' => sprintf('%s harap diisi', $lang["label"]["address"]),
    'phone.required' => sprintf('%s harap diisi', $lang["label"]["phone"]),
    'phone.max' => sprintf('%s maksimal 16 karakter', $lang["label"]["phone"]),
    'phone.regex' => sprintf('Format nomor telepon salah', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('Format nomor telepon salah', $lang["label"]["phone"]),
    'email.required' => sprintf('%s harap diisi', $lang["label"]["email"]),
    'email.unique' => sprintf('%s sudah ada', $lang["label"]["email"]),
    'email.email' => sprintf('Format email salah', $lang["label"]["email"]),
    'username.required' => sprintf('%s harap diisi', $lang["label"]["username"]),
    'username.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["username"]),
    'username.unique' => sprintf('%s is already taken', $lang["label"]["username"]),
    'password.required' => sprintf('%s harap diisi', $lang["label"]["password"]),
    'password.min' => sprintf('%s minimal diisi 4 karakter', $lang["label"]["password"]),
    'branch_id.required' => sprintf('%s harap diisi', $lang["label"]["branch_id"]),
];

return $lang;