<?php

$lang = [
    'title' => "Permission",
    'label' => [
        'id' => 'ID',
        'name' => 'Nama',
        'slug' => 'Route Name',
        'description' => 'Deskripsi',
        'model' => 'Model',
        'level' => 'Level',
        'permission' => 'Permission',
        'menu' => "Menu",
    ],
    'placeholder' => [
        'id' => 'ID',
        'name' => 'ex: Add Permission',
        'slug' => 'setting.permission.store',
        'description' => 'Can add permission',
        'level' => 'Level',
        'model' => 'Model',
    ],
    'list' => [],
    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>permission</code>',
        'description_list' => 'Daftar <code>permission</code>',
        'route_not_found' => 'Route tidak ditemukan',
    ],
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'slug.required' => sprintf('%s harap diisi', $lang["label"]["slug"]),
];

return $lang;