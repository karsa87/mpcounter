<?php

$lang = [
    'title' => "User",

    'placeholder' => [
        'id' => 'ID',
        'name' => 'ex: Agus',
        'username' => 'ex : agus',
        'email' => 'agus@gmail.com',
        'images' => 'Gambar',
        'lastlogin' => 'Last Login',
        'lastloginip' => 'Last Login IP',
        'status' => 'Status',
        'domain_id' => 'Domain',
        'role' => 'Role',
        'permission' => 'Permission',
        'password' => 'ex: password',
    ],
    'list' => [
        'status' => ['Tidak Aktif', 'Aktif']
    ],
    'message' => [
        'description_find' => 'Form <code>search</code> for get <code>user</code>',
        'description_list' => 'Daftar <code>user</code>',
        'failed_status' => 'Failed <code>%s</code>',
        'success_status' => 'Successfully <code>%s</code>',
        'not_found' => 'User <code>%s</code> not found',
    ],
    'label' => [
        'id' => 'ID',
        'name' => 'Name',
        'username' => 'Username',
        'email' => 'Email',
        'images' => 'Gambar',
        'lastlogin' => 'Last Login',
        'lastloginip' => 'Last Login IP',
        'status' => 'Status',
        'domain_id' => 'Domain',
        'role' => 'Role',
        'permission' => 'Permission',
        'password' => 'Password',
        'change_status' => 'Change Status',
        'branch_id' => 'Branch',
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'username.required' => sprintf('%s harap diisi', $lang["label"]["username"]),
    'password.required' => sprintf('%s harap diisi', $lang["label"]["password"]),
    'password.min' => sprintf('%s minimal 5 character', $lang["label"]["password"]),
    'email.email' => sprintf('%s invalid format email', $lang["label"]["email"]),
];

return $lang;