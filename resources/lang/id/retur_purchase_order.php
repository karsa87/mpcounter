<?php

$lang = [
    'title' => 'Pengembalian Pembelian',
    
    'label' => [
        'no_retur' => 'No Pengembalian', 
        'date' => 'Tanggal', 
        'purchase_order_id' => 'Pembelian', 
        'supplier_id' => 'Supplier', 
        'branch_id' => 'Cabang', 
        'bank_id' => 'Bank', 
        'information' => 'Keterangan', 
        'total_amount' => 'Total', 
        'images' => 'Gambar',
        'product_id' => 'Produk',
        'qty' => 'Qty', 
        'sell_price' => 'Harga Jual', 
        'cost_of_goods' => 'Harga Pokok Penjualan',
        'is_retur' => 'Is Return',
        'information' => 'Keterangan',
        'list_product' => 'List Produk',
        'purchase_price' => 'Harga Beli',
        'total' => 'Total',
        'sub_total' => 'Sub Total',
        'subtotal' => 'Subtotal',
        'pay' => 'Bayar',
        'return_money' => 'Uang Kembali',
        'identity' => 'Identity',
        'print_nota' => 'Cetak Nota',
        'retur_product' => 'Pengembalian Produk'
    ],
    
    'placeholder' => [
        'no_retur' => 'ex: 20200316/RPO-00001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'product_id' => 'ex: Samsung',
        'qty' => 'ex: 1', 
        'sell_price' => 'ex: 1,000,000', 
        'cost_of_goods' => 'ex: 1,000,000',
        'information' => 'ex: This order for Product A',
        'supplier_id' => 'ex: PT. XXX',
        'bank_id' => 'ex: BCX',
        'branch_id' => 'ex: Branch Suhat',
        'purchase_order_id' => 'ex: 20200316/PO-00001', 
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>purchase order</code>',
        'description_list' => 'Daftar <code>purchase order</code>',
        'description_delete' => 'Apakah Anda yakin akan menghapus item ini?',
        'error_add_qty' => 'Produk ini memiliki jumlah maksimum 1',
        'error_max_qty' => 'Produk ini memiliki jumlah maksimum',
        'error_duplicate_identity' => 'Identitas rangkap',
        'error_required_identity' => 'Identitas diperlukan',
        'error_less_down_payment' => 'Uang dibayarkan kurang %s',
        'change_so' => 'Anda yakin mengubah Pesanan Pembelian?',
    ],

    'list' => [
        'status' => ['Not Yet Paid', 'Paid'],
        'type' => ['CASH', 'DEBT'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('%s format tanggal salah harusnya Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('%s harap diisi', $lang["label"]["date"]),
    'purchase_order_id.required' => sprintf('%s harap diisi', $lang["label"]["purchase_order_id"]),
    'bank_id.required' => sprintf('%s harap diisi', $lang["label"]["bank_id"]),
    'supplier_id.required' => sprintf('%s harap diisi', $lang["label"]["supplier_id"]),
    'branch_id.required' => sprintf('%s harap diisi', $lang["label"]["branch_id"]),
    'total_amount.numeric' => sprintf('%s format nomor salah', $lang["label"]["total_amount"]),
];

return $lang;