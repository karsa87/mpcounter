<?php

$lang = [
    'title' => 'Pengeluaran',
    
    'label' => [
        'date' => 'Tanggal',
        'expend_type' => 'Jenis Pengeluaran',
        'no_transaction' => 'No Transaksi',
        'amount' => 'Jumlah Uang',
        'information' => 'Keterangan',
        'bank_id' => 'Bank',
        'branch_id' => 'Cabang',
        'total' => 'Total',
        'category_expend_id' => 'Kategori Pengeluaran',
    ],
    
    'placeholder' => [
        'date' => 'ex: 2020-04-14 21:04 - 2020-04-14 21:04',
    ],

    'message' => [
    ],

    'list' => [
        'expend_type' => [
            'Semua', 
            'Pembelian',
            'Pembayaran Hutang Pembelian',
            'Pengembalian Penjualan',
            'Pengurangan Insentif',
            // 'Pengeluaran',
        ],
    ]
];

return $lang;