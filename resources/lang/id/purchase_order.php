<?php

$lang = [
    'title' => 'Pembelian',
    
    'label' => [
        'no_invoice' => 'No Faktur', 
        'no_reference' => 'No Referensi', 
        'date' => 'Tanggal', 
        'type' => 'Jenis', 
        'date_max_payable' => 'Jatuh Tempo',
        'images' => 'Image Proof',
        'supplier_id' => 'Supplier',
        'branch_id' => 'Cabang',
        'bank_id' => 'Bank',
        'total_amount' => 'Jumlah Total',
        'down_payment' => 'Uang Muka',
        'paid_off' => 'Terbayarkan',
        'status' => 'Status',
        'purchase_order_id' => 'Pembelian',
        'product_id' => 'Produk',
        'qty' => 'Qty', 
        'sell_price' => 'Harga Jual', 
        'cost_of_goods' => 'Harga Poko Penjualan',
        'is_retur' => 'Dikembalikan',
        'information' => 'Keterangan',
        'list_product' => 'List Produk',
        'purchase_price' => 'Harga Beli',
        'sell_price' => 'Harga Jual',
        'total' => 'Total',
        'sub_total' => 'Sub Total',
        'subtotal' => 'Subtotal',
        'pay' => 'Bayar',
        'return_money' => 'Uang Kembalian',
        'identity' => 'Identity',
        'print_nota' => 'Cetak Nota',
        'buy_product' => 'Produk yang dibeli',
        'remaining_debt' => 'Sisa Hutang',
        'pic' => 'PIC',
        'debt_payment' => 'Pembayaran Hutang',
    ],
    
    'placeholder' => [
        'no_invoice' => 'ex: 20200316/PO-00001', 
        'no_reference' => 'ex: FK-0001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'date_max_payable' => 'ex: 2020-03-16 10:00',
        'product_id' => 'ex: Samsung',
        'qty' => 'ex: 1', 
        'sell_price' => 'ex: 1,000,000', 
        'cost_of_goods' => 'ex: 1,000,000',
        'information' => 'ex: This order for Product A',
        'identity' => 'Identity (ex: 9921987123198712)',
        'expired_date' => 'ex: 2020-04-12',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>purchase order</code>',
        'description_list' => 'Daftar <code>purchase order</code>',
        'description_delete' => 'Apakah Anda yakin akan menghapus item ini?',
        'error_add_qty' => 'Produk ini memiliki jumlah maksimum 1',
        'error_duplicate_identity' => 'Identitas rangkap',
        'error_required_identity' => 'Identitas diperlukan',
        'error_less_down_payment' => 'Kurang dari %s uang dibayarkan',
        'error_detail' => 'Pilih produk minimal 1',
        'error_cant_edit' => "Tidak dapat mengedit pesanan pembelian ini karena memiliki riwayat pengembalian",
        'desc_type' => 'CASH: pembayaran langsung terbayar, HUTANG: pembayaran berjangka',
    ],

    'list' => [
        'status' => ['Belum lunas', 'Lunas'],
        'type' => ['CASH', 'DEBT', 'Transfer'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('%s format tanggal salah harusnya Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('%s harap diisi', $lang["label"]["date"]),
    'bank_id.required' => sprintf('%s harap diisi', $lang["label"]["bank_id"]),
    'supplier_id.required' => sprintf('%s harap diisi', $lang["label"]["supplier_id"]),
    'branch_id.required' => sprintf('%s harap diisi', $lang["label"]["branch_id"]),
    'total_amount.numeric' => sprintf('%s format nomor salah', $lang["label"]["total_amount"]),
    'date_max_payable.required_if' => sprintf('kolom %s harus diisi jika %s %s.', $lang["label"]["date_max_payable"], $lang["label"]["type"], $lang["list"]["type"][1]),
];

return $lang;