<?php

$lang = [
    'title' => 'Pemasukkan',
    
    'label' => [
        'date' => 'Tanggal',
        'branch_id' => 'Cabang',
        'income_type' => 'Jenis Pemasukkan',
        'no_transaction' => 'No Transaksi',
        'amount' => 'Total Uang',
        'information' => 'Keterangan',
        'bank_id' => 'Bank',
        'branch_id' => 'Cabang',
        'total' => 'Total',
        'sell_price' => 'Harga Jual'
    ],
    
    'placeholder' => [
        'date' => 'ex: 2020-04-14 21:04 - 2020-04-14 21:04',
    ],

    'message' => [
    ],

    'list' => [
        'income_type' => [
            'Semua', 
            'Penjualan',
            'Pembayaran Hutang Penjualan',
            'Pengembalian Pembelian',
            'Penambahan Insentif',
            'Pulsa',
        ],
    ]
];

return $lang;