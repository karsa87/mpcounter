<?php

$lang = [
    'title' => 'Member',
    
    'label' => [
        'name' => 'Nama',
        'identity_type' => 'Tipe Identity',
        'identity' => 'Identity',
        'address' => 'Alamar',
        'phone' => 'Telepon',
        'birthdate' => 'Tanggal Lahir',
        'poin' => 'Poin',
        'member_number' => 'No. Member',
        'type' => 'Jenis',
        'max_day_debt' => 'Maksimal Hutang (Hari)',
        'max_debt' => 'Maksimal Hutang (Nominal)',
        'status' => 'Status',
        'member' => 'Member',
    ],
    
    'placeholder' => [
        'name' => 'ex: Agus',
        'identity' => 'ex: 78243893992892',
        'address' => 'ex: Jl. A. Yani',
        'phone' => 'ex: 089297339292',
        'birthdate' => 'ex: 1996-03-21',
        'member_number' => 'ex: 98298329012',
        'max_day_debt' => 'ex: 0',
        'max_debt' => 'ex: 1,000,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>member</code>',
        'description_list' => 'Daftar <code>member</code>',
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'identity_type' => [
            1 => 'Student Card', 
            2 => 'KTP',
            3 => 'SIM',
            4 => 'PASSPORT',
        ],
        'type' => [
            1 => 'Umum', 
            2 => 'Reseller 1',
            3 => 'Reseller 2',
        ],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'name.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["name"]),
    'address.required' => sprintf('%s harap diisi', $lang["label"]["address"]),
    'phone.required' => sprintf('%s harap diisi', $lang["label"]["phone"]),
    'phone.max' => sprintf('%s maksimal diisi 16 karakter', $lang["label"]["phone"]),
    'phone.regex' => sprintf('Format telepon salah', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('Format telepon salah', $lang["label"]["phone"]),
    'identity_type.required' => sprintf('%s harap diisi', $lang["label"]["identity_type"]),
    'identity.required' => sprintf('%s harap diisi', $lang["label"]["identity"]),
    'member_number.required' => sprintf('%s harap diisi', $lang["label"]["member_number"]),
    'max_day_debt.numeric' => sprintf('%s format nomor salah', $lang["label"]["max_day_debt"]),
    'max_day_debt.min' => sprintf('%s minimal diisi 0', $lang["label"]["max_day_debt"]),
    'max_debt.numeric' => sprintf('%s format nomor salah', $lang["label"]["max_debt"]),
    'max_debt.min' => sprintf('%s minimal diisi 0', $lang["label"]["max_debt"]),
];

return $lang;