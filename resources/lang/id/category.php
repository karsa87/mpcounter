<?php

$lang = [
    'title' => 'Kategori',
    
    'label' => [
        'name' => 'Nama',
        'status' => 'Status',
        'identifier' => 'Identifier',
        'identifier_info' => 'Info Identifier',
    ],
    
    'placeholder' => [
        'name' => 'ex : Makanan',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>kategori</code>',
        'description_list' => 'Daftar <code>kategori</code>',
        'desc_identifier' => 'Pilih jenis pengidentifikasi untuk produk kategori:<br><ol><li><strong>Tidak:</strong> untuk kategori produk yang tidak memiliki identitas unik</li><li><strong>1 Identifier 1 Produk:</strong> untuk kategori produk yang memiliki identitas unik yang berlaku untuk 1 produk saja. Contoh: <strong>IMEI</strong></li><li><strong>Multi Identifier 1 Produk:</strong> untuk kategori produk yang memiliki banyak identitas unik. Contoh: <strong>BARCODE SNACK</strong></li></ol>'
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'identifier' => ['Tidak', '1 Identifier 1 Produk', 'Multi Identifier 1 Produk'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'identifier.required' => sprintf('%s harap diisi', $lang["label"]["identifier"]),
];

return $lang;