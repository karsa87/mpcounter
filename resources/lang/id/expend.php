<?php

$lang = [
    'title' => 'Pengeluaran',
    
    'label' => [
        'no_transaction' => 'No Transaksi',
        'expend_date' => 'Tanggal Pengeluaran',
        'bank_id' => 'Bank',
        'branch_id' => 'Cabang', 
        'category_expend_id' => 'Kategori Pengeluaran',
        'employee_id' => 'Penanggung Jawab',
        'amount' => 'Total Uang',
        'information' => 'Keterangan',
        'images' => 'Gambar',
        'date' => 'Tanggal',
    ],
    
    'placeholder' => [
        'no_transaction' => 'ex: EXP-0000001',
        'expend_date' => 'ex: 2020-03-01 19:00',
        'amount' => 'ex: 1.000.000',
        'information' => 'ex: Gaji Maret 2020',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>pengeluaran</code>',
        'description_list' => 'Daftar <code>pengeluaran</code>',
    ],

    'list' => [
    ]
];

$lang['errors'] = [
    'no_transaction.required' => sprintf('%s harap diisi', $lang["label"]["no_transaction"]),
    'expend_date.required' => sprintf('%s harap diisi', $lang["label"]["expend_date"]),
    'expend_date.date_format' => sprintf('%s format tanggal salah', $lang["label"]["expend_date"]),
    'bank_id.required' => sprintf('%s harap diisi', $lang["label"]["bank_id"]),
    'branch_id.required' => sprintf('%s harap diisi', $lang["label"]["branch_id"]),
    'category_expend_id.required' => sprintf('%s harap diisi', $lang["label"]["category_expend_id"]),
    'amount.numeric' => sprintf('%s format nomor salah', $lang["label"]["amount"]),
];

return $lang;