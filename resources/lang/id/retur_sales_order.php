<?php

$lang = [
    'title' => 'Pengembalian Penjualan',
    
    'label' => [
        'no_retur' => 'No Pengembalian', 
        'date' => 'Tanggal', 
        'sales_order_id' => 'Penjualan', 
        'member_id' => 'Nama', 
        'branch_id' => 'Cabang', 
        'bank_id' => 'Bank', 
        'information' => 'Keterangan', 
        'total_amount' => 'Total', 
        'images' => 'Gambar',
        'product_id' => 'Produk',
        'qty' => 'Qty', 
        'sell_price' => 'Harga Jual', 
        'cost_of_goods' => 'Harga Pokok Penjualan',
        'is_retur' => 'Is Retur',
        'information' => 'Keterangan',
        'list_product' => 'List Produk',
        'total' => 'Total',
        'sub_total' => 'Sub Total',
        'subtotal' => 'Subtotal',
        'pay' => 'Pay',
        'return_money' => 'Uang Kembali',
        'identity' => 'Identity',
        'print_nota' => 'Cetak Nota',
        'phone' => 'Telepon',
        'address' => 'Alamat',
        'discount_promo' => 'Diskon',
        'total_discount_detail' => 'Total Diskon Promo',
        'retur_product' => 'Pengembalian Produk'
    ],
    
    'placeholder' => [
        'no_retur' => 'ex: 20200316/RPO-00001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'product_id' => 'ex: Samsung',
        'qty' => 'ex: 1', 
        'sell_price' => 'ex: 1,000,000', 
        'cost_of_goods' => 'ex: 1,000,000',
        'information' => 'ex: This order for Product A',
        'member_id' => 'ex: Agus',
        'bank_id' => 'ex: BCX',
        'branch_id' => 'ex: Branch Suhat',
        'sales_order_id' => 'ex: 20200316/SO-00001', 
        'phone' => 'ex: 089883888xxx',
        'address' => 'ex: Jl. Malang',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>sales order</code>',
        'description_list' => 'Daftar <code>sales order</code>',
        'description_delete' => 'Apakah Anda yakin akan menghapus item ini?',
        'error_add_qty' => 'Produk ini memiliki jumlah maksimum 1',
        'error_max_qty' => 'Produk ini memiliki jumlah maksimum',
        'error_duplicate_identity' => 'Identitas rangkap',
        'error_required_identity' => 'Identitas diperlukan',
        'error_less_down_payment' => 'Uang dibayarkan kurang %s',
        'error_detail' => 'Pilih produk minimal 1',
        'change_so' => 'Anda yakin mengubah Pesanan Penjualan?',
    ],

    'list' => [
        'status' => ['Belum Lunas', 'Lunas'],
        'type' => ['CASH', 'DEBT'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('%s format tanggal salah harusnya Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('%s harap diisi', $lang["label"]["date"]),
    'sales_order_id.required' => sprintf('%s harap diisi', $lang["label"]["sales_order_id"]),
    'bank_id.required' => sprintf('%s harap diisi', $lang["label"]["bank_id"]),
    'member_id.required' => sprintf('%s harap diisi', $lang["label"]["member_id"]),
    'branch_id.required' => sprintf('%s harap diisi', $lang["label"]["branch_id"]),
    'total_amount.numeric' => sprintf('%s format nomor salah', $lang["label"]["total_amount"]),
];

return $lang;