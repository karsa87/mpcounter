<?php

$lang = [
    'title' => 'Promo',
    
    'label' => [
        'code' => 'Code', 
        'name' => 'Nama', 
        'information' => 'Keterangan', 
        'period' => 'Periode', 
        'date_start' => 'Dari', 
        'date_end' => 'Ke', 
        'category_id' => 'Kategori', 
        'brand_id' => 'Brand', 
        'discount' => 'Diskon', 
        'discount_percent' => '(%)', 
        'discount_price' => '(Harga)', 
        'status' => 'Status', 
        'images' => 'Gambar',
        'product' => 'Produk',
        'after_discount' => 'Diskon',
        'selected_product' => 'Pilih Produk',
    ],
    
    'placeholder' => [
        'code' => 'ex: DC-0001',
        'name' => 'ex: Promo Valentine',
        'period' => 'ex: 2020-03-29 14:00 - 2020-04-10 00:00',
        'discount_percent' => 'ex: 40', 
        'discount_price' => 'ex: 50,000', 
        'information' => 'ex: this promo for valentine day', 
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>promo</code>',
        'description_list' => 'Daftar <code>promo</code>',
        'error_choose_one' => 'Pilih satu diskon (%) atau (Harga)',
        'desc_select_product' => 'Pilih produk yang akan didiskon',
        'desc_category_id' => 'Pilih kategori, jika Anda ingin mendiskon semua produk dari kategori',
        'desc_brand_id' => 'Pilih merek, jika Anda ingin mendiskon semua produk merek',
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
    ]
];

$lang['errors'] = [
    'code.required' => sprintf('%s harap diisi', $lang["label"]["code"]),
    'code.unique' => sprintf('%s sudah ada', $lang["label"]["code"]),
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'name.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["name"]),
    'category_id.required' => sprintf('%s harap diisi', $lang["label"]["category_id"]),
    'date_start.required' => sprintf('%s harap diisi', $lang["label"]["date_start"]),
    'date_start.date_format' => sprintf('%s format tanggal salah', $lang["label"]["period"]),
    'date_end.required' => sprintf('%s harap diisi', $lang["label"]["date_end"]),
    'date_end.date_format' => sprintf('%s format tanggal salah', $lang["label"]["period"]),
    'discount_percent.numeric' => sprintf('%s format nomor salah', $lang["label"]["discount_percent"]),
    'discount_percent.min' => sprintf('%s minimal diisi 1', $lang["label"]["discount_percent"]),
    'discount_percent.max' => sprintf('%s maksimal diisi 100', $lang["label"]["discount_percent"]),
    'discount_price.numeric' => sprintf('%s format tanggal salah', $lang["label"]["discount_price"]),
    'discount_price.min' => sprintf('%s minimal diisi 1', $lang["label"]["discount_price"]),
    'products.required' => sprintf('Pilih minimal 1 produk jika kategori kosong', $lang["label"]["product"]),
    'period.required' => sprintf('%s harap diisi', $lang["label"]["period"]),
];

return $lang;