<?php

$lang = [
    'title' => 'Riwayat Bank',
    
    'label' =>  [
        'log_datetime' => 'Tanggal Riwayat', 
        'user_id' => 'User', 
        'bank_id' => 'Bank', 
        'transaction_id' => 'Transaksi ID', 
        'transaction_type' => 'Tipe Transaksi', 
        'type' => 'Jenis', 
        'information' => 'Keterangan', 
        'debit_amount' => 'Uang Masuk', 
        'credit_amount' => 'Uang Keluar', 
        'amount_before' => 'Saldo Sebelumnya', 
        'amount_after' => 'Saldo Selanjutnya', 
        'table' => 'Tabel'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>log transaksi bank</code>',
        'description_list' => 'Daftar <code>log transaksi bank</code>',
    ],

    'list' => [
        'type' => [
            'Credit',
            'Debit',
        ]
    ]
];

return $lang;