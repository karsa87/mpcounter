<?php

$lang = [
    'title' => 'Cabang',
    
    'label' => [
        'name' => 'Nama',
        'phone' => 'Telepon',
        'address' => 'Alamat',
        'employee_id' => 'Manager Cabang',
        'status' => 'Status',
    ],
    
    'placeholder' => [
        'name' => 'ex: Cabang Suhat',
        'phone' => 'ex: 08983939393',
        'address' => 'ex: Jl. margonda',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>cabang</code>',
        'description_list' => 'Daftar <code>cabang</code>',
        'desc_employee_id' => 'Daftar karyawan'
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'name.max' => sprintf('%s is 255 max character', $lang["label"]["name"]),
    'address.required' => sprintf('%s harap diisi', $lang["label"]["address"]),
    'phone.required' => sprintf('%s harap diisi', $lang["label"]["phone"]),
    'phone.max' => sprintf('%s is 16 max character', $lang["label"]["phone"]),
    'phone.regex' => sprintf('%s is invalid format phone', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('%s is invalid format phone', $lang["label"]["phone"]),
    'employee_id.required' => sprintf('%s harap diisi', $lang["label"]["employee_id"]),
];

return $lang;