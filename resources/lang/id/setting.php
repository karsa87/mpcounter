<?php

$lang = [
    'title' => 'Setting',
    
    'label' => [
        'name' => 'Name', 
        'key' => 'Key', 
        'value' => 'Value', 
        'status' => 'Status', 
        'type' => 'Type'
    ],
    
    'placeholder' => [
        'name' => 'ex: Header nota small SO',
        'key' => 'ex: HEADER_NOTA_SMALL_SO',
        'value' => 'ex: Agus',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>setting</code>',
        'description_list' => 'Daftar <code>bank</code>',
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'type' => ['Text', 'Number'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'key.required' => sprintf('%s harap diisi', $lang["label"]["key"]),
    'key.unique' => sprintf('%s is already exists', $lang["label"]["key"]),
    'value.required' => sprintf('%s harap diisi', $lang["label"]["value"]),
];

return $lang;