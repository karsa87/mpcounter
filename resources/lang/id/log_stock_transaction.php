<?php

$lang = [
    'title' => 'Riwayat Stok',
    
    'label' =>  [
        'log_datetime' => 'Tanggal Riwayat', 
        'user_id' => 'User', 
        'stock_id' => 'Stok', 
        'product_id' => 'Produk', 
        'branch_id' => 'Cabang', 
        'transaction_id' => 'Transaksi ID', 
        'transaction_type' => 'Transaksi Type', 
        'type' => 'Tipe', 
        'information' => 'Keterangan', 
        'stock_in' => 'Stok Masuk', 
        'stock_out' => 'Stok Keluar', 
        'stock_before' => 'Stok Sebelumnya', 
        'stock_after' => 'Stok Setelahnya', 
        'table' => 'Tabel'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>log stok transaksi</code>',
        'description_list' => 'Daftar <code>log stok transaksi</code>',
    ],

    'list' => [
        'type' => [
            'Keluar',
            'Masuk',
        ]
    ]
];

return $lang;