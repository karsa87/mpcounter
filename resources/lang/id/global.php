<?php 

return [
    "save"=> "Simpan",
    "edit"=> "Edit",
    "show_detail" => "Tampil Detail",
    "detail" => "Detail",
    "reset" => "Reset",
    "cancel"=> "Batal",
    "ok"=> "OK",
    "show_all"=> "Tampil Semua",
    "search"=> "Cari",
    "export"=> "Ekspor",
    "delete"=> "Hapus",
    "close" => "Tutup",
    "disable" => "Nonaktifkan",
    "assign"=> "Tetapkan",
    "download"=> "Unduh",
    "sign_in" => "Masuk",
    "open"=> "Buka",
    "select"=> "Pilih",
    "select_all"=> "Pilih Semua",
    "diselect"=> "Batalkan Pilihan",
    "diselect_all"=> "Batalkan Pilihan Semua",
    "add" => "Tambah",
    "back"=> "Kembali",
    "update"=> "Edit",
    "no"=> "No",
    "action"=> "Tidakan",
    "first" => "Awal",
    "prev"=> "Sebelumnya",
    "next"=> "Selanjutnya",
    "last"=> "Terakhir",
    "showing" => "Menampilkan <strong>%d</strong> - <strong>%d</strong> dari <strong>%d</strong> data",
    "find"=> "Cari",
    "keyword" => "Kata Kunci",
    "column"=> "Kolom",
    "add" => "Tambah",
    "edit"=> "Edit",
    "all" => "Semua",
    'from' => 'Dari',
    "to"=> "Ke",
    "start" => "Mulai",
    "end" => "Akhir",
    "date"=> "Tanggal",
    "result"=> "Hasil",
    "list"=> "Daftar",
    "table" => "Tabel",
    "report" => "Laporan",
    'form' => 'Form',
    "success_save"=> "Berhasil menyimpan",
    "failed_save" => "Gagal menyimpan",
    "success_delete"=> "Berhasil dihapus",
    "failed_delete" => "Gagal dihapus",
    "success_update"=> "Berhasil diperbarui",
    "failed_update" => "Pembaruan gagal",
    "error_switch_domain" => "Gagal beralih domain",
    "success_switch_domain" => "Berhasil beralih domain",
    "unique_with" => ":attribute sudah diambil",
    "not_found" => "Tidak ditemukan",
    "data_not_available" => "Data tidak tersedia",
    "last_update" => "Terakhir diperbarui",
    "before" => "Sebelum",
    "after" => "Setelah",
    'click' => 'Klik',
    'access' => 'Mengakses',
    'id' => 'ID',
    'total' => 'Total',

    'menu' => [
        'dashboard' => 'Dashboard',
        "settings" => 'Pengaturan',
        "master" => 'Master',
        "transaction" => "Transaksi",
        "finance" => 'Keuangan',
        "log" => 'Riwayat',
        "setting/setting" => 'Pengaturan Default', 
        "setting/menu" => 'Menu', 
        "setting/permission" => 'Permission', 
        "setting/role" => 'Role', 
        "setting/user" => 'User', 
        "master/category" => 'Kategori', 
        "master/brand" => 'Merek', 
        "master/product" => 'Produk', 
        "master/bank" => 'Bank', 
        "master/branch" => 'Cabang',
        "master/supplier" => 'Supplier',
        "master/member" => 'Member',
        "master/employee" => 'Pegawai',
        "master/promo" => 'Promo',
        "transaction/purchase-order" => 'Pembelian',
        "transaction/sales-order" => 'Penjualan',
        "transaction/retur-purchase-order" => 'Pengembalian Pembelian',
        "transaction/retur-sales-order" => 'Pengembalian Penjualan',
        "finance/category_expend" => 'Kategori Pengeluaran',
        "finance/expend" => 'Pengeluaran',
        "finance/incentive" => 'Insentif',
        "finance/debt-payment-po" => 'Pembayaran Hutang Pembelian',
        "finance/debt-payment-so" => 'Pembayaran Hutang Penjualan',
        "log/log_history" => 'Riwayat',
        "log/log-stock-transaction" => 'Riwayat Stok',
        "log/log_bank_transaction" => 'Riwayat Bank',
        "report" => 'Laporan',
        "report/stock" => 'Laporan Stok',
        "report/sales-order" => 'Laporan Penjualan',
        "report/purchase-order" => 'Laporan Pembelian',
        "report/expend-all" => 'Laporan Pengeluaran Uang',
        "report/income-all" => 'Laporan Pemasukkan Uang',
        "report/financial-statement" => 'Laporan Keuangan',
        "report/loss-profit" => 'Laporan Laba Rugi',
        "report/product-identity" => 'Laporan Produk Identity',
        "transaction/mutation-product" => 'Mutasi Barang',
        "transaction/stock-opname" => 'Stock Opname',
        "master/product-identity" => 'Kode Produk',
        'report/supplier-tax' => 'Pajak Supplier',
        'report/income-expend' => 'Pemasukkan dan Pengeluaran',
        'transaction/pulse' => 'Pulsa',
    ],

    'array' => [
        'operator' => ['Semua', '=', '>', '<', '>=', '<='],
        'days' => [1=> 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
        'months' => [1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        'sorting' => ['ASC', 'DESC']
    ],

    'message' => [
        'desc_status' => 'Aktif atau tidak aktif, jika aktif dapat menggunakannya pada transaksi lain dan jika dinonaktifkan tidak dapat menggunakannya pada transaksi lain, '
    ]
];