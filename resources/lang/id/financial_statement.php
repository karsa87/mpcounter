<?php

$lang = [
    'title' => 'Laporan Keuangan',
    
    'label' => [
        'period' => 'Periode',
        'month' => 'Bulan',
        'year' => 'Tahun',
        'bank_id' => 'Bank',
        'print_date' => 'Tanggal Cetak',
        'detail' => 'Detail',
        'sales_order' => 'Penjualan',
        'purchase_order' => 'Pembelian',
        'loss_profi' => 'Laba dan Rugi',
        'discount' => 'Diskon',
        'incentive' => 'Insentif',
        'decrease' => 'Pengurangan',
        'increase' => 'Penambahan',
        'expend' => 'Pengeluaran',
        'return_po' => 'Pengembalian Pembelian',
        'return_so' => 'Pengembalian Penjualan',
        'dso' => 'Pembayaran Hutang Penjualan',
        'dpo' => 'Pembayaran Hutang Pembelian',
        'total_income_expend' => 'Total Pengeluaran dan Pemasukkan',
        'total_po_first' => 'Total Awal',
        'total_po_paid' => 'Total Terbayar',
        'total_po_not_paid' => 'Total Belum Terbayar',
        'total_po' => 'Total',
        'total_so_first' => 'Total Awal',
        'total_so_paid' => 'Total Terbayar',
        'total_so_not_paid' => 'Total Belum Terbayar',
        'total_so' => 'Total Penjualan',
        'total_return_po' => 'Total Pengembalian Pembelian',
        'total_return_so' => 'Total Pengembalian Penjualan',
        'total_discount_promo' => 'Diskon Promo',
        'total_discount_sales' => 'Diskon Sales',
        'total_discount_detail' => 'Diskon Produk',
        'total_shipping_cost' => 'Biaya Pengiriman',
        'total_additional_cost' => 'Biaya Lain-lain',
        'total_discount' => 'Total Diskon',
        'total_add_cost' => 'Total Biaya Lain - lain',
        'add_cost' => 'Biaya Logistik',
        'total_expend' => 'Total Pengeluaran',
        'current_balance' => 'Saldo total saat ini',
        'pulse' => 'Pulsa',
    ],
    
    'placeholder' => [
        'period' => 'ex: January',
        'month' => 'ex: January',
        'year' => 'ex: 2020',
        'bank_id' => 'ex: BCA',
    ],

    'message' => [
        
    ],

    'list' => [
        
    ]
];

return $lang;