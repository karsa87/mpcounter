<?php

$lang = [
    'title' => 'Supplier',
    
    'label' => [
        'name' => 'Nama',
        'address' => 'Alamat',
        'phone' => 'Telepon',
        'sales_name' => 'Nama Sales',
        'sales_phone' => 'Telepon Sales',
        'information' => 'Keterangan',
        'status' => 'Status',
        'has_tax' => 'Pajak'
    ],
    
    'placeholder' => [
        'name' => 'ex: PT. XXX',
        'phone' => 'ex: (0341) 7726192',
        'address' => 'ex: Jl. A. Yani',
        'sales_name' => 'ex: Agus',
        'sales_phone' => 'ex: 089297339292',
        'information' => 'ex: Supplier Barang A',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>supplier</code>',
        'description_list' => 'Daftar <code>supplier</code>',
        'desc_has_tax' => 'Jika supplier memiliki pajak yang harus dilaporkan'
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'has_tax' => ['Tidak', 'Ya'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'name.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["name"]),
    'address.required' => sprintf('%s harap diisi', $lang["label"]["address"]),
    'phone.required' => sprintf('%s harap diisi', $lang["label"]["phone"]),
    'phone.max' => sprintf('%s maksimal diisi 16 karakter', $lang["label"]["phone"]),
    'phone.regex' => sprintf('%s format telepon salah', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('%s format telepon salah', $lang["label"]["phone"]),
    'sales_name.required' => sprintf('%s harap diisi', $lang["label"]["sales_name"]),
    'sales_phone.required' => sprintf('%s harap diisi', $lang["label"]["sales_phone"]),
    'sales_phone.max' => sprintf('%s maksimal diisi 16 karakter', $lang["label"]["sales_phone"]),
    'sales_phone.regex' => sprintf('%s format telepon salah', $lang["label"]["sales_phone"]),
    'sales_phone.note_regex' => sprintf('%s format telepon salah', $lang["label"]["sales_phone"]),
];

return $lang;