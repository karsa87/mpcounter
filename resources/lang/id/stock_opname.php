<?php

$lang = [
    'title' => 'Stock Opname',
    
    'label' => [
        'title' => 'Judul', 
        'month' => 'Bulan', 
        'year' => 'Tahun', 
        'branch_id' => 'Cabang', 
        'product_id' => 'Produk', 
        'product_identity_id' => 'Identititas', 
        'created_by' => 'Dibuat Oleh', 
        'updated_by' => 'Diupdate Oleh', 
        'information' => 'Informasi',
        'status' => 'Status',
        'stock_on_hand' => 'Stok di gudang',
        'stock_on_system' => 'Stok di sistem',
        'product' => 'Produk',
        'change_waiting' => 'Proses',
        'change_process' => 'Terverfikasi',
        'revert_verified' => 'Batalkan verikasi',
        'print' => 'Print',
        'input_stock' => 'Masukkan stok',
    ],
    
    'placeholder' => [
        'title' => 'ex: stock opname june', 
        'month' => 'ex: juni', 
        'year' => 'ex: 2020', 
        'branch_id' => 'ex: Gudang Malang', 
        'information' => 'ex: some product missing'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>stock opname</code>',
        'description_list' => 'List data <code>stock opname</code>',
        'success_change_waiting' => 'dalam proses :name',
        'success_change_process' => 'Terverifikasi :name',
        'success_revert_verified' => 'Sukses membatalkan :name',
        'failed_change_waiting' => 'Gagal memproses :name',
        'failed_change_process' => 'Gagal memverifikasi :name',
        'failed_revert_verified' => 'Gagal membatalkan :name',
    ],

    'list' => [
        'status' => ['Menunggu', 'Dalam Proses', 'Terverifikasi'],
        'match_status' => ['Tidak Cocok', 'Cocok'],
    ]
];

$lang['errors'] = [
    'title.required' => sprintf('kolom %s harus diisi', $lang["label"]["title"]),
    'month.required' => sprintf('kolom %s harus diisi', $lang["label"]["month"]),
    'year.required' => sprintf('kolom %s harus diisi', $lang["label"]["year"]),
    'branch_id.required' => sprintf('kolom %s harus diisi', $lang["label"]["branch_id"]),
];

return $lang;