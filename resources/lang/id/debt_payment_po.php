<?php

$lang = [
    'title' => 'Pembayaran Hutang Pembelian',
    
    'label' => [
        'no_debt' => 'No Hutang', 
        'purchase_order_id' => 'Pembelian', 
        'bank_id' => 'Bank', 
        'date' => 'Tanggal', 
        'information' => 'Keterangan', 
        'images' => 'Gambar', 
        'paid' => 'Bayar', 
        'another_paid' => 'Pembayaran Lain', 
        'total_paid' => 'Total Terbayar', 
        'remaining_debt' => 'Sisa Hutang',
        'total' => 'Total',
    ],
    
    'placeholder' => [
        'no_debt' => 'ex: DPO-0001', 
        'date' => 'ex: 2020-04-11 12:04', 
        'information' => 'Ex: Debt Payment PO for PO-200411/000001', 
        'paid' => 'ex: 1,000,000', 
        'total_paid' => 'ex: 1,000,000', 
        'bank_id' => 'ex: KAS', 
        'remaining_debt' => 'ex: 1,000,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>pembayaran hutang pembelian</code>',
        'description_list' => 'Daftar <code>pembayaran hutang pembelian</code>',
    ],

    'list' => [
        
    ]
];

$lang['errors'] = [
    'date.required' => sprintf('%s harap diisi', $lang["label"]["date"]),
    'date.date_format' => sprintf('%s format tanggal salah', $lang["label"]["date"]),
    'bank_id.required' => sprintf('%s harap diisi', $lang["label"]["bank_id"]),
    'purchase_order_id.required' => sprintf('%s harap diisi', $lang["label"]["purchase_order_id"]),
    'no_debt.required' => sprintf('%s harap diisi', $lang["label"]["no_debt"]),
    'paid.numeric' => sprintf('%s format nomor salah', $lang["label"]["paid"]),
];

return $lang;