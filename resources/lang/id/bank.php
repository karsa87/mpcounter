<?php

$lang = [
    'title' => 'Bank',
    
    'label' => [
        'name' => 'Nama',
        'account_number' => 'Nomor rekening',
        'account_bank' => 'Akun bank',
        'name_owner' => 'Nama Pemilik',
        'saldo' => 'Saldo',
        'status' => 'Status',
        'is_default' => 'Default',
    ],
    
    'placeholder' => [
        'name' => 'ex: BCA',
        'account_number' => 'ex: 332298432',
        'name_owner' => 'ex: Agus',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>bank</code>',
        'description_list' => 'Daftar <code>bank</code>',
        'description_is_default' => 'Aktifkan untuk menjadikannya bank default dari setiap transaksi, Hanya 1 bank yang dapat ditetapkan ke default'
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'default' => ['No', 'Ya'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'is_default.unique' => sprintf('%s default bank sudah ada', $lang["label"]["is_default"]),
    'account_number.numeric' => sprintf('%s format nomor salah', $lang["label"]["account_number"]),
    'status.in' => sprintf("%s tidak dapat dinonaktifkan, karena catatan ini adalah bank default", $lang["label"]["status"]),
];

return $lang;