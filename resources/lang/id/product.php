<?php

$lang = [
    'title' => 'Produk',
    
    'label' => [
        'code' => 'Code',
        'name' => 'Nama',
        'category_id' => 'Kategori',
        'brand_id' => 'Merek',
        'price' => 'Harga',
        'cost_of_goods' => 'Harga Pokok penjualan (HPP)',
        'sell_price' => 'Harga Jual',
        'images' => 'Gambar',
        'information' => 'Keterangan',
        'status' => 'Status',
        'unit' => 'Unit',
        'product_identity' => 'Produk Identity',
        'available_stock' => 'Stok Tersedia',
        'stock' => 'Stok',
        'branch_id' => 'Cabang',
        'reseller1_sell_price' => 'Reseller 1 Sell Price', 
        'reseller2_sell_price' => 'Reseller 2 Sell Price',
        'with_expired_date' => 'Tanggal Kadaluarsa',
        'expired_date' => 'Tanggal Kadaluarsa',
        'gross_profit' => 'Laba Kotor',
        'net_profit' => 'Laba Bersih',
        'tax' => 'Pajak',
    ],
    
    'placeholder' => [
        'code' => 'ex: PR-0001',
        'name' => 'ex: Product A',
        'cost_of_goods' => 'ex: 10,000',
        'sell_price' => 'ex: 15,000',
        'information' => 'ex: product distribution',
        'reseller1_sell_price' => 'ex: 14,000', 
        'reseller2_sell_price' => 'ex: 13,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>product</code>',
        'description_list' => 'Daftar <code>product</code>',
        'desc_cost_of_goods' => 'Harga pokok penjualan (HPP) adalah semua biaya langsung yang dikeluarkan untuk mendapatkan penjualan barang atau jasa.',
        'desc_with_expired_date' => 'Nyalakan untuk memberikan tanggal kedaluwarsa pada produk ini',
    ],

    'list' => [
        'status' => ['Tidak Aktif', 'Aktif'],
        'unit' => ['Pcs', 'Box', 'Kg'],
        'expired' => ['Tidak', 'Ya'],
    ]
];

$lang['errors'] = [
    'code.required' => sprintf('%s harap diisi', $lang["label"]["code"]),
    'code.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["code"]),
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'name.max' => sprintf('%s maksimal diisi 255 karakter', $lang["label"]["name"]),
    'category_id.required' => sprintf('%s harap diisi', $lang["label"]["category_id"]),
    'brand_id.required' => sprintf('%s harap diisi', $lang["label"]["brand_id"]),
    'sell_price.numeric' => sprintf('%s format nomor salah', $lang["label"]["sell_price"]),
    'sell_price.min' => sprintf('%s minimal 0', $lang["label"]["sell_price"]),
];

return $lang;