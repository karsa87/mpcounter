<?php

$lang = [
    'title' => 'Mutasi Produk',
    
    'label' => [
        'no_mutation' => 'No Mutation', 
        'date' => 'Tanggal', 
        'employee_id' => "Pegawai", 
        'from_branch_id' => "Dari Cabang", 
        'to_branch_id' => "Ke Cabang", 
        'shipping_cost' => "Biaya Pengiriman", 
        'additional_cost' => "Biaya Lain-lain", 
        'created_by' => "Dibuat Oleh", 
        'updated_by' => "Diedit Oleh", 
        'print_nota' => "Cetak Nota", 
        'information' => "Keterangan", 
        'product_id' => 'Produk', 
        'product_identity_id' => 'Dari Product Identity', 
        'to_product_identity_id' => 'Ke Product Identity', 
        'qty' => 'Qty',
        'identity' => 'Identity',
        'select_product' => 'Pilih Produk',
        'total_amount' => 'Total',
        'sell_price' => 'Harga Jual',
        'total' => 'Total',
        'subtotal' => 'Subtotal',
    ],
    
    'placeholder' => [
        'no_mutation' => 'ex: MP-00001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'employee_id' => "ex: agus", 
        'from_branch_id' => "ex: Cabang Suhat", 
        'to_branch_id' => "ex: Cabang Batu", 
        'shipping_cost' => 'ex: 1,000,000', 
        'additional_cost' => 'ex: 1,000,000',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>purchase order</code>',
        'description_list' => 'Daftar <code>purchase order</code>',
        'description_delete' => 'Apakah Anda yakin akan menghapus item ini?',
        'error_max_qty' => 'Produk ini adalah jumlah maksimum',
        'error_buy_qty' => 'Produk ini dapat dibeli maksimal 1',
        'error_duplicate_identity' => 'Identitas rangkap',
        'error_required_identity' => 'Identitas harus diisi',
        'error_less_down_payment' => 'Kurang dari %s yang terbayarkan',
        'error_detail' => 'Pilih produk minimal 1',
        'member_only' => 'Hanya anggota yang dapat menggunakan poin',
        'point_not_enough' => 'Poin anggota tidak cukup',
        'error_cant_edit' => "Tidak dapat mengedit pesanan pembelian ini karena memiliki riwayat pengembalian",
        'desc_type' => 'CASH: pembayaran langsung terbayar, DEBT: pembayaran berjangka',
        'same_branch' => 'Tidak dapat memilih cabang yang sama',
        'change_branch' => 'Anda telah memilih beberapa produk, jika Anda mengganti cabang maka produk yang dipilih akan dihapus',
        'sure' => 'Apakah anda yakin ?',
    ],

    'list' => [
        'status' => ['Not Yet Paid', 'Paid'],
        'type' => ['CASH', 'DEBT'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('%s is invalid format date Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('%s harap diisi', $lang["label"]["date"]),
    'from_branch_id.required' => sprintf('%s harap diisi', $lang["label"]["from_branch_id"]),
    'to_branch_id.required' => sprintf('%s harap diisi', $lang["label"]["to_branch_id"]),
    'employee_id.required' => sprintf('%s harap diisi', $lang["label"]["employee_id"]),
    'shipping_cost.numeric' => sprintf('%s format nomor salah', $lang["label"]["shipping_cost"]),
    'additional_cost.numeric' => sprintf('%s format nomor salah', $lang["label"]["additional_cost"]),
];

return $lang;