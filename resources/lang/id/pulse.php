<?php

$lang = [
    'title' => 'Pulsa',
    
    'label' => [
        'no_transaction' => 'No Transaksi',
        'date' => 'Tanggal',
        'bank_id' => 'Bank',
        'branch_id' => 'Cabang', 
        'total_pulse' => 'Pulsa',
        'capital_price' => 'Harga Modal',
        'sell_price' => 'Harga Jual',
        'created_by' => 'Dibuat Oleh',
        'updated_by' => 'Diedit Oleh',
        'phone' => 'No Telp.',
        'information' => 'Informasi',
    ],
    
    'placeholder' => [
        'no_transaction' => 'ex: EXP-0000001',
        'date' => 'ex: 2020-03-01 19:00',
        'phone' => 'ex: 08982338xxxxx',
        'total_pulse' => 'ex: 1.000.000',
        'sell_price' => 'ex: 1.000.000',
        'capital_price' => 'ex: 1.000.000',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>pulse</code>',
        'description_list' => 'List data <code>pulse</code>',
    ],

    'list' => [
    ]
];

$lang['errors'] = [
    'no_transaction.required' => sprintf('kolom %s harus diisi', $lang["label"]["no_transaction"]),
    'date.required' => sprintf('kolom %s harus diisi', $lang["label"]["date"]),
    'date.date_format' => sprintf('kolom %s format tanggal salah', $lang["label"]["date"]),
    'bank_id.required' => sprintf('kolom %s harus diisi', $lang["label"]["bank_id"]),
    'branch_id.required' => sprintf('kolom %s harus diisi', $lang["label"]["branch_id"]),
    'total_pulse.numeric' => sprintf('kolom %s format nomor salah', $lang["label"]["total_pulse"]),
    'total_pulse.min' => sprintf('kolom %s minimal is 5000', $lang["label"]["total_pulse"]),
    'sell_price.numeric' => sprintf('kolom %s format nomor salah', $lang["label"]["sell_price"]),
    'sell_price.min' => sprintf('kolom %s minimal is 0', $lang["label"]["sell_price"]),
    'capital_price.numeric' => sprintf('kolom %s format nomor salah', $lang["label"]["capital_price"]),
    'capital_price.min' => sprintf('kolom %s minimal is 0', $lang["label"]["capital_price"]),
    'phone.required' => sprintf('kolom %s harus diisi', $lang["label"]["phone"]),
    'phone.max' => sprintf('kolom %s maksimal 16 karakter', $lang["label"]["phone"]),
    'phone.regex' => sprintf('kolom %s format nomor telepon salah', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('kolom %s format nomor telepon salah', $lang["label"]["phone"]),
];

return $lang;