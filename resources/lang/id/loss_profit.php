<?php

$lang = [
    'title' => 'Laba dan Rugi',
    
    'label' => [
        'period' => 'Periode',
        'month' => 'Bulan',
        'year' => 'Tahun',
        'bank_id' => 'Bank',
        'branch_id' => 'Cabang',
        'print_date' => 'Tanggal Cetak',
        'sales_order' => 'Penjualan',
        'discount' => 'Diskon',
        'incentive' => 'Insentif',
        'decrease' => 'Pengurangan',
        'expend' => 'Penambahan',
        'total_so' => 'Total Penjualan',
        'total_discount_promo' => 'Diskon Promo',
        'total_discount_sales' => 'Diskon Sales',
        'total_discount' => 'Total Diskon',
        'total_expend' => 'Total Pengeluaran',
        'current_balance' => 'Saldo total saat ini',
        'gross_profit' => 'Laba Kotor',
        'total_gross_profit' => 'Total Laba Kotor',
        'total_incentive_increase' => 'Total Penambahan Insentif'
    ],
    
    'placeholder' => [
        'period' => 'ex: January',
        'month' => 'ex: January',
        'year' => 'ex: 2020',
        'bank_id' => 'ex: BCA',
    ],

    'message' => [
        
    ],

    'list' => [
        
    ]
];

return $lang;