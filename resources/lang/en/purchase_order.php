<?php

$lang = [
    'title' => 'Purchase Order',
    
    'label' => [
        'no_invoice' => 'No Invoice', 
        'no_reference' => 'No Reference', 
        'date' => 'Date', 
        'type' => 'Type', 
        'date_max_payable' => 'Payment Due',
        'images' => 'Image Proof',
        'supplier_id' => 'Supplier',
        'branch_id' => 'Branch',
        'bank_id' => 'Bank',
        'total_amount' => 'Total Amount',
        'down_payment' => 'Down Payment',
        'paid_off' => 'Paid Off',
        'status' => 'Status',
        'purchase_order_id' => 'Purchase Order',
        'product_id' => 'Product',
        'qty' => 'Qty', 
        'sell_price' => 'Sell Price', 
        'cost_of_goods' => 'Cost Of Goods',
        'is_retur' => 'Is Retur',
        'information' => 'Information',
        'list_product' => 'List Product',
        'purchase_price' => 'Purchase Price',
        'sell_price' => 'Sell Price',
        'total' => 'Total',
        'sub_total' => 'Sub Total',
        'subtotal' => 'Subtotal',
        'pay' => 'Pay',
        'return_money' => 'Retur Money',
        'identity' => 'Identity',
        'print_nota' => 'Print Nota',
        'buy_product' => 'Product Purchased',
        'remaining_debt' => 'Remaining Debt',
        'pic' => 'PIC',
        'debt_payment' => 'Debt Payment',
    ],
    
    'placeholder' => [
        'no_invoice' => 'ex: 20200316/PO-00001', 
        'no_reference' => 'ex: FK-0001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'date_max_payable' => 'ex: 2020-03-16 10:00',
        'product_id' => 'ex: Samsung',
        'qty' => 'ex: 1', 
        'sell_price' => 'ex: 1,000,000', 
        'cost_of_goods' => 'ex: 1,000,000',
        'information' => 'ex: This order for Product A',
        'identity' => 'Identity (ex: 9921987123198712)',
        'expired_date' => 'ex: 2020-04-12',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>purchase order</code>',
        'description_list' => 'List data <code>purchase order</code>',
        'description_delete' => 'Are you sure for delete this item ?',
        'error_add_qty' => 'This product has a maximum quantity of 1',
        'error_duplicate_identity' => 'Duplicate identity',
        'error_required_identity' => 'A identity is required',
        'error_less_down_payment' => 'Less than %s money paid',
        'error_detail' => 'Select min 1 product',
        'error_cant_edit' => "Can not edit this purchase order because it has a history of returns",
        'desc_type' => 'CASH: direct payment paid off, DEBT: term payments',
    ],

    'list' => [
        'status' => ['Not Yet Paid', 'Paid'],
        'type' => ['CASH', 'DEBT', 'Transfer'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('A %s is invalid format date Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('A %s is required', $lang["label"]["date"]),
    'bank_id.required' => sprintf('A %s is required', $lang["label"]["bank_id"]),
    'supplier_id.required' => sprintf('A %s is required', $lang["label"]["supplier_id"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
    'total_amount.numeric' => sprintf('A %s is invalid format number', $lang["label"]["total_amount"]),
    'date_max_payable.required_if' => sprintf('The %s field is required when %s is %s.', $lang["label"]["date_max_payable"], $lang["label"]["type"], $lang["list"]["type"][1]),
];

return $lang;