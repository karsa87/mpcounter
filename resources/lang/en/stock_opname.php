<?php

$lang = [
    'title' => 'Stock Opname',
    
    'label' => [
        'title' => 'Title', 
        'month' => 'Month', 
        'year' => 'Year', 
        'branch_id' => 'Branch', 
        'product_id' => 'Product', 
        'product_identity_id' => 'Identity', 
        'created_by' => 'Created By', 
        'updated_by' => 'Updated By', 
        'information' => 'Information',
        'status' => 'Status',
        'stock_on_hand' => 'Stock On Hand',
        'stock_on_system' => 'Stock On System',
        'product' => 'Product',
        'change_waiting' => 'Processed',
        'change_process' => 'Verified',
        'revert_verified' => 'Void to processed',
        'print' => 'Print',
        'input_stock' => 'Input Stock',
    ],
    
    'placeholder' => [
        'title' => 'ex: stock opname june', 
        'month' => 'ex: juni', 
        'year' => 'ex: 2020', 
        'branch_id' => 'ex: Gudang Malang', 
        'information' => 'ex: some product missing'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>stock opname</code>',
        'description_list' => 'List data <code>stock opname</code>',
        'success_change_waiting' => 'On Process :name',
        'success_change_process' => 'Verified :name',
        'success_revert_verified' => 'Successfully void :name',
        'failed_change_waiting' => 'Failed On Process :name',
        'failed_change_process' => 'Failed Verified :name',
        'failed_revert_verified' => 'Failed void :name',
    ],

    'list' => [
        'status' => ['Waiting', 'On Process', 'Verified'],
        'match_status' => ['Not Match', 'Match'],
    ]
];

$lang['errors'] = [
    'title.required' => sprintf('A %s is required', $lang["label"]["title"]),
    'month.required' => sprintf('A %s is required', $lang["label"]["month"]),
    'year.required' => sprintf('A %s is required', $lang["label"]["year"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
];

return $lang;