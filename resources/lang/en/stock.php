<?php

$lang = [
    'title' => 'Stock',
    
    'label' => [
        'stock' => 'Stock',
        'stock_in' => 'Stock In',
        'stock_out' => 'Stock Out',
        'product_id' => 'Product',
        'branc_id' => 'Branch',
    ],
    
    'placeholder' => [
        
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>stock</code>',
        'description_list' => 'List data <code>stock</code>',
    ],

    'list' => [
        
    ]
];

return $lang;