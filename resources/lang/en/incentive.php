<?php

$lang = [
    'title' => 'Incentive',
    
    'label' => [
        'bank_id' => 'Bank', 
        'branch_id' => 'Branch', 
        'no_transaction' => 'No Transaction', 
        'date_transaction' => 'Date', 
        'investor' => 'PIC', 
        'type' => 'Type', 
        'information' => 'Information', 
        'amount' => 'Amount'
    ],
    
    'placeholder' => [
        'no_transaction' => 'ex: INC-0001', 
        'date_transaction' => 'ex: 2020-03-05', 
        'investor' => 'ex: Agus', 
        'information' => 'ex: Penambahan modal', 
        'amount' => 'ex: 1,000,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>incentive</code>',
        'description_list' => 'List data <code>incentive</code>',
        'desc_type' => 'Decrease: reduce bank balance, Increase: add bank balance'
    ],

    'list' => [
        'type' => ['Decrease', 'Increase']
    ]
];

$lang['errors'] = [
    'date_transaction.required' => sprintf('A %s is required', $lang["label"]["date_transaction"]),
    'date_transaction.date_format' => sprintf('A %s is invalid date format', $lang["label"]["date_transaction"]),
    'bank_id.required' => sprintf('A %s is required', $lang["label"]["bank_id"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
    'no_transaction.required' => sprintf('A %s is required', $lang["label"]["no_transaction"]),
    'amount.numeric' => sprintf('A %s is invalid format number', $lang["label"]["amount"]),
    'amount.min' => sprintf('A %s is minmal 0', $lang["label"]["amount"]),
];

return $lang;