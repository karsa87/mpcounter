<?php

$lang = [
    'title' => 'Expend',
    
    'label' => [
        'no_transaction' => 'No Transaction',
        'expend_date' => 'Expend Date',
        'bank_id' => 'Bank',
        'branch_id' => 'Branch', 
        'category_expend_id' => 'Category Expend',
        'employee_id' => 'Person In Charge (PIC)',
        'amount' => 'Amount',
        'information' => 'Information',
        'images' => 'Images',
        'date' => 'Date',
    ],
    
    'placeholder' => [
        'no_transaction' => 'ex: EXP-0000001',
        'expend_date' => 'ex: 2020-03-01 19:00',
        'amount' => 'ex: 1.000.000',
        'information' => 'ex: Gaji Maret 2020',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>expend</code>',
        'description_list' => 'List data <code>expend</code>',
    ],

    'list' => [
    ]
];

$lang['errors'] = [
    'no_transaction.required' => sprintf('A %s is required', $lang["label"]["no_transaction"]),
    'expend_date.required' => sprintf('A %s is required', $lang["label"]["expend_date"]),
    'expend_date.date_format' => sprintf('A %s is invalid date format', $lang["label"]["expend_date"]),
    'bank_id.required' => sprintf('A %s is required', $lang["label"]["bank_id"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
    'category_expend_id.required' => sprintf('A %s is required', $lang["label"]["category_expend_id"]),
    'amount.numeric' => sprintf('A %s is invalid format number', $lang["label"]["amount"]),
];

return $lang;