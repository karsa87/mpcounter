<?php

$lang = [
    'title' => 'Cash Out',
    
    'label' => [
        'date' => 'Date',
        'expend_type' => 'Expend Type',
        'no_transaction' => 'No Transaction',
        'amount' => 'Amount',
        'information' => 'Information',
        'bank_id' => 'Bank',
        'branch_id' => 'Branch',
        'total' => 'Total',
        'category_expend_id' => 'Category Expend',
    ],
    
    'placeholder' => [
        'date' => 'ex: 2020-04-14 21:04 - 2020-04-14 21:04',
    ],

    'message' => [
    ],

    'list' => [
        'expend_type' => [
            'All', 
            'Purchase Order',
            'Debt Payment PO',
            'Retur Sales Order',
            'Incentive Decrease',
            // 'Expend',
        ],
    ]
];

return $lang;