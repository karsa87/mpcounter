<?php

$lang = [
    'title' => 'Member',
    
    'label' => [
        'name' => 'Name',
        'identity_type' => 'Identity Type',
        'identity' => 'Identity',
        'address' => 'Address',
        'phone' => 'Phone',
        'birthdate' => 'Birtdate',
        'poin' => 'Poin',
        'member_number' => 'No. Member',
        'type' => 'Type',
        'max_day_debt' => 'Max Day Debt',
        'max_debt' => 'Max Debt',
        'status' => 'Status',
        'member' => 'Member',
    ],
    
    'placeholder' => [
        'name' => 'ex: Agus',
        'identity' => 'ex: 78243893992892',
        'address' => 'ex: Jl. A. Yani',
        'phone' => 'ex: 089297339292',
        'birthdate' => 'ex: 1996-03-21',
        'member_number' => 'ex: 98298329012',
        'max_day_debt' => 'ex: 0',
        'max_debt' => 'ex: 1,000,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>member</code>',
        'description_list' => 'List data <code>member</code>',
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'identity_type' => [
            1 => 'Student Card', 
            2 => 'KTP',
            3 => 'SIM',
            4 => 'PASSPORT',
        ],
        'type' => [
            1 => 'General', 
            2 => 'Reseller 1',
            3 => 'Reseller 2',
        ],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'name.max' => sprintf('A %s is 255 max character', $lang["label"]["name"]),
    'address.required' => sprintf('A %s is required', $lang["label"]["address"]),
    'phone.required' => sprintf('A %s is required', $lang["label"]["phone"]),
    'phone.max' => sprintf('A %s is 16 max character', $lang["label"]["phone"]),
    'phone.regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'identity_type.required' => sprintf('A %s is required', $lang["label"]["identity_type"]),
    'identity.required' => sprintf('A %s is required', $lang["label"]["identity"]),
    'member_number.required' => sprintf('A %s is required', $lang["label"]["member_number"]),
    'max_day_debt.numeric' => sprintf('A %s is invalid format numeric', $lang["label"]["max_day_debt"]),
    'max_day_debt.min' => sprintf('A %s is min 0', $lang["label"]["max_day_debt"]),
    'max_debt.numeric' => sprintf('A %s is invalid format numeric', $lang["label"]["max_debt"]),
    'max_debt.min' => sprintf('A %s is min 0', $lang["label"]["max_debt"]),
];

return $lang;