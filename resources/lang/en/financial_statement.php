<?php

$lang = [
    'title' => 'Financial Statement',
    
    'label' => [
        'period' => 'Period',
        'month' => 'Month',
        'year' => 'Year',
        'bank_id' => 'Bank',
        'print_date' => 'Print Date',
        'detail' => 'Detail',
        'sales_order' => 'Sales Order',
        'purchase_order' => 'Purchase Order',
        'loss_profi' => 'Profit and Loss',
        'discount' => 'Discount',
        'incentive' => 'Incentive',
        'decrease' => 'Decrease',
        'increase' => 'Increase',
        'expend' => 'Expend',
        'return_po' => 'Return Purchase Order',
        'return_so' => 'Return Sales Order',
        'dso' => 'Debt Payment Sales Order',
        'dpo' => 'Debt Payment Purchase Order',
        'total_income_expend' => 'Total Income Expend',
        'total_po_first' => 'Total First',
        'total_po_paid' => 'Total Paid',
        'total_po_not_paid' => 'Total Not Paid',
        'total_po' => 'Total',
        'total_so_first' => 'Total First',
        'total_so_paid' => 'Total Paid',
        'total_so_not_paid' => 'Total Not Paid',
        'total_so' => 'Total Sales Order',
        'total_return_po' => 'Total Return PO',
        'total_return_so' => 'Total Return SO',
        'total_discount_promo' => 'Discount Promo',
        'total_discount_sales' => 'Discount Sales',
        'total_discount_detail' => 'Discount Product',
        'total_shipping_cost' => 'Shipping Cost',
        'total_additional_cost' => 'Additional Cost',
        'total_discount' => 'Total Discount',
        'total_add_cost' => 'Total Another Cost',
        'add_cost' => 'Logistics Cost',
        'total_expend' => 'Total Expend',
        'current_balance' => 'Current Total Balance',
        'pulse' => 'Pulse',
    ],
    
    'placeholder' => [
        'period' => 'ex: January',
        'month' => 'ex: January',
        'year' => 'ex: 2020',
        'bank_id' => 'ex: BCA',
    ],

    'message' => [
        
    ],

    'list' => [
        
    ]
];

return $lang;