<?php

$lang = [
    'title' => 'Cash In',
    
    'label' => [
        'date' => 'Date',
        'branch_id' => 'Branch',
        'income_type' => 'Income Type',
        'no_transaction' => 'No Transaction',
        'amount' => 'Amount',
        'information' => 'Information',
        'bank_id' => 'Bank',
        'branch_id' => 'Branch',
        'total' => 'Total',
        'sell_price' => 'Sell Price'
    ],
    
    'placeholder' => [
        'date' => 'ex: 2020-04-14 21:04 - 2020-04-14 21:04',
    ],

    'message' => [
    ],

    'list' => [
        'income_type' => [
            'All', 
            'Sales Order',
            'Debt Payment SO',
            'Retur Purchase Order',
            'Incentive Increase',
            'Pulse',
        ],
    ]
];

return $lang;