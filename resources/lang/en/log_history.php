<?php

$lang = [
    'title' => 'Log History',
    
    'label' => [
        'log_datetime' => 'Log Date',
        'user_id' => 'User',
        'transaction_type' => 'Type',
        'information' => 'Information',
        'record_id' => 'Record ID',
        'data_before' => 'Data Before',
        'data_after' => 'Data After',
        'data_change' => 'Data Change',
        'record_type' => 'Class',
        'table' => 'Table'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>log history</code>',
        'description_list' => 'List data <code>log history</code>',
    ],

    'list' => [
        'transaction_type' => [
            1 => 'CREATE',
            2 => 'UPDATE',
            3 => 'DELETE',
        ]
    ]
];

return $lang;