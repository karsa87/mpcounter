<?php

$lang = [
    'title' => 'Product',
    
    'label' => [
        'code' => 'Code',
        'name' => 'Name',
        'category_id' => 'Category',
        'brand_id' => 'Brand',
        'price' => 'Price',
        'cost_of_goods' => 'Cost of goods',
        'sell_price' => 'Sell Price',
        'images' => 'Images',
        'information' => 'Information',
        'status' => 'Status',
        'unit' => 'Unit',
        'product_identity' => 'Product Identity',
        'available_stock' => 'Available Stock',
        'stock' => 'Stock',
        'branch_id' => 'Branch',
        'reseller1_sell_price' => 'Reseller 1 Sell Price', 
        'reseller2_sell_price' => 'Reseller 2 Sell Price',
        'with_expired_date' => 'Expired Date',
        'expired_date' => 'Expired Date',
        'gross_profit' => 'Gross Profit',
        'net_profit' => 'Net Profit',
        'tax' => 'Tax',
    ],
    
    'placeholder' => [
        'code' => 'ex: PR-0001',
        'name' => 'ex: Product A',
        'cost_of_goods' => 'ex: 10,000',
        'sell_price' => 'ex: 15,000',
        'information' => 'ex: product distribution',
        'reseller1_sell_price' => 'ex: 14,000', 
        'reseller2_sell_price' => 'ex: 13,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>product</code>',
        'description_list' => 'List data <code>product</code>',
        'desc_cost_of_goods' => 'cost of good sold (COGS) is all direct costs incurred to obtain goods or services sales.',
        'desc_with_expired_date' => 'Turn on to give an expired date on this product',
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'unit' => ['Pcs', 'Box', 'Kg'],
        'expired' => ['No', 'Yes'],
    ]
];

$lang['errors'] = [
    'code.required' => sprintf('A %s is required', $lang["label"]["code"]),
    'code.max' => sprintf('A %s is 255 max character', $lang["label"]["code"]),
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'name.max' => sprintf('A %s is 255 max character', $lang["label"]["name"]),
    'category_id.required' => sprintf('A %s is required', $lang["label"]["category_id"]),
    'brand_id.required' => sprintf('A %s is required', $lang["label"]["brand_id"]),
    'sell_price.numeric' => sprintf('A %s is invalid format', $lang["label"]["sell_price"]),
    'sell_price.min' => sprintf('A %s is min 0', $lang["label"]["sell_price"]),
];

return $lang;