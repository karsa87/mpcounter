<?php

$lang = [
    'title' => 'Pulse',
    
    'label' => [
        'no_transaction' => 'No Transaction',
        'date' => 'Date',
        'bank_id' => 'Bank',
        'branch_id' => 'Branch', 
        'total_pulse' => 'Total Pulse',
        'capital_price' => 'Capital Price',
        'sell_price' => 'Sell Price',
        'created_by' => 'Created By',
        'updated_by' => 'Updated By',
        'phone' => 'Phone',
        'information' => 'Information',
    ],
    
    'placeholder' => [
        'no_transaction' => 'ex: EXP-0000001',
        'date' => 'ex: 2020-03-01 19:00',
        'phone' => 'ex: 08982338xxxxx',
        'total_pulse' => 'ex: 1.000.000',
        'sell_price' => 'ex: 1.000.000',
        'capital_price' => 'ex: 1.000.000',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>pulse</code>',
        'description_list' => 'List data <code>pulse</code>',
    ],

    'list' => [
    ]
];

$lang['errors'] = [
    'no_transaction.required' => sprintf('A %s is required', $lang["label"]["no_transaction"]),
    'date.required' => sprintf('A %s is required', $lang["label"]["date"]),
    'date.date_format' => sprintf('A %s is invalid date format', $lang["label"]["date"]),
    'bank_id.required' => sprintf('A %s is required', $lang["label"]["bank_id"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
    'total_pulse.numeric' => sprintf('A %s is invalid format number', $lang["label"]["total_pulse"]),
    'total_pulse.min' => sprintf('A %s minimal is 5000', $lang["label"]["total_pulse"]),
    'sell_price.numeric' => sprintf('A %s is invalid format number', $lang["label"]["sell_price"]),
    'sell_price.min' => sprintf('A %s minimal is 0', $lang["label"]["sell_price"]),
    'capital_price.numeric' => sprintf('A %s is invalid format number', $lang["label"]["capital_price"]),
    'capital_price.min' => sprintf('A %s minimal is 0', $lang["label"]["capital_price"]),
    'phone.required' => sprintf('A %s is required', $lang["label"]["phone"]),
    'phone.max' => sprintf('A %s is 16 max character', $lang["label"]["phone"]),
    'phone.regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
];

return $lang;