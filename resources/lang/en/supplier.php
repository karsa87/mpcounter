<?php

$lang = [
    'title' => 'Supplier',
    
    'label' => [
        'name' => 'Name',
        'address' => 'Address',
        'phone' => 'Phone',
        'sales_name' => 'Sales Name',
        'sales_phone' => 'Sales Phone',
        'information' => 'Information',
        'status' => 'Status',
        'has_tax' => 'Has Tax'
    ],
    
    'placeholder' => [
        'name' => 'ex: PT. XXX',
        'phone' => 'ex: (0341) 7726192',
        'address' => 'ex: Jl. A. Yani',
        'sales_name' => 'ex: Agus',
        'sales_phone' => 'ex: 089297339292',
        'information' => 'ex: Supplier Barang A',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>supplier</code>',
        'description_list' => 'List data <code>supplier</code>',
        'desc_has_tax' => 'If the supplier has tax that must be reported'
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'has_tax' => ['No', 'Yes'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'name.max' => sprintf('A %s is 255 max character', $lang["label"]["name"]),
    'address.required' => sprintf('A %s is required', $lang["label"]["address"]),
    'phone.required' => sprintf('A %s is required', $lang["label"]["phone"]),
    'phone.max' => sprintf('A %s is 16 max character', $lang["label"]["phone"]),
    'phone.regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'sales_name.required' => sprintf('A %s is required', $lang["label"]["sales_name"]),
    'sales_phone.required' => sprintf('A %s is required', $lang["label"]["sales_phone"]),
    'sales_phone.max' => sprintf('A %s is 16 max character', $lang["label"]["sales_phone"]),
    'sales_phone.regex' => sprintf('A %s is invalid format phone', $lang["label"]["sales_phone"]),
    'sales_phone.note_regex' => sprintf('A %s is invalid format phone', $lang["label"]["sales_phone"]),
];

return $lang;