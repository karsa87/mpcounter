<?php

$lang = [
    'title' => 'Debt Payment Purchase Order',
    
    'label' => [
        'no_debt' => 'No Debt', 
        'purchase_order_id' => 'Purchase Order', 
        'bank_id' => 'Bank', 
        'date' => 'Date', 
        'information' => 'Information', 
        'images' => 'Images', 
        'paid' => 'Paid', 
        'another_paid' => 'Another Paid', 
        'total_paid' => 'Total Paid', 
        'remaining_debt' => 'Remaining Debt',
        'total' => 'Total',
    ],
    
    'placeholder' => [
        'no_debt' => 'ex: DPO-0001', 
        'date' => 'ex: 2020-04-11 12:04', 
        'information' => 'Ex: Debt Payment PO for PO-200411/000001', 
        'paid' => 'ex: 1,000,000', 
        'total_paid' => 'ex: 1,000,000', 
        'bank_id' => 'ex: KAS', 
        'remaining_debt' => 'ex: 1,000,000'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>debt payment purchase order</code>',
        'description_list' => 'List data <code>debt payment purchase order</code>',
    ],

    'list' => [
        
    ]
];

$lang['errors'] = [
    'date.required' => sprintf('A %s is required', $lang["label"]["date"]),
    'date.date_format' => sprintf('A %s is invalid date format', $lang["label"]["date"]),
    'bank_id.required' => sprintf('A %s is required', $lang["label"]["bank_id"]),
    'purchase_order_id.required' => sprintf('A %s is required', $lang["label"]["purchase_order_id"]),
    'no_debt.required' => sprintf('A %s is required', $lang["label"]["no_debt"]),
    'paid.numeric' => sprintf('A %s is invalid format numeric', $lang["label"]["paid"]),
];

return $lang;