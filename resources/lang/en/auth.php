<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Sign in to start your session',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'message' => [
        'password_not_correct' => 'Password not correct',
        'username_not_found' => 'Username not found',
        'username_deactived' => 'Username deactivated',
    ],
    
    'validation' => [
        'email' => [
            'required' => 'Please enter a email address'
        ],
        'password' => [
            'required' => 'Please provide a password'
        ],
    ],

    'meta' => [
        'title' => 'Login',
        'description' => 'Global POS is system retail management',
        'keyword' => 'pos, point of sales, point, of, sales, mochamad karsa syaifudin jupri, mochamad, karsa, syaifudin, jupri, wikacell, system, retail, management, system retail management'
    ]
];
