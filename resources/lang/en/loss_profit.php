<?php

$lang = [
    'title' => 'Loss and Profit',
    
    'label' => [
        'period' => 'Period',
        'month' => 'Month',
        'year' => 'Year',
        'bank_id' => 'Bank',
        'branch_id' => 'Branch',
        'print_date' => 'Print Date',
        'sales_order' => 'Sales Order',
        'discount' => 'Discount',
        'incentive' => 'Incentive',
        'decrease' => 'Decrease',
        'expend' => 'Expend',
        'total_so' => 'Total Sales Order',
        'total_discount_promo' => 'Discount Promo',
        'total_discount_sales' => 'Discount Sales',
        'total_discount' => 'Total Discount',
        'total_expend' => 'Total Expend',
        'current_balance' => 'Current Total Balance',
        'gross_profit' => 'Gross Profit',
        'total_gross_profit' => 'Total Gross Profit',
        'total_incentive_increase' => 'Total Incentive Increase'
    ],
    
    'placeholder' => [
        'period' => 'ex: January',
        'month' => 'ex: January',
        'year' => 'ex: 2020',
        'bank_id' => 'ex: BCA',
    ],

    'message' => [
        
    ],

    'list' => [
        
    ]
];

return $lang;