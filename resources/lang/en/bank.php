<?php

$lang = [
    'title' => 'Bank',
    
    'label' => [
        'name' => 'Name',
        'account_number' => 'Account Number',
        'account_bank' => 'Account Bank',
        'name_owner' => 'Name Owner',
        'saldo' => 'Saldo',
        'status' => 'Status',
        'is_default' => 'Default',
    ],
    
    'placeholder' => [
        'name' => 'ex: BCA',
        'account_number' => 'ex: 332298432',
        'name_owner' => 'ex: Agus',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>bank</code>',
        'description_list' => 'List data <code>bank</code>',
        'description_is_default' => 'Enable it to make it the default bank of each transaction, Only 1 bank can set to default'
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'default' => ['No', 'Yes'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'is_default.unique' => sprintf('A %s is already default bank', $lang["label"]["is_default"]),
    'account_number.numeric' => sprintf('A %s is invalid format number', $lang["label"]["account_number"]),
    'status.in' => sprintf("A %s can not deactivated, because this record is default bank", $lang["label"]["status"]),
];

return $lang;