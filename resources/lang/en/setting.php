<?php

$lang = [
    'title' => 'Setting',
    
    'label' => [
        'name' => 'Name', 
        'key' => 'Key', 
        'value' => 'Value', 
        'status' => 'Status', 
        'type' => 'Type'
    ],
    
    'placeholder' => [
        'name' => 'ex: Header nota small SO',
        'key' => 'ex: HEADER_NOTA_SMALL_SO',
        'value' => 'ex: Agus',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>setting</code>',
        'description_list' => 'List data <code>bank</code>',
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'type' => ['Text', 'Number'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'key.required' => sprintf('A %s is required', $lang["label"]["key"]),
    'key.unique' => sprintf('A %s is already exists', $lang["label"]["key"]),
    'value.required' => sprintf('A %s is required', $lang["label"]["value"]),
];

return $lang;