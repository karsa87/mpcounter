<?php

$lang = [
    'title' => 'Category',
    
    'label' => [
        'name' => 'Name',
        'status' => 'Status',
        'identifier' => 'Identifier',
        'identifier_info' => 'Info Identifier',
    ],
    
    'placeholder' => [
        'name' => 'ex : Makanan',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>category</code>',
        'description_list' => 'List data <code>categorys</code>',
        'desc_identifier' => 'Select identifier type for category product:<br><ol><li><strong>No:</strong> for product categories that do not have a unique identity</li><li><strong>1 Identifier 1 Product:</strong> for product categories that have unique identities that apply to 1 product only. Ex: <strong>IMEI</strong></li><li><strong>Multi Identifier 1 Product:</strong> for product categories that have many unique identities. Ex: <strong>BARCODE SNACK</strong></li></ol>'
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'identifier' => ['No', '1 Identifier 1 Product', 'Multi Identifier 1 Product'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'identifier.required' => sprintf('A %s is required', $lang["label"]["identifier"]),
];

return $lang;