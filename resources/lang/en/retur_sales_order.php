<?php

$lang = [
    'title' => 'Retur Sales Order',
    
    'label' => [
        'no_retur' => 'No Retur', 
        'date' => 'Date', 
        'sales_order_id' => 'Sales Order', 
        'member_id' => 'Name', 
        'branch_id' => 'Branch', 
        'bank_id' => 'Bank', 
        'information' => 'Information', 
        'total_amount' => 'Total Amount', 
        'images' => 'Images',
        'product_id' => 'Product',
        'qty' => 'Qty', 
        'sell_price' => 'Sell Price', 
        'cost_of_goods' => 'Cost Of Goods',
        'is_retur' => 'Is Retur',
        'information' => 'Information',
        'list_product' => 'List Product',
        'sales_price' => 'Sales Price',
        'sell_price' => 'Sell Price',
        'total' => 'Total',
        'sub_total' => 'Sub Total',
        'subtotal' => 'Subtotal',
        'pay' => 'Pay',
        'return_money' => 'Retur Money',
        'identity' => 'Identity',
        'print_nota' => 'Print Nota',
        'phone' => 'Phone',
        'address' => 'Address',
        'discount_promo' => 'Discount',
        'total_discount_detail' => 'Total Disount Promo',
        'retur_product' => 'Return Product'
    ],
    
    'placeholder' => [
        'no_retur' => 'ex: 20200316/RPO-00001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'product_id' => 'ex: Samsung',
        'qty' => 'ex: 1', 
        'sell_price' => 'ex: 1,000,000', 
        'cost_of_goods' => 'ex: 1,000,000',
        'information' => 'ex: This order for Product A',
        'member_id' => 'ex: Agus',
        'bank_id' => 'ex: BCX',
        'branch_id' => 'ex: Branch Suhat',
        'sales_order_id' => 'ex: 20200316/SO-00001', 
        'phone' => 'ex: 089883888xxx',
        'address' => 'ex: Jl. Malang',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>sales order</code>',
        'description_list' => 'List data <code>sales order</code>',
        'description_delete' => 'Are you sure for delete this item ?',
        'error_add_qty' => 'This product has a maximum quantity of 1',
        'error_max_qty' => 'This product has a maximum quantity',
        'error_duplicate_identity' => 'Duplicate identity',
        'error_required_identity' => 'A identity is required',
        'error_less_down_payment' => 'Less than %s money paid',
        'error_detail' => 'Select min 1 product',
        'change_so' => 'Are you sure change Sales Order ?',
    ],

    'list' => [
        'status' => ['Not Yet Paid', 'Paid'],
        'type' => ['CASH', 'DEBT'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('A %s is invalid format date Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('A %s is required', $lang["label"]["date"]),
    'sales_order_id.required' => sprintf('A %s is required', $lang["label"]["sales_order_id"]),
    'bank_id.required' => sprintf('A %s is required', $lang["label"]["bank_id"]),
    'member_id.required' => sprintf('A %s is required', $lang["label"]["member_id"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
    'total_amount.numeric' => sprintf('A %s is invalid format number', $lang["label"]["total_amount"]),
];

return $lang;