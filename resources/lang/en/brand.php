<?php

$lang = [
    'title' => 'Brand',
    
    'label' => [
        'name' => 'Name',
        'status' => 'Status',
    ],
    
    'placeholder' => [
        'name' => 'ex : Samsung',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>brand</code>',
        'description_list' => 'List data <code>brand</code>',
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
];

return $lang;