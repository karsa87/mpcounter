<?php

$lang = [
    'title' => 'Category Expend',
    
    'label' => [
        'name' => 'Name',
        'status' => 'Status',
    ],
    
    'placeholder' => [
        'name' => 'ex: Operasional',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>category expend</code>',
        'description_list' => 'List data <code>category expend</code>',
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
];

return $lang;