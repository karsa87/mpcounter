<?php

$lang = [
    'title' => "Role",

    'placeholder' => [
        'id' => 'ID',
        'name' => 'ex: Administrator',
        'slug' => 'ex: administrator',
        'description' => 'ex: Role for user administrator',
        'level' => 'ex: administrator',
    ],
    'list' => [
        'level' => [
            100 => 'Developer',
            95 => 'Administrator WIKA',
            90 => 'Administrator',
            80 => 'Employee',
        ]
    ],
    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>role</code>',
        'description_list' => 'List data <code>role</code>',
    ],
    'label' => [
        'menu_access' => "Menu Access",
        'id' => 'ID',
        'name' => 'Name',
        'slug' => 'Slug',
        'description' => 'Description',
        'level' => 'Level',
        'permission' => 'Permission',
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'slug.required' => sprintf('A %s is required', $lang["label"]["slug"]),
    'level.required' => sprintf('A %s is required', $lang["label"]["level"]),
    'level.numeric' => sprintf('A %s is wrong format number', $lang["label"]["level"]),
];

return $lang;