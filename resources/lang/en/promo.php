<?php

$lang = [
    'title' => 'Promo',
    
    'label' => [
        'code' => 'Code', 
        'name' => 'Name', 
        'information' => 'Information', 
        'period' => 'Period', 
        'date_start' => 'From', 
        'date_end' => 'To', 
        'category_id' => 'Category', 
        'brand_id' => 'Brand', 
        'discount' => 'Discount', 
        'discount_percent' => '(%)', 
        'discount_price' => '(Price)', 
        'status' => 'Status', 
        'images' => 'Images',
        'product' => 'Product',
        'after_discount' => 'Discount',
        'selected_product' => 'Selected Product',
    ],
    
    'placeholder' => [
        'code' => 'ex: DC-0001',
        'name' => 'ex: Promo Valentine',
        'period' => 'ex: 2020-03-29 14:00 - 2020-04-10 00:00',
        'discount_percent' => 'ex: 40', 
        'discount_price' => 'ex: 50,000', 
        'information' => 'ex: this promo for valentine day', 
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>promo</code>',
        'description_list' => 'List data <code>promo</code>',
        'error_choose_one' => 'Choose one discount (%) or (Price)',
        'desc_select_product' => 'Select the product to be discounted',
        'desc_category_id' => 'Select category, if you want to discount all product of category',
        'desc_brand_id' => 'Select brand, if you want to discount all product of brand',
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
    ]
];

$lang['errors'] = [
    'code.required' => sprintf('A %s is required', $lang["label"]["code"]),
    'code.unique' => sprintf('A %s is duplicate value', $lang["label"]["code"]),
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'name.max' => sprintf('A %s is 255 max character', $lang["label"]["name"]),
    'category_id.required' => sprintf('A %s is required', $lang["label"]["category_id"]),
    'date_start.required' => sprintf('A %s is required', $lang["label"]["date_start"]),
    'date_start.date_format' => sprintf('A %s is invalid format date', $lang["label"]["period"]),
    'date_end.required' => sprintf('A %s is required', $lang["label"]["date_end"]),
    'date_end.date_format' => sprintf('A %s is invalid format date', $lang["label"]["period"]),
    'discount_percent.numeric' => sprintf('A %s is invalid format', $lang["label"]["discount_percent"]),
    'discount_percent.min' => sprintf('A %s is min 1', $lang["label"]["discount_percent"]),
    'discount_percent.max' => sprintf('A %s is max 100', $lang["label"]["discount_percent"]),
    'discount_price.numeric' => sprintf('A %s is invalid format', $lang["label"]["discount_price"]),
    'discount_price.min' => sprintf('A %s is min 1', $lang["label"]["discount_price"]),
    'products.required' => sprintf('Choose minimal 1 product if category empty', $lang["label"]["product"]),
    'period.required' => sprintf('A %s is required', $lang["label"]["period"]),
];

return $lang;