<?php

$lang = [
    'title' => 'Log Bank',
    
    'label' =>  [
        'log_datetime' => 'Log Date', 
        'user_id' => 'User', 
        'bank_id' => 'Bank', 
        'transaction_id' => 'Transaction ID', 
        'transaction_type' => 'Transaction Type', 
        'type' => 'Type', 
        'information' => 'Information', 
        'debit_amount' => 'Cash In', 
        'credit_amount' => 'Cash Out', 
        'amount_before' => 'Amount Before', 
        'amount_after' => 'Amount After', 
        'table' => 'Table'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>log bank transaction</code>',
        'description_list' => 'List data <code>log bank transaction</code>',
    ],

    'list' => [
        'type' => [
            'Credit',
            'Debit',
        ]
    ]
];

return $lang;