<?php

$lang = [
    'title' => 'Log Stock',
    
    'label' =>  [
        'log_datetime' => 'Log Date', 
        'user_id' => 'User', 
        'stock_id' => 'Stock', 
        'product_id' => 'Product', 
        'branch_id' => 'Branch', 
        'transaction_id' => 'Transaction ID', 
        'transaction_type' => 'Transaction Type', 
        'type' => 'Type', 
        'information' => 'Information', 
        'stock_in' => 'Stock In', 
        'stock_out' => 'Stock Out', 
        'stock_before' => 'Stock Before', 
        'stock_after' => 'Stock After', 
        'table' => 'Table'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>log stock transaction</code>',
        'description_list' => 'List data <code>log stock transaction</code>',
    ],

    'list' => [
        'type' => [
            'Out',
            'In',
        ]
    ]
];

return $lang;