<?php

$lang = [
    'title' => 'Branch',
    
    'label' => [
        'name' => 'Name',
        'phone' => 'Phone',
        'address' => 'Address',
        'employee_id' => 'Branch Manager',
        'status' => 'Status',
    ],
    
    'placeholder' => [
        'name' => 'ex: Cabang Suhat',
        'phone' => 'ex: 08983939393',
        'address' => 'ex: Jl. margonda',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>branch</code>',
        'description_list' => 'List data <code>branch</code>',
        'desc_employee_id' => 'List of employee'
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'name.max' => sprintf('A %s is 255 max character', $lang["label"]["name"]),
    'address.required' => sprintf('A %s is required', $lang["label"]["address"]),
    'phone.required' => sprintf('A %s is required', $lang["label"]["phone"]),
    'phone.max' => sprintf('A %s is 16 max character', $lang["label"]["phone"]),
    'phone.regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'employee_id.required' => sprintf('A %s is required', $lang["label"]["employee_id"]),
];

return $lang;