<?php

$lang = [
    'title' => 'Mutation Product',
    
    'label' => [
        'no_mutation' => 'No Mutation', 
        'date' => 'Date', 
        'employee_id' => "Employee", 
        'from_branch_id' => "From Branch", 
        'to_branch_id' => "To Branch", 
        'shipping_cost' => "Shipping Cost", 
        'additional_cost' => "Additional Cost", 
        'created_by' => "Created By", 
        'updated_by' => "Updated By", 
        'print_nota' => "Print Nota", 
        'information' => "Information", 
        'product_id' => 'Product', 
        'product_identity_id' => 'From Product Identity', 
        'to_product_identity_id' => 'To Product Identity', 
        'qty' => 'Qty',
        'identity' => 'Identity',
        'select_product' => 'Select Product',
        'total_amount' => 'Total Amount',
        'sell_price' => 'Sell Price',
        'total' => 'Total',
        'subtotal' => 'Subtotal',
    ],
    
    'placeholder' => [
        'no_mutation' => 'ex: MP-00001', 
        'date' => 'ex: 2020-03-16 10:00', 
        'employee_id' => "ex: agus", 
        'from_branch_id' => "ex: Cabang Suhat", 
        'to_branch_id' => "ex: Cabang Batu", 
        'shipping_cost' => 'ex: 1,000,000', 
        'additional_cost' => 'ex: 1,000,000',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>purchase order</code>',
        'description_list' => 'List data <code>purchase order</code>',
        'description_delete' => 'Are you sure for delete this item ?',
        'error_max_qty' => 'This product is maximum quantity',
        'error_buy_qty' => 'This product can be purchased for a maximum of 1',
        'error_duplicate_identity' => 'Duplicate identity',
        'error_required_identity' => 'A identity is required',
        'error_less_down_payment' => 'Less than %s money paid',
        'error_detail' => 'Select min 1 product',
        'member_only' => 'Only member can use point',
        'point_not_enough' => 'Member points not enough',
        'error_cant_edit' => "Can not edit this purchase order because it has a history of returns",
        'desc_type' => 'CASH: direct payment paid off, DEBT: term payments',
        'same_branch' => 'Can not choose same branch',
        'change_branch' => 'You have already selected several products, if you change branches then the selected products will be deleted',
        'sure' => 'Are you sure ?',
    ],

    'list' => [
        'status' => ['Not Yet Paid', 'Paid'],
        'type' => ['CASH', 'DEBT'],
    ]
];

$lang['errors'] = [
    'date.date_format' => sprintf('A %s is invalid format date Y-m-d H:i', $lang["label"]["date"]),
    'date.required' => sprintf('A %s is required', $lang["label"]["date"]),
    'from_branch_id.required' => sprintf('A %s is required', $lang["label"]["from_branch_id"]),
    'to_branch_id.required' => sprintf('A %s is required', $lang["label"]["to_branch_id"]),
    'employee_id.required' => sprintf('A %s is required', $lang["label"]["employee_id"]),
    'shipping_cost.numeric' => sprintf('A %s is invalid format number', $lang["label"]["shipping_cost"]),
    'additional_cost.numeric' => sprintf('A %s is invalid format number', $lang["label"]["additional_cost"]),
];

return $lang;