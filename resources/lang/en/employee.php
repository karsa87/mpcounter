<?php

$lang = [
    'title' => 'Employee',
    
    'label' => [
        'name' => 'Name',
        'contact' => 'contact',
        'email' => 'Email',
        'phone' => 'Phone',
        'address' => 'Address',
        'type_identifier' => 'Type Identifier',
        'identifier' => 'Identifier',
        'status' => 'Status',
        'username' => 'Username',
        'password' => 'Password',
        'information' => 'Information',
        'role' => 'Role',
        'branch_id' => 'Branch',
    ],
    
    'placeholder' => [
        'name' => 'ex: Agus',
        'email' => 'ex: agus@gmail.com',
        'phone' => 'ex 08983939393',
        'address' => 'ex: Jl. margonda',
        'identifier' => 'ex: 129487274820234',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>employee</code>',
        'description_list' => 'List data <code>employee</code>',
        'desc_user_role' => 'Role acces for login employee'
    ],

    'list' => [
        'status' => ['Deactive', 'Active'],
        'identifier' => ['KTP', 'SIM', 'NPWP'],
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'name.max' => sprintf('A %s is 255 max character', $lang["label"]["name"]),
    'address.required' => sprintf('A %s is required', $lang["label"]["address"]),
    'phone.required' => sprintf('A %s is required', $lang["label"]["phone"]),
    'phone.max' => sprintf('A %s is 16 max character', $lang["label"]["phone"]),
    'phone.regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'phone.note_regex' => sprintf('A %s is invalid format phone', $lang["label"]["phone"]),
    'email.required' => sprintf('A %s is required', $lang["label"]["email"]),
    'email.unique' => sprintf('A %s is already taken', $lang["label"]["email"]),
    'email.email' => sprintf('A %s is invalid format email', $lang["label"]["email"]),
    'username.required' => sprintf('A %s is required', $lang["label"]["username"]),
    'username.max' => sprintf('A %s is 255 max character', $lang["label"]["username"]),
    'username.unique' => sprintf('A %s is already taken', $lang["label"]["username"]),
    'password.required' => sprintf('A %s is required', $lang["label"]["password"]),
    'password.min' => sprintf('A %s is 5 min character', $lang["label"]["password"]),
    'branch_id.required' => sprintf('A %s is required', $lang["label"]["branch_id"]),
];

return $lang;