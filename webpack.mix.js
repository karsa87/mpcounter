const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.copyDirectory('node_modules/tinymce/plugins', 'public/node_modules/tinymce/plugins');
mix.copyDirectory('node_modules/tinymce/skins', 'public/node_modules/tinymce/skins');
mix.copyDirectory('node_modules/tinymce/themes', 'public/node_modules/tinymce/themes');
mix.copy('node_modules/tinymce/jquery.tinymce.js', 'public/node_modules/tinymce/jquery.tinymce.js');
mix.copy('node_modules/tinymce/jquery.tinymce.min.js', 'public/node_modules/tinymce/jquery.tinymce.min.js');
mix.copy('node_modules/tinymce/icons/default/icons.js', 'public/node_modules/tinymce/icons/default/icons.js');
mix.copy('node_modules/tinymce/tinymce.js', 'public/node_modules/tinymce/tinymce.js');
mix.copy('node_modules/tinymce/tinymce.min.js', 'public/node_modules/tinymce/tinymce.min.js');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .copyDirectory('resources/assets/img', 'public/img')
    .copyDirectory('resources/assets/admin/js', 'public/js/admin')
    .copyDirectory('resources/assets/admin/css', 'public/css/admin');

mix.styles([
    'resources/assets/admin/plugins/*.css',
    'resources/assets/admin/plugins/*/*.css',
    'resources/assets/admin/plugins/*/*/*.css',
    'resources/assets/admin/plugins/*/*/*/*.css',
    'resources/assets/admin/plugins/*/*/*/*/*.css',
], 'public/css/vendor.css')
.scripts([
    'resources/assets/admin/plugins/*.js',
    'resources/assets/admin/plugins/*/*.js',
    'resources/assets/admin/plugins/*/*/*.js',
    'resources/assets/admin/plugins/*/*/*/*.js',
    'resources/assets/admin/plugins/*/*/*/*/*.js',
], 'public/js/vendor.js')

if (mix.inProduction()) {
    mix.version();
}