<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBranchIdIncentiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incentive', function (Blueprint $table) {
            $table->bigInteger('branch_id')->unsigned()->nullable();

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('expend', function (Blueprint $table) {
            $table->bigInteger('branch_id')->unsigned()->nullable();

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incentive', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->dropColumn(['branch_id']);
        });

        Schema::table('expend', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->dropColumn(['branch_id']);
        });
    }
}
