<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMutationProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutation_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_mutation',255);
            $table->dateTime('date');
            $table->bigInteger('employee_id');
            $table->bigInteger('from_branch_id');
            $table->bigInteger('to_branch_id');
            $table->double('shipping_cost', 12, 2)->default(0);
            $table->double('additional_cost', 12, 2)->default(0);
            $table->double('total_amount_detail', 12, 2)->default(0);
            $table->double('total_amount', 12, 2)->default(0);
            $table->text('images')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutation_product');
    }
}
