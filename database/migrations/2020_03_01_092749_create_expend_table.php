<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expend', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('expend_date');
            $table->bigInteger('bank_id')->unsigned();
            $table->bigInteger('category_expend_id')->unsigned();
            $table->bigInteger('employee_id')->nullable()->unsigned();
            $table->double('amount', 12, 2)->default(0);
            $table->text('information')->nullable();
            $table->text('images')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expend');
    }
}
