<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitReturSoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retur_sales_order_detail', function (Blueprint $table) {
            $table->double('discount', 0, 12)->default(0);
            $table->double('total_discount', 0, 12)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retur_sales_order_detail', function (Blueprint $table) {
            $table->dropColumn(['discount', 'total_discount']);
        });
    }
}
