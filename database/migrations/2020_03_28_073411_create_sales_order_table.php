<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_invoice',255);
            $table->dateTime('date');
            $table->bigInteger('member_id')->nullable()->unsigned();
            $table->string('name', 255);
            $table->text('address')->nullable();
            $table->string('phone', 16)->nullable();
            $table->text('information')->nullable();
            $table->text('images')->nullable();
            $table->tinyInteger('type')->default(0);
            $table->date('date_max_payable')->nullable();
            $table->bigInteger('branch_id')->unsigned();
            $table->bigInteger('bank_id')->unsigned();
            $table->bigInteger('sales_id')->nullable()->unsigned();
            $table->bigInteger('promo_id')->nullable()->unsigned();
            $table->double('total_amount_first', 12, 2)->default(0);
            $table->double('discount_promo', 12, 2)->default(0);
            $table->double('discount_sales', 12, 2)->default(0);
            $table->double('total_discount_detail', 12, 2)->default(0);
            $table->double('shipping_cost', 12, 2)->default(0);
            $table->double('additional_cost', 12, 2)->default(0);
            $table->double('total_amount', 12, 2)->default(0);
            $table->double('down_payment', 12, 2)->default(0);
            $table->double('paid_off', 12, 2)->default(0);
            $table->tinyInteger('is_ppn')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order');
    }
}
