<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLongAmountLogBankTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_bank_transaction', function (Blueprint $table) {
            $table->decimal('debit_amount', 18, 2)->default(0)->change();
            $table->decimal('credit_amount', 18, 2)->default(0)->change();
            $table->decimal('amount_before', 18, 2)->default(0)->change();
            $table->decimal('amount_after', 18, 2)->default(0)->change();
        });

        Schema::table('bank', function (Blueprint $table) {
            $table->decimal('saldo', 18, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_bank_transaction', function (Blueprint $table) {
            $table->decimal('debit_amount', 12, 2)->default(0)->change();
            $table->decimal('credit_amount', 12, 2)->default(0)->change();
            $table->decimal('amount_before', 12, 2)->default(0)->change();
            $table->decimal('amount_after', 12, 2)->default(0)->change();
        });

        Schema::table('bank', function (Blueprint $table) {
            $table->decimal('saldo', 12, 2)->default(0)->change();
        });
    }
}
