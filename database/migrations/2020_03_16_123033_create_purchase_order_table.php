<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_invoice', 255);
            $table->string('no_reference', 255)->nullable();
            $table->dateTime('date');
            $table->tinyInteger('type')->default(0);
            $table->date('date_max_payable')->nullable();
            $table->text('images')->nullable();
            $table->bigInteger('supplier_id')->unsigned();
            $table->bigInteger('branch_id')->unsigned();
            $table->bigInteger('bank_id')->unsigned();
            $table->text('information')->nullable();
            $table->double('total_amount', 12, 2)->default(0);
            $table->double('down_payment', 12, 2)->default(0);
            $table->double('paid_off', 12, 2)->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
