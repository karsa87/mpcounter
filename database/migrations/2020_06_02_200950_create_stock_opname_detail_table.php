<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOpnameDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opname_detail', function (Blueprint $table) {
            $table->bigInteger('stock_opname_id');
            $table->bigInteger('product_identity_id');
            $table->bigInteger('product_id');
            $table->integer('stock_on_system')->default(0);
            $table->integer('stock_on_hand')->default(0);
            $table->text('information')->nullable();
            $table->tinyInteger('status')->default(0);

            $table->primary(['stock_opname_id', 'product_identity_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opname_detail');
    }
}
