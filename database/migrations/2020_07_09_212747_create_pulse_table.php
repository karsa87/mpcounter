<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePulseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulse', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_transaction', 32);
            $table->dateTime('date');
            $table->string('phone', 255);
            $table->bigInteger('branch_id');
            $table->bigInteger('bank_id');
            $table->double('total_pulse', 12, 2);
            $table->double('sell_price', 12, 2);
            $table->double('capital_price', 12, 2);
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulse');
    }
}
