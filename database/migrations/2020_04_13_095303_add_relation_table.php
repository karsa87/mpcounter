<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_permission', function (Blueprint $table): void {
            $table->foreign('permission_id')
                    ->references('id')
                    ->on('permissions')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('menu_id')
                    ->references('id')
                    ->on('menu')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('debt_payment_purchase_order', function (Blueprint $table): void {
            $table->foreign('purchase_order_id')
                    ->references('id')
                    ->on('purchase_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('debt_payment_sales_order', function (Blueprint $table): void {
            $table->foreign('sales_order_id')
                    ->references('id')
                    ->on('sales_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('expend', function (Blueprint $table): void {
            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('incentive', function (Blueprint $table): void {
            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('product_identity', function (Blueprint $table): void {
            $table->foreign('product_id')
                    ->references('id')
                    ->on('product')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('promo_detail', function (Blueprint $table): void {
            $table->foreign('promo_id')
                    ->references('id')
                    ->on('promo')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('purchase_order_detail', function (Blueprint $table): void {
            $table->foreign('purchase_order_id')
                    ->references('id')
                    ->on('purchase_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('product_id')
                    ->references('id')
                    ->on('product')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('purchase_order', function (Blueprint $table): void {
            $table->foreign('supplier_id')
                    ->references('id')
                    ->on('supplier')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('retur_purchase_order', function (Blueprint $table): void {
            $table->foreign('supplier_id')
                    ->references('id')
                    ->on('supplier')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('purchase_order_id')
                    ->references('id')
                    ->on('purchase_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('retur_purchase_order_detail', function (Blueprint $table): void {
            $table->foreign('retur_purchase_order_id')
                    ->references('id')
                    ->on('retur_purchase_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('purchase_order_detail_id')
                    ->references('id')
                    ->on('purchase_order_detail')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('product_id')
                    ->references('id')
                    ->on('product')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('retur_sales_order', function (Blueprint $table): void {
            $table->foreign('sales_order_id')
                    ->references('id')
                    ->on('sales_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('retur_sales_order_detail', function (Blueprint $table): void {
            $table->foreign('retur_sales_order_id')
                    ->references('id')
                    ->on('retur_sales_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('sales_order_detail_id')
                    ->references('id')
                    ->on('sales_order_detail')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('product_id')
                    ->references('id')
                    ->on('product')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('sales_order', function (Blueprint $table): void {
            $table->foreign('member_id')
                    ->references('id')
                    ->on('member')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('branch_id')
                    ->references('id')
                    ->on('branch')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('bank_id')
                    ->references('id')
                    ->on('bank')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('sales_id')
                    ->references('id')
                    ->on('employee')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::table('sales_order_detail', function (Blueprint $table): void {
            $table->foreign('sales_order_id')
                    ->references('id')
                    ->on('sales_order')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('promo_id')
                    ->references('id')
                    ->on('promo')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('product_id')
                    ->references('id')
                    ->on('product')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_permission', function (Blueprint $table): void {
            $table->dropForeign(['permission_id']);
            $table->dropForeign(['menu_id']);
        });

        Schema::table('debt_payment_purchase_order', function (Blueprint $table): void {
            $table->dropForeign(['purchase_order_id']);
            $table->dropForeign(['bank_id']);
        });

        Schema::table('debt_payment_sales_order', function (Blueprint $table): void {
            $table->dropForeign(['sales_order_id']);
            $table->dropForeign(['bank_id']);
        });

        Schema::table('expend', function (Blueprint $table): void {
            $table->dropForeign(['bank_id']);
        });

        Schema::table('incentive', function (Blueprint $table): void {
            $table->dropForeign(['bank_id']);
        });

        Schema::table('product_identity', function (Blueprint $table): void {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['branch_id']);
        });

        Schema::table('promo_detail', function (Blueprint $table): void {
            $table->dropForeign(['promo_id']);
        });

        Schema::table('purchase_order_detail', function (Blueprint $table): void {
            $table->dropForeign(['purchase_order_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::table('purchase_order', function (Blueprint $table): void {
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['bank_id']);
            $table->dropForeign(['supplier_id']);
        });

        Schema::table('retur_purchase_order', function (Blueprint $table): void {
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['bank_id']);
            $table->dropForeign(['supplier_id']);
            $table->dropForeign(['purchase_order_id']);
        });

        Schema::table('retur_purchase_order_detail', function (Blueprint $table): void {
            $table->dropForeign(['retur_purchase_order_id']);
            $table->dropForeign(['purchase_order_detail_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::table('retur_sales_order', function (Blueprint $table): void {
            $table->dropForeign(['sales_order_id']);
            $table->dropForeign(['bank_id']);
            $table->dropForeign(['branch_id']);
        });

        Schema::table('retur_sales_order_detail', function (Blueprint $table): void {
            $table->dropForeign(['retur_sales_order_id']);
            $table->dropForeign(['sales_order_detail_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::table('sales_order', function (Blueprint $table): void {
            $table->dropForeign(['member_id']);
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['bank_id']);
            $table->dropForeign(['sales_id']);
        });

        Schema::table('sales_order_detail', function (Blueprint $table): void {
            $table->dropForeign(['sales_order_id']);
            $table->dropForeign(['product_id']);
            $table->dropForeign(['promo_id']);
        });
    }
}
