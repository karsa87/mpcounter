<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreatedUpdatedByTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('sales_order', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('expend', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('incentive', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('promo', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('retur_purchase_order', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('retur_sales_order', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('debt_payment_purchase_order', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('debt_payment_sales_order', function (Blueprint $table) {
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });

        Schema::table('sales_order', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('expend', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('incentive', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('promo', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('retur_purchase_order', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('retur_sales_order', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('debt_payment_purchase_order', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
        
        Schema::table('debt_payment_sales_order', function (Blueprint $table) {
            $table->dropColumn(['updated_by', 'created_by']);
        });
    }
}
