<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sales_order_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('product_identity_id')->unsigned();
            $table->integer('qty')->default(0);
            $table->double('cost_of_goods', 12, 2)->default(0);
            $table->double('sell_price', 12, 2)->default(0);
            $table->bigInteger('promo_id')->nullable()->unsigned();
            $table->double('total_amount_first', 12, 2)->default(0);
            $table->double('discount_promo', 12, 2)->default(0);
            $table->double('total_amount', 12, 2)->default(0);
            $table->tinyInteger('is_retur')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_detail');
    }
}
