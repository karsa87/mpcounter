<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->tinyInteger('identity_type');
            $table->string('identity', 255);
            $table->text('address');
            $table->string('phone', 16);
            $table->string('birthdate', 255)->nullable();
            $table->integer('poin')->default(0);
            $table->string('member_number',100);
            $table->tinyInteger('type');
            $table->integer('max_day_debt')->default(0);
            $table->double('max_debt', 12, 2)->default(0);
            $table->tinyInteger('status')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
