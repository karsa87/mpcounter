<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogStockTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_stock_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('log_datetime');
            $table->bigInteger('user_id');
            $table->bigInteger('stock_id');
            $table->bigInteger('transaction_id');
            $table->string('transaction_type', 255);
            $table->tinyInteger('type');
            $table->text('information')->nullable();
            $table->string('table', 255);
            $table->double('stock_in', 12, 2)->default(0);
            $table->double('stock_out', 12, 2)->default(0);
            $table->double('stock_before', 12, 2)->default(0);
            $table->double('stock_after', 12, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_stock_transaction');
    }
}
