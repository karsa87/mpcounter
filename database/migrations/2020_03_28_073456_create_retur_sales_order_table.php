<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturSalesOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_sales_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_retur',255);
            $table->bigInteger('sales_order_id')->unsigned();
            $table->dateTime('date');
            $table->bigInteger('branch_id')->unsigned();
            $table->bigInteger('bank_id')->unsigned();
            $table->bigInteger('promo_id')->nullable()->unsigned();
            $table->text('information')->nullable();
            $table->text('images')->nullable();
            $table->double('total_amount_first', 12, 2)->default(0);
            $table->double('total_discount_detail', 12, 2)->default(0);
            $table->double('total_amount', 12, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retur_sales_order');
    }
}
