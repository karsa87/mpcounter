<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebtPaymentSalesOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debt_payment_sales_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_debt',255);
            $table->bigInteger('sales_order_id')->unsigned();
            $table->bigInteger('bank_id')->unsigned();
            $table->dateTime('date');
            $table->text('information')->nullable();
            $table->text('images')->nullable();
            $table->double('paid', 12, 2)->default(0);
            $table->double('total_paid', 12, 2)->default(0);
            $table->double('remaining_debt', 12, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debt_payment_sales_order');
    }
}
