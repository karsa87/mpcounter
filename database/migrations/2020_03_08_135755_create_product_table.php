<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255);
            $table->string('name', 255);
            $table->bigInteger('category_id');
            $table->bigInteger('brand_id');
            $table->double('cost_of_goods', 12, 2)->default(0);
            $table->double('sell_price', 12, 2)->default(0);
            $table->text('images')->nullable();
            $table->text('information')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('unit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
