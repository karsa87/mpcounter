<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoTransactionExpendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expend', function (Blueprint $table) {
            $table->string('no_transaction', 255)->default('EXP-');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expend', function (Blueprint $table) {
            $table->dropColumn(['no_transaction']);
        });
    }
}
