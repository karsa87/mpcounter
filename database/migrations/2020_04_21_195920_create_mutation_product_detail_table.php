<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMutationProductDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutation_product_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('mutation_product_id');
            $table->bigInteger('product_id');
            $table->bigInteger('product_identity_id');
            $table->bigInteger('to_product_identity_id');
            $table->integer('qty');
            $table->double('cost_of_goods', 12, 2)->default(0);
            $table->double('sell_price', 12, 2)->default(0);
            $table->double('total_amount', 12, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutation_product_detail');
    }
}
