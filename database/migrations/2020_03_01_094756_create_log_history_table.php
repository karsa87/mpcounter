<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('log_datetime');
            $table->bigInteger('user_id');
            $table->text('information');
            $table->string('table', 255);
            $table->bigInteger('record_id');
            $table->string('record_type', 255);
            $table->tinyInteger('transaction_type');
            $table->text('data_before')->nullable();
            $table->text('data_after')->nullable();
            $table->text('data_change')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_history');
    }
}
