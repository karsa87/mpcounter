<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogBankTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_bank_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('log_datetime');
            $table->bigInteger('user_id');
            $table->bigInteger('bank_id');
            $table->bigInteger('transaction_id');
            $table->string('transaction_type', 255);
            $table->tinyInteger('type');
            $table->text('information')->nullable();
            $table->string('table', 255);
            $table->double('debit_amount', 12, 2)->default(0);
            $table->double('credit_amount', 12, 2)->default(0);
            $table->double('amount_before', 12, 2)->default(0);
            $table->double('amount_after', 12, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_bank_transaction');
    }
}
