<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',255);
            $table->string('name',255);
            $table->text('information')->nullable();
            $table->text('images')->nullable();
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->bigInteger('category_id')->nullable();
            $table->decimal('discount_percent', 12, 2)->default(0)->nullable();
            $table->double('discount_price', 12, 2)->default(0)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo');
    }
}
