<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        Model::unguard();
            $this->call(\MenuSeeder::class);
            $this->call(\PermissionsTableSeeder::class);
            $this->call(\RolesTableSeeder::class);
            $this->call(\ConnectRelationshipsSeeder::class);
            $this->call(\UsersTableSeeder::class);
            $this->call(\BankSeeder::class);
            $this->call(\SettingSeeder::class);
            $this->call(\StockOpnameSeeder::class);
            $this->call(\ProductCodeSeeder::class);
            $this->call(\ReportSupplierTaxSeeder::class);
            $this->call(\MPCounterDefault::class);
            $this->call(\ReportIncomeExpend::class);
            $this->call(\PulseSeeder::class);

        Model::reguard();
    }
}
