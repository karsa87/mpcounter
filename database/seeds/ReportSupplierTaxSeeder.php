<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class ReportSupplierTaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'report')->first();
        if ($menu_master) {
            $menu_id = DB::table('menu')->insertGetId(
                [
                    'name' => 'Supplier Tax',
                    'order' => 8,
                    'parent_id' => $menu_master->id,
                    'url' => 'report/supplier-tax',
                    'icon' => "",
                    'route' => 'report.supplier.tax.index',
                    'is_parent' => Menu::IS_NOT_PARENT
                ]
            );

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Supplier Tax index',
                    'report.supplier.tax.index',
                    'Can view report supplier tax'
                ],
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::create([
                        'name' => $Permissionitem[0],
                        'slug' => $Permissionitem[1],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */ 
            $menu = Menu::find($menu_id);
            $menu->permissions()->syncWithoutDetaching($permissions);

            $role = config('roles.models.role')::where('slug', '=', 'developer')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'adminwika')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'mpcounter')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }
        }
    }
}
