<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting_id = DB::table('menu')->insertGetId(
            [
                'name' => 'Settings',
                'order' => 0,
                'parent_id' => null,
                'url' => 'settings',
                'icon' => "fas fa-cog",
                'route' => null,
                'is_parent' => Menu::IS_PARENT
            ]
        );

        $master_id = DB::table('menu')->insertGetId(
            [
                'name' => 'Master',
                'order' => 1,
                'parent_id' => null,
                'url' => 'master',
                'icon' => "fas fa-box",
                'route' => null,
                'is_parent' => Menu::IS_PARENT
            ]
        );

        $transaction_id = DB::table('menu')->insertGetId(
            [
                'name' => 'Transaction',
                'order' => 2,
                'parent_id' => null,
                'url' => 'transaction',
                'icon' => "fas fa-exchange-alt",
                'route' => null,
                'is_parent' => Menu::IS_PARENT
            ]
        );

        $finance_id = DB::table('menu')->insertGetId(
            [
                'name' => 'Finance',
                'order' => 3,
                'parent_id' => null,
                'url' => 'finance',
                'icon' => "fas fa-money-bill-wave",
                'route' => null,
                'is_parent' => Menu::IS_PARENT
            ]
        );

        $report_id = DB::table('menu')->insertGetId(
            [
                'name' => 'Report',
                'order' => 4,
                'parent_id' => null,
                'url' => 'report',
                'icon' => "fas fa-chart-bar",
                'route' => null,
                'is_parent' => Menu::IS_PARENT
            ]
        );

        $log_id = DB::table('menu')->insertGetId(
            [
                'name' => 'Log',
                'order' => 5,
                'parent_id' => null,
                'url' => 'log',
                'icon' => "fas fa-history",
                'route' => null,
                'is_parent' => Menu::IS_PARENT
            ]
        );

        $this->createMenuSetting($setting_id);
        $this->createMenuMaster($master_id);
        $this->createMenuTransaction($transaction_id);
        $this->createMenuFinance($finance_id);
        $this->createMenuReport($report_id);
        $this->createMenuLog($log_id);
    }

    private function createMenuSetting($setting_id)
    {
        DB::table('menu')->insert([
            [
                'name' => 'Setting ',
                'url' => "setting/setting",
                'icon' => "",
                'route' => "setting.setting.index",
                'order' => 0,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'Menu',
                'url' => "setting/menu",
                'icon' => "",
                'route' => "setting.menu.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'Permission',
                'url' => "setting/permission",
                'icon' => "",
                'route' => "setting.permission.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'Role',
                'url' => "setting/role",
                'icon' => "",
                'route' => "setting.role.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'User',
                'url' => "setting/user",
                'icon' => "",
                'route' => "setting.user.index",
                'order' => 4,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], 
        ]);
    }

    private function createMenuMaster($master_id)
    {
        DB::table('menu')->insert([
            [
                'name' => 'Employee',
                'url' => "master/employee",
                'icon' => "",
                'route' => "master.employee.index",
                'order' => 7,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Branch',
                'url' => "master/branch",
                'icon' => "",
                'route' => "master.branch.index",
                'order' => 0,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Category',
                'url' => "master/category",
                'icon' => "",
                'route' => "master.category.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Brand',
                'url' => "master/brand",
                'icon' => "",
                'route' => "master.brand.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Product',
                'url' => "master/product",
                'icon' => "",
                'route' => "master.product.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Bank',
                'url' => "master/bank",
                'icon' => "",
                'route' => "master.bank.index",
                'order' => 4,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Supplier',
                'url' => "master/supplier",
                'icon' => "",
                'route' => "master.supplier.index",
                'order' => 5,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Member',
                'url' => "master/member",
                'icon' => "",
                'route' => "master.member.index",
                'order' => 6,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], [
                'name' => 'Promo',
                'url' => "master/promo",
                'icon' => "",
                'route' => "master.promo.index",
                'order' => 8,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $master_id,
            ], 
        ]);
    }

    private function createMenuTransaction($transaction_id)
    {
        DB::table('menu')->insert([
            [
                'name' => 'Purchase Order',
                'url' => "transaction/purchase-order",
                'icon' => "",
                'route' => "transaction.purchase.order.index",
                'order' => 0,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $transaction_id,
            ], [
                'name' => 'Sales Order',
                'url' => "transaction/sales-order",
                'icon' => "",
                'route' => "transaction.sales.order.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $transaction_id,
            ], [
                'name' => 'Retur Purchase Order',
                'url' => "transaction/retur-purchase-order",
                'icon' => "",
                'route' => "transaction.retur.purchase.order.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $transaction_id,
            ], [
                'name' => 'Retur Sales Order',
                'url' => "transaction/retur-sales-order",
                'icon' => "",
                'route' => "transaction.retur.sales.order.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $transaction_id,
            ], [
                'name' => 'Mutation Product',
                'url' => "transaction/mutation-product",
                'icon' => "",
                'route' => "transaction.mutation.product.index",
                'order' => 4,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $transaction_id,
            ],
        ]);
    }

    private function createMenuFinance($finance_id)
    {
        DB::table('menu')->insert([
            [
                'name' => 'Category Expend',
                'url' => "finance/category_expend",
                'icon' => "",
                'route' => "finance.category.expend.index",
                'order' => 0,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $finance_id,
            ], [
                'name' => 'Expend',
                'url' => "finance/expend",
                'icon' => "",
                'route' => "finance.expend.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $finance_id,
            ], [
                'name' => 'Incentive',
                'url' => "finance/incentive",
                'icon' => "",
                'route' => "finance.incentive.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $finance_id,
            ], [
                'name' => 'Debt Payment PO',
                'url' => "finance/debt-payment-po",
                'icon' => "",
                'route' => "finance.debt.payment.po.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $finance_id,
            ], [
                'name' => 'Debt Payment SO',
                'url' => "finance/debt-payment-so",
                'icon' => "",
                'route' => "finance.debt.payment.so.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $finance_id,
            ],
        ]);
    }

    private function createMenuLog($log_id)
    {
        DB::table('menu')->insert([
            [
                'name' => 'Log History',
                'url' => "log/log_history",
                'icon' => "",
                'route' => "log.log.history.index",
                'order' => 0,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $log_id,
            ], [
                'name' => 'Log Stock',
                'url' => "log/log-stock-transaction",
                'icon' => "",
                'route' => "log.log.stock.transaction.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $log_id,
            ], [
                'name' => 'Log Bank',
                'url' => "log/log_bank_transaction",
                'icon' => "",
                'route' => "log.log.bank.transaction.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $log_id,
            ],
        ]);
    }

    private function createMenuReport($report_id)
    {
        DB::table('menu')->insert([
            [
                'name' => 'Report Purchase Order',
                'url' => "report/purchase-order",
                'icon' => "",
                'route' => "report.purchase.order.index",
                'order' => 0,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Report Sales Order',
                'url' => "report/sales-order",
                'icon' => "",
                'route' => "report.sales.order.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Report Cash Out',
                'url' => "report/expend-all",
                'icon' => "",
                'route' => "report.expend.all.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Report Cash In',
                'url' => "report/income-all",
                'icon' => "",
                'route' => "report.income.all.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Financial Statement',
                'url' => "report/financial-statement",
                'icon' => "",
                'route' => "report.financial.statement.index",
                'order' => 4,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Profit and Loss',
                'url' => "report/loss-profit",
                'icon' => "",
                'route' => "report.loss.profit.index",
                'order' => 5,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Product Identity',
                'url' => "report/product-identity",
                'icon' => "",
                'route' => "report.product.identity.index",
                'order' => 6,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ], [
                'name' => 'Stock',
                'url' => "report/stock",
                'icon' => "",
                'route' => "report.stock.index",
                'order' => 7,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $report_id,
            ],
        ]);
    }
}
