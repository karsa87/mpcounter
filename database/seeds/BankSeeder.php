<?php

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank')->insert([
            [
                'name' => 'KAS BESAR', 
                'account_number' => '000000000', 
                'name_owner' => 'xxx', 
                'status' => Bank::STATUS_ACTIVE, 
                'is_default' => Bank::DEFAULT_YES
            ]
        ]);
    }
}
