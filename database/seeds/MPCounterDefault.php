<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Branch;
use App\Models\Role;
use App\Models\Menu;
use App\Models\User;
use App\Models\Bank;

class MPCounterDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = $this->createRole();
        $menus = $this->setMenu($role);
        $this->setPermission($menus, $role);
        $this->createUser($role);
        $employee_id = $this->defaultEmployee();
        $this->defaultBranch($employee_id);
    }

    public function createRole()
    {
        /*
         * Delete old Role Items
         *
         */
        Role::whereIn('slug', ['adminwika', 'administrator'])->delete();

        /*
         * Add Role Items
         *
         */
        $role = Role::create([
            'name'          => 'MPCounter',
            'slug'          => 'mpcounter',
            'description'   => 'MPCounter Role',
            'level'         => Role::LEVEL_ADMINISTRATOR,
        ]);

        return $role;
    }

    private function setMenu($role)
    {
        /*
         * Add Users Administrator
         *
         */
        $menus = Menu::with('permissions')->whereNotIn('name',[
            'Permission',
            'Menu',
            'Role',
            'Employee',
            'Bank',
            'Member',
            'Promo',
            'Stock Opname',
            'Category Expend',
            'Expend',
            'Report Purchase Order',
            'Report Sales Order',
            'Product Identity',
            'Log',
            'Log History',
            'Log Stock',
            'Log Bank',
        ])->get();

        $role->menus()->syncWithoutDetaching($menus->pluck('id')->toArray());

        return $menus;
    }

    private function setPermission($menus, $role)
    {

        /*
         * Add Users Administrator
         *
         */
        $menus = Menu::with('permissions')->whereNotIn('name',[
            'Permission',
            'Menu',
            'Role',
            'Employee',
            'Bank',
            'Member',
            'Promo',
            'Stock Opname',
            'Category Expend',
            'Expend',
            'Report Purchase Order',
            'Report Sales Order',
            'Product Identity',
            'Log',
            'Log History',
            'Log Stock',
            'Log Bank',
        ])->get();
        
        $permissions = [];
        foreach ($menus as $menu) {
            if ($menu->permissions->count() > 0) {
                foreach ($menu->permissions as $permission) {
                    $permissions[] = $permission->id;
                }
            }
        }

        $role->permissions()->syncWithoutDetaching($permissions);
    }

    public function createUser($role)
    {
        User::whereIn('username', ['administrator'])->delete();

        $user = User::create([
            'name' => 'MP Counter', 
            'username' => 'mpcounter', 
            'email' => 'mpcounter@gmail.com', 
            'status' => '1', 
            'password' => 'MPcounter162020'
        ]);
        $user->roles()->syncWithoutDetaching([$role->id]);
    }

    public function defaultEmployee()
    {
        $employee = Employee::create([
            'name' => 'MP Employee', 
            'address' => 'Malang', 
            'phone' => '089878xxxxxx', 
            'type_identifier' => Employee::IDENTIFIER_KTP, 
            'identifier' => '129784328xxxxx', 
            'email' => 'employee@gmail.com', 
            'status' => '1',
            'branch_id' => null
        ]);


        $user = new User();
        $user->name = $employee->name;
        $user->username = 'mpemployee';
        $user->password = 'mpemployee162020';
        $user->email = $employee->email;
        $user->status = $employee->status;
        $user->branch_id = $employee->branch_id;
        $user->status = 0;
        $user->save();

        $employee->user_id = $user->id;
        $employee->save();

        return $employee->id;
    }

    public function defaultBranch($employee_id)
    {
        $branchs = [
            [
                'name' => 'Kesamben',
                'phone' => '085103375600',
                'address' => 'Jl. Raya Kesamben Timur Kantor Polisi Kesamben',
                'employee_id' => $employee_id,
                'status' => Branch::STATUS_ACTIVE,
            ], [
                'name' => 'Sukun',
                'phone' => '085103375600',
                'address' => 'Jl. Raya Kepuh 28. Malang Depan Unikama',
                'employee_id' => $employee_id,
                'status' => Branch::STATUS_ACTIVE,
            ], [
                'name' => 'Buring',
                'phone' => '085103375600',
                'address' => 'Jl. Raya Mayjen Sungkono 02 Malang',
                'employee_id' => $employee_id,
                'status' => Branch::STATUS_ACTIVE,
            ], [
                'name' => 'MP',
                'phone' => '085103375600',
                'address' => 'Jl. Raya Kh Agus Salim 26-28, Malang Plasa Lt.3',
                'employee_id' => $employee_id,
                'status' => Branch::STATUS_ACTIVE,
            ]
        ];
        
        Branch::insert($branchs);
    }
}
