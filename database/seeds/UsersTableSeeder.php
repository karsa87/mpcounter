<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Users Developer
         *
         */
        $roles = config('roles.models.role')::where('name', '=', 'Developer')->first();
        $user = config('roles.models.defaultUser')::create([
            'name' => 'Developer', 
            'username' => 'developer', 
            'email' => 'karsaaif87@gmail.com', 
            'status' => '1', 
            'password' => "developer87"
        ]);
        $user->syncRoles([$roles->id]);

        /*
         * Add Users Administrator
         *
         */
        $roles = config('roles.models.role')::where('name', '=', 'Adminwika')->first();
        $user = config('roles.models.defaultUser')::create([
            'name' => 'Administrator WIKA', 
            'username' => 'adminiwika', 
            'email' => 'adminiwika@gmail.com', 
            'status' => '1', 
            'password' => 'adminiwika12345'
        ]);
        $user->syncRoles([$roles->id]);

        /*
         * Add Users Administrator
         *
         */
        $roles = config('roles.models.role')::where('name', '=', 'Administrator')->first();
        $user = config('roles.models.defaultUser')::create([
            'name' => 'Administrator', 
            'username' => 'administrator', 
            'email' => 'administrator@gmail.com', 
            'status' => '1', 
            'password' => 'admin12345'
        ]);
        $user->syncRoles([$roles->id]);

        /*
         * Add Users Cashier
         *
         */
        $roles = config('roles.models.role')::where('name', '=', 'Cashier')->first();
        $user = config('roles.models.defaultUser')::create([
            'name' => 'Cashier', 
            'username' => 'cashier', 
            'email' => 'cashier@gmail.com', 
            'status' => '1', 
            'password' => 'cashier12345'
        ]);
        $user->syncRoles([$roles->id]);

        /*
         * Add Users Sales
         *
         */
        $roles = config('roles.models.role')::where('name', '=', 'Sales')->first();
        $user = config('roles.models.defaultUser')::create([
            'name' => 'Sales', 
            'username' => 'sales', 
            'email' => 'sales@gmail.com', 
            'status' => '1', 
            'password' => 'sales12345'
        ]);
        $user->syncRoles([$roles->id]);
    }
}
