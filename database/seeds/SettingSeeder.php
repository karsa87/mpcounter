<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting')->insert([
            [
                'name' => 'Sales Order - Header Small Note', 
                'key' => 'SALES_ORDER_HEADER_SMALL_NOTE', 
                'value' => '&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 18pt;&quot;&gt;Metro Cell&lt;/span&gt;&lt;/p&gt;', 
                'status' => 1, 
                'type' => 0
            ],[
                'name' => 'Sales Order - Footer Small Note', 
                'key' => 'SALES_ORDER_FOOTER_SMALL_NOTE', 
                'value' => '&lt;div style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 8pt;&quot;&gt;Terima Kasih Atas Kunjungan Anda&amp;nbsp;&lt;/span&gt;&lt;/div&gt;', 
                'status' => 1, 
                'type' => 0
            ],[
                'name' => 'Convertion Member Point To Rupiah', 
                'key' => 'CONVERTION_MEMBER_POINT_TO_RUPIAH', 
                'value' => '1000', 
                'status' => 1, 
                'type' => 1
            ],[
                'name' => 'Convertion Total Buying To Member Point', 
                'key' => 'CONVERTION_TOTAL_BUYING_TO_MEMBER_POINT', 
                'value' => '1000000', 
                'status' => 1, 
                'type' => 1
            ],[
                'name' => 'Default Language', 
                'key' => 'DEFAULT_LANGUAGE', 
                'value' => 'id', 
                'status' => 1, 
                'type' => 0
            ]
        ]);
    }
}
