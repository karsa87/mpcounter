<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\Role;

class ConnectRelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Users Developer
         *
         */
        $permissions = config('roles.models.permission')::all()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'developer')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users Administrator WIKA
         *
         */
        $permissions = config('roles.models.permission')::whereNotIn('slug', [
            'setting.permission.create',
            'setting.permission.destroy',
            'setting.permission.edit',
            'setting.permission.index'
        ])->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'adminwika')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users Administrator
         *
         */
        $permissions = config('roles.models.permission')::whereNotIn('slug', [
            'setting.permission.create',
            'setting.permission.destroy',
            'setting.permission.edit',
            'setting.permission.index'
        ])->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'administrator')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users Cashier
         *
         */
        $permissions = config('roles.models.permission')::whereIn('slug', [
            'transaction.sales.order.note.small',
            'transaction.sales.order.upload',
            'transaction.sales.order.show',
            'transaction.sales.order.edit',
            'transaction.sales.order.create',
            'transaction.sales.order.index',
            'transaction.purchase.order.index',
            'transaction.purchase.order.create',
            'transaction.purchase.order.edit',
            'transaction.purchase.order.upload',
            'transaction.purchase.order.show',
            'transaction.retur.purchase.order.index',
            'transaction.retur.purchase.order.create',
            'transaction.retur.purchase.order.edit',
            'transaction.retur.purchase.order.upload',
            'transaction.retur.purchase.order.show',
            'transaction.retur.sales.order.index',
            'transaction.retur.sales.order.create',
            'transaction.retur.sales.order.edit',
            'transaction.retur.sales.order.show',
            'finance.debt.payment.so.index',
            'finance.debt.payment.so.create',
            'finance.debt.payment.so.edit',
            'finance.debt.payment.so.show',
            'finance.debt.payment.so.upload',
            'finance.debt.payment.po.index',
            'finance.debt.payment.po.create',
            'finance.debt.payment.po.edit',
            'finance.debt.payment.po.show',
            'finance.debt.payment.po.upload',
            'dashboard',
            'login',
            'logout'
        ])->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'cashier')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users Sales
         *
         */
        $permissions = config('roles.models.permission')::whereIn('slug', [
            'transaction.sales.order.note.small',
            'transaction.sales.order.upload',
            'transaction.sales.order.show',
            'transaction.sales.order.edit',
            'transaction.sales.order.create',
            'transaction.sales.order.index',
            'dashboard',
            'login',
            'logout'
        ])->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'sales')->first();
        $role->permissions()->sync($permissions);

        // relations menu permission
        $menus = Menu::child()->orWhere(function($q){
            $q->whereNotIn('route', ['dashboard','login','logout']);
        })->get();

        foreach ($menus as $menu) {
            $slug = str_replace('index','',$menu->route);

            $permissions = config('roles.models.permission')::query();
            if (config('database.default') == 'mysql') {
                $permissions->where('slug', 'like', "%$slug%");
            } else {
                $permissions->where('slug', 'ilike', "%$slug%");
            }
            $permissions = $permissions->get()->pluck('id')->toArray();

            $menu->permissions()->sync($permissions);
        }

        /*
         * Add Users Developer
         *
         */
        $menus = Menu::all()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'developer')->first();
        $role->menus()->sync($menus);

        /*
         * Add Users Administrator Wika
         *
         */
        $menus = Menu::whereNotIn('name',['Permission'])->get()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'adminwika')->first();
        $role->menus()->sync($menus);

        /*
         * Add Users Administrator
         *
         */
        $menus = Menu::whereNotIn('name',['Permission'])->get()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'administrator')->first();
        $role->menus()->sync($menus);

        /*
         * Add Users Cashier
         *
         */
        $menus = Menu::whereIn('name', [
                        'Transaction', 
                        'Finance', 
                        'Purchase Order',
                        'Sales Order',
                        'Retur Purchase Order',
                        'Retur Sales Order',
                        'Debt Payment SO',
                        'Debt Payment PO'
                    ])
                    ->get()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'cashier')->first();
        $role->menus()->sync($menus);

        /*
         * Add Users Sales
         *
         */
        $menus = Menu::whereIn('name', [
                        'Transaction', 
                        'Sales Order',
                    ])
                    ->get()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'sales')->first();
        $role->menus()->sync($menus);
    }
}
