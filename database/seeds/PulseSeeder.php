<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class PulseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Menu
         *
         */
        $menu_transaction = Menu::where('url', 'transaction')->first();
        if ($menu_transaction) {
            $menu_id = DB::table('menu')->insertGetId(
                [
                    'name' => 'Pulse',
                    'order' => 6,
                    'parent_id' => $menu_transaction->id,
                    'url' => 'transaction/pulse',
                    'icon' => "",
                    'route' => 'transaction.pulse.index',
                    'is_parent' => Menu::IS_NOT_PARENT
                ]
            );

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Pulse Index',
                    'transaction.pulse.index',
                    'Can view index pulse'
                ], [
                    'Pulse add',
                    'transaction.pulse.create',
                    'can add pulse'
                ], [
                    'Pulse edit',
                    'transaction.pulse.edit',
                    'can edit pulse'
                ], [
                    'Pulse delete',
                    'transaction.pulse.destroy',
                    'can delete pulse',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::create([
                        'name' => $Permissionitem[0],
                        'slug' => $Permissionitem[1],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */ 
            $menu = Menu::find($menu_id);
            $menu->permissions()->syncWithoutDetaching($permissions);

            $role = config('roles.models.role')::where('slug', '=', 'developer')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'adminwika')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'mpcounter')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }
        }
    }
}
