<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class ReportIncomeExpend extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Menu
         *
         */
        $menu_report = Menu::where('url', 'report')->first();
        if ($menu_report) {
            $menu_id = DB::table('menu')->insertGetId(
                [
                    'name' => 'Income Expend',
                    'order' => 4,
                    'parent_id' => $menu_report->id,
                    'url' => 'report/income-expend',
                    'icon' => "",
                    'route' => 'report.income.expend.index',
                    'is_parent' => Menu::IS_NOT_PARENT
                ]
            );

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Report Income Expend',
                    'report.income.expend.index',
                    'Can view index stock opname'
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::create([
                        'name' => $Permissionitem[0],
                        'slug' => $Permissionitem[1],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */ 
            $menu = Menu::find($menu_id);
            $menu->permissions()->syncWithoutDetaching($permissions);

            $role = config('roles.models.role')::where('slug', '=', 'developer')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'adminwika')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'mpcounter')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }
        }
    }
}
