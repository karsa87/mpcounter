<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Role Types
         *
         */
        $RoleItems = [
            [
                'name'        => 'Developer',
                'slug'        => 'developer',
                'description' => 'Developer Role',
                'level'       => Role::LEVEL_DEVELOPER,
            ],
            [
                'name'        => 'Adminwika',
                'slug'        => 'adminwika',
                'description' => 'Administrator WIKA Role',
                'level'       => Role::LEVEL_ADMINISTRATOR_WIKA,
            ],
            [
                'name'        => 'Administrator',
                'slug'        => 'administrator',
                'description' => 'Administrator Role',
                'level'       => Role::LEVEL_ADMINISTRATOR,
            ],
            [
                'name'        => 'Cashier',
                'slug'        => 'cashier',
                'description' => 'Cashier Role',
                'level'       => Role::LEVEL_EMPLOYEE,
            ],
            [
                'name'        => 'Sales',
                'slug'        => 'sales',
                'description' => 'Sales Role',
                'level'       => Role::LEVEL_EMPLOYEE,
            ],
        ];

        /*
         * Add Role Items
         *
         */
        DB::table('roles')->insert($RoleItems);
        // foreach ($RoleItems as $RoleItem) {
        //     $newRoleItem = config('roles.models.role')::where('slug', '=', $RoleItem['slug'])->first();
        //     if ($newRoleItem === null) {
        //         $newRoleItem = config('roles.models.role')::create([
        //             'name'          => $RoleItem['name'],
        //             'slug'          => $RoleItem['slug'],
        //             'description'   => $RoleItem['description'],
        //             'level'         => $RoleItem['level'],
        //             'deleted_at'    => isset($RoleItem['deleted_at']) ? $RoleItem['deleted_at'] : null,
        //         ]);
        //     }
        // }
    }
}
