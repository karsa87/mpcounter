<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Permission Types
         *
         */
        $Permissionitems = [
            [
                'Dashboard',
                'dashboard',
                ''
            ], [
                'Login',
                'login',
                ''
            ], [
                'Logout',
                'logout',
                ''
            ], [
                'Permission index',
                'setting.permission.index',
                'Can view index permission'
            ], [
                'Permission add',
                'setting.permission.create',
                'Can create permission'
            ], [
                'Permission edit',
                'setting.permission.edit',
                'can edit permission'
            ], [
                'Permission delete',
                'setting.permission.destroy',
                'can delete permission'
            ], [
                'Menu index',
                'setting.menu.index',
                'can add menu'
            ], [
                'Menu add',
                'setting.menu.create',
                'can add menu'
            ], [
                'Menu edit',
                'setting.menu.edit',
                'can edit menu'
            ], [
                'Menu delete',
                'setting.menu.destroy',
                'can delete menu'
            ], [
                'Role index',
                'setting.role.index',
                'can view role index'
            ], [
                'Role add',
                'setting.role.create',
                'can add role'
            ], [
                'Role edit',
                'setting.role.edit',
                'can edit role'
            ], [
                'Role delete',
                'setting.role.destroy',
                'can delete destroy'
            ], [
                'Role access',
                'setting.role.access',
                'Can change access role'
            ], [
                'User index',
                'setting.user.index',
                'can view user index'
            ], [
                'User add',
                'setting.user.create',
                'can add user'
            ], [
                'User edit',
                'setting.user.edit',
                'can edit user'
            ], [
                'User delete',
                'setting.user.destroy',
                'can delete user'
            ], [
                'User change status',
                'setting.user.change.status',
                'Can activate and deactivate user'
            ], [
                'Brand Index',
                'master.brand.index',
                'can view inder brand'
            ], [
                'Brand add',
                'master.brand.create',
                'can add brand'
            ], [
                'Brand edit',
                'master.brand.edit',
                'can edit brand'
            ], [
                'Brand delete',
                'master.brand.destroy',
                'can delete brand'
            ], [
                'Category index',
                'master.category.index',
                'can view index category'
            ], [
                'Category add',
                'master.category.create',
                'can add category'
            ], [
                'Category edit',
                'master.category.edit',
                'can edit category'
            ], [
                'Category delete',
                'master.category.destroy',
                'can delete category'
            ], [
                'Bank index',
                'master.bank.index',
                'can view index'
            ], [
                'Bank add',
                'master.bank.create',
                'can add bank'
            ], [
                'Bank edit',
                'master.bank.edit',
                'can edit bank'
            ], [
                'Bank delete',
                'master.bank.destroy',
                'can delete bank',
            ], [
                "Employee index",
                "master.employee.index",
                "can view index employee"
            ], [
                "Employee add",
                "master.employee.create",
                "can add employee"
            ], [
                "Employee edit",
                "master.employee.edit",
                "can edit employee"
            ], [
                "Employee delete",
                "master.employee.destroy",
                "can delete employee"
            ], [
                "Branch index",
                "master.branch.index",
                "can view index branch"
            ], [
                "Branch add",
                "master.branch.create",
                "can add branch"
            ], [
                "Branch edit",
                "master.branch.edit",
                "can edit branch"
            ], [
                "Branch delete",
                "master.branch.destroy",
                "can delete branch"
            ], [
                "Category Expend index",
                "finance.category.expend.index",
                "can view index category expend"
            ], [
                "Category expend add",
                "finance.category.expend.create",
                "can add category expend"
            ], [
                "Category expend edit",
                "finance.category.expend.edit",
                "can edit category expend"
            ], [
                "Category expend delete",
                "finance.category.expend.destroy",
                "can delete category expend"
            ], [
                "Expend index",
                "finance.expend.index",
                "can view index expend"
            ], [
                "Expend add",
                "finance.expend.create",
                "can add expend"
            ], [
                "Expend edit",
                "finance.expend.edit",
                "can edit expend"
            ], [
                "Expend delete",
                "finance.expend.destroy",
                "can delete expend"
            ], [
                "Expend upload",
                "finance.expend.upload",
                "can upload image expend"
            ], [
                "Log History index",
                "log.log.history.index",
                "can view index log history"
            ], [
                "Log Bank index",
                "log.log.bank.transaction.index",
                "can view index log bank transaction"
            ], [
                "Incentive index",
                "finance.incentive.index",
                "can view index incentive"
            ], [
                "Incentive add",
                "finance.incentive.create",
                "can add incentive"
            ], [
                "Incentive edit",
                "finance.incentive.edit",
                "can edit incentive"
            ], [
                "Incentive delete",
                "finance.incentive.destroy",
                "can delete incentive"
            ], [
                "Member index",
                "master.member.index",
                "can view index member"
            ], [
                "Member add",
                "master.member.create",
                "can add member"
            ], [
                "Member edit",
                "master.member.edit",
                "can edit member"
            ], [
                "Member delete",
                "master.member.destroy",
                "can delete member"
            ], [
                "Supplier index",
                "master.supplier.index",
                "can view index supplier"
            ], [
                "Supplier add",
                "master.supplier.create",
                "can add supplier"
            ], [
                "Supplier edit",
                "master.supplier.edit",
                "can edit supplier"
            ], [
                "Supplier delete",
                "master.supplier.destroy",
                "can delete supplier"
            ], [
                "Product index",
                "master.product.index",
                "can view index product"
            ], [
                "Product add",
                "master.product.create",
                "can add product"
            ], [
                "Product edit",
                "master.product.edit",
                "can edit product"
            ], [
                "Product delete",
                "master.product.destroy",
                "can delete product"
            ], [
                "Product upload",
                "master.product.upload",
                "can upload image product"
            ], [
                "Product detail",
                "master.product.show",
                "can view detail product"
            ], [
                "Show Costs Of Goods",
                "master.product.show.hpp",
                "show hpp products"
            ], [
                "Log Stock index",
                "log.log.stock.transaction.index",
                "can view index log stock"
            ], [
                "Purchase Order Index",
                "transaction.purchase.order.index",
                "can view index purchase order"
            ], [
                "Purchase Order add",
                "transaction.purchase.order.create",
                "can add purchase order"
            ], [
                "Purchase Order edit",
                "transaction.purchase.order.edit",
                "can edit purchase order"
            ], [
                "Purchase Order delete",
                "transaction.purchase.order.destroy",
                "can delete purchase order"
            ], [
                "Purchase Order upload",
                "transaction.purchase.order.upload",
                "can upload image purchase order"
            ], [
                "Purchase Order Detail",
                "transaction.purchase.order.show",
                "Can view detail purchase order"
            ], [
                "Promo index",
                "master.promo.index",
                "can view index promo"
            ], [
                "Promo add",
                "master.promo.create",
                "can add promo"
            ], [
                "Promo edit",
                "master.promo.edit",
                "can edit promo"
            ], [
                "Promo delete",
                "master.promo.destroy",
                "can delete promo"
            ], [
                "Promo detail",
                "master.promo.show",
                "can view detail promo"
            ], [
                "Promo upload image",
                "master.promo.upload",
                "can upload image promo"
            ], [
                "Sales Order index",
                "transaction.sales.order.index",
                "can view index sales order"
            ], [
                "Sales Order add",
                "transaction.sales.order.create",
                "can add sales order"
            ], [
                "Sales Order edit",
                "transaction.sales.order.edit",
                "can edit sales order"
            ], [
                "Sales Order delete",
                "transaction.sales.order.destroy",
                "can delete sales order"
            ], [
                "Sales Order detail",
                "transaction.sales.order.show",
                "can view detail sales order"
            ], [
                "Sales Order upload",
                "transaction.sales.order.upload",
                "can upload image sales order"
            ], [
                "Sales Order Print note small",
                "transaction.sales.order.note.small",
                "can print note small"
            ], [
                "Sales Order Show Discount Sales",
                "transaction.sales.order.discount.sales",
                "show field discount sales"
            ], [
                "Mutation Product index",
                "transaction.mutation.product.index",
                "can view index mutation product"
            ], [
                "Mutation Product add",
                "transaction.mutation.product.create",
                "can add mutation product"
            ], [
                "Mutation Product edit",
                "transaction.mutation.product.edit",
                "can edit mutation product"
            ], [
                "Mutation Product delete",
                "transaction.mutation.product.destroy",
                "can delete mutation product"
            ], [
                "Mutation Product detail",
                "transaction.mutation.product.show",
                "can view detail mutation product"
            ], [
                "Mutation Product upload",
                "transaction.mutation.product.upload",
                "can upload image mutation product"
            ], [
                "Setting index",
                "setting.setting.index",
                "can view index setting"
            ], [
                "Setting add",
                "setting.setting.create",
                "can add setting"
            ], [
                "Setting edit",
                "setting.setting.edit",
                "can edit setting"
            ], [
                "Setting delete",
                "setting.setting.destroy",
                "can delete setting"
            ], [
                "Retur Purchase Order Index",
                "transaction.retur.purchase.order.index",
                "can view index retur purchase order"
            ], [
                "Retur Purchase Order Add",
                "transaction.retur.purchase.order.create",
                "can add retur purchase order"
            ], [
                "Retur Purchase Order Edit",
                "transaction.retur.purchase.order.edit",
                "can edit retur purchase order"
            ], [
                "Retur Purchase Order Delete",
                "transaction.retur.purchase.order.destroy",
                "can delete retur purchase order"
            ], [
                "Retur Purchase Order Upload",
                "transaction.retur.purchase.order.upload",
                "can upload image retur purchase order"
            ], [
                "Retur Purchase Order Detail",
                "transaction.retur.purchase.order.show",
                "can view detail retur purchase order"
            ], [
                "Retur Sales Order Index",
                "transaction.retur.sales.order.index",
                "can view index retur sales order"
            ], [
                "Retur Sales Order Add",
                "transaction.retur.sales.order.create",
                "can add retur sales order"
            ], [
                "Retur Sales Order Edit",
                "transaction.retur.sales.order.edit",
                "can edit retur sales order"
            ], [
                "Retur Sales Order Detail",
                "transaction.retur.sales.order.show",
                "can view detail retur sales order"
            ], [
                "Retur Sales Order delete",
                "transaction.retur.sales.order.destroy",
                "can delete retur sales order"
            ], [
                "Debt Payment SO Index",
                "finance.debt.payment.so.index",
                "can view index sales order"
            ], [
                "Debt Payment SO add",
                "finance.debt.payment.so.create",
                "can add debt payment sales order"
            ], [
                "Debt Payment SO edit",
                "finance.debt.payment.so.edit",
                "can edit debt payment sales order"
            ], [
                "Debt Payment SO delete",
                "finance.debt.payment.so.destroy",
                "can delete debt payment sales order"
            ], [
                "Debt Payment SO detail",
                "finance.debt.payment.so.show",
                "can view detail debt payment sales order"
            ], [
                "Debt Payment SO upload",
                "finance.debt.payment.so.upload",
                "can upload debt payment sales order"
            ], [
                "Debt Payment PO index",
                "finance.debt.payment.po.index",
                "can view index debt payment purchase order"
            ], [
                "Debt Payment PO add",
                "finance.debt.payment.po.create",
                "can add debt payment purchase order"
            ], [
                "Debt Payment PO edit",
                "finance.debt.payment.po.edit",
                "can edit debt payment purchase order"
            ], [
                "Debt Payment PO detail",
                "finance.debt.payment.po.show",
                "can view detail debt payment purchase order"
            ], [
                "Debt Payment PO delete",
                "finance.debt.payment.po.destroy",
                "can delete debt payment purchase order"
            ], [
                "Debt Payment PO upload",
                "finance.debt.payment.po.upload",
                "can upload debt payment purchase order"
            ], [
                "Report All Expend",
                "report.expend.all.index",
                "can view index report all expend"
            ], [
                "Report All Income",
                "report.income.all.index",
                "can view index report all income"
            ], [
                "Report Financial Statement",
                "report.financial.statement.index",
                "can view index report financial statement"
            ], [
                "Report Profit and Loss",
                "report.loss.profit.index",
                "can view index report profit and loss"
            ], [
                "Report Purchase Order",
                "report.purchase.order.index",
                "can view index report purchase order"
            ], [
                "Report Sales Order",
                "report.sales.order.index",
                "can view index report sales order"
            ], [
                "Report Stock",
                "report.stock.index",
                "can view index report stock"
            ], [
                "Report Product Identity",
                "report.product.identity.index",
                "can view index report product identity"
            ],
        ];

        /*
         * Add Permission Items
         *
         */
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::create([
                    'name' => $Permissionitem[0],
                    'slug' => $Permissionitem[1],
                    'description' => $Permissionitem[2],
                    'model' => '',
                ]);
            }
        }
    }
}
