<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class StockOpnameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Menu
         *
         */
        $menu_transaction = Menu::where('url', 'transaction')->first();
        if ($menu_transaction) {
            $menu_id = DB::table('menu')->insertGetId(
                [
                    'name' => 'Stock Opname',
                    'order' => 5,
                    'parent_id' => $menu_transaction->id,
                    'url' => 'transaction/stock-opname',
                    'icon' => "",
                    'route' => 'transaction.stock.opname.index',
                    'is_parent' => Menu::IS_NOT_PARENT
                ]
            );

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Stock Opname index',
                    'transaction.stock.opname.index',
                    'Can view index stock opname'
                ], [
                    'Stock Opname add',
                    'transaction.stock.opname.create',
                    'Can create stock opname'
                ], [
                    'Stock Opname edit',
                    'transaction.stock.opname.edit',
                    'Can edit stock opname'
                ], [
                    'Stock Opname view detail',
                    'transaction.stock.opname.show',
                    'Can view detail stock opname'
                ], [
                    'Stock Opname delete',
                    'transaction.stock.opname.destroy',
                    'Can delete stock opname'
                ], [
                    'Stock Opname upload',
                    'transaction.stock.opname.upload',
                    'Can upload stock opname'
                ], [
                    'Stock Opname Process',
                    'transaction.stock.opname.change.waiting',
                    'Can update status to process'
                ], [
                    'Stock Opname Verified',
                    'transaction.stock.opname.change.process',
                    'Can verified stock opname'
                ], [
                    'Stock Opname Void Verified',
                    'transaction.stock.opname.revert.verified',
                    'Can revert verified'
                ], [
                    'Stock Opname Print',
                    'transaction.stock.opname.print',
                    'Can print stock opname'
                ], [
                    'Stock Opname Input Stock',
                    'transaction.stock.opname.form.verified',
                    ''
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::create([
                        'name' => $Permissionitem[0],
                        'slug' => $Permissionitem[1],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */ 
            $menu = Menu::find($menu_id);
            $menu->permissions()->syncWithoutDetaching($permissions);

            $role = config('roles.models.role')::where('slug', '=', 'developer')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }

            $role = config('roles.models.role')::where('slug', '=', 'adminwika')->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
            }
        }
    }
}
